import QtQuick 2.0

Item {
    id: scrollBar

    // The properties that define the scrollbar's state.
    // position and pageSize are in the range 0.0 - 1.0.  They are relative to the
    // height of the page, i.e. a pageSize of 0.5 means that you can see 50%
    // of the height of the view.
    // orientation can be either Qt.Vertical or Qt.Horizontal
    property real position
    property real pageSize
    property variant orientation : Qt.Vertical
    property var scroll_orientation : Qt.Horizontal

    // A light, semi-transparent background
    Rectangle {
        id: background
        anchors.fill: parent
        radius: orientation == scroll_orientation ? (width/2 - 1) : (height/2 - 1)
        color: "white"
        opacity: 0.3
    }

    // Size the bar to the required size, depending upon the orientation.
    Rectangle {
        x: orientation == scroll_orientation ? 1 : (scrollBar.position * (scrollBar.width-2) + 1)
        y: orientation == scroll_orientation ? (scrollBar.position * (scrollBar.height-2) + 1) : 1
        width: orientation == scroll_orientation ? (parent.width-2) : (scrollBar.pageSize * (scrollBar.width-2))
        height: orientation == scroll_orientation ? (scrollBar.pageSize * (scrollBar.height-2)) : (parent.height-2)
        radius: orientation == scroll_orientation ? (width/2 - 1) : (height/2 - 1)
        color: "red"
        opacity: 0.7
    }
}
