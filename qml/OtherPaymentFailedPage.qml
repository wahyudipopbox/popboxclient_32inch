import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: other_payment_fail_page

    property int timer_value: 30
    property var press:"0"
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            cancel_button.enabled = true
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    FullWidthReminderText{
        id:main_notif_text
        x: 0
        y:160
        remind_text:qsTr("Unfortunately your payment has failed.")
        remind_text_size:"35"
    }

    Text{
        id: detail_notif_text
        x: 97
        y:464
        width: 830
        height: 83
        text:qsTr("Please ensure that your balance is sufficient and the total amount is not greater than 1.000.000 IDR.")
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.AutoText
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        font.pixelSize: 30
        font.family:"Microsoft YaHei"
        color:"#ffffff"
    }


    LoadingView{
        id:loading
    }

    Image {
        id: img_other_payment_fail
        x: 400
        y: 224
        width: 225
        height: 225
        source: "img/otherImages/error_notif.png"
    }


    Component.onCompleted: {
    }

    Component.onDestruction: {
    }

    DoorButton{
        id:cancel_button
        y:588
        x:373
        show_text:qsTr("Cancel")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                cancel_button.show_image = "img/door/2.png"
            }
            onExited:{
                cancel_button.show_image = "img/door/1.png"
            }
        }
    }
}
