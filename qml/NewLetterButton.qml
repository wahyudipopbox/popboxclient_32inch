import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:80
    height:80
    color:"transparent"
    property var show_text:""
    property bool status_enable: true

    Rectangle{
        id: opacity_rec
        anchors.fill: parent
        color: (status_enable==true) ? "transparent" : "silver"

    }

    Image{
        id:new_letter_button
        visible: (status_enable==true) ? true : false
        width:80
        height:80
        source: "img/button/KeyButton_1.png"
    }

    Text{
        text: show_text
        color: "red"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent
        font.italic: true
    }

    MouseArea {
        enabled: status_enable
        anchors.fill: parent
        onClicked: {
            email_keyboard.letter_button_clicked(show_text)
        }
        onEntered:{
            new_letter_button.source = "img/button/Letterdown.png"
        }
        onExited:{
            new_letter_button.source = "img/button/KeyButton_1.png"
        }
    }

}
