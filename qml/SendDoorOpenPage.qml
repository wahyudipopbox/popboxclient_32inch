import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    width: 1024
    height: 768
    property int timer_value: 60
    property var press: "0"
    property var store_type:"store"
    property variant containerqml: null
    property var press_timer_button: 0
    property var type:""
    property int change_press

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log("change_press [door_open] status :", change_press)
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            confirm_button.enabled = true
            if(type == ""){
                change_box_button.enabled = false
                change_box_button.visible = false
            }
            if(type == "none"){
                change_box_button.enabled = true
                change_box_button.visible = true
            }
            if(change_press == 3){
                change_box_button.enabled = false
                change_box_button.show_color = "#B1AEAE"
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    DoorEvey{
        y:251
        x:263
    }

    Timer{
        id:press_timer
        interval:3000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            open_again_button.enabled = true
            press_timer_button = 0
            press_timer.stop()
        }
    }

    DoorButton {
        id:change_box_button
        x: 66
        y: 640
        width: 279
        height:64
        show_text: qsTr("change-box")
        show_image:"img/door/1.png"

        Text {
            id:change_number_text
            width: 35
            height: 35
            color: "#ab312e"
            text: change_press
            verticalAlignment: Text.AlignTop
            font.family:"Microsoft YaHei"
            font.pixelSize: 20
            anchors.right: parent.right
            anchors.rightMargin: 0
            horizontalAlignment: Text.AlignHCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {               
                change_door_notif.open()
            }
            onEntered:{
                change_box_button.show_image = "img/door/2.png"
            }
            onExited:{
                change_box_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton {
        id:open_again_button
        y:640
        x:365
        show_text:qsTr("Open again")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press_timer_button == 0){
                    abc.counter = timer_value
                    my_timer.restart()
                    press_timer.start()
                    slot_handler.start_open_mouth_again()
                    open_again_button.enabled = false
                    press_timer_button = 1
                    confirm_button.enabled = true
                    press = "0"
                }
            }
            onEntered:{
                open_again_button.show_image = "img/door/2.png"
            }
            onExited:{
                open_again_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton {
        id: confirm_button
        y:640
        x:663
        show_text:qsTr("Confirm receipt")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                slot_handler.start_store_customer_express()
                my_stack_view.push(s_drop_off_thanks_page)
            }
            onEntered:{
                confirm_button.show_image = "img/door/2.png"
            }
            onExited:{
                confirm_button.show_image = "img/door/1.png"
            }
        }
    }

    Row {
        x: 270
        y: 140
        layoutDirection: Qt.LeftToRight

        Text {
            id: box_open_tips_1
            color: "#FFFFFF"
            text: qsTr("Your locker is located at Box No ")
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }
        Text {
            id:mouth_number
            color: "#FFFFFF"
            text: qsTr("X")
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }
    }

    Column {
        x: 315
        y: 175
        spacing: 5

        Row {
            spacing: 10

            Text {
                color: "#FFFFFF"
                text: qsTr("The system will refresh in")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }

            Rectangle{
                width:39
                height:11
                y:10
                color:"transparent"
                QtObject{
                    id:abc
                    property int counter
                    Component.onCompleted:{
                        abc.counter = timer_value
                    }
                }

                Text{
                    id:countShow_test
                    x:183
                    y:500
                    anchors.centerIn:parent
                    color:"#FFFFFF"
                    font.family:"Microsoft YaHei"
                    font.pixelSize:24
                }

                Timer{
                    id:my_timer
                    interval:1000
                    repeat:true
                    running:true
                    triggeredOnStart:true
                    onTriggered:{
                        countShow_test.text = abc.counter
                        abc.counter -= 1
                        if(abc.counter <= 1){
                            confirm_button.enabled = false
                        }
                        if(abc.counter < 0){
                            my_timer.stop()
                            my_stack_view.push(s_drop_off_thanks_page)
                            slot_handler.start_store_customer_express()
                        }
                    }
                }
            }

            Text {
                color: "#FFFFFF"
                text: qsTr("sec")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }
        }
    }

    FullWidthReminderText {
        x: 0
        y: 200
        remind_text:qsTr("Please ensure compartment door closure after deposit")
        remind_text_size:"24"
    }


    Component.onCompleted: {
        root.mouth_number_result.connect(show_text)
        slot_handler.get_express_mouth_number()
    }

    Component.onDestruction: {
        root.mouth_number_result.disconnect(show_text)
    }

    function show_text(t){
        mouth_number.text = t
    }

    function clickedfunc(temp){
        containerqml.clickedfunc(temp)
    }

    HideWindow{
        id:change_door_notif
        //visible: true

        Image {
            id: img_change_door_notif
            x: 370
            y: 361
            width: 285
            height: 225
            fillMode: Image.PreserveAspectFit
            source: "img/item/close-locker.png"
        }

        Text {
            id: change_notif_text
            x: 113
            y:250
            width: 800
            height: 100
            text: qsTr("Please ensure you have closed the empty previous door before continuing")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.RichText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:change_door_notif_button
            x:373
            y:592
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(type == "none"){
                        containerqml.clickedfunc("change")
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 4) return true}), {change_press:change_press})
                    }
                }
                onEntered:{
                    change_door_notif_button.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    change_door_notif_button.show_source = "img/button/7.png"
                }
            }
        }
    }
}
