import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: grocery_payment_success
    width: 1024
    height: 768
    property int timer_value: 60
    property int timer_thanks: 15
    property var card:"-"
    property var last_balance:"-"
    property var show_date:"-"
    property var locker:"-"
    property var tid:""
    property var payment_result:'{"show_date": 1483511917000, "locker": "Demo Locker", "last_balance": "995000", "card_no": "1234567887654321", "terminal_id": "01234567"}'
    property var amount: "0"
    property var summary_data
    property var cust_name:"-"
    property var cust_phone:"-"
    property var invoice_order:"XXX"
    property var payment_type:"emoney"
    property var product_detail:"-"
    property var product_:"-"
    property var customer_:"-"
    property var purchase_:"-"
    property var address_:"-"
    property var new_amount:"-"

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            show_payment_result(payment_result)
            thank_you_notif.close()
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            show_timer_thanks.stop()
        }
    }

    Component.onCompleted: {
        root.start_popshop_transaction_result.connect(invoice_result)
    }

    Component.onDestruction: {
        root.start_popshop_transaction_result.disconnect(invoice_result)
    }

    function show_payment_result(payment_result){
        console.log("settlement_result : " + payment_result)
        if(payment_result == ""){
            return
        }

        var obj = JSON.parse(payment_result)

        tid = obj.terminal_id
        locker = obj.locker
        last_balance = obj.last_balance
        card = obj.card_no
        new_amount = insert_flg(amount)

        var now_date = new Date(obj.show_date)
        var time_Y = add_zero(now_date.getFullYear());
        var mouth = (now_date.getMonth()+1 < 10 ? +(now_date.getMonth()+1) : now_date.getMonth()+1)
        switch (mouth){
            case 1: var time_M = "January"
                break;
            case 2: var time_M = "February"
                break;
            case 3: var time_M = "March"
                break;
            case 4: var time_M = "April"
                break;
            case 5: var time_M = "May"
                break;
            case 6: var time_M = "June"
                break;
            case 7: var time_M = "July"
                break;
            case 8: var time_M = "August"
                break ;
            case 9: var time_M = "September"
                break;
            case 10: var time_M = "October"
                break;
            case 11: var time_M = "November"
                break;
            case 12: var time_M = "December"
                break;
        }
        var time_D = add_zero(now_date.getDate());
        var time_h = add_zero(now_date.getHours());
        var time_m = add_zero(now_date.getMinutes());
        var time_s = add_zero(now_date.getSeconds());
        show_date = time_D+" "+time_M+" "+time_Y+" "+time_h+":"+time_m+":"+time_s

        //Start defining parameters
        define_member(summary_data)
    }

    function add_zero(temp){
        if(temp<10) return "0"+temp;
        else return temp;
    }

    function change_mouth(mouth){
        switch (mouth){
            case "1": print("January")
                break;
            case "2": print("February")
                break;
            case "3": print("March")
                break;
            case "4": print("April")
                break;
            case "5": print("May")
                break;
            case "6": print("June")
                break;
            case "7": print("July")
                break;
            case "8": print("August")
                break ;
            case "9": print("September")
                break;
            case "10": print("October")
                break;
            case "11": print("November")
                break;
            case "12": print("December")
                break;
        }
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function invoice_result(result){
        console.log('start_popshop_transaction_result :' + result)
        if(result != "ERROR"){
            invoice_order = result
        }else{
            show_timer_thanks.restart()
        }
    }

    function define_member(summary){
        var info = summary.split("|")
        cust_name = info[4]
        product_detail = info[0] + " - " + info[2] + " pcs."
        cust_phone = info[6]

        //Setting parameters for Popshop Transaction,
        product_ = info[0] + "|" + info[1] + "|" + info[2] + "|" + info[3]
        customer_ = info[4] + "|" + info[5] + "|" + info[6]
        purchase_ =  locker + "|" + card + "|" + last_balance + "|" + payment_type
        address_ = info[7]

        //Start the Popshop Transaction
        slot_handler.start_popshop_transaction(product_, customer_, purchase_, address_)

        //EXAMPLE
        //"product_info" : "PBSDIM00298|Mobile Phone Stand Suit - White|3|105000",
        //"customer_info" : "wahyudi|wahyudi@popbox.asia|085710157057",
        //"purchase_info" : "Simulator TestStation|6032984026023022|509277|emoney|2017-03-20 11:30",
        //"delivery_address" :"Grand Slipi Tower"
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    main_page.enabled = false
                    ok_button_main_page.enabled = false
                    show_timer_thanks.start()
                    thank_you_notif.open()
                }
            }
        }
    }

    Rectangle{
        id: main_page
        FullWidthReminderText{
           id:title_text
           y:140
           remind_text:qsTr("Order Details")
           remind_text_size:"35"
        }

        FullWidthReminderText{
           id:notif_text
           x: 0
           y:566
           remind_text:qsTr("The sms notification will be sent to you for this product purchase")
           remind_text_size:"25"
        }

        DoorButton{
            id:ok_button_main_page
            y:671
            x:373
            show_text:qsTr("OK")
            show_image:"img/door/1.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    main_page.enabled = false
                    ok_button_main_page.enabled = false
                    show_timer_thanks.start()
                    thank_you_notif.open()
                }
                onEntered:{
                    ok_button_main_page.show_image = "img/door/2.png"
                }
                onExited:{
                    ok_button_main_page.show_image = "img/door/1.png"
                }
            }
        }

        Row{
            id: row_details
            x:223
            y:200
            spacing:30
            Column{
                id: labels
                spacing:15
                Text {
                    id: card_no_label
                    text: qsTr("Card No.")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: last_balance_label
                    text: qsTr("Last Balance")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: product_item_label
                    text: qsTr("Product Item")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: amount_label
                    text: qsTr("Amount")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: date_label
                    text: qsTr("Date")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: terminal_id_label
                    text: qsTr("Terminal ID")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: locker_name_label
                    text: qsTr("Locker Name")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
            }
            Row{
                id: separator
                spacing:10
                Column{
                    id: separators
                    spacing:15
                    Text {
                        id: sep_1
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: sep_2
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: sep_3
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: sep_4
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: sep_5
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: sep_6
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: sep_7
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                }
                Column{
                    id: values
                    width: 450
                    spacing:15
                    Text {
                        id: card_no_text
                        text: card
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: last_balance_text
                        text: insert_flg(last_balance)
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: product_item_text
                        text: 'Popshop' + ' - ' + product_detail
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                        font.capitalization: Font.Capitalize
                    }
                    Text {
                        id: amount_text
                        text: new_amount
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: date_text
                        text: show_date
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: tid_text
                        text: tid
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                    }
                    Text {
                        id: locker_text
                        text: locker
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 25
                        font.capitalization: Font.Capitalize
                    }
                }
            }
        }

        FullWidthReminderText {
            id: notif2_text
            x: 0
            y: 612
            remind_text: qsTr("Please do not forget to take your prepaid card")
            remind_text_size: "25"
        }
    }

    FloatingWindow{
        id:thank_you_notif
        //visible:true

        Text {
            id: text_thank_you_notif_1
            x:192
            y:233
            width: 650
            height: 50
            text: "Hi, " + cust_name.toString()
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
            font.capitalization: Font.Capitalize
        }

        Text {
            id: text_thank_you_notif_2
            x:290
            y:289
            width: 600
            height: 118
            text: qsTr("Your order : ") + invoice_order.toString() + qsTr(", will be processed within 1 x 24 hours. Kindly check your email.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_thank_you_notif_3
            x:290
            y:419
            width: 600
            height: 106
            text: qsTr("Thank you for shopping with PopBox Locker Shop. For assistance please call 021-29022537/38")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_thanks
                property int counter
                Component.onCompleted:{
                    time_thanks.counter = timer_thanks
                }
            }
            Row{
                x:848
                y:502
                spacing:5
                Text {
                    id:show_time_thanks
                    width: 50
                    font.family:"Microsoft YaHei"
                    color:"red"
                    textFormat: Text.PlainText
                    font.pointSize:25
                    wrapMode: Text.WordWrap
                }
            }

            Timer{
                id:show_timer_thanks
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_thanks.text = time_thanks.counter
                    time_thanks.counter -= 1                 
                    if(time_thanks.counter == 7){
                        console.log('settle popshop : ' + cust_phone + '>' + "popshop" + ':' + product_ + '>' + amount + '>' + payment_type)
                        slot_handler.start_push_data_settlement(cust_phone + '-' + "popshop" + '-' + product_ + '-' + amount + '-' + payment_type)
                    }
                    if(time_thanks.counter < 0){
                        show_timer_thanks.stop()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                }
            }
        }
    }
}
