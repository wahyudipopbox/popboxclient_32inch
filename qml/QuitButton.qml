import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:100
    height:93
    color:"transparent"
    property var show_text:""
    property var show_source: "img/button/quit.png"


    Image{
        width:100
        height:93
        source:show_source
    }
    Rectangle{
        y:30
        width:100
        height:93
        color:"transparent"

        Text{
            font.family:"Microsoft YaHei"
            text:show_text
            color:"#d1d3d4"
            font.pixelSize:24
            anchors.centerIn: parent;
        }
    }
}

