import serial
import time
import datetime
import codecs
from binascii import *
from struct import *
from functools import wraps
import Configurator
############################################################################################
tokenSAM = Configurator.get_or_set_value('popbox', 'tokenSAM')
terminalID = Configurator.get_or_set_value('popbox', 'terminalID')
institutionID = Configurator.get_or_set_value('popbox', 'institutionID')
nfcPort = Configurator.get_or_set_value('popbox', 'nfcPort')
shiftID = Configurator.get_or_set_value('popbox', 'shiftID')
price = 0
startID = b'\x10\x02'
header = b'\x08\x00\x00\x00\x00\x00\x00'
instID = bytes(institutionID, 'ascii')
termID = bytes(terminalID, 'ascii')
samlogin = bytes(tokenSAM, 'ascii')
endID = b'\x10\x03'
timeOut = bytes('015', 'ascii')
#############################################################################################
def counter(func):
    @wraps(func)
    def tmp(*args, **kwargs):
        tmp.count += 1
        return func(*args, **kwargs)
    tmp.count = 0
    return tmp

def strfrHLE(h):
    outH = 0
    if len(h) != 0:
        outH = str(int(str(''.join([h[i:i+2] for i in range(0, len(h), 2)][::-1])), 16))
    return outH

def appCou(a):
    outA = 0
    if len(a)%4 != 0:
        newA = a.replace('03','')
        outA = ''.join(str(int(i, 16)) for i in reversed([newA[i:i+2] for i in range(0, len(newA), 2)]))
    return outA

def decfrHLE(d):
    outD = 0
    if len(d) != 8:
        newD = d.zfill(8)
        outD = ''.join([newD[i:i+2] for i in range(0, len(newD), 2)][::-1])
    return outD

def getSTRfrHexLE(h):  # Function to Get String Value from Hexadecimal Little Endian
    strHexLE = 0
    if len(h) != 0:
        strHexLE = ''.join(str(int(i, 16)) for i in reversed([h[i:i + 2] for i in range(0, len(h), 2)]))
    return strHexLE

def calcLRC(input):  # Function to calculate Longitudinal Redundancy Check (LRC)
    #input_new = ':'.join(input)
    input_new = [chr(int(x, 16)) for x in input]
    lrc = 0
    for x in input_new:
        lrc ^= ord(x)
    return lrc

def HexDataArray(input): #-> Input should be in bytes format b'01234567ABCDEF'
    data_hex = ''.join(hex(ord(x)) for x in input.decode('ascii')).replace('0x', '')
    data_array = [data_hex[i:i + 2] for i in range(0, len(data_hex), 2)]
    return data_array

def ByteHexDataArray(input): #-> Input should be in bytes format b'\x08\x00\x00\x00\x00\x00\x00'
    byte_hex = str(b2a_hex(input), 'ascii')
    byte_array = [byte_hex[i:i + 2] for i in range(0, len(byte_hex), 2)]
    return byte_array

def init_serial(x):  # Serial Connection Initiation
    global ser
    ser = serial.Serial()
    ser.port = nfcPort
    ser.baudrate = 38400
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = int(x)
    ser.open()
    if ser.isOpen():
        return ser.isOpen()
    else:
        return 'FAILED'

def initSAM():  # Secure Access Module Initiation - MUST BE RUN FIRST before any transaction
    cmd = b'\x30'
    data = cmd + samlogin + instID + termID
    length = pack('>h', len(data))
    checksumData = pack('B', calcLRC(ByteHexDataArray(header+length) + HexDataArray(data)))
    init_serial(1)
    initSAMData = startID + header + length + data + checksumData + endID
    ser.write(initSAMData)
    response = ser.read(255)
    time.sleep(1)
    ser.close()
    return response.decode('ascii')[12:16]

def balanceInfo():  # Cek Balance to the pre-paid card
    cmd = b'\x31'
    timeStamp = bytes(time.strftime('%d%m%y%H%M%S', time.localtime(time.time())), 'ascii')
    data = cmd + timeStamp + timeOut
    length = pack('>h', len(data))
    checksumData = pack('B', calcLRC(ByteHexDataArray(header+length) + HexDataArray(data)))
    init_serial(3)
    time.sleep(1)
    balanceInfoData = startID + header + length + data + checksumData + endID
    ser.write(balanceInfoData)
    response = ser.read(110)
    p = response[12:110]
    statTrans = p[:4].decode('ascii')
    balance = response.decode('ascii')[16:26].lstrip('0')
    time.sleep(1)
    ser.close()
    if statTrans == '0000':
        if balance != '' :
            return balance
        else:
            return 'EMPTY'
    elif statTrans == '6A82':
        return 'WRONG-CARD'
    elif statTrans == 'FFFE':
        return 'WRONG-CARD'
    else:
        return 'ERROR'

def purcDeb():  # Purchase Debit transcation
    global dataRep

    cmd = b'\x32'
    amount = bytes(str(price).zfill(10), 'ascii')
    timeStamp = bytes(time.strftime('%d%m%y%H%M%S', time.localtime(time.time())), 'ascii')
    data = cmd + timeStamp + amount + timeOut
    length = pack('>h', len(data))
    checksumData = pack('B', calcLRC(ByteHexDataArray(header+length) + HexDataArray(data)))
    init_serial(3)
    time.sleep(1)
    purcDebData = startID + header + length + data + checksumData + endID
    ser.write(purcDebData)
    response = ser.read(110)
    p = response[12:110]
    transStat = p[:4].decode('ascii')
    cardNo = p[4:20].decode('ascii')
    transCode = p[46:50].decode('ascii')
    transAmount = strfrHLE(h=p[50:58].decode('ascii'))
    lastBalance = strfrHLE(h=p[58:66].decode('ascii'))
    timeTrans = p[66:78].decode('ascii')
    dataRep = transStat + cardNo + transCode + str(transAmount).zfill(8) + str(lastBalance).zfill(8) + timeTrans
    time.sleep(1)
    ser.close()
    if transStat == '0000':
        # paymentSuccess.count += 1
        return "SUCCESS"
    else:
        return transStat

def purcDebGlobal(amountX):  # Purchase Debit transcation
    global dataRep

    cmd = b'\x32'
    amount = bytes(str(amountX).zfill(10), 'ascii')
    timeStamp = bytes(time.strftime('%d%m%y%H%M%S', time.localtime(time.time())), 'ascii')
    data = cmd + timeStamp + amount + timeOut
    length = pack('>h', len(data))
    checksumData = pack('B', calcLRC(ByteHexDataArray(header+length) + HexDataArray(data)))
    init_serial(3)
    time.sleep(1)
    purcDebData = startID + header + length + data + checksumData + endID
    ser.write(purcDebData)
    response = ser.read(110)
    p = response[12:110]
    transStat = p[:4].decode('ascii')
    cardNo = p[4:20].decode('ascii')
    transCode = p[46:50].decode('ascii')
    transAmount = strfrHLE(h=p[50:58].decode('ascii'))
    lastBalance = strfrHLE(h=p[58:66].decode('ascii'))
    timeTrans = p[66:78].decode('ascii')
    dataRep = transStat + cardNo + transCode + str(transAmount).zfill(8) + str(lastBalance).zfill(8) + timeTrans
    time.sleep(1)
    ser.close()
    if transStat == '0000':
        # paymentSuccess.count += 1
        return "SUCCESS"
    else:
        return transStat

@counter
def paymentSuccess():
    return dataRep

def syncTime():  # Set new Date & Time setting to Reader
    cmd = b'\xF0'
    timeStamp = bytes(time.strftime('%d%m%y%H%M%S0%w', time.localtime(time.time())), 'ascii')
    data = cmd + timeStamp
    length = pack('>h', len(data))
    checksumData = pack('B', calcLRC(ByteHexDataArray(header+length+cmd) + HexDataArray(timeStamp)))
    init_serial(1)
    syncTimeData = startID + header + length + data + checksumData + endID
    ser.write(syncTimeData)
    response = ser.read(255)
    statSync = response[12:16].decode('ascii')
    time.sleep(1)
    ser.close()
    return statSync

def resetPartial():
    cmd = b'B'
    data = cmd
    length = pack('>h', len(data))
    checksumData = pack('B', calcLRC(ByteHexDataArray(header+length) + HexDataArray(data)))
    init_serial(1)
    resetPartialData = startID + header + length + data + checksumData + endID
    ser.write(resetPartialData)
    response = ser.read(255)
    statReset = response[12:16].decode('ascii')
    time.sleep(1)
    ser.close()
    return statReset

def getReport():  # Function to print last transaction report
    cmd = b'\x33'
    data = cmd
    length = pack('>h', len(data))
    checksumData = pack('B', calcLRC(ByteHexDataArray(header+length) + HexDataArray(data)))
    init_serial(3)
    getReportData = startID + header + length + data + checksumData + endID
    ser.write(getReportData)
    response = ser.read(255)
    time.sleep(1)
    ser.close()
    dr = response[12:110]
    #transStat = dr[:4].decode('ascii')
    #cardNo = dr[4:20].decode('ascii')
    #transCode = dr[46:50].decode('ascii')
    #transTime = time.strptime(dr[66:78].decode('ascii'), '%d%m%y%H%M%S')
    #mainCounter = strfrHLE(h = dr[78:86].decode('ascii'))
    #appCounter = appCou(a = dr[86:92].decode('ascii')).lstrip('0')
    #mac = dr[92:98].decode('ascii')
    #lastBalDLE = decfrHLE(d = lastBalance)
    #transAmoDLE = decfrHLE(d = transAmount)
    transAmount = strfrHLE(h=dr[50:58].decode('ascii'))
    lastBalance = strfrHLE(h=dr[58:66].decode('ascii'))
    dt_ = dr[4:50].decode('ascii') + transAmount.zfill(8) + lastBalance.zfill(8) + dr[66:98].decode('ascii') + shiftID
    return dt_

def settlement():
    sd1 = getReport()
    if len(sd1) != 0:
        settData = sd1 + str(paymentSuccess.count).zfill(6)
    else:
        settData = 'FAILED'
    return settData

def main():
    return 'Python Mandiri Reader Module for PopBox Locker'

if __name__ == '__main__':
    main()
