import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQml.Models 2.1
import "poplockers.js" as JS

Background{
    id: base_bground
    width: 1024
    height: 768
    logo_y: 25
    property int timer_value: 60
    property var press:"0"
    property var usageOf:"undefined"
    property var lockers_list:""
    property var province:"DKI Jakarta"
    property var city:"Jakarta Barat"
    property var datas:""
    property var total_amount:""
    property var getMethod:"offline"

    Stack.onStatusChanged:{
        console.log("data_received : " + datas + ", with amount : " + total_amount)
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            loadingGif.open()
            locker_data_view.visible = false
            if(getMethod=="offline"){
                buttonGetLocker(province, city)
            }else{
                slot_handler.get_locker_data(province,city)
            }
            if(city=="Jakarta Barat"){
                jakbar_button.button_color = "darkred"
            }else{
                jakbar_button.button_color = "white"
            }
            if(lockers_list != ""){
                loadingGif.close()
                locker_data_view.visible = true
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            lockersListModel.clear()
        }
    }

    Component.onCompleted: {
        root.get_locker_data_result.connect(handle_locker_result)
    }

    Component.onDestruction: {
        root.get_locker_data_result.disconnect(handle_locker_result)
    }

    function insert_flg(a){
        var newstrA=""
        newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstrA
    }

    function handle_locker_result(result){
        //console.log("start_get_locker_data_result : " + result)
        if(result == "FAILED"){
            network_error.open()
            main_page.enabled = false
        }else{
            var list_locker = JSON.parse(result)
            var total_lockers = list_locker.total
            lockers_list = list_locker.data
            parse_data_locker()
            if(lockers_list != "" && total_lockers > 0){
                //notif_box_firstLoad.visible = false
                loadingGif.close()
                locker_data_view.visible = true
            }else{
                loadingGif.open()
                locker_data_view.visible = false
            }
        }
    }

    function parse_data_locker(lockers_list_){
        //Undefined the gridview model
        gridview.model = undefined
        var lockers = lockers_list_
        if(!lockersListModel.count){
            for(var x in lockers) {
                var show_lock_name = lockers[x].name
                var show_lock_addr_1 = lockers[x].address
                var show_lock_addr_2 = lockers[x].address_2
                var show_lock_building = lockers[x].building_type
                var show_lock_operational = lockers[x].operational_hours
                //Appending Product lockers
                lockersListModel.append({"lock_name_": show_lock_name,
                                                 "lock_addr_1_":show_lock_addr_1,
                                                 "lock_addr_2_": show_lock_addr_2,
                                                 "lock_building_": show_lock_building,
                                                 "lock_operational_": show_lock_operational})
            }
        }else{
            return
        }
        //Re-define the gridview model
        gridview.model = lockersListModel
        console.log('Total Lockers in ' + city.toUpperCase() + ' : ' + lockersListModel.count);

        /*for(var i = 0; i < lockersListModel.count; ++i) {
            console.log(lockersListModel.get(i).prod_sku_ +" -> "+ lockersListModel.get(i).prod_name_);
        }*/
    }

    function buttonGetLocker(x,y){
        abc.counter = timer_value
        my_timer.restart()
        lockersListModel.clear()
        loadingGif.open()
        locker_data_view.visible = false
        province = x
        city = y
        if(getMethod=="offline"){
            lockers_list=JS.define_locker(province, city)
            parse_data_locker(lockers_list)
            if(lockers_list != ""){
                loadingGif.close()
                locker_data_view.visible = true
            }
        }else{
            slot_handler.get_locker_data(province,city)
        }
    }

    LoadingView{
        id: loadingGif
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter == 30 && lockers_list == ""){
                    network_error.open()
                }
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id: main_page

        BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("Back")

            MouseArea {
                id: ms_back
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
        }

        Item{
            id: locker_data_view
            x: 73
            y: 105
            width: 790
            height: 650
            anchors.bottom: parent.bottom
            anchors.bottomMargin: -767
            anchors.right: parent.right
            anchors.rightMargin: -1023
            focus: true

            ScrollBarPop{
                id: vertical_sbar
                flickable: gridview
                x: 975
                y: 266
                width: 10
                height: locker_data_view.height
                color: "white"
                anchors.rightMargin: -10
            }

            GridView{
                id: gridview
                width: 790
                anchors.verticalCenterOffset: 0
                anchors.horizontalCenterOffset: -26
                anchors.rightMargin: 45
                anchors.leftMargin: -56
                focus: true
                clip: true
                anchors.bottomMargin: 0
                anchors.topMargin: 0
                flickDeceleration: 750
                maximumFlickVelocity: 1500
                anchors.centerIn: parent
                layoutDirection: Qt.LeftToRight
                flow: GridView.FlowLeftToRight
                boundsBehavior: Flickable.StopAtBounds
                snapMode: GridView.SnapToRow
                anchors.fill: parent
                anchors.margins: 20
                delegate: delegate_item_view
                //model: lockersListModel
                cellWidth: 385
                cellHeight: 160
            }

            ListModel {
                id: lockersListModel
                dynamicRoles: true
            }

            Component{
                id: delegate_item_view
                LockerCardView{
                    id: item_view
                    lock_name: lock_name_
                    lock_addr_1: lock_addr_1_
                    lock_addr_2: lock_addr_2_
                    lock_building: lock_building_
                    lock_operational: lock_operational_

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(usageOf=="popshop"){
                                var data_all = datas + "|" + item_view.lock_name
                                my_stack_view.push(grocery_express_info, {data_all:data_all, amount:total_amount})
                            }else{
                                my_stack_view.push(on_develop_view)
                            }
                        }
                    }
                }
            }
        }

        Rectangle{
            id: locker_city_buttons
            focus: true
            visible: true
            color: "transparent"
            anchors.right: locker_data_view.left
            anchors.rightMargin: 0
            x: 0
            y: 117
            width: base_bground.width - locker_data_view.width
            height: locker_data_view.height

            Column{
                x: -6
                y: 0
                spacing: 2
                //1st Button
                LockerCityButton{
                    id: bandung_button
                    button_text: qsTr("Bandung")
                    //enabled: (button_color=="darkred") ? false : true
                    visible: false
                    enabled: false
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bandung_button.button_color = "darkred"
                        bekasi_button.button_color = "white"
                        bogor_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("Jawa Barat","Bandung")
                        }
                    }
                }
                //2nd Button
                LockerCityButton{
                    id: bekasi_button
                    button_text: qsTr("Bekasi")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bekasi_button.button_color = "darkred"
                        bandung_button.button_color = "white"
                        bogor_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("Jawa Barat","Bekasi")
                        }
                    }
                }
                //3rd Button
                LockerCityButton{
                    id: bogor_button
                    button_text: qsTr("Bogor")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bogor_button.button_color = "darkred"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("Jawa Barat","Bogor")
                        }
                    }
                }
                //4th Button
                LockerCityButton{
                    id: cibinong_button
                    button_text: qsTr("Cibinong")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        cibinong_button.button_color = "darkred"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("Jawa Barat","Cibinong")
                        }
                    }
                }
                //5th Button
                LockerCityButton{
                    id: depok_button
                    button_text: qsTr("Depok")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        depok_button.button_color = "darkred"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("Jawa Barat","Depok")
                        }
                    }
                }
                //6th Button
                LockerCityButton{
                    id: jakbar_button
                    button_text: qsTr("Jakarta Barat")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jakbar_button.button_color = "darkred"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("DKI Jakarta","Jakarta Barat")
                        }
                    }
                }
                //7th Button
                LockerCityButton{
                    id: jakpus_button
                    button_text: qsTr("Jakarta Pusat")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jakpus_button.button_color = "darkred"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("DKI Jakarta","Jakarta Pusat")
                        }
                    }
                }
                //8th Button
                LockerCityButton{
                    id: jaksel_button
                    button_text: qsTr("Jakarta Selatan")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jaksel_button.button_color = "darkred"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("DKI Jakarta","Jakarta Selatan")
                        }
                    }
                }
                //9th Button
                LockerCityButton{
                    id: jaktim_button
                    button_text: qsTr("Jakarta Timur")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jaktim_button.button_color = "darkred"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("DKI Jakarta","Jakarta Timur")
                        }
                    }
                }
                //10th Button
                LockerCityButton{
                    id: jakut_button
                    button_text: qsTr("Jakarta Utara")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jakut_button.button_color = "darkred"
                        jaktim_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        buttonGetLocker("DKI Jakarta","Jakarta Utara")
                        }
                    }
                }
                //11th Button
                LockerCityButton{
                    id: tangerang_button
                    button_text: qsTr("Tangerang")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        tangerang_button.button_color = "darkred"
                        jakut_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        buttonGetLocker("Banten","Tangerang")
                        }
                    }
                }
            }
        }
    }

    //Not Used
    Rectangle{
        id:notif_box_firstLoad
        x: 183
        y: 294
        width: 800
        height: 200
        visible: false
        color: "white"
        anchors.verticalCenter: parent.verticalCenter
        opacity: 0.7
        Text{
            id: txt_notif_big
            x: 198
            y: 241
            width: 569
            height: 125
            text: qsTr("Please press Locker City on The Left Side Panel")
            anchors.verticalCenter: parent.verticalCenter
            font.italic: true
            textFormat: Text.PlainText
            color: "red"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 35
            font.family: "Microsoft YaHei"
            font.bold: true

        }
        Image{
            id: img_arrow
            width: 100
            height: 50
            anchors.verticalCenter: parent.verticalCenter
            source: "img/button/white-arrow-left.png"
            x:61
            y:275

        }
    }

    HideWindow{
        id:network_error
        //visible: true

        Image {
            id: img_network_error
            x: 437
            y: 412
            width: 150
            height: 180
            source: "img/otherImages/network-offline.png"
        }

        Text {
            id: text_notif
            x: 112
            y:232
            width: 800
            height: 150
            text: qsTr("Currently we are not connected to the system, Please retry later")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:network_error_back_button
            x:373
            y:607
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    network_error_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    network_error_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }
}
