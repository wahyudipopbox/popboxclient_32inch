import json
import logging
import math
import random
import datetime
import time
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import Configurator
import box.repository.BoxDao as BoxDao
import company.service.CompanyService as CompanyService
import company.repository.CompanyDao as CompanyDao
import express.repository.ExpressDao as ExpressDao
import box.service.BoxService
import user.service.UserService as UserService
from network import HttpClient
from device import Scanner
from device import CoinMachine
from device import WeighingMachine
from device import QP3000S
__author__ = 'gaoyang'


class ExpressSignalHandler(QObject):
    customer_take_express_signal = pyqtSignal(str)
    overdue_cost_signal = pyqtSignal(str)
    barcode_signal = pyqtSignal(str)
    store_express_signal = pyqtSignal(str)
    phone_number_signal = pyqtSignal(str)
    paid_amount_signal = pyqtSignal(str)
    customer_store_express_signal = pyqtSignal(str)
    overdue_express_list_signal = pyqtSignal(str)
    overdue_express_count_signal = pyqtSignal(int)
    staff_take_overdue_express_signal = pyqtSignal(str)
    load_express_list_signal = pyqtSignal(str)
    customer_store_express_cost_signal = pyqtSignal(str)
    store_customer_express_result_signal = pyqtSignal(str)
    customer_express_cost_insert_coin_signal = pyqtSignal(str)
    send_express_list_signal = pyqtSignal(str)
    send_express_count_signal = pyqtSignal(int)
    staff_take_send_express_signal = pyqtSignal(str)
    imported_express_result_signal = pyqtSignal(str)
    customer_reject_express_signal = pyqtSignal(str)
    reject_express_signal = pyqtSignal(str)
    reject_express_list_signal = pyqtSignal(str)
    product_file_signal = pyqtSignal(str)
    start_get_express_info_result_signal = pyqtSignal(str)
    start_get_pakpobox_express_info_signal = pyqtSignal(str)
    start_get_customer_info_by_card_signal = pyqtSignal(int)
    start_payment_by_card_signal = pyqtSignal(str)
    start_get_cod_status_signal = pyqtSignal(str)

logger = logging.getLogger()
express_signal_handler = ExpressSignalHandler()
barcode_run_flag = False
barcode_start = False
overdue_cost = 0

if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
    still_ebox = True
else:
    still_ebox = False

def init_express(param, mouth, box_param):
    express = {'id': param['id'],
               'expressType': param['expressType'],
               'status': param['status'],
               'storeTime': param['storeTime'],
               'syncFlag': 1,
               'version': param['version'],
               'box_id': mouth['box_id'],
               'logisticsCompany_id': ClientTools.get_value('id', ClientTools.get_value('logisticsCompany', param)),
               'operator_id': box_param['operator_id'],
               'mouth_id': mouth['id'],
               'storeUser_id': ClientTools.get_value('id', ClientTools.get_value('storeUser', param)),
               'groupName': load_param_or_default('groupName', param),
               'expressNumber': load_param_or_default('expressNumber', param),
               'customerStoreNumber': load_param_or_default('customerStoreNumber', param),
               'overdueTime': load_param_or_default('overdueTime', param),
               'takeUserPhoneNumber': load_param_or_default('takeUserPhoneNumber', param),
               'storeUserPhoneNumber': ClientTools.get_value('phoneNumber', ClientTools.get_value('storeUser', param)),
               'validateCode': load_param_or_default('validateCode', param),
               'designationSize': load_param_or_default('designationSize', param),
               'chargeType': load_param_or_default('chargeType', param),
               'continuedHeavy': load_param_or_default('continuedHeavy', param),
               'endAddress': load_param_or_default('endAddress', param),
               'continuedPrice': load_param_or_default('continuedPrice', param),
               'startAddress': load_param_or_default('startAddress', param),
               'firstHeavy': load_param_or_default('firstHeavy', param),
               'firstPrice': load_param_or_default('firstPrice', param),
               'payOfAmount': load_param_or_default('payOfAmount', param),
               'electronicCommerce_id': ClientTools.get_value('id', ClientTools.get_value('electronicCommerce', param)),
               'takeUser_id': ClientTools.get_value('id', ClientTools.get_value('takeUser', param))}
    ExpressDao.init_express(express)
    if express['electronicCommerce_id'] is not None:
        company_list = CompanyDao.get_company_by_id({'id': express['electronicCommerce_id']})
        electronic_commerce = {'id': express['electronicCommerce_id'],
                               'companyType': 'ELECTRONIC_COMMERCE',
                               'name': param['electronicCommerce']['name'],
                               'deleteFlag': 0,
                               'parentCompany_id': ClientTools.get_value('parentCompany_id', param['electronicCommerce'], None)}
        if len(company_list) == 0:
            CompanyDao.insert_company(electronic_commerce)
        else:
            CompanyDao.update_company(electronic_commerce)

def load_param_or_default(key, dict_, default_value=None):
    if key in dict_.keys():
        return dict_[key]
    return default_value

def start_barcode_take_express():
    global barcode_start
    if barcode_start:
        return
    barcode_start = True
    Scanner.scanner_signal_handler.barcode_result.connect(get_express_barcode_text)
    Scanner.start_get_text_info()

def stop_barcode_take_express():
    global barcode_start
    if not barcode_start:
        return
    barcode_start = False
    Scanner.scanner_signal_handler.barcode_result.disconnect(get_express_barcode_text)
    Scanner.start_stop_scanner()

def start_customer_take_express(validate):
    ClientTools.get_global_pool().apply_async(customer_take_express, (validate,))

def customer_take_express(validate_code):
    logger.info(validate_code)
    param = {'validateCode': validate_code,
             'status': 'IN_STORE'}
    expresses = ExpressDao.get_express_by_validate(param)
    pre_take_express(expresses)

def get_express_barcode_text(text_info):
    global express_signal_handler
    global barcode_start
    if not barcode_start:
        return
    barcode_start = False
    if text_info == 'ERROR':
        return
    if text_info == '':
        express_signal_handler.customer_take_express_signal.emit('NotInput')
        return
    if text_info.find('|') == -1:
        express_signal_handler.customer_take_express_signal.emit('Error')
        return
    express_info = text_info.split('|')
    param = {'id': express_info[0].strip(),
             'validateCode': express_info[1].strip(),
             'status': 'IN_STORE'}
    expresses = ExpressDao.get_express_by_id_and_validate(param)
    pre_take_express(expresses)

def pre_take_express(expresses):
    if len(expresses) != 1:
        express_signal_handler.customer_take_express_signal.emit('Error')
        return
    express = expresses[0]
    if express['overdueTime'] > ClientTools.now():
        take_express(express)
    else:
        pre_take_overdue_express(express)

def take_express(param, overdue=False):
    express = {'takeTime': ClientTools.now(),
               'status': 'CUSTOMER_TAKEN',
               'syncFlag': 0,
               'version': param['version'] + 1,
               'id': param['id'],
               'staffTakenUser_id': None}
    ExpressDao.take_express(express)
    mouth_param = box.service.BoxService.get_mouth(express)
    box.service.BoxService.free_mouth(mouth_param)
    box.service.BoxService.open_mouth(mouth_param['id'])
    if not still_ebox:
        try:
            express['expressNumber'] = param['expressNumber']
            express['takeUserPhoneNumber'] = param['takeUserPhoneNumber']
            express['overdueTime'] = param['overdueTime']
            express['storeTime'] = param['storeTime']
            express['validateCode'] = param['validateCode']
            express['box_id'] = param['box_id']
            express['logisticsCompany_id'] = param['logisticsCompany_id']
            express['mouth_id'] = param['mouth_id']
            express['operator_id'] = param['operator_id']
            express['storeUser_id'] = param['storeUser_id']
        except Exception as e:
            logger.warning(("Take_express additional params ERROR : ", e))
    express_signal_handler.customer_take_express_signal.emit('Success')
    if overdue:
        record_list = ExpressDao.get_record({'express_id': express['id']})
        if len(record_list) == 1:
            record = record_list[0]
            record_param = {'id': record['id'],
                            'createTime': record['createTime'],
                            'amount': record['amount']}
            express['transactionRecords'] = [record_param]
    result, status_code = HttpClient.post_message('express/customerTakeExpress', express)
    if status_code == 200 and result['id'] == express['id']:
        ExpressDao.mark_sync_success(express)

overdue_express = None

def pre_take_overdue_express(express_param):
    global overdue_express
    global paid_amount
    global overdue_cost
    overdue_express = express_param
    mouth_param = box.service.BoxService.get_mouth(express_param)
    time_span = ClientTools.now() - express_param['overdueTime']
    day_span = math.ceil(time_span / 1000.0 / 60.0 / 60.0 / 24.0)
    overdue_cost = day_span * mouth_param['overduePrice']
    record_param = {'express_id': overdue_express['id']}
    record_list = ExpressDao.get_record(record_param)
    if len(record_list) == 0:
        record_param = {'id': ClientTools.get_uuid(),
                        'paymentType': 'CASH',
                        'transactionType': 'PAYMENT_FOR_OVERDUE',
                        'amount': 0,
                        'express_id': overdue_express['id'],
                        'createTime': ClientTools.now()}
        ExpressDao.save_record(record_param)
        paid_amount = 0
    else:
        record = record_list[0]
        paid_amount = math.ceil(record['amount'] * -1)
    express_signal_handler.customer_take_express_signal.emit('Overdue')

paid_amount = 0
coin_machine_connect_flag = False

def start_pay_cash_for_overdue_express():
    global coin_machine_connect_flag
    if not coin_machine_connect_flag:
        coin_machine_connect_flag = True
        CoinMachine.coin_machine_signal_handler.coin_result.connect(pay_cash_for_overdue_express)
        CoinMachine.start_get_coin()

def stop_pay_cash_for_overdue_express():
    global coin_machine_connect_flag
    if coin_machine_connect_flag:
        CoinMachine.coin_machine_signal_handler.coin_result.disconnect(pay_cash_for_overdue_express)
        coin_machine_connect_flag = False

def pay_cash_for_overdue_express(text):
    global paid_amount
    if text == 'False':
        CoinMachine.start_get_coin()
        return
    paid_amount += 100
    record = {'express_id': overdue_express['id'],
              'amount': paid_amount * -1}
    ExpressDao.update_recode(record)
    express_signal_handler.paid_amount_signal.emit(str(math.ceil(paid_amount / 100)))
    if paid_amount < overdue_cost:
        CoinMachine.start_get_coin()
    else:
        stop_pay_cash_for_overdue_express()
        take_express(overdue_express, overdue=True)

def take_overdue_express():
    pass

def get_overdue_cost():
    express_signal_handler.overdue_cost_signal.emit(str(math.ceil(overdue_cost / 100)))

scanner_signal_connect_flag = False

def start_get_express_number_by_barcode():
    global scanner_signal_connect_flag
    if not scanner_signal_connect_flag:
        Scanner.scanner_signal_handler.barcode_result.connect(get_express_number_by_barcode)
        scanner_signal_connect_flag = True
    Scanner.start_get_text_info()

express_id = ''
imported_express = None

def get_express_number_by_barcode(text):
    global imported_express
    global express_id
    if text == '' or text == 'ERROR':
        return
    #print(text)
    express_signal_handler.barcode_signal.emit(text)
    express_message, status_code = HttpClient.get_message('express/imported/' + text)
    if status_code == 200:
        if ClientTools.get_value("statusCode", express_message) == 404:
            express_signal_handler.imported_express_result_signal.emit('no_imported')
            imported_express = None
            express_id = ''
        else:
            imported_express = express_message
            express_id = imported_express['id']
            express_signal_handler.phone_number_signal.emit(imported_express['takeUserPhoneNumber'])
    else:
        imported_express = None
        express_id = ''

def stop_get_express_number_by_barcode():
    global scanner_signal_connect_flag
    if scanner_signal_connect_flag:
        Scanner.scanner_signal_handler.barcode_result.disconnect(get_express_number_by_barcode)
        scanner_signal_connect_flag = False
    Scanner.start_stop_scanner()

def start_get_imported_express(text):
    get_express_number_by_text(text)

def get_express_number_by_text(text):
    global imported_express
    global express_id
    express_message, status_code = HttpClient.get_message('express/imported/' + text)
    if status_code == 200:
        if ClientTools.get_value("statusCode", express_message) == 404:
            express_signal_handler.imported_express_result_signal.emit('no_imported')
            imported_express = None
            express_id = ''
        else:
            imported_express = express_message
            express_id = imported_express['id']
            express_signal_handler.imported_express_result_signal.emit(imported_express['takeUserPhoneNumber'])
    else:
        express_signal_handler.imported_express_result_signal.emit('no_imported')
        imported_express = None
        express_id = ''

express_number = ''

def set_express_number(express_number__):
    global express_number
    express_number = express_number__

phone_number = ''

def set_phone_number(phone_number__):
    global phone_number
    phone_number = phone_number__

def start_store_express():
    ClientTools.get_global_pool().apply_async(store_express)

def store_express():
    global imported_express
    global express_id
    mouth_result = box.service.BoxService.mouth
    logger.debug(('mouth_result:', mouth_result))
    user_result = UserService.get_user()
    user_result['wallet']['balance'] = user_result['wallet']['balance'] - mouth_result['usePrice']
    box_result = box.service.BoxService.get_box()
    if not box_result:
        express_signal_handler.store_express_signal.emit('Error')
        return
    overdue_time = get_overdue_timestamp(box_result)
    operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
    if not operator_result:
        express_signal_handler.store_express_signal.emit('Error')
        return
    express_param = {'expressNumber': express_number,
                     'expressType': 'COURIER_STORE',
                     'overdueTime': overdue_time,
                     'status': 'IN_STORE',
                     'storeTime': ClientTools.now(),
                     'syncFlag': 0,
                     'takeUserPhoneNumber': phone_number,
                     'validateCode': random_validate(box_result['validateType']),
                     'version': 0,
                     'box_id': box_result['id'],
                     'logisticsCompany_id': user_result['company']['id'],
                     'mouth_id': mouth_result['id'],
                     'operator_id': box_result['operator_id'],
                     'storeUser_id': user_result['id']}
    if ClientTools.get_value('groupName', imported_express) is not None:
        express_param['groupName'] = imported_express['groupName']
    else:
        express_param['groupName'] = ""
    if express_id == '':
        express_param['id'] = ClientTools.get_uuid()
    else:
        express_param['id'] = express_id
    ExpressDao.save_express(express_param)
    mouth_param = {'id': mouth_result['id'],
                   'express_id': express_param['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    express_param['box'] = {'id': express_param['box_id']}
    express_param.pop('box_id')
    express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
    express_param.pop('logisticsCompany_id')
    express_param['mouth'] = {'id': express_param['mouth_id']}
    express_param.pop('mouth_id')
    express_param['operator'] = {'id': express_param['operator_id']}
    express_param.pop('operator_id')
    express_param['storeUser'] = {'id': express_param['storeUser_id']}
    express_param.pop('storeUser_id')
    express_signal_handler.store_express_signal.emit('Success')
    express_id = ''
    imported_express = None
    logger.debug("express_param : " + str(express_param))
    message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
    if status_code == 200 and message['id'] == express_param['id']:
        ExpressDao.mark_sync_success(express_param)

def get_overdue_timestamp(box_result):
    try:
        start_time = datetime.datetime.now()
        overdue_flag = ClientTools.get_value('overdueFlag', imported_express, default_value=None)
        if overdue_flag is None:
            overdue_type = box_result['overdueType']
            if overdue_type == 'HOUR':
                overdue_time = box_result['freeHours']
            else:
                overdue_time = box_result['freeDays']
        else:
            overdue_type = overdue_flag.split(':')[0]
            overdue_time = overdue_flag.split(':')[1]
        if overdue_type == 'HOUR':
            end_time = start_time + datetime.timedelta(hours=int(overdue_time))
        else:
            end_time = (start_time + datetime.timedelta(days=int(overdue_time))).replace(hour=23, minute=0, second=0)
        return int(time.mktime(time.strptime(end_time.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')) * 1000)
    except Exception as e:
        logger.debug(('get_overdue_timestamp Error', imported_express))
        logger.debug(('get_overdue_timestamp Error', e))

def random_validate(validate_type):
    while True:
        if validate_type == 'LETTER_AND_NUMBER_VALIDATE_CODE':
            chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'
            validate_code = ''
            i = 0
            while i < 6:
                validate_code += random.choice(chars)
                i += 1

            if len(ExpressDao.get_express_by_validate({'validateCode': validate_code, 'status': 'IN_STORE'})) == 0:
                return validate_code
        else:
            return ''

customer_store_number = ''
customer_store_express = ''

def start_get_customer_store_express_info(text):
    global customer_store_number
    customer_store_number = text
    ClientTools.get_global_pool().apply_async(get_customer_store_express_info)

def get_customer_store_express_info():
    global customer_store_express
    message, status_code = HttpClient.get_message('express/customerExpress/' + customer_store_number)
    if status_code == 200:
        if ClientTools.get_value("statusCode", message) == 404:
            express_signal_handler.customer_store_express_signal.emit('False')
        else:
            customer_store_express = message
            express_signal_handler.customer_store_express_signal.emit(str(json.dumps(customer_store_express)))
    else:
        express_signal_handler.customer_store_express_signal.emit('False')

customer_reject_number = ''
customer_reject_express = ''

def start_get_customer_reject_express_info(text):
    global customer_reject_number
    customer_reject_number = text
    ClientTools.get_global_pool().apply_async(get_customer_reject_express_info)

def get_customer_reject_express_info():
    global customer_reject_express
    message, status_code = HttpClient.get_message('express/rejectExpress/' + customer_reject_number)
    if status_code == 200:
        customer_reject_express = message
        express_signal_handler.customer_reject_express_signal.emit(str(json.dumps(customer_reject_express)))
    else:
        express_signal_handler.customer_reject_express_signal.emit('False')

customer_scanner_signal_connect_flag = False

def start_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        Scanner.scanner_signal_handler.barcode_result.connect(customer_scan_qr_code)
        customer_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()

def stop_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        return
    customer_scanner_signal_connect_flag = False
    Scanner.scanner_signal_handler.barcode_result.disconnect(customer_scan_qr_code)
    Scanner.start_stop_scanner()

def customer_scan_qr_code(text):
    global customer_scanner_signal_connect_flag
    global customer_store_number
    if text == '' or 'ERROR':
        customer_scanner_signal_connect_flag = False
        return
    express_signal_handler.barcode_signal.emit(text)
    customer_store_number = text
    ClientTools.get_global_pool().apply_async(get_customer_store_express_info)
    customer_scanner_signal_connect_flag = False

def start_load_courier_overdue_express_count():
    ClientTools.get_global_pool().apply_async(load_courier_overdue_express_count)

def load_courier_overdue_express_count():
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'COURIER_STORE'}
    count_list = ExpressDao.get_overdue_express_count_by_logistics_id(param)
    express_signal_handler.overdue_express_count_signal.emit(count_list[0]['count'])

def start_courier_load_overdue_express_list(page):
    ClientTools.get_global_pool().apply_async(courier_load_overdue_express, (page,))
    express_signal_handler.load_express_list_signal.emit('Success')

def courier_load_overdue_express(page):
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'COURIER_STORE',
             'startLine': (int(page) - 1) * 5}
    overdue_express_list_ = ExpressDao.get_overdue_express_by_logistics_id(param)
    for overdue_express_ in overdue_express_list_:
        overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)

    express_signal_handler.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))

def start_load_manager_overdue_express_count():
    ClientTools.get_global_pool().apply_async(load_manager_overdue_express_count)

def load_manager_overdue_express_count():
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'expressType': 'COURIER_STORE'}
    count_list = ExpressDao.get_overdue_express_count_by_manager(param)
    express_signal_handler.overdue_express_count_signal.emit(count_list[0]['count'])

def start_manager_load_overdue_express_list(page):
    ClientTools.get_global_pool().apply_async(manager_load_overdue_express, (page,))
    express_signal_handler.load_express_list_signal.emit('Success')

def manager_load_overdue_express(page):
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'expressType': 'COURIER_STORE',
             'startLine': (int(page) - 1) * 5}
    overdue_express_list_ = ExpressDao.get_overdue_express_by_manager(param)
    for overdue_express_ in overdue_express_list_:
        overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)

    express_signal_handler.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))

def start_staff_take_all_overdue_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_overdue_express)

def staff_take_all_overdue_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'overdueTime': ClientTools.now(),
                   'expressType': 'COURIER_STORE',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_overdue_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        __param = {'status': 'IN_STORE',
                    'overdueTime': ClientTools.now(),
                    'expressType': 'COURIER_STORE'}
        __express_list = ExpressDao.get_all_overdue_express_by_manager(__param)
    logger.info('overdue_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        express_signal_handler.staff_take_overdue_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_overdue_express(__user, __express)
        time.sleep(2)

    express_signal_handler.staff_take_overdue_express_signal.emit('Success')

def start_staff_take_overdue_express_list(express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_overdue_express_list, (express_id_list,))

def staff_take_overdue_express_list(express_id_list):
    logger.info(('express_id_list:', express_id_list))
    __express_list = json.loads(express_id_list)
    if len(__express_list) == 0:
        express_signal_handler.staff_take_overdue_express_signal.emit('None')
        return
    __user = UserService.get_user()
    for __express in __express_list:
        express_result_list = ExpressDao.get_express_by_id({'id': __express})
        express = express_result_list[0]
        staff_take_overdue_express(__user, express)
        time.sleep(2)

    express_signal_handler.staff_take_overdue_express_signal.emit('Success')

def staff_take_overdue_express(__user, express):
    express['takeTime'] = ClientTools.now()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        express['status'] = 'COURIER_TAKEN'
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        express['status'] = 'OPERATOR_TAKEN'
    express['staffTakenUser_id'] = __user['id']
    express['version'] += 1
    express['syncFlag'] = 0
    ExpressDao.take_express(express)
    box.service.BoxService.free_mouth({'id': express['mouth_id']})
    box.service.BoxService.open_mouth(express['mouth_id'])
    express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
    ClientTools.get_global_pool().apply_async(sync_staff_take_overdue_express, (express,))

def sync_staff_take_overdue_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeOverdueExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)

def get_scan_not_sync_express_list():
    return ExpressDao.get_not_sync_express_list({'syncFlag': 0})

def mark_sync_success(express_):
    ExpressDao.mark_sync_success(express_)

calculate_customer_express_cost_flag = False

def start_calculate_customer_express_cost():
    global calculate_customer_express_cost_flag
    if not calculate_customer_express_cost_flag:
        calculate_customer_express_cost_flag = True
        WeighingMachine.weighing_machine_signal_handler.weight_signal.connect(calculate_customer_express_cost)
        WeighingMachine.start_get_weight()

def stop_calculate_customer_express_cost():
    global calculate_customer_express_cost_flag
    if calculate_customer_express_cost_flag:
        calculate_customer_express_cost_flag = False

customer_express_weight = 0
customer_express_cost = 0

def calculate_customer_express_cost(weight):
    global customer_express_weight
    global customer_express_cost
    if not calculate_customer_express_cost_flag:
        return
    customer_express_weight = weight
    range_price_ = customer_store_express
    cost = 0
    cost += range_price_['firstPrice']
    continued_weight = weight - range_price_['firstHeavy']
    if continued_weight < 0:
        continued_weight = 0
    cost += math.ceil(continued_weight / range_price_['continuedHeavy']) * range_price_['continuedPrice']
    cost_result_ = {'heavy': weight, 'cost': cost}
    customer_express_cost = cost
    express_signal_handler.customer_store_express_cost_signal.emit(json.dumps(cost_result_))

pay_cash_for_customer_express_flag = False

def start_pay_cash_for_customer_express():
    global pay_cash_for_customer_express_flag
    if not pay_cash_for_customer_express_flag:
        pay_cash_for_customer_express_flag = True
        pre_pay_cash_for_customer_express()

def stop_pay_cash_for_customer_express():
    global pay_cash_for_customer_express_flag
    if pay_cash_for_customer_express_flag:
        CoinMachine.coin_machine_signal_handler.coin_result.disconnect(pay_cash_for_customer_express)
        pay_cash_for_customer_express_flag = False

def start_pull_pre_pay_cash_for_customer_express(express_cost):
    global customer_express_cost
    customer_express_cost = express_cost

def pre_pay_cash_for_customer_express():
    global customer_paid_amount
    record_list = ExpressDao.get_record({'express_id': customer_store_express['id']})
    CoinMachine.coin_machine_signal_handler.coin_result.connect(pay_cash_for_customer_express)
    if len(record_list) == 0:
        record_param = {'id': ClientTools.get_uuid(),
                        'paymentType': 'CASH',
                        'transactionType': 'PAYMENT_FOR_SEND_EXPRESS',
                        'amount': 0,
                        'express_id': customer_store_express['id'],
                        'createTime': ClientTools.now()}
        ExpressDao.save_record(record_param)
        customer_paid_amount = 0
    else:
        record = record_list[0]
        customer_paid_amount = math.ceil(record['amount'] * -1)
        express_signal_handler.paid_amount_signal.emit(str(math.ceil(customer_paid_amount / 100)))
        if customer_paid_amount == customer_express_cost:
            return
        CoinMachine.start_get_coin()

customer_paid_amount = 0

def pay_cash_for_customer_express(text):
    global customer_paid_amount
    if text == 'False':
        CoinMachine.start_get_coin()
        return
    customer_paid_amount += 100
    record = {'express_id': customer_store_express['id'],
              'amount': customer_paid_amount * -1}
    ExpressDao.update_recode(record)
    express_signal_handler.paid_amount_signal.emit(str(math.ceil(customer_paid_amount / 100)))
    if customer_paid_amount < customer_express_cost:
        CoinMachine.start_get_coin()
    else:
        stop_pay_cash_for_customer_express()

calculate_customer_reject_express_cost_flag = False

def start_calculate_customer_reject_express_cost():
    global calculate_customer_reject_express_cost_flag
    if not calculate_customer_reject_express_cost_flag:
        calculate_customer_reject_express_cost_flag = True
        WeighingMachine.weighing_machine_signal_handler.weight_signal.connect(calculate_customer_reject_express_cost)
        WeighingMachine.start_get_weight()

def stop_calculate_customer_reject_express_cost():
    global calculate_customer_reject_express_cost_flag
    if calculate_customer_reject_express_cost_flag:
        calculate_customer_reject_express_cost_flag = False

customer_reject_express_weight = 0
customer_reject_express_cost = 0

def calculate_customer_reject_express_cost(weight):
    global customer_reject_express_cost
    global customer_reject_express_weight
    if not calculate_customer_reject_express_cost_flag:
        return
    customer_reject_express_weight = weight
    range_price_ = customer_reject_express
    cost = 0
    cost += range_price_['firstPrice']
    continued_weight = weight - range_price_['firstHeavy']
    if continued_weight < 0:
        continued_weight = 0
    cost += math.ceil(continued_weight / range_price_['continuedHeavy']) * range_price_['continuedPrice']
    cost_result_ = {'heavy': weight, 'cost': cost}
    customer_reject_express_cost = cost
    express_signal_handler.customer_store_express_cost_signal.emit(json.dumps(cost_result_))

pay_cash_for_customer_reject_express_flag = False

def start_pay_cash_for_customer_reject_express():
    global pay_cash_for_customer_reject_express_flag
    if not pay_cash_for_customer_reject_express_flag:
        pay_cash_for_customer_reject_express_flag = True
        pre_pay_cash_for_customer_reject_express()

def stop_pay_cash_for_customer_reject_express():
    global pay_cash_for_customer_reject_express_flag
    if pay_cash_for_customer_reject_express_flag:
        CoinMachine.coin_machine_signal_handler.coin_result.disconnect(pay_cash_for_customer_reject_express)
        pay_cash_for_customer_reject_express_flag = False

def start_pull_pre_pay_cash_for_customer_reject_express(express_cost):
    global customer_reject_express_cost
    customer_reject_express_cost = express_cost

def pre_pay_cash_for_customer_reject_express():
    global customer_paid_amount
    record_list = ExpressDao.get_record({'express_id': customer_reject_express['id']})
    CoinMachine.coin_machine_signal_handler.coin_result.connect(pay_cash_for_customer_reject_express)
    if len(record_list) == 0:
        record_param = {'id': ClientTools.get_uuid(),
                        'paymentType': 'CASH',
                        'transactionType': 'PAYMENT_FOR_SEND_EXPRESS',
                        'amount': 0,
                        'express_id': customer_reject_express['id'],
                        'createTime': ClientTools.now()}
        ExpressDao.save_record(record_param)
        customer_paid_amount = 0
    else:
        record = record_list[0]
        customer_paid_amount = math.ceil(record['amount'] * -1)
        express_signal_handler.paid_amount_signal.emit(str(math.ceil(customer_paid_amount / 100)))
        if customer_paid_amount == customer_reject_express_cost:
            return
        CoinMachine.start_get_coin()

def pay_cash_for_customer_reject_express(text):
    global customer_paid_amount
    if text == 'False':
        CoinMachine.start_get_coin()
        return
    customer_paid_amount += 100
    record = {'express_id': customer_reject_express['id'],
              'amount': customer_paid_amount * -1}
    ExpressDao.update_recode(record)
    express_signal_handler.paid_amount_signal.emit(str(math.ceil(customer_paid_amount / 100)))
    if customer_paid_amount < customer_reject_express_cost:
        CoinMachine.start_get_coin()
    else:
        stop_calculate_customer_reject_express_cost()

store_customer_express_flag = False

def start_store_customer_express():
    global store_customer_express_flag
    if not store_customer_express_flag:
        store_customer_express_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_express)

def store_customer_express():
    global store_customer_express_flag
    box_info = box.service.BoxService.get_box()
    customer_store_express['box_id'] = box_info['id']
    customer_store_express['logisticsCompany_id'] = customer_store_express['logisticsCompany']['id']
    mouth_result = box.service.BoxService.mouth
    mouth_param = {'id': mouth_result['id'],
                   'express_id': customer_store_express['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    customer_store_express['mouth'] = mouth_result
    customer_store_express['mouth_id'] = mouth_result['id']
    customer_store_express['operator_id'] = box_info['operator_id']
    customer_store_express['storeUser_id'] = "POPSEND"
    customer_store_express['weight'] = customer_express_weight
    customer_store_express['storeTime'] = ClientTools.now()
    customer_store_express['chargeType'] = customer_store_express['chargeType']
    ExpressDao.save_customer_express(customer_store_express)
    express_signal_handler.store_customer_express_result_signal.emit('Success')
    record_list = ExpressDao.get_record({'express_id': customer_store_express['id']})
    if len(record_list) == 1:
        record = record_list[0]
        record_param = {'id': record['id'],
                        'createTime': record['createTime'],
                        'amount': record['amount']}
        customer_store_express['transactionRecords'] = [record_param]
    customer_store_express['box'] = {'id': customer_store_express['box_id']}
    logger.debug("customer_store_express : " + str(customer_store_express))
    message, status_code = HttpClient.post_message('express/customerStoreExpress', customer_store_express)
    if status_code == 200 and message['id'] == customer_store_express['id']:
        ExpressDao.mark_sync_success(customer_store_express)
    store_customer_express_flag = False

store_customer_reject_express_flag = False

def start_store_customer_reject_express():
    global store_customer_reject_express_flag
    if not store_customer_reject_express_flag:
        store_customer_reject_express_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_reject_express)

def store_customer_reject_express():
    global store_customer_reject_express_flag
    customer_reject_express['box_id'] = box.service.BoxService.get_box()['id']
    customer_reject_express['logisticsCompany_id'] = customer_reject_express['logisticsCompany']['id']
    mouth_result = box.service.BoxService.mouth
    mouth_param = {'id': mouth_result['id'],
                   'express_id': customer_reject_express['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    customer_reject_express['mouth'] = mouth_result
    customer_reject_express['mouth_id'] = mouth_result['id']
    customer_reject_express['operator_id'] = box.service.BoxService.get_box()['operator_id']
    customer_reject_express['storeUser_id'] = ClientTools.get_value('id', ClientTools.get_value('storeUser',
                                                                                                customer_reject_express, {}))
    customer_reject_express['weight'] = customer_reject_express_weight
    customer_reject_express['endAddress'] = ClientTools.get_value('endAddress', customer_reject_express, None)
    customer_reject_express['startAddress'] = ClientTools.get_value('startAddress', customer_reject_express, None)
    customer_reject_express['recipientName'] = ClientTools.get_value('recipientName', customer_reject_express, None)
    customer_reject_express['recipientUserPhoneNumber'] = ClientTools.get_value('recipientUserPhoneNumber',
                                                                                customer_reject_express, None)
    customer_reject_express['storeTime'] = ClientTools.now()
    customer_reject_express['chargeType'] = customer_reject_express['chargeType']
    ExpressDao.save_customer_express(customer_reject_express)
    express_signal_handler.store_customer_express_result_signal.emit('Success')
    record_list = ExpressDao.get_record({'express_id': customer_reject_express['id']})
    if len(record_list) == 1:
        record = record_list[0]
        record_param = {'id': record['id'],
                        'createTime': record['createTime'],
                        'amount': record['amount']}
        customer_reject_express['transactionRecords'] = [record_param]
    customer_reject_express['box'] = {'id': customer_reject_express['box_id']}
    message, status_code = HttpClient.post_message('express/customerRejectExpress', customer_reject_express)
    if status_code == 200 and message['id'] == customer_reject_express['id']:
        ExpressDao.mark_sync_success(customer_reject_express)
    store_customer_reject_express_flag = False

def start_get_customer_express_cost():
    ClientTools.get_global_pool().apply_async(get_customer_express_cost)

def get_customer_express_cost():
    express_signal_handler.customer_express_cost_insert_coin_signal.emit(str(math.ceil(customer_express_cost / 100)))

def start_get_customer_reject_express_cost():
    ClientTools.get_global_pool().apply_async(get_customer_reject_express_cost)

def get_customer_reject_express_cost():
    express_signal_handler.customer_express_cost_insert_coin_signal.emit(str(math.ceil(customer_reject_express_cost / 100)))

def get_scan_sync_express_transaction_record(param):
    return ExpressDao.get_record(param)

def start_load_customer_send_express_count():
    ClientTools.get_global_pool().apply_async(load_customer_send_express_count)

def load_customer_send_express_count():
    __user = UserService.get_user()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'logisticsCompany_id': __user['company']['id']}
        count_list = ExpressDao.get_send_express_count_by_logistics_id(param)
        express_signal_handler.send_express_count_signal.emit(count_list[0]['count'])
    elif __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE'}
        count_list = ExpressDao.get_send_express_count_by_operator(param)
        express_signal_handler.send_express_count_signal.emit(count_list[0]['count'])

def start_load_customer_reject_express_count():
    ClientTools.get_global_pool().apply_async(load_customer_reject_express_count)

def load_customer_reject_express_count():
    __user = UserService.get_user()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'logisticsCompany_id': __user['company']['id']}
        count_list = ExpressDao.get_send_express_count_by_logistics_id(param)
        express_signal_handler.send_express_count_signal.emit(count_list[0]['count'])
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT'}
        count_list = ExpressDao.get_send_express_count_by_operator(param)
        express_signal_handler.send_express_count_signal.emit(count_list[0]['count'])

def get_send_amount(express__):
    return ExpressDao.get_mouth(express__)

def start_customer_load_send_express_list(page):
    ClientTools.get_global_pool().apply_async(customer_load_send_express, (page,))
    express_signal_handler.load_express_list_signal.emit('Success')

def customer_load_send_express(page):
    __user = UserService.get_user()
    logger.debug('customer_load_send_express __user is : ', __user)
    send_express_list_ = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'logisticsCompany_id': __user['company']['id'],
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_logistics_id(param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_operator(param)
    for send_express_ in send_express_list_:
        send_express_['mouth'] = box.service.BoxService.get_mouth(send_express_)
        send_express_['amount'] = get_send_amount(send_express_)

    express_signal_handler.send_express_list_signal.emit(json.dumps(send_express_list_))

def start_customer_load_reject_express_list(page):
    ClientTools.get_global_pool().apply_async(customer_load_reject_express, (page,))
    express_signal_handler.load_express_list_signal.emit('Success')

def customer_load_reject_express(page):
    __user = UserService.get_user()
    logger.debug('customer_load_reject_express __user is : ', __user)
    send_express_list_ = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'logisticsCompany_id': __user['company']['id'],
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_logistics_id(param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_operator(param)
    for send_express_ in send_express_list_:
        send_express_['mouth'] = box.service.BoxService.get_mouth(send_express_)
        send_express_['amount'] = get_send_amount(send_express_)
        send_express_['electronicCommerce'] = CompanyDao.get_company_by_id({'id': send_express_['electronicCommerce_id']})[0]

    express_signal_handler.reject_express_list_signal.emit(json.dumps(send_express_list_))

def start_staff_take_all_send_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_send_express)

def staff_take_all_send_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_STORE',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_send_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_STORE',
                   'overdueTime': ClientTools.now()}
        __express_list = ExpressDao.get_all_send_express_by_manager(__param)
    logger.info('send_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        express_signal_handler.staff_take_send_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_send_express(__user, __express)

    express_signal_handler.staff_take_send_express_signal.emit('Success')

def start_staff_take_send_express_list(send_express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_send_express_list, (send_express_id_list,))

def staff_take_send_express_list(send_express_id_list):
    logger.info(('express_id_list:', send_express_id_list))
    __express_list = json.loads(send_express_id_list)
    if len(__express_list) == 0:
        express_signal_handler.staff_take_send_express_signal.emit('None')
        return
    __user = UserService.get_user()
    for __express in __express_list:
        express_result_list = ExpressDao.get_express_by_id({'id': __express})
        express = express_result_list[0]
        staff_take_send_express(__user, express)

    express_signal_handler.staff_take_send_express_signal.emit('Success')

def staff_take_send_express(__user, express):
    express['takeTime'] = ClientTools.now()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        express['status'] = 'COURIER_TAKEN'
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        express['status'] = 'OPERATOR_TAKEN'
    express['staffTakenUser_id'] = __user['id']
    express['version'] += 1
    express['syncFlag'] = 0
    ExpressDao.take_express(express)
    box.service.BoxService.free_mouth({'id': express['mouth_id']})
    box.service.BoxService.open_mouth(express['mouth_id'])
    express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
    ClientTools.get_global_pool().apply_async(sync_staff_take_send_express, (express,))

def sync_staff_take_send_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeUserSendExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)

def start_staff_take_all_reject_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_reject_express)

def staff_take_all_reject_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_REJECT',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_send_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_REJECT',
                   'overdueTime': ClientTools.now()}
        __express_list = ExpressDao.get_all_send_express_by_manager(__param)
    logger.info('reject_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        express_signal_handler.staff_take_send_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_reject_express(__user, __express)

    express_signal_handler.staff_take_send_express_signal.emit('Success')

def start_staff_take_reject_express_list(reject_express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_reject_express_list, (reject_express_id_list,))

def staff_take_reject_express_list(reject_express_id_list):
    logger.info(('express_id_list:', reject_express_id_list))
    __express_list = json.loads(reject_express_id_list)
    if len(__express_list) == 0:
        express_signal_handler.staff_take_send_express_signal.emit('None')
        return
    __user = UserService.get_user()
    for __express in __express_list:
        express_result_list = ExpressDao.get_express_by_id({'id': __express})
        express = express_result_list[0]
        staff_take_reject_express(__user, express)

    express_signal_handler.staff_take_send_express_signal.emit('Success')

def staff_take_reject_express(__user, express):
    express['takeTime'] = ClientTools.now()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        express['status'] = 'COURIER_TAKEN'
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        express['status'] = 'OPERATOR_TAKEN'
    express['staffTakenUser_id'] = __user['id']
    express['version'] += 1
    express['syncFlag'] = 0
    ExpressDao.take_express(express)
    box.service.BoxService.free_mouth({'id': express['mouth_id']})
    box.service.BoxService.open_mouth(express['mouth_id'])
    express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
    ClientTools.get_global_pool().apply_async(sync_staff_take_reject_express, (express,))

def sync_staff_take_reject_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeUserRejectExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)

def start_service_pull_store_express(express_massage, timestamp=0):
    logger.info(('Server_pull timestamp is ', timestamp))
    if ClientTools.now() - timestamp >= 30000:
        return
    service_pull_store_express(express_massage)

def service_pull_store_express(express_massage):
    logger.info(('service_pull_store_express_info:', express_massage))
    param = {'id': express_massage['mouth']['box']['id'],
             'deleteFlag': 0}
    pull_box_info = BoxDao.get_box_by_box_id(param)
    if len(pull_box_info) != 1:
        #logger.debug('bad box_id')
        return False
    check_free_mouth_param = {'id': express_massage['mouth']['id'],
                              'status': 'ENABLE'}
    #logger.debug('start BoxDao')
    pull_mouth_result = BoxDao.get_free_mouth_by_id(check_free_mouth_param)
    if len(pull_mouth_result) != 1:
        #logger.debug('no free mouth')
        return False
    box.service.BoxService.pull_open_mouth(express_massage['mouth']['id'])
    #logger.debug('opened')
    express_param = {'id': express_massage['id'],
                     'expressNumber': express_massage['expressNumber'],
                     'expressType': express_massage['expressType'],
                     'overdueTime': express_massage['overdueTime'],
                     'status': express_massage['status'],
                     'storeTime': express_massage['storeTime'],
                     'syncFlag': 1, 'takeUserPhoneNumber': express_massage['takeUserPhoneNumber'],
                     'validateCode': express_massage['validateCode'],
                     'version': 0, 'box_id': express_massage['mouth']['box']['id'],
                     'logisticsCompany_id': express_massage['logisticsCompany']['id'],
                     'mouth_id': express_massage['mouth']['id'],
                     'operator_id': pull_box_info[0]['operator_id'],
                     'storeUser_id': express_massage['storeUser']['id'],
                     'groupName': ClientTools.get_value('groupName', express_massage)}
    logger.debug('express_param is :', express_param)
    ExpressDao.save_express(express_param)
    #logger.debug('save express end')
    mouth_param = {'id': express_massage['mouth']['id'],
                   'express_id': express_massage['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    #logger.debug('use_mouth end')
    return True

def start_reset_express(express_massage):
    ClientTools.get_global_pool().apply_async(reset_express, (express_massage,))

def reset_express(express_massage):
    #print(str(express_massage))
    logger.info(('reset_express_info:', express_massage))
    param = {'id': express_massage['box']['id'],
             'deleteFlag': 0}
    pull_box_info = BoxDao.get_box_by_box_id(param)
    if len(pull_box_info) != 1:
        reset_express_post_message(express_massage['id'], 'ERROR BOX_ID', 'ERROR')
        return
    express_info_list = ExpressDao.get_express_by_id({'id': express_massage['express']['id']})
    if len(express_info_list) != 1:
        reset_express_post_message(express_massage['id'], 'NO SUCH EXPRESS', 'ERROR')
        return
    express_info = express_info_list[0]
    if express_massage['express']['overdueTime'] <= express_massage['express']['storeTime']:
        reset_express_post_message(express_massage['id'], 'OVERDUE TIME ERROR', 'ERROR')
        return
    express_param = {'id': express_massage['express']['id'],
                     'overdueTime': express_massage['express']['overdueTime'],
                     'status': express_massage['express']['status'],
                     'syncFlag': 1,
                     'takeUserPhoneNumber': express_massage['express']['takeUserPhoneNumber'],
                     'validateCode': express_massage['express']['validateCode'],
                     'version': express_info['version'] + 1}
    if express_info['status'] == 'IN_STORE':
        #logger.debug('the express in box')
        express_param['lastModifiedTime'] = ClientTools.now()
        ExpressDao.reset_express(express_param)
        reset_express_post_message(express_massage['id'], 'IN_STORE EXPRESS RESET DONE', 'SUCCESS')
        return
    if express_info['status'] == 'CUSTOMER_TAKEN':
        #logger.debug('the express taken by customer')
        mouth_info_list = BoxDao.get_mouth_by_id({'id': express_massage['express']['mouth']['id']})
        if len(mouth_info_list) != 1:
            reset_express_post_message(express_massage['id'], 'MOUTH_ID ERROR', 'ERROR')
            return
        mouth_info = mouth_info_list[0]
        if mouth_info['status'] != 'ENABLE':
            reset_express_post_message(express_massage['id'], 'MOUTH_STATUS ERROR', 'ERROR')
            return
        if mouth_info['status'] == 'ENABLE':
            mouth_param = {'status': 'USED',
                           'id': mouth_info['id'],
                           'express_id': express_massage['express']['id']}
            BoxDao.use_mouth(mouth_param)
            express_param['lastModifiedTime'] = ClientTools.now()
            ExpressDao.reset_express(express_param)
            reset_express_post_message(express_massage['id'], 'CUSTOMER_TAKEN EXPRESS DONE', 'SUCCESS')
            return
    if express_info['status'] == 'OPERATOR_TAKEN' or express_info['status'] == 'COURIER_TAKEN':
        #logger.debug('the express taken by staff')
        mouth_info_list = BoxDao.get_mouth_by_id({'id': express_massage['express']['mouth']['id']})
        if len(mouth_info_list) != 1:
            reset_express_post_message(express_massage['id'], 'MOUTH_ID ERROR', 'ERROR')
            return
        mouth_info = mouth_info_list[0]
        if mouth_info['status'] != 'ENABLE':
            reset_express_post_message(express_massage['id'], 'MOUTH_STATUS ERROR', 'ERROR')
            return
        if mouth_info['status'] == 'ENABLE':
            mouth_param = {'status': 'USED',
                           'id': mouth_info['id'],
                           'express_id': express_massage['express']['id']}
            BoxDao.use_mouth(mouth_param)
            express_param['lastModifiedTime'] = ClientTools.now()
            ExpressDao.reset_express(express_param)
            reset_express_post_message(express_massage['id'], 'STAFF_TAKEN EXPRESS DONE', 'SUCCESS')
            return
    reset_express_post_message(express_massage['id'], 'EXPRESS STATUS ERROR', 'ERROR')

def reset_express_post_message(task_id, result, reset_status):
    reset_express_result = {'id': task_id,
                            'result': result,
                            'statusType': reset_status}
    HttpClient.post_message('task/finish', reset_express_result)
    logger.debug("reset_express result : ", str(reset_express_result))

reject_merchant_name = ''

def start_customer_reject_select_merchant(merchant_name):
    global reject_merchant_name
    reject_merchant_name = merchant_name

electronic_commerce_reject_number = ''
electronic_reject_express = ''

def start_customer_reject_for_electronic_commerce(barcode):
    global electronic_commerce_reject_number
    electronic_commerce_reject_number = barcode
    ClientTools.get_global_pool().apply_async(customer_reject_for_electronic_commerce)

def customer_reject_for_electronic_commerce():
    global electronic_reject_express
    message, status_code = HttpClient.get_message('express/reject/checkRule/' +
                                                  electronic_commerce_reject_number + '?type=' + reject_merchant_name)
    if status_code == 200:
        if ClientTools.get_value("statusCode", message) == 404:
            express_signal_handler.customer_reject_express_signal.emit('False')
        else:
            electronic_reject_express = message
            electronic_reject_express['chargeType'] = 'NOT_CHARGE'
            electronic_reject_express['package_id'] = electronic_commerce_reject_number
            express_signal_handler.customer_reject_express_signal.emit(str(json.dumps(electronic_reject_express)))
    else:
        express_signal_handler.customer_reject_express_signal.emit('False')

def start_get_electronic_commerce_reject_express():
    ClientTools.get_global_pool().apply_async(get_electronic_commerce_reject_express)

def get_electronic_commerce_reject_express():
    global electronic_reject_express
    box_info = box.service.BoxService.get_box()
    electronic_reject_express['phone_number'] = phone_number
    electronic_reject_express['box_name'] = box_info['name']
    express_signal_handler.reject_express_signal.emit(str(json.dumps(electronic_reject_express)))

store_customer_reject_for_electronic_commerce_flag = False

def start_store_customer_reject_for_electronic_commerce():
    global store_customer_reject_for_electronic_commerce_flag
    if not store_customer_reject_for_electronic_commerce_flag:
        store_customer_reject_for_electronic_commerce_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_reject_for_ecommerce)

def store_customer_reject_for_ecommerce():
    global store_customer_reject_for_electronic_commerce_flag
    box_info = box.service.BoxService.get_box()
    reject_express = dict()
    if ClientTools.get_value('id', electronic_reject_express) is not None:
        reject_express['id'] = electronic_reject_express['id']
    else:
        reject_express['id'] = ClientTools.get_uuid()
    reject_express['box_id'] = box_info['id']
    reject_express['logisticsCompany_id'] = electronic_reject_express['logisticsCompany']['id']
    reject_express['groupName'] = ClientTools.get_value('groupName', electronic_reject_express,
                                                        reject_merchant_name)
    mouth_result = box.service.BoxService.mouth
    mouth_param = {'id': mouth_result['id'],
                   'express_id': reject_express['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    reject_express['mouth'] = mouth_result
    reject_express['mouth_id'] = mouth_result['id']
    reject_express['operator_id'] = box_info['operator_id']
    reject_express['storeUser_id'] = 'C_' + reject_express['id']
    reject_express['endAddress'] = reject_express['groupName'] + "_WAREHOUSE"
    reject_express['startAddress'] = 'PopBox @ ' + box_info['name']
    reject_express['recipientName'] = reject_express['groupName'] + "_CUSTOMER"
    reject_express['weight'] = 0
    if ClientTools.get_value('storeUserPhoneNumber', electronic_reject_express) is not None:
        reject_express['storeUserPhoneNumber'] = electronic_reject_express['storeUserPhoneNumber']
    else:
        reject_express['storeUserPhoneNumber'] = phone_number
    reject_express['storeTime'] = ClientTools.now()
    reject_express['chargeType'] = electronic_reject_express['chargeType']
    reject_express['customerStoreNumber'] = electronic_commerce_reject_number
    reject_express['expressType'] = 'CUSTOMER_REJECT'
    reject_express['electronicCommerce_id'] = electronic_reject_express['electronicCommerce']['id']
    ExpressDao.save_customer_reject_express(reject_express)
    company_list = CompanyDao.get_company_by_id({'id': reject_express['electronicCommerce_id']})
    electronic_commerce = {'id': electronic_reject_express['electronicCommerce']['id'],
                           'companyType': 'ELECTRONIC_COMMERCE',
                           'name': electronic_reject_express['electronicCommerce']['name'],
                           'deleteFlag': 0,
                           'parentCompany_id': ""}
    if len(company_list) == 0:
        CompanyDao.insert_company(electronic_commerce)
    else:
        CompanyDao.update_company(electronic_commerce)
    express_signal_handler.store_customer_express_result_signal.emit('Success')
    reject_express['box'] = {'id': reject_express['box_id']}
    reject_express['logisticsCompany'] = electronic_reject_express['logisticsCompany']
    reject_express['electronicCommerce'] = electronic_reject_express['electronicCommerce']
    logger.debug("reject_express : " + str(reject_express))
    message, status_code = HttpClient.post_message('express/rejectExpressNotImported', reject_express)
    if status_code == 200 and message['id'] == reject_express['id'] :
        ExpressDao.mark_sync_success(reject_express)
    store_customer_reject_for_electronic_commerce_flag = False

def start_get_product_file():
    ClientTools.get_global_pool().apply_async(get_product_file)

def get_product_file():
    box_info = box.service.BoxService.get_box()
    express_signal_handler.product_file_signal.emit(box_info['orderNo'])

other_service_scanner_signal_connect_flag = False

def start_get_express_info_by_barcode():
    global other_service_scanner_signal_connect_flag
    if not other_service_scanner_signal_connect_flag:
        Scanner.scanner_signal_handler.barcode_result.connect(get_express_info)
        other_service_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()

def stop_get_express_info_by_barcode():
    global other_service_scanner_signal_connect_flag
    if other_service_scanner_signal_connect_flag:
        Scanner.scanner_signal_handler.barcode_result.disconnect(get_express_info)
        other_service_scanner_signal_connect_flag = False
    Scanner.start_stop_scanner()

pakpobox_express_info = ''

def start_get_express_info(express_text):
    ClientTools.get_global_pool().apply_async(get_express_info, (express_text,))

def get_express_info(text):
    global pakpobox_order_number
    global pakpobox_express_info
    if text == '':
        return
    pakpobox_express_info = ''
    pakpobox_info = dict()
    pakpobox_info['token'] = Configurator.get_value('popbox', 'Token')
    start_url = Configurator.get_value('popbox', 'serveraddress')
    pakpobox_order_number = text
    pakpobox_info['order_number'] = text
    express_message, status_code = HttpClient.pakpobox_get_message(start_url, pakpobox_info)
    if status_code == 200:
        if express_message['response']['message'] == 'AVAILABLE':
            if len(express_message['data'][0]) != 0:
                logger.debug(('get_express_info express_message: ', express_message['data']))
                pakpobox_express_info = express_message['data'][0]
                QP3000S.price = express_message['data'][0]['order_amount']
                express_signal_handler.start_get_express_info_result_signal.emit('success')
            else:
                express_signal_handler.start_get_express_info_result_signal.emit('Error')
        elif express_message['response']['message'] == 'NOT AVAILABLE':
            express_signal_handler.start_get_express_info_result_signal.emit('Error')
        elif express_message['response']['message'] == 'PAID':
            if len(express_message['data'][0]) != 0:
                logger.debug(('get_express_info express_message: ', express_message['data']))
                pakpobox_express_info = express_message['data'][0]
                express_signal_handler.start_get_express_info_result_signal.emit('PAID')
            else:
                express_signal_handler.start_get_express_info_result_signal.emit('Error')
    else:
        express_signal_handler.start_get_express_info_result_signal.emit('Error')

def start_get_pakpobox_express_info():
    ClientTools.get_global_pool().apply_async(get_pakpobox_express_info)

def get_pakpobox_express_info():
    express_signal_handler.start_get_pakpobox_express_info_signal.emit(json.dumps(pakpobox_express_info))

def start_get_customer_info_by_card():
    ClientTools.get_global_pool().apply_async(get_customer_info_by_card)

def get_customer_info_by_card():
    global card_info
    logger.debug('start_get_customer_info_by_card')
    flag = QP3000S.init_serial(x=1)
    logger.debug(('start_get_customer_info_by_card flag: ', flag))
    if flag:
        card_info = QP3000S.balanceInfo()
        logger.debug(('start_get_customer_info_by_card customer_card_amount: ', card_info))
    else:
        card_info = '0'
    express_signal_handler.start_get_customer_info_by_card_signal.emit(int(card_info))

def start_payment_by_card():
    ClientTools.get_global_pool().apply_async(payment_by_card)

def payment_by_card():
    global time_stamp
    payment_result_info = dict()
    logger.debug('start_payment_by_card')
    flag = QP3000S.init_serial(x=1)
    logger.debug(('start_get_card_info flag: ', flag))
    if flag:
        payment_info_flag = QP3000S.purcDeb()
        logger.debug(('start_get_card_info : ', payment_info_flag))
        if payment_info_flag == '0000':
            payment_info = QP3000S.getReport()
            logger.debug(('start_get_card_info getReport result : ', payment_info))
            payment_result_info['last_balance'] = payment_info[32:40].lstrip('0')
            payment_result_info['card_no'] = payment_info[4:20]
            time_stamp = int(time.time())
            payment_result_info['show_date'] = time_stamp * 1000
            payment_result_info['terminal_id'] = Configurator.get_value('popbox', 'terminalID')
            box_info = box.service.BoxService.get_box()
            payment_result_info['locker'] = box_info['name']
            express_signal_handler.start_payment_by_card_signal.emit(json.dumps(payment_result_info))
        else:
            express_signal_handler.start_payment_by_card_signal.emit('ERROR')
    else:
        express_signal_handler.start_payment_by_card_signal.emit('ERROR')

def start_get_cod_status():
    ClientTools.get_global_pool().apply_async(get_cod_status)

def get_cod_status():
    cod_status = Configurator.get_value('COP', 'status')
    express_signal_handler.start_get_cod_status_signal.emit(cod_status)

def start_finish_payment_operate():
    ClientTools.get_global_pool().apply_async(finish_payment_operate)

def finish_payment_operate():
    pakpobox_finish_info = dict()
    box_result = box.service.BoxService.get_box()
    logger.debug('start_finish_payment_operate')
    flag = QP3000S.init_serial(x=1)
    logger.debug(('start_finish_payment_operate flag: ', flag))
    if flag:
        settlement = QP3000S.settlement()
        logger.debug(('start_finish_payment_operate settlement: ', settlement))
        pakpobox_finish_info['settle_code'] = settlement
        pakpobox_finish_info['order_number'] = pakpobox_order_number
        pakpobox_finish_info['token'] = Configurator.get_value('popbox', 'Token')
        pakpobox_finish_info['order_amount'] = int(pakpobox_express_info['order_amount'])
        time_array = time.localtime(time_stamp)
        other_style_time = time.strftime('%Y-%m-%d %H:%M:%S', time_array)
        pakpobox_finish_info['settle_timestamp'] = other_style_time
        pakpobox_finish_info['settle_place'] = box_result['name']
        logger.debug(('start_finish_payment_operate info: ', pakpobox_finish_info))
        start_retry_push_customer_info(pakpobox_finish_info)

def start_retry_push_customer_info(customer_info):
    ClientTools.get_global_pool().apply_async(retry_push_customer_info, (customer_info,))

def retry_push_customer_info(customer_info):
    end_url = Configurator.get_value('popbox', 'endserveraddress')
    while True:
        express_message, status_code = HttpClient.pakpobox_get_message(end_url, customer_info)
        if status_code == 200 and express_message['response']['message'] == 'SUCCESS':
            return
        time.sleep(2)

def start_deposit_express():
    ClientTools.get_global_pool().apply_async(store_deposit_express)

popdeposit_express = customer_store_express

def store_deposit_express():
    mouth_result = box.service.BoxService.mouth
    logger.debug(('mouth_result:', mouth_result))
    box_result = box.service.BoxService.get_box()
    if not box_result:
        express_signal_handler.store_express_signal.emit('Error')
        return
    overdue_time = get_overdue_timestamp(box_result)
    operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
    if not operator_result:
        express_signal_handler.store_express_signal.emit('Error')
        return
    express_param = {'expressNumber': popdeposit_express['customerStoreNumber'],
                     'expressType': 'COURIER_STORE',
                     'overdueTime': overdue_time,
                     'status': 'IN_STORE',
                     'storeTime': ClientTools.now(),
                     'syncFlag': 0,
                     'takeUserPhoneNumber': popdeposit_express['takeUserPhoneNumber'],
                     'validateCode': random_validate(box_result['validateType']),
                     'version': 1,
                     'box_id': box_result['id'],
                     'logisticsCompany_id': popdeposit_express['logisticsCompany']['id'],
                     'mouth_id': mouth_result['id'],
                     'operator_id': box_result['operator_id'],
                     'storeUser_id': '402880825dbcd4c3015de54d98c5518e',
                     'groupName': 'POPDEPOSIT',
                     'id': popdeposit_express['id']}
    ExpressDao.save_express(express_param)
    mouth_param = {'id': express_param['mouth_id'],
                   'express_id': express_param['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    express_param['box'] = {'id': express_param['box_id']}
    express_param.pop('box_id')
    express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
    express_param.pop('logisticsCompany_id')
    express_param['mouth'] = {'id': express_param['mouth_id']}
    express_param.pop('mouth_id')
    express_param['operator'] = {'id': express_param['operator_id']}
    express_param.pop('operator_id')
    express_param['storeUser'] = {'id': express_param['storeUser_id']}
    express_param.pop('storeUser_id')
    express_signal_handler.store_express_signal.emit('Success')
    logging.info('POPDEPOSIT express stored : ' + str(json.dumps(express_param)))
    message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
    if status_code == 200 and message['id'] == express_param['id']:
        ExpressDao.mark_sync_success(express_param)
