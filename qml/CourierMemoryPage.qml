import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:courier_memory_input
    width: 1024
    height: 768
    property var show_text:""
    property int timer_value: 60
    property var press: "0"
    property var show_text_count:0
    property var c_access
    property var pref_login_user:""

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log('courier_credential : ' + c_access + ', with prefix_user_account : ' + pref_login_user)
            if(courier_memory_input.show_text!=""){
                courier_memory_input.show_text=""
            }
            slot_handler.start_courier_scan_barcode()
            press = "0"
            show_text_count = 0
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            slot_handler.stop_courier_scan_barcode()
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }

        }
    }

Rectangle{
    id:main_page
    Component.onCompleted: {
        root.barcode_result.connect(handle_text)
        root.phone_number_result.connect(handle_phone_number)
        root.imported_express_result.connect(get_imported_express_result)
    }

    Component.onDestruction: {
        root.barcode_result.disconnect(handle_text)
        root.phone_number_result.disconnect(handle_phone_number)
        root.imported_express_result.disconnect(get_imported_express_result)
    }

    function handle_phone_number(text){
        console.log('handle_phone_no : ' + text)
        slot_handler.set_express_number(show_text)
        my_stack_view.push(courier_input_phone_sure_view, {show_text:text, pref_login_user:pref_login_user})
    }

    function handle_text(text){
        console.log("scanner barcode result : " + text)
        if(text.length > 24){
            main_page.enabled = false
            error_tips.open()
        }else{
            show_text = text
            touch_keyboard.count = text.length
        }
    }

    function get_imported_express_result(text){
        console.log('status_import : ' + text + ', courier_credential : ' + c_access)
        if(text == "no_imported" && c_access == "full"){
            slot_handler.set_express_number(show_text)
            my_stack_view.push(courier_input_phone_view, {pref_login_user:pref_login_user, expressNo: show_text})
        }else if(text == "no_imported" && c_access == "limited"){
            not_imported_notif.open()
        }else{
            slot_handler.set_express_number(show_text)
            my_stack_view.push(courier_input_phone_sure_view, {show_text:text, pref_login_user:pref_login_user})
        }
    }

    FullWidthReminderText {
        x: 0
        y: 125
        remind_text:qsTr("Please scan or enter a tracking number")
        remind_text_size:"35"
    }

   Rectangle{
   x:220
   y:190
    width:620
    height:72
    color:"transparent"

    Image{
        width:620
        height:72
        source:"img/courier11/input1.png"
    }

    Text{
        id:textin
        y:10
        x:20
        font.family:"Microsoft YaHei"
        text:show_text
        color:"#FFFFFF"
        font.pixelSize:40       
    }
}

    Image{
        x: 412
        y: 280
        width:199
        height:64
        source:"img/button/barcode.png"
    }

    FullKeyboard{
        id:touch_keyboard
        x:29
        y:360
        property var count:show_text_count
        property var validate_code:""

        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(show_validate_code)
            touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function on_function_button_clicked(str){
            if(str == "ok"){
                if(press != "0"){
                    return
                }
                press = "1"
                if(courier_memory_input.show_text == ""){
                    main_page.enabled = false
                    error_tips.open()
                }
                else{
                    count = 0
                    slot_handler.start_get_imported_express(show_text)
                }
            }
            if(str=="delete"){
                if(count>=24){
                    count=23
                }
            }
        }

        function show_validate_code(str){
            if (str == "" && count > 0){
                if(count>=24){
                    count=24
                }
                count--
                courier_memory_input.show_text=courier_memory_input.show_text.substring(0,count);
            }
            if (str != "" && count < 24){
                count++
            }
            if (count>=24){
                str=""
            }
            else{
                courier_memory_input.show_text += str
            }
            abc.counter = timer_value
            my_timer.restart()
        }
    }
}

    HideWindow{
        id:error_tips
        //visible: true

        Text {
            x: 112
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Oops...")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: text_error
            x: 120
            y:300
            width: 809
            height: 250
            text: qsTr("We detected the order number is not entered yet. Please retry and enter the correct order number.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:error_tips_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    courier_memory_input.show_text=""
                    show_text_count = 0
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    error_tips.close()
                }
            }
        }
    }

    HideWindow{
        id:not_imported_notif
        //visible: true

        Text {
            x: 112
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Oops...")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            x: 118
            y:300
            width: 811
            height: 250
            text: qsTr("Please ensure the order number and locker location is correct. Please contact our CS line at 021-29022537/38 for assistance.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:not_imported_notif_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    courier_memory_input.show_text=""
                    show_text_count = 0
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    not_imported_notif.close()
                }
            }
        }
    }
}
