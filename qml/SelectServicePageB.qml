import QtQuick 2.4
import QtQuick.Controls 1.2

Background{

    property int timer_value: 60
    property var press:"0"
    property var faqUrl
    property var groUrl
    property var emoney_reader: undefined

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_cod_status()
            take_button.show_source = "img/button/1.png"
            payment_emoney_button.show_source = "img/button/1.png"
            drop_button.show_source = "img/button/1.png"
            select_service_back_button.show_source = "img/button/7.png"
            return_button.show_source = "img/button/10.png"
            faq_button.show_source = "img/button/10.png"
            qrshop_button.show_source = "img/button/10.png"
            popsend_button.show_source = "img/button/10.png"
            laundry_button.show_source = "img/button/10.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            slot_handler.start_get_free_mouth_mun()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.free_mouth_result.connect(show_free_mouth_num)
        root.start_get_cod_status_result.connect(cod_result)
    }

    Component.onDestruction: {
        root.free_mouth_result.disconnect(show_free_mouth_num)
        root.start_get_cod_status_result.disconnect(cod_result)
    }

    function show_free_mouth_num(text){
        var obj = JSON.parse(text)
        for(var i in obj){
            if(i == "XL"){
                extra_big_num.text = obj[i]
            }
            if(i == "L"){
                big_num.text = obj[i]
            }
            if(i == "M"){
                mid_num.text = obj[i]
            }
            if(i == "S"){
                small_num.text = obj[i]
            }
            if(i == "MINI"){
                mini_num.text = obj[i]
            }
        }
    }

    function cod_result(result){
        console.log("cod_result is ", result)
        if(result==""){
            emoney_reader = "disabled"
        }else{
            emoney_reader = result
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    FullWidthReminderText{
        id:title_text
        y:148
        remind_text:qsTr("please select service option")
        remind_text_size:"45"
    }

    SelectLoginButton{
        x:725
        y:50
        width: 236
        height: 71

        MouseArea {
            width: 236
            height: 71
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(background_login_view,{"identity":"OPERATOR_USER"})
            }
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            onEntered:{
                select_service_back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                select_service_back_button.show_source = "img/button/7.png"
            }
        }
    }

    SelectServiceButton{
        id:take_button
        x:134
        y:222
        show_image:"img/button/B_01.jpg"
        show_source:"img/button/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(customer_take_express_view)
            }
            onEntered:{
                //take_button.show_source = "img/bottondown/button3.png"
                take_button.show_image = "img/bottondown/red-01.jpg"
            }
            onExited:{
                take_button.show_source = "img/button/1.png"
                take_button.show_image = "img/button/B_01.jpg"
            }
        }
    }

    SelectServiceButton{
        id:drop_button
        x:416
        y:222
        show_image:"img/button/B_02.jpg"
        show_source:"img/button/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(background_login_view,{"identity":"LOGISTICS_COMPANY_USER"})
            }
            onEntered:{
                drop_button.show_image = "img/bottondown/red-02.jpg"
                //drop_button.show_source = "img/bottondown/button3.png"
            }
            onExited:{
                drop_button.show_source = "img/button/1.png"
                drop_button.show_image = "img/button/B_02.jpg"
            }
        }
    }

    SelectServiceButtonSmall{
        id:return_button
        x:134
        y:500
        show_image:"img/button/new_return_b.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.bottomMargin: -16
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(reject_select_merchant_name_page)
            }
            onEntered:{
                return_button.show_image = "img/bottondown/red-05.jpg"
            }
            onExited:{
                return_button. show_image = "img/button/new_return_b.png"
            }
        }
    }

    SelectServiceButtonSmall{
        id:popsend_button
        x:322
        y:500
        show_image:"img/button/new_popsend_b_white.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.bottomMargin: -16
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(send_input_memory_page)
            }
            onEntered:{
                popsend_button.show_image = "img/button/new_popsend_b_red.png"
            }
            onExited:{
                popsend_button.show_image = "img/button/new_popsend_b_white.png"
            }
        }
    }

    SelectServiceButtonSmall{
        id:qrshop_button
        x:510
        y:500
        show_image:"img/button/qrshopping_b_white.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.bottomMargin: -17
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                my_stack_view.push(grocery_product_list_new, {emoney:emoney_reader})
                press = "1"
                my_timer.stop()
            }
            onEntered:{
                qrshop_button.show_image = "img/button/qrshopping_b_red.png"
            }
            onExited:{
                qrshop_button.show_image = "img/button/qrshopping_b_white.png"
            }
        }
    }

    SelectServiceButtonTiny{
            id:express_login_button
            x:699
            y:222
            height: 90
            show_image:"img/button/expressLogin_white.png"
            show_source:"img/button/1.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.push(express_login_view,{"identity":"LOGISTICS_COMPANY_USER"})
                }
                onEntered:{
                    express_login_button.show_image = "img/button/expressLogin_red.png"
                }
                onExited:{
                    express_login_button.show_image = "img/button/expressLogin_white.png"
                }
            }
        }

    SelectServiceButtonSmall{
        id:payment_emoney_button
        x:699
        y:315
        show_image:"img/button/B_payment_white.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.bottomMargin: -16
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                if(emoney_reader == "enabled" && press == "0"){
                    my_stack_view.push(emoney_service_page)
                }else{
                    my_stack_view.push(on_develop_view)
                }
                press = "1"
                my_timer.stop()
            }
            onEntered:{
                payment_emoney_button.show_image = "img/button/B_payment_red.png"
            }
            onExited:{
                payment_emoney_button.show_image = "img/button/B_payment_white.png"
            }
        }
    }

    SelectServiceButtonTiny{
            id:laundry_button
            x:699
            y:502
            show_image:"img/button/tiny_laundry_white.png"
            show_source:"img/button/10.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    my_timer.stop()
                    my_stack_view.push(laundry_info_page)
                }
                onEntered:{
                    laundry_button.show_image = "img/button/tiny_laundry_red.png"
                }
                onExited:{
                    laundry_button.show_image = "img/button/tiny_laundry_white.png"
                }
            }
    }

    SelectServiceButtonTiny{
        id:faq_button
        x:699
        y:595
        show_image:"img/button/new_faq_white.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(faq_page_ina, {initurl:faqUrl})
            }
            onEntered:{
                faq_button.show_image = "img/button/new_faq_red.png"
            }
            onExited:{
                faq_button.show_image = "img/button/new_faq_white.png"
            }
        }
    }

    Row {
        x: 240
        y: 700
        spacing: 20

        Text {
            id: mouth_title
            text: qsTr("Available Lockers: ")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 24
        }

        Row {
            spacing: 5
            Text {
                id: mini_type
                text: qsTr("XS :")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: mini_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: small_type
                text: qsTr("S :")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: small_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: mid_type
                text: qsTr("M:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: mid_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: big_type
                text: qsTr("L:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: big_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: extra_big_type
                text: qsTr("XL:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: extra_big_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }
    }

}
