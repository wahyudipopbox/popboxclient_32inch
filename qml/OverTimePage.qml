import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:over_time

    property var payment:"0.00"
    property var press: "0"
    property int timer_value: 60

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            over_time_button.show_source = "img/button/7.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = 30
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component.onCompleted: {
        root.overdue_cost_result.connect(process_result)
        slot_handler.customer_get_overdue_cost()
    }

    Component.onDestruction: {
        root.overdue_cost_result.disconnect(process_result)
    }

    function process_result(cost){
        over_time.payment = cost
    }

    FullWidthReminderText{
        x: 0
        y:459
        remind_text:qsTr("Your parcel has expired and returned to the sender.")
        remind_text_size:"35"
    }

    FullWidthReminderText{
        x: 0
        y:524
        remind_text:qsTr("Please contact (021) 2902 2537 for further inquiry.")
        remind_text_size:"30"
    }

    Image {
        id: img_parcel_return
        x: 374
        y: 146
        width: 275
        height: 275
        source: "img/otherImages/parcel_returned.png"
    }

    OverTimeButton{
        id:over_time_button
        x:372
        y:616
        show_text:qsTr("Back")
        show_x:0
        show_image:"img/05/back_red.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                over_time_button.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                over_time_button.show_source = "img/button/7.png"
            }
        }
    }
}
