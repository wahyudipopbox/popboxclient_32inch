import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: background1
    property var press: "0"
    property int timer_value: 60
    property var store_type:"store"
    property variant containerqml: null

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            help_button.show_image = "img/door/1.png"
            ok_button.show_image = "img/door/1.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            ok_button.enabled = true
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }


    DoorEvey{
        y:251
        x:263
    }

    DoorButton{
        id:help_button
        y:640
        x:232
        show_text:qsTr("Help")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(user_select_view,{store_type:"store",containerqml:background1})
            }
            onEntered:{
                help_button.show_image = "img/door/2.png"
            }
            onExited:{
                help_button.show_image = "img/door/1.png"
            }
        }
    }


    DoorButton{
        id:ok_button
        y:640
        x:513
        show_text:qsTr("Complete and exit")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                slot_handler.start_store_express()
                my_stack_view.push(r_drop_off_thanks_page)
            }
            onEntered:{
                ok_button.show_image = "img/door/2.png"
            }
            onExited:{
                ok_button.show_image = "img/door/1.png"
            }
        }
    }


    Row {
        x: 270
        y: 140
        layoutDirection: Qt.LeftToRight

        Text {
            id: box_open_tips_1
            color: "#FFFFFF"
            text: qsTr("Your locker is located at Box No ")
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }

        Text {
            id:mouth_number
            color: "#FFFFFF"
            text: qsTr("X")
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }
    }


    Column {
        x: 315
        y: 175
        spacing: 5

        Row {
            spacing: 10

            Text {
                color: "#FFFFFF"
                text: qsTr("The system will refresh in")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }

      Rectangle{
                width:39
                height:11
                y:10
                color:"transparent"
                QtObject{
                    id:abc
                    property int counter
                    Component.onCompleted:{
                        abc.counter = timer_value
                    }
                }

                Text{
                    id:countShow_test
                    x:183
                    y:500
                    anchors.centerIn:parent
                    color:"#FFFFFF"
                    font.family:"Microsoft YaHei"
                    font.pixelSize:24
                }


                Timer{
                    id:my_timer
                    interval:1000
                    repeat:true
                    running:true
                    triggeredOnStart:true
                    onTriggered:{
                        countShow_test.text = abc.counter
                        abc.counter -= 1
                        if(abc.counter <= 1){
                            ok_button.enabled = false
                        }
                        if(abc.counter < 0){
                            my_timer.stop()
                            slot_handler.start_store_express()
                            my_stack_view.push(r_drop_off_thanks_page)
                        }
                    }
                }
            }

            Text {
                color: "#FFFFFF"
                text: qsTr("sec")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }
        }
    }


    FullWidthReminderText {
        x: 0
        y: 200
        remind_text:qsTr("Please ensure compartment door closure after deposit")
        remind_text_size:"24"
    }

    Component.onCompleted: {
        root.mouth_number_result.connect(show_text)
        slot_handler.get_express_mouth_number()
    }

    Component.onDestruction: {
        root.mouth_number_result.disconnect(show_text)
    }

    function show_text(t){
        mouth_number.text = t
    }

    function clickedfunc(temp){
        containerqml.clickedfunc(temp)
    }

}
