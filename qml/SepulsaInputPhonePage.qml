import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:sepulsa_input_phone
    width: 1024
    height: 768
    property var show_text:""
    property int timer_value: 60
    property var press: "0"
    property var count: 0
    property var service: ""
    property var list_provider: '[{"init":"0814", "name":"Indosat"},{"init":"0815", "name":"Indosat"},
{"init":"0816", "name":"Indosat"},{"init":"0855", "name":"Indosat"},{"init":"0856", "name":"Indosat"},
{"init":"0857", "name":"Indosat"},{"init":"0858", "name":"Indosat"},{"init":"0811", "name":"Telkomsel"},
{"init":"0812", "name":"Telkomsel"},{"init":"0813", "name":"Telkomsel"},{"init":"0821", "name":"Telkomsel"},
{"init":"0822", "name":"Telkomsel"},{"init":"0823", "name":"Telkomsel"},{"init":"0851", "name":"Telkomsel"},
{"init":"0852", "name":"Telkomsel"},{"init":"0853", "name":"Telkomsel"},{"init":"0817", "name":"XL-Axiata"},
{"init":"0818", "name":"XL-Axiata"},{"init":"0819", "name":"XL-Axiata"},{"init":"0859", "name":"XL-Axiata"},
{"init":"0877", "name":"XL-Axiata"},{"init":"0878", "name":"XL-Axiata"},{"init":"0879", "name":"XL-Axiata"},
{"init":"0831", "name":"Axis"},{"init":"0832", "name":"Axis"},{"init":"0838", "name":"Axis"},
{"init":"9991", "name":"Bolt"},{"init":"9992", "name":"Bolt"},{"init":"9993", "name":"Bolt"},
{"init":"9994", "name":"Bolt"},{"init":"9995", "name":"Bolt"},{"init":"9988", "name":"Bolt"},
{"init":"0881", "name":"Smartfren"},{"init":"0882", "name":"Smartfren"},{"init":"0883", "name":"Smartfren"},
{"init":"0884", "name":"Smartfren"},{"init":"0885", "name":"Smartfren"},{"init":"0886", "name":"Smartfren"},
{"init":"0887", "name":"Smartfren"},{"init":"0888", "name":"Smartfren"},{"init":"0889", "name":"Smartfren"},
{"init":"0895", "name":"3-Three"},{"init":"0896", "name":"3-Three"},{"init":"0897", "name":"3-Three"},
{"init":"0898", "name":"3-Three"},{"init":"0899", "name":"3-Three"}]'

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            if(sepulsa_input_phone.show_text!=""){
                sepulsa_input_phone.show_text=""
            }
            count = 0
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            main_page.enabled = true
            error_tips.close()
            }            
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
    }

Rectangle{
    id:main_page

    Text {
        id: text_define_general
        x: 0
        y: 180
        width: 1024
        height: 60
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        text: qsTr("Please enter your phone number")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.PlainText
        font.pointSize:28
        visible: true
    }    

    Rectangle{
        x:137
        y:256
        width:750
        height:75
        color:"transparent"

        Image{
            width:750
            height:75
            source:"img/courier11/input1.png"
        }

        TextEdit{
            id:input_phone
            text:show_text
            y:10
            x:30
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize:35
        }
    }

    OkButton{
        id:select_service_ok_button
        x:699
        y:620
        show_text:qsTr("OK")
        color: "#3b8f23"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                if(sepulsa_input_phone.show_text != ""){
                    var cells = JSON.parse(list_provider)
                    for(var c in cells){
                        if(sepulsa_input_phone.show_text.substring(0,4) == cells[c].init){
                            count=0
                            my_stack_view.push(sepulsa_input_phone_sure_view,{phone_no:show_text, service:"Sepulsa", list_provider:list_provider})
                        }else{
                            main_page.enabled = false
                            error_tips.open()
                        }
                    }
                }
                else{
                    main_page.enabled = false
                    error_tips.open()
                }
            }
            onEntered:{
            }
            onExited:{
            }
        }
    }

    Rectangle{
        id: check_button
        x:-36
        y:350
        width: 125
        height:100
        color: "white"
        radius: 15

        Image{
            id: img_check
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source:"img/otherImages/check_stat-white.png"
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(sepulsa_check_input_view)
            }
            onEntered:{
                img_check.source = "img/otherImages/check_stat-red.png"
                check_button.color = "transparent"
            }
            onExited:{
                img_check.source = "img/otherImages/check_stat-white.png"
                check_button.color = "white"
            }
        }
    }

    NumKeyboard{
        id:touch_keyboard
        x:378
        y:350

        property var validate_code:""

        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(show_validate_code)
            touch_keyboard.function_button_clicked.connect(function_button_action)
        }

        function function_button_action(str){
            if (str == "BACK" || str == "BATAL"){
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 4) return true }))
            }
            if (str == "DELETE" || str == "HAPUS"){
                show_validate_code("")
            }
        }

        function show_validate_code(str){
            if (str == "" && count > 0){
                if(count>=15){
                    count=15
                }
                count--
                sepulsa_input_phone.show_text=sepulsa_input_phone.show_text.substring(0,count);
            }
            if(count==0){
                sepulsa_input_phone.show_text=""
            }
            if (str != "" && count < 15){
                count++
            }
            if (count>=15){
                str=""
            }
            else{
                sepulsa_input_phone.show_text += str
            }
            abc.counter = timer_value
            my_timer.restart()
        }
    }
}

    HideWindow{
        id:error_tips
        //visible: true

        Image {
            id: img_error_tips
            x: 413
            y: 366
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            x: 90
            y:246
            width: 854
            height: 100
            text: qsTr("Please enter the correct number")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:error_tips_back
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    sepulsa_input_phone.show_text = ""
                    count = 0
                    main_page.enabled = true
                    error_tips.close()
                }
                onEntered:{
                }
                onExited:{
                }
            }
        }
    }
}
