import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: parent
    width:88
    height:88
    radius: 22
    color: "silver"

    property var show_text:""

    Image{
        id:num_button
        width:88
        height:88
        source:""
    }

    Text{
        text:show_text
        color:"black"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent;
        font.bold: true
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            full_keyboard.letter_button_clicked(show_text)
        }
        onEntered:{
            parent.color = "white"
        }
        onExited:{
            parent.color = "silver"
        }
    }

}
