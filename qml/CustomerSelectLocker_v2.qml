import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQml.Models 2.1

Background{
    id: base_bground
    width: 1024
    height: 768
    logo_y: 25
    property int timer_value: 60
    property var press:"0"
    property var usageOf:"undefined"
    property var lockers_list:""
    property var province:"DKI Jakarta"
    property var city:"Jakarta Barat"
    property var datas:""
    property var total_amount:""

    Stack.onStatusChanged:{
        console.log("data_received : " + datas + ", with amount : " + total_amount)
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            loadingGif.open()
            locker_data_view.visible = false
            //slot_handler.get_locker_data(province,city)
            getLockersOffline(province, city)
            if(city=="Jakarta Barat"){
                jakbar_button.button_color = "darkred"
            }else{
                jakbar_button.button_color = "white"
            }
            if(lockers_list != ""){
                loadingGif.close()
                locker_data_view.visible = true
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            lockersListModel.clear()
        }
    }

    Component.onCompleted: {
        root.get_locker_data_result.connect(handle_locker_result)
    }

    Component.onDestruction: {
        root.get_locker_data_result.disconnect(handle_locker_result)
    }

    function insert_flg(a){
        var newstrA=""
        newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstrA
    }

    function handle_locker_result(result){
        //console.log("start_get_locker_data_result : " + result)
        if(result == "FAILED"){
            network_error.open()
            main_page.enabled = false
        }else{
            var list_locker = JSON.parse(result)
            var total_lockers = list_locker.total
            lockers_list = list_locker.data
            parse_data_locker()
            if(lockers_list != "" && total_lockers > 0){
                //notif_box_firstLoad.visible = false
                loadingGif.close()
                locker_data_view.visible = true
            }else{
                loadingGif.open()
                locker_data_view.visible = false
            }
        }
    }

    function parse_data_locker(){
        //Undefined the gridview model
        gridview.model = undefined
        var lockers = lockers_list
        if(!lockersListModel.count){
            for(var x in lockers) {
                var show_lock_name = lockers[x].name
                var show_lock_addr_1 = lockers[x].address
                var show_lock_addr_2 = lockers[x].address_2
                var show_lock_building = lockers[x].building_type
                var show_lock_operational = lockers[x].operational_hours
                //Appending Product lockers
                lockersListModel.append({"lock_name_": show_lock_name,
                                                 "lock_addr_1_":show_lock_addr_1,
                                                 "lock_addr_2_": show_lock_addr_2,
                                                 "lock_building_": show_lock_building,
                                                 "lock_operational_": show_lock_operational})
            }
        }else{
            return
        }
        //Re-define the gridview model
        gridview.model = lockersListModel
        console.log('Total Lockers in ' + city.toUpperCase() + ' : ' + lockersListModel.count);

        /*for(var i = 0; i < lockersListModel.count; ++i) {
            console.log(lockersListModel.get(i).prod_sku_ +" -> "+ lockersListModel.get(i).prod_name_);
        }*/
    }


    function parse_data_locker_v2(data_lockers){
        //Undefined the gridview model
        gridview.model = undefined
        var lockers = JSON.parse(data_lockers)
        if(!lockersListModel.count){
            for(var x in lockers) {
                var show_lock_name = lockers[x].name
                var show_lock_addr_1 = lockers[x].address
                var show_lock_addr_2 = lockers[x].address_2
                var show_lock_building = lockers[x].building_type
                var show_lock_operational = lockers[x].operational_hours
                //Appending Product lockers
                lockersListModel.append({"lock_name_": show_lock_name,
                                                 "lock_addr_1_":show_lock_addr_1,
                                                 "lock_addr_2_": show_lock_addr_2,
                                                 "lock_building_": show_lock_building,
                                                 "lock_operational_": show_lock_operational})
            }
        }else{
            return
        }
        //Re-define the gridview model
        gridview.model = lockersListModel
        console.log('Total Lockers in ' + city.toUpperCase() + ' : ' + lockersListModel.count);

        /*for(var i = 0; i < lockersListModel.count; ++i) {
            console.log(lockersListModel.get(i).prod_sku_ +" -> "+ lockersListModel.get(i).prod_name_);
        }*/
    }

    function buttonGetLocker(x,y){
        abc.counter = timer_value
        my_timer.restart()
        lockersListModel.clear()
        loadingGif.open()
        locker_data_view.visible = false
        province = x
        city = y
        slot_handler.get_locker_data(province,city)
    }

    function getLockersOffline(x,y){
        abc.counter = timer_value
        my_timer.restart()
        lockersListModel.clear()
        loadingGif.open()
        locker_data_view.visible = false
        province = x
        city = y
        if(x=="Jawa Barat" && y=="Bandung"){
            lockers_list = '[
                {
                    "id": "402880835751659201576557cdcb129b",
                    "name": "SuperIndo Dago",
                    "address": "Jl. Ir. H. Djuanda No.40-42, Dago, Bandung - 40135",
                    "address_2": "Dekat Kasir",
                    "zip_code": "40135",
                    "latitude": "-6.877403",
                    "longitude": "107.617294",
                    "city": "Bandung",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "67"
                },
                {
                    "id": "402880835751659201576557cdea12a0",
                    "name": "SuperIndo Kopo",
                    "address": "Jl. Kopo Sayati No 100A Kec. Margahayu, Desa Sayati, Bandung - 40228",
                    "address_2": "Dekat Kasir",
                    "zip_code": "40228",
                    "latitude": "-6.966983",
                    "longitude": "107.574762",
                    "city": "Bandung",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557ce0612a5",
                    "name": "SuperIndo Metro Bandung",
                    "address": "Metro Building Jl. Soekarno Hatta No.628-630, Bandung - 40222",
                    "address_2": "Dekat Kasir",
                    "zip_code": "40222",
                    "latitude": "-6.939305",
                    "longitude": "107.667640",
                    "city": "Bandung",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "402880835751659201576557ce7512b9",
                    "name": "SuperIndo Ujung Berung",
                    "address": "Jl.Raya Ujung Berung No.24, RT 006/RW 04, Pasanggrahan, Ujung Berung, Bandung - 40616",
                    "address_2": "Dekat Kasir",
                    "zip_code": "40616",
                    "latitude": "-6.916177",
                    "longitude": "107.705540",
                    "city": "Bandung",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "4028808258aeeac20158cdccf9f64b85",
                    "name": "DayTrans Dipatiukur",
                    "address": "Jl Dipatiukur No. 107 Pav Bandung",
                    "address_2": "Di Dalam Kantor",
                    "zip_code": "40132",
                    "latitude": "-6.888190",
                    "longitude": "107.615503",
                    "city": "Bandung",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Travel Outlet",
                    "operational_hours": "Mon - Sun (05.00 AM - 20.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "4028808258aeeac20158cdccf9dc4b82",
                    "name": "DayTrans Pasteur",
                    "address": "Jl Dr. Djunjunan No.55B Bandung 40173",
                    "address_2": "Di Depan Kantor",
                    "zip_code": "40173",
                    "latitude": "-6.897200",
                    "longitude": "107.591923",
                    "city": "Bandung",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Travel Outlet",
                    "operational_hours": "Mon - Sun (05.00 AM - 20.00 PM)",
                    "locker_capacity": "39"
                },
                {
                    "id": "2c9180c051a3de6c0151a888f98b039c",
                    "name": "DayTrans Cihampelas",
                    "address": "Jl Raya Cihampelas No.210 Bandung",
                    "address_2": "Di Depan Kantor",
                    "zip_code": "40131",
                    "latitude": "-6.890731",
                    "longitude": "107.604583",
                    "city": "Bandung",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Travel Outlet",
                    "operational_hours": "Mon - Sun (05.00 AM - 20.00 PM)",
                    "locker_capacity": "36"
                }
            ]'
        }else if(x=="Jawa Barat" && y=="Bekasi"){
            lockers_list = '[
                {
                    "id": "40288083566d7b7f015678b5b6f11e23",
                    "name": "SuperIndo Taman Harapan Baru",
                    "address": "Plaza Taman Harapan Baru (THB) , Perum. THB Jln. THB raya No. 1 Kel. Pejuang Kec. Medan Satria Bekasi Barat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "17131",
                    "latitude": "-6.182380",
                    "longitude": "106.994658",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678aa15341c44",
                    "name": "SuperIndo Kincan",
                    "address": "Jl. Kincan Raya No. 22, Kel. Jatibening, Kec. Pondok Gede, Bekasi 17412",
                    "address_2": "Dekat Kasir",
                    "zip_code": "17412",
                    "latitude": "-6.256233",
                    "longitude": "106.942538",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f015678aa14e51c1d",
                    "name": "SuperIndo Jati Bening",
                    "address": "Jl. Dr. Ratna, Jati Bening, Pondok Gede, Bekasi",
                    "address_2": "Dekat Kasir",
                    "zip_code": "17412",
                    "latitude": "-6.263897",
                    "longitude": "106.950200",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "4028808258aeeac20158cdccfa614b91",
                    "name": "SuperIndo Pondok Ungu",
                    "address": "Jl. Komplek Candrabaga, Kel. Bahagia, Kec. Babelan, Kab. Bekasi, Jawa Barat 17610",
                    "address_2": "Dekat Area Kasir",
                    "zip_code": "17610",
                    "latitude": "-6.186121",
                    "longitude": "107.010153",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08:00 AM – 10:00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "9178272d141011e5bdbd0242ac110001",
                    "name": "KAI Bekasi",
                    "address": "Jl. H. Juanda, Kota Bekasi, Jawa Barat 17112",
                    "address_2": "Di dekat ATM Center",
                    "zip_code": "17112",
                    "latitude": "-6.236246",
                    "longitude": "106.999532",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "40288083566d7b7f015678b5b69e1e14",
                    "name": "President University",
                    "address": "Jababeka Education Park, Jl. Ki Hajar Dewantara, Kota Jababeka, Cikarang, Bekasi, Jawa Barat 17550",
                    "address_2": "Pintu Utama Dorm",
                    "zip_code": "17530",
                    "latitude": "-6.282597",
                    "longitude": "107.170653",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "University",
                    "operational_hours": "Mon - Sun (07:30 AM - 23:00 PM)",
                    "locker_capacity": "116"
                },
                {
                    "id": "2c9180984f4e28b4014fab6ad3e5068c",
                    "name": "Blu Plaza Bekasi",
                    "address": "Jl. Chairil Anwar No. 27-36, Jawa Barat 17113",
                    "address_2": "Ground Floor, dekat ATM Plaza & Karaoke Suka-Suka",
                    "zip_code": "17113",
                    "latitude": "-6.256171",
                    "longitude": "107.008034",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "2c9180c65225a1fb015258bc1e1f100c",
                    "name": "City Walk Lippo Cikarang",
                    "address": "Jl. M.H. Thamrin, Lippo Cikarang, Cikarang, Bekasi, Jawa Barat 17550",
                    "address_2": "Dekat ATM Center",
                    "zip_code": "17550",
                    "latitude": "-6.326860",
                    "longitude": "107.127092",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "4028808258aeeac20158cdccfa7c4b94",
                    "name": "Plaza Metropolitan - Tambun",
                    "address": "Jl. Sultan Hasanudin, Tambun Selatan, Tambun, Bekasi, Jawa Barat 17510",
                    "address_2": "Di Dalam Superindo",
                    "zip_code": "17510",
                    "latitude": "-6.266174",
                    "longitude": "107.072836",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM – 10:00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "4028808258aeeac20158cdccf91f4b6d",
                    "name": "OldTown Harapan Indah",
                    "address": "Sentra Kuliner 3 Kav.26B No 08,Harapan Indah Medan Satria, Pejuang, Bekasi - Jawa Barat 17131",
                    "address_2": "Di Dalam Cafe",
                    "zip_code": "17131",
                    "latitude": "-6.179850",
                    "longitude": "106.973186",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Cafe",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "4028808258aeeac20158cdccf9514b73",
                    "name": "OldTown Cikarang",
                    "address": "Cikarang Groove Jl. Mh.thamrin Kav 117-117A, Lippo Cikarang Bekasi, Cikarang Selatan 17530",
                    "address_2": "Di Dalam Cafe",
                    "zip_code": "17530",
                    "latitude": "-6.332134",
                    "longitude": "107.139810",
                    "city": "Bekasi",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Cafe",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "39"
                }
            ]'
        }else if(x=="Jawa Barat" && y=="Bogor"){
            lockers_list = '[
                {
                    "id": "40288083566d7b7f015678aa17911da3",
                    "name": "SuperIndo Tajur",
                    "address": "Jl. Raya Tajur No.158 Kec. Bogor Selatan",
                    "address_2": "Dekat Kasir",
                    "zip_code": "16137",
                    "latitude": "-6.641165",
                    "longitude": "106.835758",
                    "city": "Bogor",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678aa17501d7c",
                    "name": "SuperIndo Pahlawan Bogor",
                    "address": "Jl. Pahlawan No.78, Kel.Empang Kec.Bogor Selatan, Bogor",
                    "address_2": "Dekat kasir",
                    "zip_code": "16132",
                    "latitude": "-6.615846",
                    "longitude": "106.803585",
                    "city": "Bogor",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557ccb01269",
                    "name": "KAI Cilebut",
                    "address": "Jl. Raya Cilebut, Cilebut Timur, Sukaraja, Bogor 16710",
                    "address_2": "Samping ATM BNI",
                    "zip_code": "16710",
                    "latitude": "-6.530652",
                    "longitude": "106.800526",
                    "city": "Bogor",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "39"
                },
                {
                    "id": "402880835751659201576557cccb126e",
                    "name": "KAI Citayam",
                    "address": "Jl. Raya Citayam, Pabuaran, Bojonggede - Bogor 16320",
                    "address_2": "Dalam Area Stasiun, Depan Kios Roti Maryam",
                    "zip_code": "16320",
                    "latitude": "-6.448760",
                    "longitude": "106.802503",
                    "city": "Bogor",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "39"
                },
                {
                    "id": "402880835751659201576557cce61273",
                    "name": "KAI Bojong Gede",
                    "address": "Jl. Kerinci Raya, Bojong Gede, Bogor, Jawa Barat 16922",
                    "address_2": "Dalam Area Stasiun Pintu Selatan",
                    "zip_code": "16922",
                    "latitude": "-6.493656",
                    "longitude": "106.794905",
                    "city": "Bogor",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "39"
                },
                {
                    "id": "4028808258aeeac20158cdccfa2d4b8b",
                    "name": "PGB Merdeka Bogor",
                    "address": "Jl. Veteran Blok Barja No. 27, Kebon Kelapa, Bogor Tengah, Kota Bogor, Jawa Barat 16125",
                    "address_2": "Area Kampung Batik Lt.1",
                    "zip_code": "16125",
                    "latitude": "-6.593194",
                    "longitude": "106.786744",
                    "city": "Bogor",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM – 10:00 PM)",
                    "locker_capacity": "43"
                }
            ]'
        }else if(x=="Jawa Barat" && y=="Cibinong"){
            lockers_list = '[
                {
                    "id": "40288083566d7b7f0156789029d71b11",
                    "name": "SuperIndo Cibinong",
                    "address": "Gedung ITC Cibinong, Jl Mayor Oking No 6 Cibinong",
                    "address_2": "Dekat Kasir",
                    "zip_code": "16918",
                    "latitude": "-6.480292",
                    "longitude": "106.862737",
                    "city": "Cibinong",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678b0fbd21de0",
                    "name": "SuperIndo Pabuaran Cibinong",
                    "address": "Jl. Raya Jakarta Bogor Km. 42, RT 06/RW 09, Kel. Pabuaran, Kec. Cibinong",
                    "address_2": "Dekat Kasir",
                    "zip_code": "16124",
                    "latitude": "-6.470269",
                    "longitude": "106.850445",
                    "city": "Cibinong",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                }
            ]'
        }else if(x=="Jawa Barat" && y=="Depok"){
            lockers_list = '[
                {
                    "id": "40288083566d7b7f0156789027df1a39",
                    "name": "SuperIndo Cinere",
                    "address": "Cinere Mall, Jl.Cinere Raya, Pangkalan Jati, Cinere Depok",
                    "address_2": "Dekat Kasir",
                    "zip_code": "16514",
                    "latitude": "-6.323983",
                    "longitude": "106.783789",
                    "city": "Depok",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "2c91809a4fcaade601501da172280808",
                    "name": "KAI Depok Baru",
                    "address": "Jl. Margonda Raya, Pancoran Mas, Jawa Barat 16431",
                    "address_2": "Disamping Roti-O",
                    "zip_code": "16431",
                    "latitude": "-6.391211",
                    "longitude": "106.821667",
                    "city": "Depok",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cd111278",
                    "name": "KAI Universitas Indonesia",
                    "address": "Jl. Margonda Raya Gg. Sawo Universitas Indonesia, Beji, Depok",
                    "address_2": "Dekat Pintu Keluar/Masuk, Sebelah Vending Machine",
                    "zip_code": "16424",
                    "latitude": "-6.360025",
                    "longitude": "106.833210",
                    "city": "Depok",
                    "province": "Jawa Barat",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "36"
                }
            ]'
        }else if(x=="DKI Jakarta" && y=="Jakarta Barat"){
            lockers_list = '[
                {
                    "id": "2c9180884e4daccc014e4dad17c80001",
                    "name": "Grand Slipi Tower",
                    "address": "Jl. S.Parman Kav.22-24, Slipi, Palmerah - Jakarta Barat 11480",
                    "address_2": "Lobby, belakang Apotik Century",
                    "zip_code": "11480",
                    "latitude": "-6.200748",
                    "longitude": "106.798385",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (24 hour)",
                    "locker_capacity": "77"
                },
                {
                    "id": "2c9180984f4e28b4014fa6940fd70511",
                    "name": "Gramedia Kompas Jl. Panjang",
                    "address": "Jl. Panjang 8A, Kebon Jeruk, Jakarta Barat, 11530",
                    "address_2": "Samping ATM BRI",
                    "zip_code": "11530",
                    "latitude": "-6.194683",
                    "longitude": "106.768466",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (08:00 AM - 18:00 PM)",
                    "locker_capacity": "35"
                },
                {
                    "id": "40288083566d7b7f0156789028351a5d",
                    "name": "SuperIndo Citra Garden",
                    "address": "Jl. Peta Timur Kel. Pegadungan, Kalideres, Jakarta Barat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "11830",
                    "latitude": "-6.142443",
                    "longitude": "106.707267",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f0156789028881a81",
                    "name": "SuperIndo Kosambi",
                    "address": "Jl. Raya Duri Kosambi No.38, Jakarta Barat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "11750",
                    "latitude": "-6.167046",
                    "longitude": "106.724534",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f01567890285f1a6f",
                    "name": "SuperIndo Taman Palem",
                    "address": "Permata Taman Palem Blok C-01, Cengkareng, Jakarta Barat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "11830",
                    "latitude": "-6.132083",
                    "longitude": "106.710916",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f0156789028b11a93",
                    "name": "SuperIndo Roxi",
                    "address": "Jl. Kyai Tapa No.1, Jakarta Barat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "11440",
                    "latitude": "-6.166238",
                    "longitude": "106.799356",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678aa16461ce0",
                    "name": "SuperIndo Daan Mogot",
                    "address": "Jl. Daan Mogot No.61, Tj. Duren Utara, Cengkareng, Jakarta Barat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "11520",
                    "latitude": "-6.165558",
                    "longitude": "106.782003",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f0156789028db1aa5",
                    "name": "SuperIndo Sunrise",
                    "address": "Jl.Arteri kedoya Blok XX No.6, Kebon Jeruk, Kedoya Utara, Jakarta Barat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "11520",
                    "latitude": "-6.174193",
                    "longitude": "106.765218",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "2c9180c65225a1fb01523fad7cef09f6",
                    "name": "Apt. Medit. Garden Res. 1-AD",
                    "address": "Jl. Tanjung Duren Raya, Jakarta Barat 11470",
                    "address_2": "Dekat Alfamart Tower Azalea",
                    "zip_code": "11470",
                    "latitude": "-6.174678",
                    "longitude": "106.788192",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "100"
                },
                {
                    "id": "40288083566d7b7f015678b5b6b81e19",
                    "name": "Apt. Season City",
                    "address": "Jl. Jembatan Besi Raya No.33, Tambora, Jembatan Besi",
                    "address_2": "Dekat Kantin Apartemen",
                    "zip_code": "11320",
                    "latitude": "-6.153358",
                    "longitude": "106.795909",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "56"
                },
                {
                    "id": "402880835751659201576557cc58125a",
                    "name": "Club House Permata Medit.",
                    "address": "Perumahan Permata Mediterania, Jl. Pos Pengumben Raya 40, Jakarta Barat 11630",
                    "address_2": "Lobby Utama",
                    "zip_code": "11630",
                    "latitude": "-6.221978",
                    "longitude": "106.764921",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cc75125f",
                    "name": "Apt. Medit. Garden Res. 2",
                    "address": "Jl. Tanjung Duren Timur RT. 09 / RW. 05, Tanjung Duren Selatan, Grogol Petamburan, Jakarta Barat 11470",
                    "address_2": "Lorong Akses ATM Center menuju Central Park",
                    "zip_code": "11470",
                    "latitude": "-6.175887",
                    "longitude": "106.788128",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "76"
                },
                {
                    "id": "4028808258aeeac20158cdccf9394b70",
                    "name": "Apt. Medit. Garden Res. 1-BC",
                    "address": "Jl. Tanjung Duren Raya, Jakarta Barat 11470",
                    "address_2": "Dekat Giant Supermarket",
                    "zip_code": "11470",
                    "latitude": "-6.174638",
                    "longitude": "106.788351",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hour)",
                    "locker_capacity": "115"
                },
                {
                    "id": "2c9180c6520c169b015210def16901b4",
                    "name": "Kampus Binus Anggrek",
                    "address": "Jl. Kebon Jeruk Raya No. 27, Kebon Jeruk, Jakarta Barat 11530",
                    "address_2": "Lt. Basement Dekat Zenta Fotocopy",
                    "zip_code": "11530",
                    "latitude": "-6.201666",
                    "longitude": "106.781333",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "University",
                    "operational_hours": "Mon - Sat (08.00 AM - 20.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c91809f50d1e9120150d21d5b3c0005",
                    "name": "Mall Ciputra",
                    "address": "Jl. Arteri S. Parman, Grogol, Jakarta Barat 11470",
                    "address_2": "LG2, didepan Lift",
                    "zip_code": "11470",
                    "latitude": "-6.166878",
                    "longitude": "106.786549",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "4028808258aeeac20158cdc6f0d74a95",
                    "name": "SPBU Pertamina Daan Mogot",
                    "address": "Jl. Daan Mogot Raya Km 1 Kav 2, Kel. Jelambar, Kec Grogol Petamburan, Jakarta Barat 11730",
                    "address_2": "Dekat Tangga Sebelah Bright Store",
                    "zip_code": "11730",
                    "latitude": "-6.154489",
                    "longitude": "106.744933",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "39"
                },
                {
                    "id": "4028808258aeeac20158cdccf9024b6a",
                    "name": "OldTown Mangga Besar",
                    "address": "Jl. Labu No.1J, RT.8/RW.4, Mangga Besar, Tamansari, Jakarta Barat, 11180",
                    "address_2": "Di Dalam Cafe",
                    "zip_code": "11180",
                    "latitude": "-6.146869",
                    "longitude": "106.818286",
                    "city": "Jakarta Barat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Cafe",
                    "operational_hours": "Mon - Sun (10:00 AM - 11:00 PM)",
                    "locker_capacity": "19"
                }
            ]'
        }else if(x=="DKI Jakarta" && y=="Jakarta Pusat"){
            lockers_list = '[
                {
                    "id": "2c9180984f4e28b4014fab6aa0ff0626",
                    "name": "Pos Indo Cikini",
                    "address": "JL. Cikini Raya, No. 8, Cikini-Menteng, Jakarta Pusat 10330",
                    "address_2": "Pintu Utama",
                    "zip_code": "10330",
                    "latitude": "-6.191235",
                    "longitude": "106.840155",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "45"
                },
                {
                    "id": "2c9180c051a3de6c0151a88797b7037f",
                    "name": "Sinar Mas Land Plaza (Tower 2)",
                    "address": "Jl. MH Thamrin Kav.22 No.51, Jakarta Pusat 10350",
                    "address_2": "Lobby Utama Tower II",
                    "zip_code": "10350",
                    "latitude": "-6.189997",
                    "longitude": "106.823757",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180c051a3de6c0151a88613480362",
                    "name": "Gedung Arthaloka",
                    "address": "Jl. Jend. Sudirman Kav.2, Karet Tengsin, Tanah Abang - Jakarta Pusat 10220",
                    "address_2": "ATM Center",
                    "zip_code": "10220",
                    "latitude": "-6.205295",
                    "longitude": "106.821494",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (07:00 AM - 10:00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "4028808258aeeac20158cdccfab24b9a",
                    "name": "Gramedia Pal Merah",
                    "address": "Jl. Palmerah Barat, RT.1/RW.2, Jakarta Pusat 10270",
                    "address_2": "Samping ATM BCA",
                    "zip_code": "10270",
                    "latitude": "-6.207953",
                    "longitude": "106.794560",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (08:00 AM – 07:00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "2c9180c051a3de6c0151a8813d4d030a",
                    "name": "FamilyMart Wisma Keiai",
                    "address": "Jl. Jend.Sudirman Kav. 3-4, Jakarta Pusat 10220",
                    "address_2": "Lantai 1",
                    "zip_code": "10220",
                    "latitude": "-6.205975",
                    "longitude": "106.821458",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Fri (07.00 AM - 11.00 PM) Sat (07.00 AM - 14.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678b5b7091e28",
                    "name": "Familymart Apartment Semanggi",
                    "address": "Apartemen Semanggi, Jl. Gatot Subroto Belakang Kav.50-52",
                    "address_2": "Dalam FamilyMart, Seberang Kasir",
                    "zip_code": "10260",
                    "latitude": "-6.202268",
                    "longitude": "106.80174",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08:30 AM - 20:00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "2c9180984f4e28b4014fa695454b0550",
                    "name": "KAI Sudirman",
                    "address": "Jl. Kendal 1, Menteng, Jakarta Pusat 10310",
                    "address_2": "Di depan Indomaret, Lantai 2",
                    "zip_code": "10310",
                    "latitude": "-6.202360",
                    "longitude": "106.823449",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "2c9180984fb15103014fb9f7f243006c",
                    "name": "KAI Juanda",
                    "address": "Jl. Ir. Haji Juanda I, Jakarta Pusat 10710",
                    "address_2": "di dalam ATM Center",
                    "zip_code": "10710",
                    "latitude": "-6.166312",
                    "longitude": "106.830444",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "2c91809a4fcaade601501d9c5e5707df",
                    "name": "KAI Tanah Abang",
                    "address": "Jl. Jatibaru Kampung Bali, Kp. Bali, Tanah Abang, Jakarta Pusat 10250",
                    "address_2": "Dekat pintu keluar/masuk",
                    "zip_code": "10250",
                    "latitude": "-6.185761",
                    "longitude": "106.810992",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c91809a4fcaade601501dc5a7cf08d1",
                    "name": "KAI Cikini",
                    "address": "Jl. Cikini Raya-Pegangsaan Timur, Menteng, Jakarta Pusat 10320",
                    "address_2": "Lantai 2, Di Seberang Loket Tiket & Musholla",
                    "zip_code": "10320",
                    "latitude": "-6.198245",
                    "longitude": "106.841359",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "35"
                },
                {
                    "id": "2c91809a4fcaade601501dad42a2085c",
                    "name": "KAI Gondangdia",
                    "address": "Jl. Srikaya No.1, Menteng, Jakarta Pusat 10340",
                    "address_2": "Di Depan Alfamart, Lantai 2",
                    "zip_code": "10340",
                    "latitude": "-6.186111",
                    "longitude": "106.832653",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cf1712d7",
                    "name": "KAI Jayakarta",
                    "address": "Jl. Mangga Besar IX/III No.28, Mangga Dua Selatan, Sawah Besar, Jakarta Pusat 10730",
                    "address_2": "Depan Loket Tiket, Di luar Gate",
                    "zip_code": "10730",
                    "latitude": "-6.141385",
                    "longitude": "106.823135",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557cf3012dc",
                    "name": "KAI Mangga Besar",
                    "address": "Jl. Mangga Besar Raya No.111, Karang Anyar, Sawah Besar, Jakarta Pusat 10740",
                    "address_2": "Lantai 1, Sebelah ATM Mandiri & BNI",
                    "zip_code": "11170",
                    "latitude": "-6.149763",
                    "longitude": "106.826979",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557cfdb12fa",
                    "name": "KAI Sawah Besar",
                    "address": "Jl. Krekot Jaya, RT.3/RW.7, Pasar Baru, Sawah Besar, Jakarta Pusat 10710",
                    "address_2": "Lantai 1, Dekat ATM BNI, Di luar Gate",
                    "zip_code": "10710",
                    "latitude": "-6.161240",
                    "longitude": "106.827800",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557cff812ff",
                    "name": "KAI Karet",
                    "address": "Jl. Dukuh Pinggir 1, Kebon Melati, Tanah Abang, Jakarta Pusat 10230",
                    "address_2": "Dekat Pintu Masuk/Gate In",
                    "zip_code": "10230",
                    "latitude": "-6.200900",
                    "longitude": "106.816534",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "40288083566d7b7f015678aa16861d07",
                    "name": "Apt. Sudirman Park",
                    "address": "Jl. Kh. Mas Mansyur Kav. 35, Karet Tengsin, Jakarta Pusat 10220",
                    "address_2": "Basement, Depan ATM",
                    "zip_code": "10220",
                    "latitude": "-6.205521",
                    "longitude": "106.817657",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180c051a3de6c0151a8744725020f",
                    "name": "Mangga Dua Mall",
                    "address": "Jl. Mangga Dua Raya, Mangga Dua Selatan, Jakarta Pusat 10730",
                    "address_2": "Lantai 3, Samping Counter JNE",
                    "zip_code": "10730",
                    "latitude": "-6.137017",
                    "longitude": "106.823892",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (09.00 AM - 20.00 PM)",
                    "locker_capacity": "76"
                },
                {
                    "id": "4028808258aeeac20158cdccf8cc4b64",
                    "name": "SPBU Pertamina Industri",
                    "address": "Jl. Industri Raya No.1, RT 001/003 Kel.Gunung Sahari, Kec.Sawah Besar, Jakarta Pusat 10720",
                    "address_2": "Area SPBU",
                    "zip_code": "10720",
                    "latitude": "-6.148971",
                    "longitude": "106.838432",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "43"
                },
                {
                    "id": "4028808258aeeac20158cdccf8af4b61",
                    "name": "SPBU Pertamina Abdul Muis",
                    "address": "Jl. Abdul Muis No.59, RT.4/RW.3, Petojo Selatan, Gambir, Jakarta Pusat 10160",
                    "address_2": "Area SPBU",
                    "zip_code": "10160",
                    "latitude": "-6.178586",
                    "longitude": "106.818191",
                    "city": "Jakarta Pusat",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "43"
                }
            ]'
        }else if(x=="DKI Jakarta" && y=="Jakarta Selatan"){
            lockers_list = '[
                {
                    "id": "2c9180984f4e28b4014fa69ba26f05db",
                    "name": "Lippo Kuningan",
                    "address": "Gedung Lippo Kuningan Jl. H.R. Rasuna Said Kav B No. 12, Jakarta Selatan 12920",
                    "address_2": "Lobby, di depan Food Mart",
                    "zip_code": "12920",
                    "latitude": "-6.216932",
                    "longitude": "106.830003",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "2c91809a4fcaade601501d8fdddb075b",
                    "name": "Indofood Tower",
                    "address": "Jl. Jend. Sudirman Kav 76-78, Kecamatan Setiabudi, Jakarta Selatan 12910",
                    "address_2": "Lobby Utama, Samping ATM Center",
                    "zip_code": "12910",
                    "latitude": "-6.207735",
                    "longitude": "106.822477",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180c653166eb901531cf31aa001f8",
                    "name": "Mayapada Tower",
                    "address": "Jl. Jend.Sudirman Kav.28, Karet, Setiabudi, Jakarta Selatan 12920",
                    "address_2": "LG Floor (Area Food Court)",
                    "zip_code": "12920",
                    "latitude": "-6.21375",
                    "longitude": "106.820114",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (08.00 AM - 21.00 PM) Sat (08.00 AM - 17.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f0156789029331ac9",
                    "name": "FamilyMart Summitmas",
                    "address": "Gedung Summitmas I-II, Jl. Jenderal Sudirman Kav. 61-62, Kebayoran Baru Jakarta Selatan 12190",
                    "address_2": "Ground Floor",
                    "zip_code": "12190",
                    "latitude": "-6.226836",
                    "longitude": "106.803415",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (08:30 AM - 20:00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678aa16cd1d2e",
                    "name": "Wisma Indocement",
                    "address": "Jl. Jend. Sudirman Kav. 70 - 71 Senayan Kebayoran Baru, Jakarta Selatan",
                    "address_2": "Lobby, Depan Bank Victoria",
                    "zip_code": "12910",
                    "latitude": "-6.205177",
                    "longitude": "106.82296",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (07:00 AM - 20:00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557ceac12c3",
                    "name": "GKM Green Tower",
                    "address": "Jl. TB.Simatupang Kav 89G, Kebagusan, Jakarta Selatan ",
                    "address_2": "Lobby Semi Basement 1, Samping Lift",
                    "zip_code": "12530",
                    "latitude": "-6.302275",
                    "longitude": "106.836274",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (08.00 AM - 05.00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "402880835751659201576557cefd12d2",
                    "name": "Beltway Office Park",
                    "address": "Jl. Ampera Raya No. 9-10, Ragunan, Pasar Minggu 12540",
                    "address_2": "Area Kantin",
                    "zip_code": "12540",
                    "latitude": "-6.291430",
                    "longitude": "106.818544",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (08.00 AM - 05.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557cec512c8",
                    "name": "Equity Tower",
                    "address": "Jl. Jenderal Sudirman, Kavling 52-53 (SCBD) Lot 9, Jakarta Selatan 12190",
                    "address_2": "LG Lobby Dekat Parkir Sepeda",
                    "zip_code": "12190",
                    "latitude": "-6.226438",
                    "longitude": "106.808494",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (08.00 AM - 05.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557d0531309",
                    "name": "Gedung Ariobimo Sentral",
                    "address": "Jl. HR Rasuna Said Blok X-2 Kav 5, RT.9/RW.4, Kuningan Timur, Jakarta Selatan 12950",
                    "address_2": "Lobby, Sebelah ATM BNI & Mandiri",
                    "zip_code": "12950",
                    "latitude": "-6.228339",
                    "longitude": "106.835604",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (07:30 AM - 19:00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "2c9180a850f625d10150f980c4d70015",
                    "name": "FamilyMart Bulungan",
                    "address": "Jl. Bulungan No.18, Kby. Baru, Jakarta Selatan 12130",
                    "address_2": "Lantai 2",
                    "zip_code": "12130",
                    "latitude": "-6.241809",
                    "longitude": "106.796049",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "15"
                },
                {
                    "id": "2c9180c051a3de6c0151a876aaf5022d",
                    "name": "FamilyMart Pejaten",
                    "address": "Jl. Pejaten Raya No. 45A, Pejaten Barat, Jakarta Selatan 12510",
                    "address_2": "Lantai 2",
                    "zip_code": "12510",
                    "latitude": "-6.279019",
                    "longitude": "106.839694",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (24 hour)",
                    "locker_capacity": "16"
                },
                {
                    "id": "2c9180c051a3de6c0151a882d4350327",
                    "name": "FamilyMart Cipete",
                    "address": "Jl. Cipete Raya No. 1 Kel. Cipete Selatan, Kec. Cilandak, Jakarta Selatan 12410",
                    "address_2": "Lantai 2",
                    "zip_code": "12410",
                    "latitude": "-6.277556",
                    "longitude": "106.800139",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (09.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f0156789029861aed",
                    "name": "SuperIndo Duren Tiga",
                    "address": "Jl. Raya Pasar Minggu, Duren Tiga, Jakarta Selatan",
                    "address_2": "Dekat Kasir",
                    "zip_code": "12860",
                    "latitude": "-6.253136",
                    "longitude": "106.842894",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f0156789029af1aff",
                    "name": "SuperIndo Mampang",
                    "address": "Warung Buncit City Point, Jl. Mampang Prapatan Raya, Duren Tiga, Pancoran",
                    "address_2": "Dekat Kasir",
                    "zip_code": "12710",
                    "latitude": "-6.260971",
                    "longitude": "106.829212",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f01567890290a1ab7",
                    "name": "FamilyMart Wisma Mulia",
                    "address": "City Plaza Wisma Mulia,  Jl. Gatot Subroto No. 42, Kel. Kuningan Barat, Kec. Mampang Prapatan, 12710",
                    "address_2": "Suite LG02",
                    "zip_code": "12710",
                    "latitude": "-6.235245",
                    "longitude": "106.823423",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08:30 AM - 20:00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f01567890295c1adb",
                    "name": "SuperIndo Pancoran",
                    "address": "Jl. MT. Haryono No.1 Pancoran",
                    "address_2": "Dekat Kasir",
                    "zip_code": "12780",
                    "latitude": "-6.242471",
                    "longitude": "106.844556",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "2c91809a4fcaade601501db150540889",
                    "name": "KAI Pasar Minggu",
                    "address": "Jl. Raya Pasar Minggu, Ps. Minggu, Jakarta Selatan 12510",
                    "address_2": "Di depan pintu masuk/pintu keluar",
                    "zip_code": "12510",
                    "latitude": "-6.283792",
                    "longitude": "106.844592",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c91809a4fcaade601501d754eff073e",
                    "name": "KAI Manggarai",
                    "address": "Jl. Manggarai Utara No. 1, Jakarta Selatan 12850",
                    "address_2": "Di dalam ATM Gallery",
                    "zip_code": "12850",
                    "latitude": "-6.210411",
                    "longitude": "106.849939",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "402880835751659201576557cc941264",
                    "name": "KAI Tebet",
                    "address": "Jl. Tebet Raya, Tebet Timur, Jakarta Selatan 12810",
                    "address_2": "Sebelah Loket Tiket",
                    "zip_code": "12810",
                    "latitude": "-6.226848",
                    "longitude": "106.858398",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cfbb12f5",
                    "name": "KAI Lenteng Agung",
                    "address": "Jl. Lenteng Agung Timur, Srengseng Sawah, Jagakarsa - Jakarta Selatan 12640",
                    "address_2": "Dalam Gate Peron Stasiun",
                    "zip_code": "12640",
                    "latitude": "-6.330710",
                    "longitude": "106.835000",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "2c9180884e53131c014e574f7faf005c",
                    "name": "Apt. Kalibata City",
                    "address": "Jl. Kalibata Raya No. 1 Rawa Jati Pancoran, Jakarta Selatan 12750",
                    "address_2": "Lobby Tower Flamboyan",
                    "zip_code": "12750",
                    "latitude": "-6.256601",
                    "longitude": "106.851616",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "52"
                },
                {
                    "id": "40288083566d7b7f015678aa17121d55",
                    "name": "Apartemen Lavande",
                    "address": "Jl. Prof. Dr. Supomo No.231, RT007/RW001, Tebet, Jakarta Selatan 12870",
                    "address_2": "Belakang Circle K",
                    "zip_code": "12870",
                    "latitude": "-6.236411",
                    "longitude": "106.843185",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557ce5412b4",
                    "name": "Apt. Puri Casablanca",
                    "address": "Jl. Casablanca, Kuningan Timur, Kecamatan Setiabudi, Jakarta Selatan 12950",
                    "address_2": "ATM Gallery",
                    "zip_code": "12950",
                    "latitude": "-6.223189",
                    "longitude": "106.838296",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "19"
                },
                {
                    "id": "2c91809a4fcaade601501d70b94b070e",
                    "name": "Univ. Atma Jaya",
                    "address": "Jl. Jenderal Sudirman No. 51, Jakarta Selatan 12930",
                    "address_2": "Di dekat Gedung Wojtyla, dekat ATM Mandiri",
                    "zip_code": "12930",
                    "latitude": "-6.218174",
                    "longitude": "106.815150",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "University",
                    "operational_hours": "Mon - Sun (09.00 AM - 06.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "402880835751659201576557cd461282",
                    "name": "Universitas Pancasila",
                    "address": "Jl. Lenteng Agung No.44, Srengseng Sawah, Jagakarsa, Jakarta Selatan 12630",
                    "address_2": "Sebelah Kantor Penerimaaan Mahasiswa Baru",
                    "zip_code": "12630",
                    "latitude": "-6.339427",
                    "longitude": "106.833960",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "University",
                    "operational_hours": "Mon - Sun (07.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180884e4daccc014e4dad17250000",
                    "name": "Lippo Kemang Mall",
                    "address": "Kemang Village Jl. P. Antasari No. 36, Kemang, Jakarta Selatan 12150",
                    "address_2": "Lantai UG, dekat Books & Beyond",
                    "zip_code": "12150",
                    "latitude": "-6.259574",
                    "longitude": "106.812113",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "115"
                },
                {
                    "id": "2c91809a4fcaade601501d942644078d",
                    "name": "Plaza Festival",
                    "address": "Jl. H.R. Rasuna Said Kav. C-22, Jakarta Selatan 12940",
                    "address_2": "Di depan ATM Mandiri, di sebelah Inul Vista",
                    "zip_code": "12940",
                    "latitude": "-6.220623",
                    "longitude": "106.833040",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180c151a8c1a70151c3fc9f8b0897",
                    "name": "Plaza Semanggi",
                    "address": "Jl. Jenderal Sudirman Kav. 50 (Jalan Jenderal Gatot Subroto), Jakarta Selatan 12930",
                    "address_2": "Lantai 1, di dekat Centro Dept. Store",
                    "zip_code": "12930",
                    "latitude": "-6.219842",
                    "longitude": "106.814542",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f015678b5b6801e0f",
                    "name": "SouthBox Prapanca",
                    "address": "Jl. Prapanca Raya No.18, Kebayoran Baru - Jakarta Selatan 12160",
                    "address_2": "Samping Bakmie RN",
                    "zip_code": "12160",
                    "latitude": "-6.250842",
                    "longitude": "106.80936",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Fri (12.00 PM - 22.00 PM) Sat - Sun (12.00 PM - 01.00 AM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cd951291",
                    "name": "Lotte Shopping Avenue",
                    "address": "Jl. Prof. Dr. Satrio Kav. 3-5, Karet Kuningan, Kecamatan Setiabudi, Jakarta Selatan",
                    "address_2": "Lantai GF dekat Breadlife",
                    "zip_code": "12940",
                    "latitude": "-6.223635",
                    "longitude": "106.822861",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cd2c127d",
                    "name": "Kampung Kemang Food Court",
                    "address": " Jl. Kemang Raya No.18, RT.10/RW.5, Bangka, Mampang Prapatan, Jakarta Selatan",
                    "address_2": "Area Pintu Masuk Food Court",
                    "zip_code": "12730",
                    "latitude": "-6.258696",
                    "longitude": "106.815055",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Thu (17.00 PM - 00.00 PM), Fri-Sat (17.00 PM - 03.00 AM), Sun (15.00 PM - 21.00 PM)",
                    "locker_capacity": "40"
                },
                {
                    "id": "402880835751659201576557d06e130e",
                    "name": "SPBU Pertamina MT. Haryono",
                    "address": "Jl. MT. Haryono Kav 18, Jakarta Selatan 12810",
                    "address_2": "Depan Counter DayTrans",
                    "zip_code": "12810",
                    "latitude": "-6.242593",
                    "longitude": "106.852409",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "4028808258aeeac20158cdccf7e14b55",
                    "name": "SPBU Pertamina Pondok Indah",
                    "address": "Jl. Sultan Iskandar Muda, RT 011/09 Kel. Kebayoran Lama Selatan, Kec. Kebayoran Lama, Jakarta Selatan 12240",
                    "address_2": "Samping Bright Store Lantai 2",
                    "zip_code": "12240",
                    "latitude": "-6.259655",
                    "longitude": "106.781971",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "39"
                },
                {
                    "id": "2c9180c652689a0a0152779406570141",
                    "name": "SPBU Pertamina Gatot Subroto",
                    "address": "Jl. Jend. Gatot Subroto Kav.31, Jakarta Selatan 12950",
                    "address_2": "Area SPBU",
                    "zip_code": "12950",
                    "latitude": "-6.233101",
                    "longitude": "106.822366",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "4028808258aeeac20158cdccf9c34b7f",
                    "name": "SPBU Pertamina Tendean",
                    "address": "Jl. Kapten Tendean No.38, Kuningan Barat, Mampang Prapatan, Jakarta Selatan c",
                    "address_2": "Area SPBU",
                    "zip_code": "12710",
                    "latitude": "-6.240285",
                    "longitude": "106.820491",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "43"
                },
                {
                    "id": "4028808258aeeac20158cdccf8924b5e",
                    "name": "SPBU Pertamina Kemang",
                    "address": "Jl. Kemang Selatan Raya No.105A, Kel.Bangka, Kec. Mampang Prapatan 12560",
                    "address_2": "Area SPBU",
                    "zip_code": "12560",
                    "latitude": "-6.491581",
                    "longitude": "106.741316",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "43"
                },
                {
                    "id": "4028808258aeeac20158cdccf8e64b67",
                    "name": "OldTown Pakubuwono",
                    "address": "Jl. Hang Lekir X, Gunung, Kebayoran Baru, RT.6/RW.6, Gunung, Jakarta Selatan 12120",
                    "address_2": "Di Dalam Cafe",
                    "zip_code": "12120",
                    "latitude": "-6.232589",
                    "longitude": "106.796023",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Cafe",
                    "operational_hours": "Mon - Sun (07.00 AM - 11.00 PM)",
                    "locker_capacity": "16"
                }
            ]'
        }else if(x=="DKI Jakarta" && y=="Jakarta Timur"){
            lockers_list = '[
                {
                    "id": "402880835751659201576557cee212cd",
                    "name": "Wisma Indomobil",
                    "address": "Jl. Mt. Haryono Kav.8, Kp. Melayu, Jatinegara, Jakarta Timur 13330",
                    "address_2": "Area Kantin Dekat Indomaret",
                    "zip_code": "13330",
                    "latitude": "-6.242466",
                    "longitude": "106.863619",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "43"
                },
                {
                    "id": "40288083566d7b7f015678aa157a1c6b",
                    "name": "SuperIndo Pondok Kelapa",
                    "address": "Jl. Pondok Kelapa Raya, Kel. Pondok Kelapa, Kec. Duren Sawit. Jakarta Timur",
                    "address_2": "Dekat Kasir",
                    "zip_code": "13450",
                    "latitude": "-6.239860",
                    "longitude": "106.934717",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f015678aa149c1bf6",
                    "name": "SuperIndo Basura City",
                    "address": "Jl. Basuki Rahmat No.1 A, Cipinang Besar Selatan, Jatinegara",
                    "address_2": "Dekat Kasir",
                    "zip_code": "13410",
                    "latitude": "-6.224196",
                    "longitude": "106.877488",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cd621287",
                    "name": "SuperIndo Pondok Bambu",
                    "address": "Gedung Tomang Tol, Jl. Inspeksi Saluran No. 1-3 Kalimalang, Jakarta Timur 13430",
                    "address_2": "Dekat Kasir",
                    "zip_code": "13430",
                    "latitude": "-6.247541",
                    "longitude": "106.912176",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "402880835751659201576557d0171304",
                    "name": "KAI Jatinegara",
                    "address": "Jl. Bekasi Barat Raya, Pisangan Baru, Matraman, Jatinegara - Jakarta Timur 13110",
                    "address_2": "Samping Pintu Keluar Penumpang Commuter Line",
                    "zip_code": "13110`",
                    "latitude": "-6.215100",
                    "longitude": "106.870376",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "2c9180c65291e4ba0152ee3cc79d0e94",
                    "name": "Apt. Gading Icon",
                    "address": "Jl. Perintis Kemerdekaan No. 99, Pulogadung, Jakarta Timur 13260",
                    "address_2": "Tower A Lantai Lobby, Dekat Ruang Engineering",
                    "zip_code": "13260",
                    "latitude": "-6.181294",
                    "longitude": "106.904403",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "56"
                },
                {
                    "id": "402880835751659201576557cd7d128c",
                    "name": "Apt. Casablanca East Res.",
                    "address": "Jl. Pahlawan Revolusi No.2, RT.1/RW.2, Pondok Bambu, Duren Sawit 13430",
                    "address_2": "Area Swimming Pool, Depan Indomaret",
                    "zip_code": "13430",
                    "latitude": "-6.223971",
                    "longitude": "106.898691",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "56"
                },
                {
                    "id": "2c91809a4fcaade601501d98826907b6",
                    "name": "STMK Trisakti",
                    "address": "Jl. Pulo Mas Selatan No. 26, Pulo Gadung, Jakarta Timur 13210",
                    "address_2": "Di dekat pintu masuk",
                    "zip_code": "13210",
                    "latitude": "-6.186247",
                    "longitude": "106.876005",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "University",
                    "operational_hours": "Mon - Fri (09.00 AM - 06.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557ce9312be",
                    "name": "Universitas Kristen Indonesia",
                    "address": "Jl. Mayor Jenderal Sutoyo No.2, Cawang, Kramatjati, Jakarta Timur 13630",
                    "address_2": "Area Biro Keuangan",
                    "zip_code": "13630",
                    "latitude": "-6.249371",
                    "longitude": "106.871996",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "University",
                    "operational_hours": "Mon - Fri (07.00 AM - 05.00 PM)",
                    "locker_capacity": "39"
                },
                {
                    "id": "2c9180984f4e28b4014fab6aba5b0659",
                    "name": "Kantor Pos Rawamangun",
                    "address": "Jl. Pemuda No. 79 Kec.Pulo Gadung",
                    "address_2": "Area Pelayanan",
                    "zip_code": "13220",
                    "latitude": "-6.193726",
                    "longitude": "106.901217",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Fri (08:00 AM - 21:00 PM), Sat (08.00 AM - 02.00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "2c9180984f4e28b4014fab6ad3f8068d",
                    "name": "Tamini Square",
                    "address": "Jl. Taman Mini I Makasar, Jakarta Timur 13560",
                    "address_2": "Lobby, lantai UG",
                    "zip_code": "13560",
                    "latitude": "-6.290657",
                    "longitude": "106.881559",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "4028808258aeeac20158cdccf8564b58",
                    "name": "SPBU Pertamina Kalimalang",
                    "address": "Jl. Swadaya Raya Rt. 002/ Rw. 08 Kel. Duren Sawit Kec. Duren Sawit - Jakarta Timur 13440",
                    "address_2": "Samping Bright Store",
                    "zip_code": "13440",
                    "latitude": "-6.248190",
                    "longitude": "106.930026",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "39"
                },
                {
                    "id": "4028808258aeeac20158cdccf8774b5b",
                    "name": "SPBU Pertamina Gandaria",
                    "address": "Jl. Raya Bogor Km 28 Kel. Pekayon, Kec. Pasar Rebo, Jakarta Timur, 13710",
                    "address_2": "Area SPBU",
                    "zip_code": "13710",
                    "latitude": "-6.352988",
                    "longitude": "106.862351",
                    "city": "Jakarta Timur",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "39"
                }
            ]'
        }else if(x=="DKI Jakarta" && y=="Jakarta Utara"){
            lockers_list = '[
                {
                    "id": "2c9180c051a3de6c0151a878be7c024c",
                    "name": "FamilyMart Kelapa Gading",
                    "address": "Jl. Boulevard Barat Raya, Blok XC, Klp. Gading, Jakarta Utara 14240",
                    "address_2": "Lantai 2",
                    "zip_code": "14240",
                    "latitude": "-6.156772",
                    "longitude": "106.898375",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (24 hour)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678aa140f1ba8",
                    "name": "SuperIndo Sport Kelapa Gading",
                    "address": "Jl. Kelapa Nias Raya Blok HF-03, Kelapa Gading, Jakarta Utara",
                    "address_2": "Dekat Kasir",
                    "zip_code": "14250",
                    "latitude": "-6.149697",
                    "longitude": "106.902850",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f015678aa15c11c92",
                    "name": "SuperIndo Teluk Gong",
                    "address": "Ruko Induk Square Dua Jl. Teluk Gong Raya No. 27, Jakarta Utara",
                    "address_2": "Dekat Kasir",
                    "zip_code": "14450",
                    "latitude": "-6.137318",
                    "longitude": "106.782783",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cc211250",
                    "name": "SuperIndo Pantai Indah Kapuk",
                    "address": "Centro Metro Broadway Blok B Pantai Indah Kapuk Jakarta Utara - 14430",
                    "address_2": "Dekat Kasir",
                    "zip_code": "14430",
                    "latitude": "-6.116679",
                    "longitude": "106.759379",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cc3d1255",
                    "name": "SuperIndo Koja Trade Mall",
                    "address": "Koja Trade Mall Jl. Bayangkara, Kec. Koja, Kel. Tugu Utara, Jakarta Utara",
                    "address_2": "Lantai Dasar, Dekat Kasir",
                    "zip_code": "14260",
                    "latitude": "-6.121642",
                    "longitude": "106.914076",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "402880835751659201576557cf9912f0",
                    "name": "KAI Kampung Bandan",
                    "address": "Jl. Kampung Bandan Raya, Kel.Ancol, Kec.Pademangan, Jakarta Utara 14430",
                    "address_2": "Dalam Peron di Jalur Bawah Dekat Gate In",
                    "zip_code": "14430",
                    "latitude": "-6.132739",
                    "longitude": "106.828442",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "40288083566d7b7f015678aa14531bcf",
                    "name": "Apt. Green Lake Sunter",
                    "address": "Jl. Danau Sunter Selatan No.16, Jakarta Utara",
                    "address_2": "Lobby Tower II",
                    "zip_code": "14350",
                    "latitude": "-6.143468",
                    "longitude": "106.876821",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180c051a3de6c0151a87b0bc40271",
                    "name": "Apt. CBD Pluit",
                    "address": "Jl. Pluit Selatan Raya, RT.23/RW.8, Penjaringan, Kota Jkt Utara 14440",
                    "address_2": "Tower Akasia",
                    "zip_code": "14440",
                    "latitude": "-6.127284",
                    "longitude": "106.789277",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f015678b5b6671e0a",
                    "name": "Apt. Gading Nias Res.",
                    "address": "Jl. Raya Pegangsaan Dua No. 12, Pegangsaan Dua, Kelapa Gading, Jakarta Utara 14250",
                    "address_2": "Tower Bougenville",
                    "zip_code": "14250",
                    "latitude": "-6.153633",
                    "longitude": "106.917536",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "56"
                },
                {
                    "id": "402880835751659201576557ce3b12af",
                    "name": "Apt. Gading Medit. Res.",
                    "address": "Jl. Boulevard Bukit Gading Raya RT.6/RW.14, Kelapa Gading Barat, Jakarta Utara - 14240",
                    "address_2": "Lobby Tower A Utara (depan pintu)",
                    "zip_code": "14240",
                    "latitude": "-6.162451",
                    "longitude": "106.892990",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Apartment/Residential",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "56"
                },
                {
                    "id": "2c9180884e53131c014e536213ee0003",
                    "name": "Baywalk Mall",
                    "address": "Green Bay Pluit, Jl. Pluit Karang Ayu B1 Utara, Jakarta Utara 14450",
                    "address_2": "Lantai LG, Didepan ATM BCA",
                    "zip_code": "14450",
                    "latitude": "-6.108157",
                    "longitude": "106.778526",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "41"
                },
                {
                    "id": "2c91809a4fcaade6014fcf2b3bed002a",
                    "name": "MOI",
                    "address": "Jl. Boulevard Barat Raya Kelapa Gading, Jakarta Utara 14240",
                    "address_2": "Lobby 1, dipintu masuk, didekat car call",
                    "zip_code": "14240",
                    "latitude": "-6.151529",
                    "longitude": "106.891481",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "63"
                },
                {
                    "id": "40288083566d7b7f015678b5b7231e2d",
                    "name": "Mall Artha Gading",
                    "address": "Jl. Artha Gading Selatan No.38, Kelapa Gading Barat, Kelapa Gading 14240",
                    "address_2": "Lantai Ground (GF), Dekat Blackberry Service",
                    "zip_code": "14240",
                    "latitude": "-6.145454",
                    "longitude": "106.892199",
                    "city": "Jakarta Utara",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "36"
                }
            ]'
        }else if(x=="Banten" && y=="Tangerang"){
            lockers_list = '[
                {
                    "id": "402880835751659201576557cdb01296",
                    "name": "EV HIVE The Breeze",
                    "address": "BSD Green Office Park, BSD City Unit #L 69-70, Jl. Grand Boulevard, Sampora, Cisauk, Tangerang, Banten 15345",
                    "address_2": "Lobby Utama",
                    "zip_code": "15345",
                    "latitude": "-6.301628",
                    "longitude": "106.653241",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (09.00 AM - 20.00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "402880835751659201576557ce2112aa",
                    "name": "Wisma BCA BSD City",
                    "address": "Wisma BCA BSD City Kav. CBD Lot I.3, Jl. Pahlawan Seribu, Tangerang, Banten 15321 ",
                    "address_2": "Lobby, Samping Lift Depan",
                    "zip_code": "15321 ",
                    "latitude": "-6.295325",
                    "longitude": "106.665768",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Office",
                    "operational_hours": "Mon - Fri (08.00 AM - 17.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180c051a3de6c0151a88474780345",
                    "name": "Lion Express Rempoa",
                    "address": "Jl. Pahlawan Raya No. 9 Kelurahan Rempoa, Kecamatan Ciputat Timur, Tangerang Selatan 15412",
                    "address_2": "Sebelah ATM BCA",
                    "zip_code": "15412",
                    "latitude": "-6.280710",
                    "longitude": "106.761450",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (07.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f01567890280a1a4b",
                    "name": "SuperIndo Cirendeu",
                    "address": "Jl. Cirendeu Raya No. 20 Pondok Cabe, Tangerang Selatan",
                    "address_2": "Dekat Kasir",
                    "zip_code": "15419",
                    "latitude": "-6.317879",
                    "longitude": "106.768244",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678aa16051cb9",
                    "name": "SuperIndo Grand Dadap City",
                    "address": "Jl. Perancis, Kel. Dadap, Kec, Kosambi, Tangerang Selatan",
                    "address_2": "Dekat Kasir",
                    "zip_code": "15211",
                    "latitude": "-6.092084",
                    "longitude": "106.692188",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f015678457bcd18fb",
                    "name": "SuperIndo Pamulang",
                    "address": "Pamulang Permai Blok SH No.13 Pamulang Barat, Ciputat",
                    "address_2": "Dekat Kasir",
                    "zip_code": "15417",
                    "latitude": "-6.343080",
                    "longitude": "106.732361",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678a648cd1b77",
                    "name": "SuperIndo Pinang",
                    "address": "Jl. KH. Hasyim Ashari, Kel. Pinang, Kec. Pinang",
                    "address_2": "Depan Super Indo",
                    "zip_code": "15145",
                    "latitude": "-6.214625",
                    "longitude": "106.687282",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "40288083566d7b7f0156789027b11a27",
                    "name": "SuperIndo RE. Martadinata",
                    "address": "Jl. RE Martadinata 1, Ciputat, Tangerang Selatan",
                    "address_2": "Dekat Kasir",
                    "zip_code": "15411",
                    "latitude": "-6.322283",
                    "longitude": "106.746748",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678b5b6341e00",
                    "name": "SuperIndo Citra Raya",
                    "address": "Jl. Taman Raya Blok K.01, Kav. 01A, Cikupa, Tangerang",
                    "address_2": "Dekat Kasir",
                    "zip_code": "15132",
                    "latitude": "-6.238408",
                    "longitude": "106.524299",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "16"
                },
                {
                    "id": "40288083566d7b7f015678b5b64f1e05",
                    "name": "SuperIndo City Mall Tangerang",
                    "address": "Jl. Raya Mauk (City Mall), Kec. Karawaci, Tangerang",
                    "address_2": "Dekat Kasir",
                    "zip_code": "15131",
                    "latitude": "-6.170913",
                    "longitude": "106.609390",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "4028808258aeeac20158cdccf9744b76",
                    "name": "SuperIndo Harapan Kita",
                    "address": "Jl. Raya Roro Jonggrang No.1, Kel.Bencongan, Kec. Kelapa Dua, Tangerang - Banten 15810",
                    "address_2": "Dekat Area Kasir",
                    "zip_code": "15810",
                    "latitude": "-6.216750",
                    "longitude": "106.601062",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08:00 AM - 10:00 PM)",
                    "locker_capacity": "39"
                },
                {
                    "id": "4028808258aeeac20158cdccf9a84b7c",
                    "name": "SuperIndo Poris",
                    "address": "Jl. Maulana Hasanudin, Kel. Cipondoh Indah, Kec. Cipondoh, Tangerang - Banten",
                    "address_2": "Dekat Area Kasir",
                    "zip_code": "15148",
                    "latitude": "-6.170893",
                    "longitude": "106.683016",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08:00 AM – 10:00 PM)",
                    "locker_capacity": "19"
                },
                {
                    "id": "4028808258aeeac20158cdccf98e4b79",
                    "name": "SuperIndo Cipondoh",
                    "address": "Jl. Hasyim Ashari Kel. Poris Plawad Utara Kec. Cipondoh, Tangerang - Banten 15141",
                    "address_2": "Dekat Area Kasir",
                    "zip_code": "15141",
                    "latitude": "-6.178305",
                    "longitude": "106.677827",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Super/Mini Market",
                    "operational_hours": "Mon - Sun (08:00 AM – 10:00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557cf4c12e1",
                    "name": "KAI Poris",
                    "address": "Jl. Akses Stasiun Poris, Porisgaga, Batuceper, Tangerang, Banten 15122",
                    "address_2": "Depan Musholla, Samping Tempat Charging",
                    "zip_code": "15122",
                    "latitude": "-6.169925",
                    "longitude": "106.680049",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557cf7e12eb",
                    "name": "KAI Pondok Ranji",
                    "address": "Jl. Stasiun Pondok Ranji, Rengas, Ciputat Timur, Tangerang Selatan, Banten 15412",
                    "address_2": "Di luar Gate, Dekat Pintu Keluar",
                    "zip_code": "15412",
                    "latitude": "-6.276468",
                    "longitude": "106.744803",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "402880835751659201576557cf6612e6",
                    "name": "KAI Serpong",
                    "address": "Jl. Raya Serpong, Serpong, Tangerang Selatan, Banten 15310",
                    "address_2": "Lantai 1, Di Dalam Gate, Sebelah Papan Penunjuk Rute",
                    "zip_code": "15310",
                    "latitude": "-6.320118",
                    "longitude": "106.665616",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Railway Station",
                    "operational_hours": "Mon - Sun (05.00 AM - 11.00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "2c91809a4fcaade60150226cea37096b",
                    "name": "UPH Karawaci",
                    "address": "Jl. Boulevard Palem Raya, Lippo Village, Kec Tangerang, Banten 15811",
                    "address_2": "Tower A, di sebelah ATM CIMB",
                    "zip_code": "15811",
                    "latitude": "-6.228935",
                    "longitude": "106.610870",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "University",
                    "operational_hours": "Mon - Sun (07.00 AM - 09.00 PM)",
                    "locker_capacity": "45"
                },
                {
                    "id": "2c9180c051a3de6c0151a87d73d402a4",
                    "name": "Supermall Karawaci",
                    "address": "Jl. Boulevard Diponegoro No. 105, Kec. Tangerang, Banten 15811",
                    "address_2": "Lantai 3, di depan Hypermart, dekat ATM",
                    "zip_code": "15811",
                    "latitude": "-6.226958",
                    "longitude": "106.607208",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM - 10:00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "2c9180c051a3de6c0151a87f9c4302d7",
                    "name": "Maxx Box Karawaci",
                    "address": "Jl. Boulevard Jend. Sudirman No. 1110, Lippo Village, Kec. Tangerang, Banten 15810",
                    "address_2": "Ground Floor, disamping ATM & Supermarket",
                    "zip_code": "15810",
                    "latitude": "-6.229136",
                    "longitude": "106.606656",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10.00 AM - 10.00 PM)",
                    "locker_capacity": "36"
                },
                {
                    "id": "4028808258aeeac20158cdccfa484b8e",
                    "name": "TerasKota - BSD City",
                    "address": "Jl. Pahlawan Seribu CBD Lot VII B, Lengkong Gudang, Serpong, Tangerang Selatan, Banten 15322",
                    "address_2": "Depan Laus Kopitiam Lantai Dasar",
                    "zip_code": "15322",
                    "latitude": "-6.295611",
                    "longitude": "106.667445",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Shopping Mall",
                    "operational_hours": "Mon - Sun (10:00 AM – 10:00 PM)",
                    "locker_capacity": "43"
                },
                {
                    "id": "2c9180a850f625d10150f987868e0026",
                    "name": "SPBU Pertamina Ciputat",
                    "address": "Jl. Ciputat Raya, Kel Cirendeu, Kec Ciputat timur, Kab Tangerang Banten",
                    "address_2": "Dekat Bright Store",
                    "zip_code": "15412",
                    "latitude": "-6.302520",
                    "longitude": "106.760835",
                    "city": "Tangerang",
                    "province": "Banten",
                    "country": "Indonesia",
                    "building_type": "Gas Station",
                    "operational_hours": "Mon - Sun (24 hours)",
                    "locker_capacity": "14"
                }
            ]'
        }
        parse_data_locker_v2(lockers_list)
        if(lockers_list != ""){
            loadingGif.close()
            locker_data_view.visible = true
        }
    }

    LoadingView{
        id: loadingGif
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter == 30 && lockers_list == ""){
                    network_error.open()
                }
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id: main_page

        BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("Back")

            MouseArea {
                id: ms_back
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
        }

        Item{
            id: locker_data_view
            x: 73
            y: 105
            width: 790
            height: 650
            anchors.bottom: parent.bottom
            anchors.bottomMargin: -767
            anchors.right: parent.right
            anchors.rightMargin: -1023
            focus: true

            ScrollBarPop{
                id: vertical_sbar
                flickable: gridview
                x: 975
                y: 266
                width: 10
                height: locker_data_view.height
                color: "white"
                anchors.rightMargin: -10
            }

            GridView{
                id: gridview
                width: 790
                anchors.verticalCenterOffset: 0
                anchors.horizontalCenterOffset: -26
                anchors.rightMargin: 45
                anchors.leftMargin: -56
                focus: true
                clip: true
                anchors.bottomMargin: 0
                anchors.topMargin: 0
                flickDeceleration: 750
                maximumFlickVelocity: 1500
                anchors.centerIn: parent
                layoutDirection: Qt.LeftToRight
                flow: GridView.FlowLeftToRight
                boundsBehavior: Flickable.StopAtBounds
                snapMode: GridView.SnapToRow
                anchors.fill: parent
                anchors.margins: 20
                delegate: delegate_item_view
                //model: lockersListModel
                cellWidth: 385
                cellHeight: 160
            }

            ListModel {
                id: lockersListModel
                dynamicRoles: true
            }

            Component{
                id: delegate_item_view
                LockerCardView{
                    id: item_view
                    lock_name: lock_name_
                    lock_addr_1: lock_addr_1_
                    lock_addr_2: lock_addr_2_
                    lock_building: lock_building_
                    lock_operational: lock_operational_

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(usageOf=="popshop"){
                                var data_all = datas + "|" + item_view.lock_name
                                my_stack_view.push(grocery_express_info, {data_all:data_all, amount:total_amount})
                            }else{
                                my_stack_view.push(on_develop_view)
                            }
                        }
                    }
                }
            }
        }

        Rectangle{
            id: locker_city_buttons
            focus: true
            visible: true
            color: "transparent"
            anchors.right: locker_data_view.left
            anchors.rightMargin: 0
            x: 0
            y: 117
            width: base_bground.width - locker_data_view.width
            height: locker_data_view.height

            Column{
                x: -6
                y: 0
                spacing: 2
                //1st Button
                LockerCityButton{
                    id: bandung_button
                    button_text: qsTr("Bandung")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bandung_button.button_color = "darkred"
                        bekasi_button.button_color = "white"
                        bogor_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("Jawa Barat","Bandung")
                        getLockersOffline("Jawa Barat","Bandung")
                        }
                    }
                }
                //2nd Button
                LockerCityButton{
                    id: bekasi_button
                    button_text: qsTr("Bekasi")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bekasi_button.button_color = "darkred"
                        bandung_button.button_color = "white"
                        bogor_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("Jawa Barat","Bekasi")
                        getLockersOffline("Jawa Barat","Bekasi")
                        }
                    }
                }
                //3rd Button
                LockerCityButton{
                    id: bogor_button
                    button_text: qsTr("Bogor")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bogor_button.button_color = "darkred"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("Jawa Barat","Bogor")
                        getLockersOffline("Jawa Barat","Bogor")
                        }
                    }
                }
                //4th Button
                LockerCityButton{
                    id: cibinong_button
                    button_text: qsTr("Cibinong")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        cibinong_button.button_color = "darkred"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        depok_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("Jawa Barat","Cibinong")
                        getLockersOffline("Jawa Barat","Cibinong")
                        }
                    }
                }
                //5th Button
                LockerCityButton{
                    id: depok_button
                    button_text: qsTr("Depok")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        depok_button.button_color = "darkred"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("Jawa Barat","Depok")
                        getLockersOffline("Jawa Barat","Depok")
                        }
                    }
                }
                //6th Button
                LockerCityButton{
                    id: jakbar_button
                    button_text: qsTr("Jakarta Barat")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jakbar_button.button_color = "darkred"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("DKI Jakarta","Jakarta Barat")
                        getLockersOffline("DKI Jakarta","Jakarta Barat")
                        }
                    }
                }
                //7th Button
                LockerCityButton{
                    id: jakpus_button
                    button_text: qsTr("Jakarta Pusat")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jakpus_button.button_color = "darkred"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("DKI Jakarta","Jakarta Pusat")
                        getLockersOffline("DKI Jakarta","Jakarta Pusat")
                        }
                    }
                }
                //8th Button
                LockerCityButton{
                    id: jaksel_button
                    button_text: qsTr("Jakarta Selatan")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jaksel_button.button_color = "darkred"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("DKI Jakarta","Jakarta Selatan")
                        getLockersOffline("DKI Jakarta","Jakarta Selatan")
                        }
                    }
                }
                //9th Button
                LockerCityButton{
                    id: jaktim_button
                    button_text: qsTr("Jakarta Timur")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jaktim_button.button_color = "darkred"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jakut_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("DKI Jakarta","Jakarta Timur")
                        getLockersOffline("DKI Jakarta","Jakarta Timur")
                        }
                    }
                }
                //10th Button
                LockerCityButton{
                    id: jakut_button
                    button_text: qsTr("Jakarta Utara")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        jakut_button.button_color = "darkred"
                        jaktim_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        tangerang_button.button_color = "white"
                        //buttonGetLocker("DKI Jakarta","Jakarta Utara")
                        getLockersOffline("DKI Jakarta","Jakarta Utara")
                        }
                    }
                }
                //11th Button
                LockerCityButton{
                    id: tangerang_button
                    button_text: qsTr("Tangerang")
                    enabled: (button_color=="darkred") ? false : true
                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        tangerang_button.button_color = "darkred"
                        jakut_button.button_color = "white"
                        jaktim_button.button_color = "white"
                        jaksel_button.button_color = "white"
                        jakpus_button.button_color = "white"
                        jakbar_button.button_color = "white"
                        depok_button.button_color = "white"
                        cibinong_button.button_color = "white"
                        bogor_button.button_color = "white"
                        bekasi_button.button_color = "white"
                        bandung_button.button_color = "white"
                        //buttonGetLocker("Banten","Tangerang")
                        getLockersOffline("Banten","Tangerang")
                        }
                    }
                }
            }
        }
    }

    //Not Used
    Rectangle{
        id:notif_box_firstLoad
        x: 183
        y: 294
        width: 800
        height: 200
        visible: false
        color: "white"
        anchors.verticalCenter: parent.verticalCenter
        opacity: 0.7
        Text{
            id: txt_notif_big
            x: 198
            y: 241
            width: 569
            height: 125
            text: qsTr("Please press Locker City on The Left Side Panel")
            anchors.verticalCenter: parent.verticalCenter
            font.italic: true
            textFormat: Text.PlainText
            color: "red"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 35
            font.family: "Microsoft YaHei"
            font.bold: true

        }
        Image{
            id: img_arrow
            width: 100
            height: 50
            anchors.verticalCenter: parent.verticalCenter
            source: "img/button/white-arrow-left.png"
            x:61
            y:275

        }
    }

    HideWindow{
        id:network_error
        //visible: true

        Image {
            id: img_network_error
            x: 437
            y: 412
            width: 150
            height: 180
            source: "img/otherImages/network-offline.png"
        }

        Text {
            id: text_notif
            x: 112
            y:232
            width: 800
            height: 150
            text: qsTr("Currently we are not connected to the system, Please retry later")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:network_error_back_button
            x:373
            y:607
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    network_error_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    network_error_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }
}
