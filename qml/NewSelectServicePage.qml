import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    width: 1024
    height: 768

    property int timer_value: 60
    property var press:"0"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            take_button.show_source = "img/button/1.png"
            other_service_button.show_source = "img/button/1.png"
            drop_button.show_source = "img/button/1.png"
            select_service_back_button.show_source = "img/button/7.png"
            return_button.show_source = "img/button/10.png"
            flash_deals_button.show_source = "img/button/10.png"
            grocery_button.show_source = "img/button/10.png"
            laundry_button.show_source = "img/button/10.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            slot_handler.start_get_free_mouth_mun()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.free_mouth_result.connect(show_free_mouth_num)
    }

    Component.onDestruction: {
        root.free_mouth_result.disconnect(show_free_mouth_num)
    }

    function show_free_mouth_num(text){
        var obj = JSON.parse(text)
        extrabig_num.text = 0
        big_num.text = 0
        mid_num.text = 0
        small_num.text = 0
        extrasmall_num.text = 0
        for(var i in obj){
            if(i == "XL"){
                extrabig_num.text = obj[i]
            }
            if(i == "L"){
                big_num.text = obj[i]
            }
            if(i == "M"){
                mid_num.text = obj[i]
            }
            if(i == "S"){
                small_num.text = obj[i]
            }
            if(i == "MINI"){
                extrasmall_num.text = obj[i]
            }
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    FullWidthReminderText{
        id:please_select_service
        y:148
        remind_text:qsTr("Please Select Service")
        remind_text_size:'45'
    }

    MouseArea {
        x:725
        y:50
        width:235
        height: 75
        onClicked: {
            if(press != "0"){
                return
            }
            press = "1"
            my_timer.stop()
            my_stack_view.push(background_login_view,{"identity":"OPERATOR_USER"})
        }
    }

    SelectServiceButton{
        id:take_button
        x:134
        y:222
        show_image:"img/button/2.png"
        show_source:"img/button/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(customer_take_express_view)
            }
            onEntered:{
                take_button.show_image = "img/bottondown/button4.png"
                take_button.show_source = "img/bottondown/button3.png"
            }
            onExited:{
                take_button.show_source = "img/button/1.png"
                take_button.show_image = "img/button/2.png"
            }
        }
    }

    SelectServiceButton{
        id:drop_button
        x:416
        y:222
        show_image:"img/button/4.png"
        show_source:"img/button/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(background_login_view,{"identity":"LOGISTICS_COMPANY_USER"})
            }
            onEntered:{
                drop_button.show_image = "img/bottondown/button5.png"
                drop_button.show_source = "img/bottondown/button3.png"
            }
            onExited:{
                drop_button.show_source = "img/button/1.png"
                drop_button.show_image = "img/button/4.png"
            }
        }
    }

    BackButtonMenu{
        id:select_service_back_button
        x:699
        y:596
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            onEntered:{
                select_service_back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                select_service_back_button.show_source = "img/button/7.png"
            }
        }
    }

    SelectServiceButtonSmall{
        id:other_service_button
        x:699
        y:222
        show_image:"img/button/3.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                other_service_button.show_source = "img/bottondown/button6.png"
                other_service_button.show_image = "img/bottondown/m13.png"
            }
            onExited:{
                other_service_button.show_source = "img/button/10.png"
                other_service_button.show_image = "img/button/3.png"
            }
        }
    }

    SelectServiceButtonSmall{
        id:return_button
        x:134
        y:501
        show_image:"img/button/5.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                return_button.show_source = "img/bottondown/button6.png"
                return_button.show_image = "img/bottondown/m10.png"
            }
            onExited:{
                return_button.show_source = "img/button/10.png"
                return_button. show_image = "img/button/5.png"
            }
        }
    }

    SelectServiceButtonSmall{
        id:grocery_button
        x:322
        y:501
        show_image:"img/button/14.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                grocery_button.show_source = "img/bottondown/button6.png"
                grocery_button.show_image = "img/bottondown/m11.png"
            }
            onExited:{
                grocery_button.show_source = "img/button/10.png"
                grocery_button.show_image = "img/button/14.png"
            }
        }
    }

    SelectServiceButtonSmall{
        id:flash_deals_button
        x:699
        y:409
        show_image:"img/button/15.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                flash_deals_button.show_source = "img/bottondown/button6.png"
                flash_deals_button.show_image = "img/bottondown/m14.png"
            }
            onExited:{
                flash_deals_button.show_source = "img/button/10.png"
                flash_deals_button.show_image = "img/button/15.png"
            }
        }
    }

    SelectServiceButtonSmall{
        id:laundry_button
        x:510
        y:501
        show_image:"img/button/16.png"
        show_source:"img/button/10.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_timer.stop()
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                laundry_button.show_source = "img/bottondown/button6.png"
                laundry_button.show_image = "img/bottondown/m12.png"
            }
            onExited:{
                laundry_button.show_source = "img/button/10.png"
                laundry_button.show_image = "img/button/16.png"
            }
        }
    }

    Row {
    x: 232
    y: 717
    spacing: 20
        Text {
            id: mouth_title
            text: qsTr("Available Lockers: ")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 24
        }

        Row {
            spacing: 5
            Text {
                id: extrasmall_type
                text: qsTr("XS:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: extrasmall_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: small_type
                text: qsTr("S:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: small_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: mid_type
                text: qsTr("M:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: mid_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: big_type
                text: qsTr("L:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: big_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }

        Row {
            spacing: 5
            Text {
                id: extrabig_type
                text: qsTr("XL:")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Text {
                id: extrabig_num
                text: qsTr("0")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }
        }
    }
}
