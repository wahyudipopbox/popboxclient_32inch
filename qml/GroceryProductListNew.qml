import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQml.Models 2.1
import "promoproduct.js" as PRODUCTS

Background{
    id: base_bground
    width: 1024
    height: 768
    logo_y: 25
    property int timer_value: 60
    property var press:"0"
    property var grocery_prod_list:"None"
    property var category:"29" //"25" for general lockers, "29" for centralization lockers.
    property var maxcount:"25"
    property variant bad_chars: ["'", "\t", "\n", "\r", "`", "&"]
    property var playMode:"online"
    property var emoney:"enabled"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log("product_list : " + grocery_prod_list)
            loadingGif.open()
            main_page.visible = false
            abc.counter = timer_value
            my_timer.restart()
            if(grocery_prod_list!="None"){
                parseDataProduct(grocery_prod_list)
                loadingGif.close()
                main_page.visible = true
            }else{
                if(playMode=="offline"){
                    grocery_prod_list = PRODUCTS.get_product("promo")
                    parseDataProduct(grocery_prod_list)
                    loadingGif.close()
                    main_page.visible = true
                }else{
                    slot_handler.get_popshop_product(category,maxcount)
                }

            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            groceryItem_listModel.clear()
        }
    }

    Component.onCompleted: {
        root.get_popshop_product_result.connect(handle_result_product)
    }

    Component.onDestruction: {
        root.get_popshop_product_result.disconnect(handle_result_product)
    }

    function insert_flg(a){
        if(a != undefined){
            var newstrA=""
            newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
            return newstrA
        }else{
            return a
        }
    }

    function patch_url(b){
        var newstrB=""
        if(b != null){
            newstrB = b.replace("https://shop.popbox.asia/assets/images","img/grocery_prod")
        }else{
            return b
        }
        return newstrB
    }

    function remove_badchar(c){
        var newstrC=""
        for (var bad in bad_chars){
            if(c.indexof(bad) > -1){
                newstrC = c.replace(bad,"")
            }else{
                return
            }
        }
        return newstrC
    }

    function handle_result_product(result){
        //console.log("start_get_popshop_product_result : " + result)
        if(result == "ERROR"){
            network_error.open()
            main_page.enabled = false
        }else{
            var list_products = JSON.parse(result)
            var total_products = list_products.prod_total
            grocery_prod_list = list_products.prod_list
            parseDataProduct(grocery_prod_list)
            if(grocery_prod_list != "None" && total_products > 0){
                loadingGif.close()
                main_page.visible = true
            }else{
                loadingGif.open()
                main_page.visible = false
            }
        }
    }

    function parseDataProduct(products){
        //Undefined the gridview model
        gridview.model = undefined
        var items = products
        if(!groceryItem_listModel.count){
            for(var x in items) {
                if(parseInt(items[x].stock)>0){
                    var show_prod_sku = items[x].sku
                    var show_prod_init_price = insert_flg(items[x].price)
                    var show_prod_disc = "Save: " + insert_flg(items[x].discount)
                    var show_prod_price = insert_flg(items[x].total)
                    var show_prod_name = items[x].name
                    var url_prod_image = patch_url(items[x].image)
                    var url_prod_image_qr = patch_url(items[x].qrcode)
                    var array_product = items[x]
                    //Appending Product Items
                    groceryItem_listModel.append({"prod_name_": show_prod_name,
                                                     "prod_price_":show_prod_price,
                                                     "prod_disc_": show_prod_disc,
                                                     "prod_init_price_": show_prod_init_price,
                                                     "prod_sku_": show_prod_sku,
                                                     "prod_image_": url_prod_image,
                                                     "prod_image_qr_": url_prod_image_qr,
                                                     "array_prod_": array_product})

                    gridview.model = groceryItem_listModel
                }else{
                    console.log(items[x].sku + " -> " + items[x].name + " is not available this time!")
                }
            }
            console.log('Total Products in Category ' + category + " : " + groceryItem_listModel.count);
        }else{
            return
        }
        //Re-define the gridview model
        /*for(var i = 0; i < groceryItem_listModel.count; ++i) {
            console.log(groceryItem_listModel.get(i).prod_sku_ +" -> "+ groceryItem_listModel.get(i).prod_name_);
        }*/
    }

    function buttonGetProduct(x,y){
        abc.counter = timer_value
        my_timer.restart()
        groceryItem_listModel.clear()
        loadingGif.open()
        main_page.visible = false
        category = x
        maxcount = y
        slot_handler.get_popshop_product(category,maxcount)
    }

    LoadingView{
        id: loadingGif
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter == 30 && grocery_prod_list == "None"){
                    network_error.open()
                }
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id: main_page

        BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("Back")

            MouseArea {
                id: ms_back
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
        }

        Text {
            id: notif_select_pro
            x: 0
            y: 137
            width: 1024
            height: 60
            color: "#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("Please Select Best Product Below")
            font.pixelSize: 35
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 25
            textFormat: Text.AutoText
        }

        Item{
            id: prod_item_view
            x: 0
            y: 250
            width: 1024
            height: 500
            anchors.bottom: parent.bottom
            anchors.bottomMargin: -718
            anchors.right: parent.right
            anchors.rightMargin: -1024
            focus: true

            ListView{
                id: gridview
                spacing: 20
                orientation: ListView.Horizontal
                flickableDirection: Flickable.HorizontalFlick
                focus: true
                clip: true
                anchors.bottomMargin: 0
                anchors.topMargin: 0
                flickDeceleration: 750
                maximumFlickVelocity: 1500
                anchors.centerIn: parent
                layoutDirection: Qt.LeftToRight
                boundsBehavior: Flickable.StopAtBounds
                snapMode: GridView.SnapToRow
                anchors.fill: parent
                anchors.margins: 20
                delegate: delegate_item_view
                //model: groceryItem_listModel -> Defined on function below
            }

            ListModel {
                id: groceryItem_listModel
                dynamicRoles: true
            }

            Component{
                id: delegate_item_view
                GroceryItemViewNew{
                    id: item_view
                    prod_name: prod_name_
                    prod_price: prod_price_
                    prod_disc: prod_disc_
                    prod_init_price: prod_init_price_
                    prod_sku: prod_sku_
                    prod_image: prod_image_
                    prod_image_qr: prod_image_qr_
                    property var array_prod: array_prod_

                    MouseArea {
                        id: img_prod_click
                        anchors.fill: parent
                        onClicked: {
                            abc.counter = timer_value
                            my_timer.restart()
                            if(emoney=="enabled"){
                                var usageOf = "popshop"
                                var data_product = item_view.array_prod
                                my_stack_view.push(grocery_input_quantity, {data_prod_: data_product, usageOf: usageOf})
                            }else{
                                return
                            }
                        }
                        onPositionChanged: {
                            abc.counter = timer_value
                            my_timer.restart()
                        }
                    }
                }
            }
        }

        Text {
            id: powered_text
            x: 91
            y: 672
            width: 250
            height: 40
            color: "#ffffff"
            text: qsTr("Powered by :")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 30
            font.family: "Microsoft YaHei"
        }

        Rectangle {
            id: rec_emoney
            x: 317
            y: 654
            width: 300
            height: 77
            color: "#ffffff"
            scale: 0.8

            Image {
                id: image_emoney
                x: 0
                y: 0
                width: 300
                height: 77
                source: "img/otherservice/logo-emoney300.png"
            }
        }

        Rectangle {
            id: rec_paybyqr
            x: 571
            y: 655
            width: 300
            height: 77
            color: "#ffffff"
            scale: 0.8

            Image {
                id: image_paybyqr
                x: 0
                y: 0
                width: 300
                height: 77
                scale: 1
                fillMode: Image.PreserveAspectFit
                source: "img/otherImages/Logo_PaybyQR.png"
            }
        }
    }
}
