import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: rectangle1
    width:300
    height:60
    property var show_image:""
    color:"transparent"
    property var show_text:""
    property var bool:false



    Image{
        x:0
        y:0
        width:300
        height:60
        source:"img/courier08/08ground.png"
    }


    Image{
        x:10
        y:15
        width:25
        height:30
        source:show_image
    }

    Image{
        x:40
        y:15
        width:5
        height:30
        source:"img/courier08/08cut.png"
    }


    TextInput{
        y:10
        width: 227
        height: 39
        cursorVisible: bool
        color:"#ff0000"
        text: show_text
        clip: true
        anchors.leftMargin: 60
        echoMode :TextInput.Password
        focus: true
        font.family:"Microsoft YaHei"
        font.pixelSize:30
        anchors.left: parent.left
    }



}
