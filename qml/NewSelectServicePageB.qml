import QtQuick 2.4
import QtQuick.Controls 1.2
import QtWebKit 3.0

Background{
    id: base_bground
    width: 1024
    height: 768
    logo_y: 25
    property var first_load: "Yes"
    property var faqUrl: "http://localhost/faq/"
    property var groUrl: "http://localhost/grocery/"
    property var locker_name: ""
    property var gui_version: "1.20157.007.210217"
    property var cs_line: "Call: 021-29022537/38"
    property int loading_time: 5

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.select_language("first.qm")
            slot_handler.start_get_locker_name()
            timer_clock.restart()
            if(first_load=="Yes"){
                base_ground_opacity.visible = true
                loading_window.visible = true
                show_timer_loading.start()
                main_page.enabled = false
            }
            if(first_load=="No"){
                base_ground_opacity.visible = false
                loading_window.visible = false
                show_timer_loading.stop()
                main_page.enabled = true
            }
            take_button.show_source = "img/button/1.png"
            payment_emoney_button.show_source = "img/button/1.png"
            drop_button.show_source = "img/button/1.png"
            return_button.show_source = "img/button/10.png"
            faq_button.show_source = "img/button/10.png"
            qrshop_button.show_source = "img/button/10.png"
            popsend_button.show_source = "img/button/10.png"
            laundry_button.show_source = "img/button/10.png"
            slot_handler.start_get_free_mouth_mun()
            slot_handler.clean_user_token()
        }
        if(Stack.status==Stack.Deactivating){
            timer_clock.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_locker_name_result.connect(show_locker_name)
        root.free_mouth_result.connect(show_free_mouth_num)
        root.start_get_cod_status_result.connect(cod_result)
    }

    Component.onDestruction: {
        root.start_get_locker_name_result.disconnect(show_locker_name)
        root.free_mouth_result.disconnect(show_free_mouth_num)
        root.start_get_cod_status_result.disconnect(cod_result)
    }

    function show_free_mouth_num(text){
        var obj = JSON.parse(text)
        for(var i in obj){
            if(i == "XL"){
                extra_big_num.text = obj[i]
            }
            if(i == "L"){
                big_num.text = obj[i]
            }
            if(i == "M"){
                mid_num.text = obj[i]
            }
            if(i == "S"){
                small_num.text = obj[i]
            }
            if(i == "MINI"){
                mini_num.text = obj[i]
            }
        }
    }

    function cod_result(result){
        console.log("cod_result is ", result)
        if(result == "enabled"){
            //my_stack_view.push(other_service_emoney)
            my_stack_view.push(emoney_service_page)
        }else{
            my_stack_view.push(on_develop_view)
        }
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name = text
    }

    Rectangle{
        id: main_page
        color: "transparent"
        width: parent.width
        height: parent.height

        FullWidthReminderText{
            id:title_text
            x: 0
            y:138
            remind_text:qsTr("Silakan Pilih Layanan")
            remind_text_size:"45"
        }

        SelectLoginButton{
            x:725
            y:50
            width: 236
            height: 71

            MouseArea {
                width: parent.width
                height: parent.height
                anchors.leftMargin: 17
                anchors.topMargin: -28
                anchors.rightMargin: -17
                anchors.bottomMargin: 28
                anchors.fill: parent
                onDoubleClicked: {
                    my_stack_view.push(background_login_view,{"identity":"OPERATOR_USER"})
                }
            }
        }

        SelectServiceButton{
            id:take_button
            x:134
            y:200
            show_image:"img/button/B_01.jpg"
            show_source:"img/button/1.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(customer_take_express_view)
                }
                onEntered:{
                    take_button.show_image = "img/bottondown/red-01.jpg"
                }
                onExited:{
                    take_button.show_source = "img/button/1.png"
                    take_button.show_image = "img/button/B_01.jpg"
                }
            }
        }

        SelectServiceButton{
            id:drop_button
            x:416
            y:200
            show_image:"img/button/B_02.jpg"
            show_source:"img/button/1.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(background_login_view,{"identity":"LOGISTICS_COMPANY_USER"})
                }
                onEntered:{
                    drop_button.show_image = "img/bottondown/red-02.jpg"
                }
                onExited:{
                    drop_button.show_source = "img/button/1.png"
                    drop_button.show_image = "img/button/B_02.jpg"
                }
            }
        }

        SelectServiceButtonSmall{
            id:return_button
            x:134
            y:478
            show_image:"img/button/new_return_b.png"
            show_source:"img/button/10.png"

            MouseArea {
                anchors.bottomMargin: -16
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(reject_select_merchant_name_page)
                }
                onEntered:{
                    return_button.show_image = "img/bottondown/red-05.jpg"
                }
                onExited:{
                    return_button. show_image = "img/button/new_return_b.png"
                }
            }
        }

        SelectServiceButtonSmall{
            id:popsend_button
            x:322
            y:478
            show_image:"img/button/new_popsend_b_white.png"
            show_source:"img/button/10.png"

            MouseArea {
                anchors.bottomMargin: -16
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(send_input_memory_page)
                }
                onEntered:{
                    popsend_button.show_image = "img/button/new_popsend_b_red.png"
                }
                onExited:{
                    popsend_button.show_image = "img/button/new_popsend_b_white.png"
                }
            }
        }

        SelectServiceButtonSmall{
            id:qrshop_button
            x:510
            y:478
            show_image:"img/button/qrshopping_b_white.png"
            show_source:"img/button/10.png"

            MouseArea {
                anchors.bottomMargin: -17
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(qr_shop_info, {initurl:groUrl})
                }
                onEntered:{
                    qrshop_button.show_image = "img/button/qrshopping_b_red.png"
                }
                onExited:{
                    qrshop_button.show_image = "img/button/qrshopping_b_white.png"
                }
            }
        }

        SelectServiceButtonTiny{
            id:express_login_button
            x:699
            y:200
            height: 90
            show_image:"img/button/expressLogin_white.png"
            show_source:"img/button/1.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(express_login_view,{"identity":"LOGISTICS_COMPANY_USER"})
                }
                onEntered:{
                    express_login_button.show_image = "img/button/expressLogin_red.png"
                }
                onExited:{
                    express_login_button.show_image = "img/button/expressLogin_white.png"
                }
            }
        }

        SelectServiceButtonSmall{
            id:payment_emoney_button
            x:699
            y:293
            show_image:"img/button/B_payment_white.png"
            show_source:"img/button/10.png"

            MouseArea {
                anchors.bottomMargin: -16
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_get_cod_status()
                }
                onEntered:{
                    payment_emoney_button.show_image = "img/button/B_payment_red.png"
                }
                onExited:{
                    payment_emoney_button.show_image = "img/button/B_payment_white.png"
                }
            }
        }

        SelectServiceButtonTiny{
            id:laundry_button
            x:699
            y:480
            show_image:"img/button/tiny_laundry_white.png"
            show_source:"img/button/10.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(laundry_info_page)
                }
                onEntered:{
                    laundry_button.show_image = "img/button/tiny_laundry_red.png"
                }
                onExited:{
                    laundry_button.show_image = "img/button/tiny_laundry_white.png"
                }
            }
        }

        SelectServiceButtonTiny{
            id:faq_button
            x:699
            y:573
            show_image:"img/button/new_faq_white.png"
            show_source:"img/button/10.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(faq_page, {initurl:faqUrl})
                }
                onEntered:{
                    faq_button.show_image = "img/button/new_faq_red.png"
                }
                onExited:{
                    faq_button.show_image = "img/button/new_faq_white.png"
                }
            }
        }

        Row{
            id: language_button
            width: 80
            height: 80
            anchors.horizontalCenterOffset: -40
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 0

            Rectangle{
                id:ina_button
                color: "#ab312e"
                width: parent.width
                height: parent.height

                Image{
                    scale: 0.9
                    source:"img/indonesia_flag_100.png"
                    fillMode: Image.PreserveAspectFit
                    anchors.fill: parent
                }

            }

            Rectangle{
                id: eng_button
                color: "white"
                width: parent.width
                height: parent.height

                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        first_load = "No"
                        my_stack_view.push(new_select_service_e_view,{first_load:first_load})
                    }
                }

                AnimatedImage {
                    scale: 0.9
                    source: "img/uk_flag_gif.gif"
                    fillMode: Image.PreserveAspectFit
                    anchors.fill: parent

                }
            }
        }

        Row {
            id: available_lockers_info_text
            x: 240
            y: 677
            anchors.horizontalCenterOffset: -18
            spacing: 20
            anchors.horizontalCenter: main_page.horizontalCenter

            Text {
                id: mouth_title
                text: qsTr("Loker Tersedia : ")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 24
            }

            Row {
                spacing: 5
                Text {
                    id: mini_type
                    text: qsTr("XS :")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: mini_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: small_type
                    text: qsTr("S :")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: small_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: mid_type
                    text: qsTr("M:")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: mid_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: big_type
                    text: qsTr("L:")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: big_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }
            }

            Row {
                spacing: 5
                Text {
                    id: extra_big_type
                    text: qsTr("XL:")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

                Text {
                    id: extra_big_num
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                }

            }
        }
    }

    Rectangle{
        id: base_ground_opacity
        visible: false
        color: "black"
        opacity: 0.75
        anchors.fill: parent
    }

    Rectangle{
        id: loading_window
        //visible: false
        x: 66
        y: 129
        color: "white"
        opacity: 0.8
        width: 700
        height: 400
        border.width: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text {
            id: please_wait_text
            x: 137
            y: 30
            width: 400
            height: 35
            text: " Loading Modules..."
            wrapMode: Text.WordWrap
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:35
            color:"#c50808"
        }

        Rectangle{
            id: timer_rec
            width: 10
            height: 10
            x:0
            y:0
            visible: false
            QtObject{
                id:time_loading
                property int counter
                Component.onCompleted:{
                    time_loading.counter = loading_time
                }
            }
            Timer{
                id:show_timer_loading
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    time_loading_text.text = time_loading.counter
                    time_loading.counter -= 1
                    if(time_loading.counter < 0){
                        show_timer_loading.stop()
                        base_ground_opacity.visible = false
                        loading_window.visible = false
                        main_page.enabled = true
                        first_load = "No"
                    }
                }
            }
        }

        AnimatedImage{
            id: loading_image
            width: 200
            height: 200
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
            source: "img/item/progress-circle.gif"

            Text {
                id:time_loading_text
                anchors.centerIn: parent
                width: 50
                height: 50
                font.family:"Microsoft YaHei"
                color:"#c50808"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                textFormat: Text.PlainText
                font.pointSize:35
                wrapMode: Text.WordWrap
            }
        }
        Text {
            id: show_gui_version
            x: 137
            y: 345
            width: 400
            height: 35
            text: 'GUI Ver. ' + gui_version
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:25
            color:"#c50808"
        }
    }

    /*WebView{
        id: webviewFAQ
        visible: false
        enabled: false
        url: faqUrl
    }*/

    Rectangle{
        id: addional_info
        color: "transparent"

        Text {
            id: locker_name_text
            x: 19
            y: 730
            width: 300
            height: 24
            text: locker_name
            font.bold: false
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:18
            color:"#ffffff"
        }

        Text {
            id: cs_line_text
            x: 706
            y: 730
            width: 300
            height: 24
            text: cs_line
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:18
            color:"#ffffff"
        }

    Rectangle{
        id: black_opacity
        color: "black"
        opacity: 0.4
        width: 260
        height: 50
        x: 0
        y: 34
        }

    Text {
        id: timeText
        x: 13
        y: 59
        height: 20
        text: new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:18
        color:"#ffffff"
    }

    Text {
        id: dateText
        x: 13
        y: 39
        height: 20
        text: new Date().toLocaleDateString(Qt.locale("id_ID"), Locale.LongFormat)
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:18
        color:"#ffffff"
    }

    Timer {
        id: timer_clock
        interval: 1000
        repeat: true
        running: true
        onTriggered:
        {
            timeText.text = new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
        }
    }

    }

}
