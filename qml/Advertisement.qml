import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_root
    color: "transparent"
    property var img_path: "../advertisement/source/"
    property variant qml_pic: ["09_popbox-adalah.png"]
    property string pic_source: ""
    property int num_pic: 0
    property var locker_group: ["Indofood Tower"]
    property bool isIndofood: false

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_locker_name()
            //init_ad_images()
        }
        if(Stack.status==Stack.Deactivating){
            slider_timer.stop()
        }
    }

    Component.onCompleted: {
        slot_handler.start_get_locker_name()
        root.start_get_locker_name_result.connect(define_indofood)
        //init_ad_images()
    }

    Component.onDestruction: {
        root.start_get_locker_name_result.disconnect(define_indofood)
        slider_timer.stop()
    }

    function define_indofood(text){

        if(text == ""){
            return
        }
        if(locker_group.indexOf(text) > -1){
            isIndofood = true
        }else{
            isIndofood = false
        }
        init_ad_images()
    }


    function init_ad_images(){
        if(isIndofood){
            qml_pic = ["04_khawatir-barang-rusak.png","04_terima-paket-gak-pake-lama.png", "05_sibuk-kerja.png",
                       "06_kirim_ambil-barang.png","07_kirim-barang-hanya-dgn-sentuhan-jari.png", "08_nunggu-kurir-lama.png",
                       "09_popbox-adalah.png", "10_popsend.png", "12_bisa-isi-pulsa-disini.png",
                       "14_popbox-agent.png", "15_chitato.jpg", "16_indoeskrim.jpg", "17_Indomilk-boboiboy.jpg", "18_Lays.jpg"]
        }else{
            qml_pic = ["04_khawatir-barang-rusak.png","04_terima-paket-gak-pake-lama.png", "05_sibuk-kerja.png",
                       "06_kirim_ambil-barang.png","07_kirim-barang-hanya-dgn-sentuhan-jari.png", "08_nunggu-kurir-lama.png",
                       "09_popbox-adalah.png", "10_popsend.png", "12_bisa-isi-pulsa-disini.png", "14_popbox-agent.png"]
        }
        loader.sourceComponent = loader.Null
        pic_source = img_path + qml_pic[0]
        loader.sourceComponent = component
        slider_timer.start()
    }


    Timer{
        id:slider_timer
        interval:10000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            if(num_pic < qml_pic.length){
                num_pic += 1
                if(num_pic == qml_pic.length){
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[0]
                    loader.sourceComponent = component
                    num_pic = 0
                }else{
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[num_pic]
                    loader.sourceComponent = component
                }
            }
        }
    }

    Component {
        id: component
        Rectangle {
            AnimatedImage {
                id:ad_pic
                x: 0
                y: 0
                width: parent_root.width
                height: parent_root.height
                source: pic_source
                fillMode: Image.PreserveAspectFit
            }
        }
    }

    Loader { id: loader }

}

