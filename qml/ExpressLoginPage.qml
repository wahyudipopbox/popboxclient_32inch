import QtQuick 2.4
import QtQuick.Controls 1.2

Background{

    property var press:"0"
    property int timer_value: 60
    property variant pref_custom : ["KNK", "GRA", "PIC", "TYK", "LOT", "VCS"]

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            courier_login_username.bool = true
            courier_login_psw.bool = false
            card_id.forceActiveFocus()
            if(courier_login_username.show_text!="" || courier_login_psw.show_text!=""){
                courier_login_username.show_text=""
                courier_login_psw.show_text=""
                ch=1
            }
            courier_login_button.show_source = "img/courier08/08ground.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    function wrong_button(str){
        if(str=="Yes"){
            main_page.enabled = false
            usage_error.open()
        }else{
            return
        }
    }

    function continue_process(str){
        if(str=="Yes"){
            slot_handler.start_video_capture(courier_login_username.show_text)
            slot_handler.background_login(courier_login_username.show_text,courier_login_psw.show_text,identity)
        }else{
            return
        }
    }

    property var identity:""
    property var ch:1

    Rectangle{
    id:main_page
    Component.onCompleted: {
        root.user_login_result.connect(handle_result)
    }

    Component.onDestruction: {
        root.user_login_result.disconnect(handle_result)
    }

    function handle_result(result){
        main_page.enabled = true
        waiting.close()
        if(result == "Success"){
            if(identity == "LOGISTICS_COMPANY_USER"){
                my_stack_view.push(courier_service_view, {fromExpress:"Yes",c_access:"limited",pref_login_user:courier_login_username.show_text.substring(0,3)})
            }
            if(identity == "OPERATOR_USER"){
                my_stack_view.push(manager_service_view)
            }
        }
        if(result == "Failure"){
            my_stack_view.push(courier_psw_error_view)
        }
        if(result == "NoPermission"){
            my_stack_view.push(courier_psw_error_view)
        }
        if(result == "NetworkError"){
            main_page.enabled = false
            network_error.open()
        }
    }

    CourierInput{
        id:courier_login_username
        x:150
        y:270
        show_image:"img/courier08/08user1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=1
                courier_login_username.bool = true
                courier_login_psw.bool = false
            }
        }
    }

    CourierInputPsw{
        id:courier_login_psw
        x:455
        y:270
        show_image:"img/courier08/08pass1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=2
                courier_login_username.bool = false
                courier_login_psw.bool = true
            }
        }
    }

    CourierLoginButton{
        id:courier_login_button
        x:760
        y:270

        MouseArea{
            anchors.fill:parent
            onClicked:{
                if(press != "0"){
                    return
                }
                if(pref_custom.indexOf(courier_login_username.show_text.substring(0,3)) > -1){
                    continue_process("Yes")
                    press = "1"
                    main_page.enabled = false
                    waiting.open()
                }else{
                    wrong_button("Yes")
                }
            }
            onEntered:{
                courier_login_button.show_source = "img/bottondown/login_down.png"
            }
            onExited:{
                courier_login_button.show_source = "img/courier08/08ground.png"
            }
        }
    }

    FullKeyboard{
        id:customer_take_express_keyboard
        x:29
        y:360

        property var count:0
        property var validate_c

        Component.onCompleted: {
            customer_take_express_keyboard.letter_button_clicked.connect(input_text)
            customer_take_express_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function on_function_button_clicked(str){
            abc.counter = timer_value
            if(str == "ok"){
                if(ch == 1){
                    if(courier_login_username.show_text != "" && courier_login_psw.show_text != ""){
                        if(press!="0"){
                            return
                        }
                        if(pref_custom.indexOf(courier_login_username.show_text.substring(0,3)) > -1){
                            continue_process("Yes")
                            press = "1"
                            abc.counter = timer_value
                            my_timer.stop()
                            main_page.enabled = false
                            waiting.open()
                        }else{
                            wrong_button("Yes")
                        }
                    }else{
                        ch = 2
                        courier_login_username.bool = false
                        courier_login_psw.bool = true
                    }
                }else if(ch == 2){
                    if(courier_login_username.show_text != "" && courier_login_psw.show_text != ""){
                        if(press!="0"){
                            return
                        }
                        if(pref_custom.indexOf(courier_login_username.show_text.substring(0,3)) > -1){
                            continue_process("Yes")
                            press = "1"
                            abc.counter = timer_value
                            my_timer.stop()
                            main_page.enabled = false
                            waiting.open()
                        }else{
                            wrong_button("Yes")
                        }
                    }else{
                        ch = 1
                        courier_login_username.bool = true
                        courier_login_psw.bool = false
                    }
                }
            }
        }

        function input_text(str){
            abc.counter = timer_value
            my_timer.restart()

            if(ch==1){
                if (str == "" && courier_login_username.show_text.length > 0){
                    courier_login_username.show_text.length--
                    courier_login_username.show_text=courier_login_username.show_text.substring(0,courier_login_username.show_text.length-1 )
                }
                if (str != "" && courier_login_username.show_text.length< 14){
                    courier_login_username.show_text.length++
                }
                if (courier_login_username.show_text.length>=14){
                    str=""
                    courier_login_username.show_text.length=14
                }else{
                    courier_login_username.show_text += str
                }
            }

            if(ch==2){
                if (str == "" && courier_login_psw.show_text.length > 0){
                    courier_login_psw.show_text.length--
                    courier_login_psw.show_text=courier_login_psw.show_text.substring(0, courier_login_psw.show_text.length-1)
                }
                if (str != "" && courier_login_psw.show_text.length < 14){
                    courier_login_psw.show_text.length++
                }
                if (courier_login_psw.show_text.length>=14){
                    str=""
                    courier_login_psw.show_text.length=14
                }else{
                    courier_login_psw.show_text += str
                }
            }
        }
    }
}

    HideWindow{
        id:waiting
        //visible: true

        Image {
            id: img_time_waiting
            x: 437
            y: 400
            width: 150
            height: 200
            source: "img/otherImages/loading.png"
        }

        Text {
            x: 0
            y:300
            width: 1024
            height: 60
            text: qsTr("Please wait")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }
    }

    HideWindow{
        id:network_error
        //visible: true

        Image {
            id: img_network_error
            x: 437
            y: 412
            width: 150
            height: 180
            source: "img/otherImages/network-offline.png"
        }

        Text {
            id:text_field_error
            x: 0
            y:241
            width: 1024
            height: 150
            text: qsTr("Network error, Please retry later")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:network_error_back_button
            x:373
            y:607
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(courier_login_username.show_text != "" || courier_login_psw.show_text != ""){
                        //courier_login_username.show_text = ""
                        courier_login_psw.show_text = ""
                        ch=2
                        courier_login_username.bool = false
                        courier_login_psw.bool = true
                    }
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    network_error.close()
                }
                onEntered:{
                    network_error_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    network_error_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:usage_error
        //visible: true

        Image {
            id: img_usage_error
            x: 123
            y: 320
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            id:text_usage_error
            x: 337
            y:345
            width: 598
            height: 150
            text: qsTr("Oops, Please ensure You are using the correct button menu.")
            font.family:"Microsoft YaHei"
            wrapMode: Text.WordWrap
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:usage_error_back_button
            x:365
            y:581
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Row {
        x: 154
        y: 271
        Text {
            id: title
            text: qsTr("CARDID:")
            font.pixelSize: 30
            visible: false
        }

        TextInput {
            id: card_id
            width: 80
            height: 20
            text: qsTr("")
            autoScroll: false
            focus: true
            font.pixelSize: 30
            visible: false
            onEditingFinished: {
                if(card_id.text==""){
                    return
                }
                else{
                    slot_handler.start_get_card_info(card_id.text, identity)
                    card_id.text = ""
                }
            }
        }
    }

    Image {
        id: express_logo
        x: 45
        y: 44
        width: 187
        height: 91
        scale: 1.4
        source: "img/express-logo.png"
    }
}
