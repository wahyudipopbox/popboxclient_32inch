import QtQuick 2.4
import QtQuick.Controls 1.2

Background{

    property var info:""
    property var take_name:""
    property var take_phone:""
    property var company:""
    property var company_type:""
    property int timer_value: 60
    property var press : "0"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component.onCompleted: {
        handle_text()
    }

    function handle_text(){
        var express_info = JSON.parse(info)
        //take_phone =
    }

    OverTimeButton{
        x:200
        y:600
        show_text:qsTr("back to menu")
        show_x:15
        show_image:"img/05/back.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 3) return true }))
            }
        }
    }

    OverTimeButton{
        x:514
        y:600
        show_text:qsTr("ok")
        show_x:205
        show_image:"img/05/ok.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(customer_send_express_view)
            }
        }
    }

    Text {
        id: text1
        x: 228
        y: 210
        width: 200
        height: 50
        text: qsTr("收件人姓名:")
        textFormat: Text.PlainText
        font.family: "Courier"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.pixelSize: 30
    }

    Text {
        id: text2
        x: 228
        y: 283
        width: 200
        height: 50
        text: qsTr("收件人手机号:")
        font.pixelSize: 30
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.family: "Courier"
        textFormat: Text.PlainText
    }

    Text {
        id: text3
        x: 228
        y: 351
        width: 200
        height: 50
        text: qsTr("收件人地址:")
        font.pixelSize: 30
        font.family: "Courier"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        textFormat: Text.PlainText
    }

    Text {
        id: text4
        x: 228
        y: 420
        width: 200
        height: 50
        text: qsTr("快递公司:")
        font.pixelSize: 30
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.family: "Courier"
        textFormat: Text.PlainText
    }

    Text {
        id: text5
        x: 228
        y: 490
        width: 200
        height: 50
        text: qsTr("快递公司业务名称:")
        font.pixelSize: 30
        font.family: "Courier"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        textFormat: Text.PlainText
    }

    Text {
        id: text6
        x: 567
        y: 210
        width: 200
        height: 50
        text: qsTr("价格说明:")
        font.pixelSize: 30
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.family: "Courier"
        textFormat: Text.PlainText
    }

    Text {
        id: text7
        x: 567
        y: 283
        width: 200
        height: 50
        text: qsTr("11111111")
        font.pixelSize: 30
        font.family: "Courier"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        textFormat: Text.PlainText
    }

    Text {
        id: text8
        x: 567
        y: 351
        width: 200
        height: 50
        text: qsTr("22222222")
        font.pixelSize: 30
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.family: "Courier"
        textFormat: Text.PlainText
    }

    Text {
        id: text9
        x: 567
        y: 420
        width: 200
        height: 50
        text: qsTr("333333333")
        font.pixelSize: 30
        font.family: "Courier"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        textFormat: Text.PlainText
    }

    Text {
        id: text10
        x: 567
        y: 490
        width: 200
        height: 50
        text: qsTr("4444444444")
        font.pixelSize: 30
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.family: "Courier"
        textFormat: Text.PlainText
    }

}
