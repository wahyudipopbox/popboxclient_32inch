import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: popsend_select_denom
    width: 1024
    height: 768
    property int timer_value: 60
    property var cust_name:""
    property var member_data
    property var data_button:'[{"name": "25k","type": "terminal","price": "25000","discount": "15%","nominal": "28750"},
    {"name": "50k","type": "web|mobile|terminal","price": "50000","discount": "15%","nominal": "57500"},
    {"name": "100k","type": "web|mobile|terminal","price": "100000","discount": "15%","nominal": "115000"},
    {"name": "200k","type": "web|mobile|terminal","price": "200000","discount": "15%","nominal": "230000"},
    {"name": "500k","type": "web|mobile","price": "500000","discount": "15%","nominal": "575000"}]'
    property int qty_button: 0
    property var getMethod:"online"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('handle_popsend_member : ' + JSON.stringify(member_data))
            loadingGif.open()
            button_listModel.clear()
            define_member(member_data)
            //Defining data get method
            if(getMethod=="offline"){
                define_button(data_button)
            }else{
                slot_handler.get_popsend_button()
            }
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.get_popsend_button_result.connect(parse_button_list)
    }

    Component.onDestruction: {
        root.get_popsend_button_result.disconnect(parse_button_list)
    }

    function define_member(member_data){
        var info = JSON.parse(member_data)
        cust_name = info.member_name
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function parse_button_list(result){
        console.log("start_get_popsend_button_result : " + result)
        if(result == "ERROR"){
            network_error.open()
            main_page.enabled = false
        }else{
            var xObj = JSON.parse(result)
            qty_button = xObj.total_button
            data_button = xObj.data_button
            if(data_button.length > 0){
                loadingGif.close()
                main_page.enabled = true
                buttons.visible = true
            }
            define_button(data_button)
        }
    }

    function define_button(array){
        //console.log("parse_popsend_button : " + JSON.stringify(array))
        //Detaching Gridview model
        gridview.model = undefined
        if(getMethod=="online"){
            var list_buttons = array
        }else{
            list_buttons = JSON.parse(array)
        }
        for(var x in list_buttons){
            var but_name = list_buttons[x].name
            var but_price = list_buttons[x].price
            var but_denom = list_buttons[x].nominal
            if(list_buttons[x].discount=="" || list_buttons[x].discount=="0" || list_buttons[x].discount=="0%"){
                var but_disc =  "-"
            }else{
                but_disc = list_buttons[x].discount
            }
            var but_type = list_buttons[x].type
            if(but_type.indexOf("terminal") > -1){
                //Appending button list if type "terminal"
                button_listModel.append({"but_name_":but_name, "but_price_":but_price,
                                        "but_denom_":but_denom, "but_disc_":but_disc})
                //Attaching Gridview model
                gridview.model = button_listModel
            }else{
                return
            }
        }
        console.log('Total Buttons : ' + button_listModel.count);
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter == 30 && button_listModel.count < 1){
                    network_error.open()
                }
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }

    Rectangle{
        id: main_page
        Text {
            id: greeting_text
            x: 37
            y: 115
            width: 200
            height: 40
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            text: 'Hi, ' + cust_name
            verticalAlignment: Text.AlignVCenter
            textFormat: Text.PlainText
            font.pointSize:24
            font.capitalization: Font.Capitalize
        }

        Text {
            id: text_select_denom
            x: 0
            y: 167
            width: 500
            height: 60
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            text: qsTr("Please Choose Denom :")
            anchors.horizontalCenterOffset: 508
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            textFormat: Text.PlainText
            font.pointSize:30
        }

        Item{
            id: buttons
            y: 254
            width: 1000
            height: 300
            anchors.horizontalCenterOffset: 508
            anchors.horizontalCenter: parent.horizontalCenter
            focus: true

            GridView{
                id: gridview
                width: 900
                anchors.leftMargin: 70
                anchors.bottomMargin: 0
                anchors.topMargin: 0
                flickDeceleration: 750
                maximumFlickVelocity: 1500
                anchors.centerIn: parent
                layoutDirection: Qt.LeftToRight
                flow: GridView.FlowLeftToRight
                boundsBehavior: Flickable.StopAtBounds
                cacheBuffer: 500
                keyNavigationWraps: true
                snapMode: GridView.SnapToRow
                anchors.fill: parent
                anchors.margins: 50
                clip: true
                focus: true
                delegate: delegate_button
                //model: button_listModel
                cellWidth: 215
                cellHeight: 225
            }

            ListModel {
                id: button_listModel
            }

            Component{
                id: delegate_button

                PopsendButton{
                    id: id_button
                    but_name: but_name_
                    but_price: but_price_
                    but_denom: insert_flg(but_denom_)
                    but_disc: but_disc_

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            my_stack_view.push(popsend_express_info,{member_data: member_data,
                                                   amount: but_price_, denom: but_denom_})
                        }
                        onEntered: {
                            id_button.base_color = "silver"
                            id_button.base_text = "black"
                        }
                    }
                }
            }
        }

        Text {
            id: powered_text
            x: 228
            y: 638
            width: 250
            height: 40
            color: "#ffffff"
            text: qsTr("Powered by :")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 30
            font.family: "Microsoft YaHei"
        }

        Rectangle {
            id: rec_emoney
            x: 488
            y: 619
            width: 300
            height: 77
            color: "#ffffff"

            Image {
                id: image_emoney
                x: 0
                y: 0
                width: 300
                height: 77
                source: "img/otherservice/logo-emoney300.png"
            }
        }
    }

    LoadingView{id: loadingGif}

    HideWindow{
        id:network_error
        //visible: true

        Image {
            id: img_network_error
            x: 437
            y: 412
            width: 150
            height: 180
            source: "img/otherImages/network-offline.png"
        }

        Text {
            id: text_notif
            x: 112
            y:232
            width: 800
            height: 150
            text: qsTr("Currently we are not connected to the system, Please retry later")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:network_error_back_button
            x:373
            y:607
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    network_error_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    network_error_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }

}
