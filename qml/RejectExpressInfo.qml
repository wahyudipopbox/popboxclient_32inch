import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    width: 1024
    height: 768
    property int timer_value: 60
    property var show_package_id:""
    property var show_recipient:""
    property var show_address:""
    property var show_sender_no:""
    property var show_locker_name: "value"
    property string reject_data:'{"id":"f37fa91cf7a211e4b7926c40088b8482","customerStoreNumber":"P201500010001","expressType":"CUSTOMER_STORE","storeUser":{"id":"ace76ad2f7a211e49b736c40088b8482"},"recipientName":"高阳","takeUserPhoneNumber":"18566691650","startAddress":{"id":"48eb7c50f7a311e49ec16c40088b8482","region":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"高新产业园"},"endAddress":{"id":"2494f228f7a311e4bb216c40088b8482","region":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"任达科技园"},"rangePrice":{"id":"a4ded9a6f7a011e48bfb6c40088b8482","businessType":{"id":"ca6ef91cf7a011e490516c40088b8482","name":"今日送"},"startRegion":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"endRegion":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"firstHeavy":5000,"firstPrice":1000,"continuedHeavy":1000,"continuedPrice":200},"version":0,"status":"IMPORTED"}'
    property string chargeType:"BY_WEIGHT"
    property string designationSize: "S"
    property int payOfAmount:0

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_electronic_commerce_reject_express()
            //show_return_data(reject_data)
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component.onCompleted: {
        // root.choose_mouth_result.connect(handle_text)
        // root.free_mouth_by_size_result.connect(check_free_mouth)
        root.reject_express_result.connect(show_return_data)
    }

    Component.onDestruction: {
        // root.choose_mouth_result.disconnect(handle_text)
        // root.free_mouth_by_size_result.disconnect(check_free_mouth)
        root.reject_express_result.disconnect(show_return_data)
    }

    function show_return_data(text){
        if(text==""){
            return
        }
        var a = JSON.parse(text)
        show_package_id = a.package_id
        show_recipient = a.electronicCommerce.name
        show_address = a.electronicCommerce.address
        show_sender_no = a.phone_number
        show_locker_name = a.box_name
        chargeType = a.chargeType
        if (show_recipient=="Blibli"){
            show_recipient = show_recipient + ".com"
        }
        if(chargeType == "FIXED_VALUE"){
            payOfAmount = a.payOfAmount
            designationSize = a.designationSize
        }
        /* if(a.chargeType == "NOT_CHARGE"){
            designationSize = a.designationSize
        }*/
    }

    /*function check_free_mouth(text){
        if(text == "Failure"){
            ok_button.enabled = false
            back_button.enabled = false
            not_mouth.open()
        }
        if(text == "Success"){
            slot_handler.start_pull_pre_pay_cash_for_customer_reject_express(payOfAmount)
            my_stack_view.push(reject_pay_ment_page,{designationSize:designationSize})
        }
    }

    function handle_text(text){
        if(text == 'Success'){
            my_stack_view.push(reject_Door_open_page)
        }
        if(text == 'NotMouth'){
            ok_button.enabled = false
            back_button.enabled = false
            not_mouth.open()
        }
    }*/

    Text {
        id: detail_return
        x: 312
        y: 150
        width: 1024
        height: 50
        color: "#ffffff"
        text: qsTr("Return Details")
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 40
        horizontalAlignment: Text.AlignHCenter
        font.bold: false
        font.family: "Microsoft YaHei"
    }

    GroupBox {
        id: group_text
        x: 132
        y: 218
        width: 750
        height: 400
        flat: true
        checkable: false
        checked: false
        //title: qsTr("Group Box")

        Text {
            y: 4
            width: 250
            height: 50
            id: package_ID
            x: 54
            text: qsTr("Package ID:")
            horizontalAlignment: Text.AlignRight
            anchors.right: package_id_show.left
            anchors.rightMargin: 38
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
        }

        Text {
            id: package_id_show
            x: 324
            y: 4
            width: 400
            height: 50
            text: show_package_id
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
        }

        Text {
            x: 54
            y: 74
            width: 250
            height: 50
            id: recipient
            text: qsTr("Onlineshop:")
            anchors.right: recipient_show.left
            anchors.rightMargin: 38
            horizontalAlignment: Text.AlignRight
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
        }

        Text {
            id: recipient_show
            x: 324
            y: 74
            width: 400
            height: 50
            text: show_recipient
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
        }

        Text {
            x: 54
            y: 214
            width: 250
            height: 50
            id: locker_name
            text: qsTr("Locker Location:")
            horizontalAlignment: Text.AlignRight
            anchors.right: locker_name_show.left
            anchors.rightMargin: 38
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
        }

        Text {
            id: locker_name_show
            x: 324
            y: 214
            width:450
            height: 50
            text: show_locker_name
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
            wrapMode: Text.WordWrap
        }

        Text {
            x: 54
            y: 144
            width: 250
            height: 50
            id: sender_no
            text: qsTr("Sender No:")
            anchors.right: sender_show.left
            anchors.rightMargin: 38
            horizontalAlignment: Text.AlignRight
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
        }

        Text {
            id: sender_show
            x: 324
            y: 144
            width:400
            height: 50
            text: show_sender_no
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 32
            wrapMode: Text.WordWrap
        }
    }

    DoorButton{
        id:back_button
        y:610
        x:178
        show_text:qsTr("cancel")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id:ok_button
        y:610
        x:564
        show_text:qsTr("OK")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(chargeType == "BY_WEIGHT"){
                    my_stack_view.push(reject_express_weigh_page)
                }
                if(chargeType == "FIXED_VALUE"){
                    if(payOfAmount == 0){
                        no_payOfAmount.open()
                    }
                    if(payOfAmount != 0){
                        slot_handler.start_free_mouth_by_size(designationSize)
                    }
                }
                if(chargeType == "NOT_CHARGE"){
                    my_stack_view.push(reject_select_box_size_page)
                }
            }
            onEntered:{
                ok_button.show_image = "img/door/2.png"
            }
            onExited:{
                ok_button.show_image = "img/door/1.png"
            }
        }
    }

    HideWindow{
        id:not_mouth
        Text {
            x: 92
            y:330
            width: 850
            height: 112
            text: qsTr("not_mouth")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:not_mouth_back
            x:350
            y:560
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ok_button.enabled = true
                    back_button.enabled = true
                    not_mouth.close()
                }
                onEntered:{
                    not_mouth_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    not_mouth_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:no_payOfAmount
        Text {
            x: 92
            y:330
            width: 850
            height: 112
            text: qsTr("no_payOfAmount")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:no_payOfAmount_back
            x:350
            y:560
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ok_button.enabled = true
                    back_button.enabled = true
                    no_payOfAmount.close()
                }
                onEntered:{
                    no_payOfAmount_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    no_payOfAmount_back.show_source = "img/button/7.png"
                }
            }
        }
    }
}
