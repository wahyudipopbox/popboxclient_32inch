import logging
import sys
import threading
import Configurator
__author__ = 'gaoyang'
import sqlite3
lock = threading.Lock()
logger = logging.getLogger()

if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
    localDB = 'pakpobox.db'
else:
    try:
        localDB = Configurator.get_value('ClientInfo', 'dbName')
    except ValueError:
        localDB = 'popboxclient.db'

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]

    return dict(((k, v) for k, v in d.items() if v is not None))

def get_conn():
    conn = sqlite3.connect(sys.path[0] + '/database/' + localDB)
    conn.row_factory = dict_factory
    return conn

def get_result_set(sql, parameter):
    try:
        lock.acquire()
        logger.info((sql, parameter))
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql, parameter)
        result = cursor.fetchall()
        logger.info(result)
    finally:
        lock.release()
    return result

def insert_or_update_database(sql, parameter):
    try:
        lock.acquire()
        logger.info((sql, parameter))
        conn__ = get_conn()
        conn__.execute(sql, parameter)
        conn__.commit()
    finally:
        lock.release()

def init_database():
    try:
        lock.acquire()
        conn__ = get_conn()
        with open(sys.path[0] + '/database/ClientDatabase.sql') as f:
            conn__.cursor().executescript(f.read())
    finally:
        lock.release()

if __name__ == '__main__':
    __conn = sqlite3.connect(localDB)
    with open('ClientDatabase.sql') as f:
        __conn.cursor().executescript(f.read())