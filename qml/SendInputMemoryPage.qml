import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:user_send_memory_input
    width: 1024
    height: 768
    property var show_text:""
    property int timer_value: 60
    property var press: "0"
    property var show_text_count:0
    property variant pref_laundry : ["KNK", "TYK", "LOT", "TAP", "OMS"]
    property var locker_name: ""

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_locker_name()
            if(user_send_memory_input.show_text!=""){
                user_send_memory_input.show_text=""
                show_text_count = 0
            }
            slot_handler.start_courier_scan_barcode()
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            slot_handler.stop_courier_scan_barcode()
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
    }

Rectangle{
    id:main_page
    Component.onCompleted: {
        root.barcode_result.connect(handle_text)
        root.customer_store_express_result.connect(handle_store_express_result)
        root.start_get_locker_name_result.connect(show_locker_name)
    }

    Component.onDestruction: {
        root.barcode_result.disconnect(handle_text)
        root.customer_store_express_result.disconnect(handle_store_express_result)
        root.start_get_locker_name_result.disconnect(show_locker_name)
    }

    function show_locker_name(text) {
        if (text == "") {
            return
        }
            locker_name = text
    }

    function handle_store_express_result(text){
        if(text != "False"){
            slot_handler.set_express_number(show_text)
            my_stack_view.push(send_express_info, {send_data:text, memory:show_text, locker_name:locker_name})
        }
        else{
            main_page.enabled = false
            no_record.open()
        }
    }

    function handle_text(text){
        show_text = text
        touch_keyboard.count = text.length
    }

    FullWidthReminderText{
        id:text
        y:125
        remind_text:qsTr("Scan Barcode or Enter Parcel Code")
        remind_text_size:"35"
    }

   Rectangle{
       x:202
       y:190
       width:620
       height:70
       color:"transparent"

    Image{
        width:620
        height:70
        source:"img/courier11/input1.png"
    }

    TextEdit{
        y:10
        x:20
        font.family:"Microsoft YaHei"
        text:show_text
        color:"#FFFFFF"
        font.pixelSize:40
    }
}
    Image{
        x: 412
        y: 280
        width:199
        height:64
        source:"img/button/barcode.png"
    }

    FullKeyboard{
        id:touch_keyboard
        x:29
        y:360
        property var count:show_text_count
        property var validate_code:""

        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(show_validate_code)
            touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function on_function_button_clicked(str){
            if(str == "ok"){
                console.log('prefix_awb_number : ', user_send_memory_input.show_text.substring(0,3))
                if(press != "0"){
                    return
                }
                press = "1"
                if(user_send_memory_input.show_text == ""){
                    main_page.enabled = false
                    error_tips.open()
                }
                if(pref_laundry.indexOf(user_send_memory_input.show_text.substring(0,3)) > -1){
                    main_page.enabled = false
                    non_popsend_notif.open()
                }
                else{
                    count = 0
                    slot_handler.start_video_capture("enduser_popsend" + "_" + show_text)
                    slot_handler.start_get_customer_store_express_info(show_text)
                }
            }

            if(str=="delete"){
                if(count>=20){
                    count=19
                }
            }
        }

        function show_validate_code(str){
            if (str == "" && count > 0){
                if(count>=20){
                    count=20
                }
                count--
                user_send_memory_input.show_text=user_send_memory_input.show_text.substring(0,count);
            }
            if (str != "" && count < 20){
                count++
            }
            if (count>=20){
                str=""
            }
            else{
                user_send_memory_input.show_text += str
            }
            abc.counter = timer_value
            my_timer.restart()
        }
    }
}

    HideWindow{
        id:error_tips
        //visible: true

        Text {
            x: 112
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Oops...")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: text_error
            x: 120
            y:300
            width: 809
            height: 250
            text: qsTr("We detected the parcel number is not entered yet. Please retry and enter the correct parcel number.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:ok_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    user_send_memory_input.show_text=""
                    show_text_count = 0
                    touch_keyboard.count = 0
                    main_page.enabled = true
                    error_tips.close()
                }
                onEntered:{
                    ok_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    ok_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:non_popsend_notif
        //visible: true

        Text {
            x: 112
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Oops...")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: text_error1
            x: 120
            y:300
            width: 809
            height: 250
            text: qsTr("We detected the parcel number is not correct. Please contact our CS line at 021-29022537/38 for assistance.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:non_popsend_notif_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    user_send_memory_input.show_text=""
                    show_text_count = 0
                    touch_keyboard.count = 0
                    main_page.enabled = true
                    non_popsend_notif.close()
                }
                onEntered:{
                    non_popsend_notif_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    non_popsend_notif_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:no_record
        //visible: true

        Text {
            x: 112
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Dear Customer")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: text_error2
            x: 120
            y:300
            width: 809
            height: 250
            text: qsTr("Sorry We are unable to verify the parcel number. Please contact our CS line at 021-29022537/38 for assistance.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:no_record_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    user_send_memory_input.show_text=""
                    show_text_count = 0
                    touch_keyboard.count = 0
                    main_page.enabled = true
                    no_record.close()
                }
                onEntered:{
                    ok_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    ok_back.show_source = "img/button/7.png"
                }
            }
        }
    }
}
