import QtQuick 2.4
import QtQuick.Controls 1.2

Background{

    property int timer_value: 60

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

   PickUpButton{
        id:pick_up_Button
        y:620
        x:60
        show_text:qsTr("Back")

        MouseArea {
            id:chk
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(courier_service_view)
                slot_handler.customer_scan_barcode_take_express()
            }
            onEntered:{
                pick_up_Button.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                pick_up_Button.show_source = "img/button/7.png"
            }

        }
    }

    PickUpButton{
        id:pick_up_Button1
        y:620
        x:360
        show_text:qsTr("Retrieve")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(courier_memory_view)
                slot_handler.customer_scan_barcode_take_express()
            }
            onEntered:{
                pick_up_Button1.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                pick_up_Button1.show_source = "img/button/7.png"
            }
        }
    }

    PickUpButton{
        id:pick_up_Button2
        y:620
        x:660
        show_text:qsTr("Retrieve All")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            onEntered:{
                pick_up_Button2.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                pick_up_Button2.show_source = "img/button/7.png"
            }
        }
    }

    Rectangle{

        Image {
            id: image1
            x: 60
            y: 241
            width: 880
            height: 45
            source: "img/courier19/one_1.png"
        }

        Row {
            id: row1
            x: 60
            y: 230
            width: 880
            height: 69

            Rectangle{
                Text{
                    font.family:"Microsoft YaHei"
                    text:"ID"
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 80
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{
                    font.family:"Microsoft YaHei"
                    text:"手机号码"
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset:  240
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{

                    font.family:"Microsoft YaHei"
                    text:"寄件时间"
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset:410
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{

                    font.family:"Microsoft YaHei"
                    text:"隔口"
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 570
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{
                    font.family:"Microsoft YaHei"
                    text:"类型"
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 700
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{
                    font.family:"Microsoft YaHei"
                    text:"备注"
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 810
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }
        }
    }

    property var id_text:""
    property var phone_text:""
    property var time_text:""
    property var door_text:""
    property var type_text:""
    property var remark_text:""
    property var y_text:""

    Rectangle{
        FastGoodInfo{
            y_text:"45"
            id_text:"1"
            phone_text:"18171843322"
            time_text:"2015-04-17"
            door_text:"66"
            type_text:"深圳"
            remark_text:"8"
        }

        FastGoodInfo{
            y_text:"100"
            id_text:"1"
            phone_text:"18171843322"
            time_text:"2015-04-17"
            door_text:"66"
            type_text:"深圳"
            remark_text:"8"
        }
    }
}
