import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:86
    height:80
    color:"#ffc125"
    property var slot_text:""
    property var show_image:""

    Image{
        id:function_button_Image
        width:86
        height:80
    }

    Text{
        text:qsTr("DEL")
        color:"red"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent;
    }

    /*MouseArea {
        anchors.fill: parent
        onClicked: {
            if(slot_text != "delete"){
                email_keyboard.function_button_clicked(slot_text)
            }
            else
                email_keyboard.letter_button_clicked("")
        }
    }*/

}
