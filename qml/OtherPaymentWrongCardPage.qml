import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: other_wrongcard_page

    property int timer_value: 30
    property var press:"0"
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"            
            back_button.enabled = true
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    FullWidthReminderText{
       id:text
       x: 0
       y:236
       remind_text:qsTr("Transaction Failed")
       remind_text_size:"35"
    }

    Text{
       x: 97
       y:318
       width: 830
       height: 83
       text:qsTr("Please use Mandiri eMoney Prepaid Card.")
       verticalAlignment: Text.AlignVCenter
       textFormat: Text.AutoText
       horizontalAlignment: Text.AlignHCenter
       wrapMode: Text.WordWrap
       font.pixelSize: 30
       font.family:"Microsoft YaHei"
       color:"#ffffff"
    }


    DoorButton{
        id:back_button
        y:496
        x:373
        show_text:qsTr("Cancel")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }


    LoadingView{
        id:loading
    }


    Component.onCompleted: {
    }

    Component.onDestruction: {
    }

}
