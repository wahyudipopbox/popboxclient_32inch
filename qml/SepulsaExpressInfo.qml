import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: sepulsa_express_info_page
    width: 1024
    height: 768
    property int timer_value: 60
    property int show_timer_value: 5
    property int show_timer_value_nf: 5
    property var press:"0"
    property int trial_count: 0
    property var show_provider:"-"
    property var phone_no:"-"
    property var service:"-"
    property var denom:"0"
    property var amount:"0"
    property var data_product
    property var product_id:"-"
    property var product_type:"-"
    property var prod_type_temp:"-"
    //property var product_nominal:""

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            trial_count = 0
            time_payment.counter = show_timer_value
            time_nf.counter = show_timer_value_nf
            ok_button.enabled = true
            back_button.enabled = true
            nonfinished_notif.close()
            payment_page.close()
            wrong_card_notif.close()
            parseDataProduct(data_product)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            show_timer_payment.stop()
            show_timer_nf.stop()
        }
    }

    Rectangle{
        x: 10
        y: 10
        width: 20
        height: 20
        color: "transparent"

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                main_timer_text.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }

        Text {
            id:main_timer_text
            visible:false
            width: 20
            height: 20
            font.family:"Microsoft YaHei"
            color:"#fff000"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.PlainText
            font.pointSize:10
            wrapMode: Text.WordWrap
        }
    }

    FullWidthReminderText{
        id:title_page
        x: 0
        y:140
        remind_text:qsTr("Order Details")
        remind_text_size:"35"
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                back_button.show_source = "img/button/7.png"
            }
        }
    }

    DoorButton{
        id:ok_button
        y:614
        x:373
        show_text:qsTr("Confirm")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                ok_button.enabled = false
                back_button.enabled = false
                payment_page.open()                
                show_timer_payment.restart()
            }
            onEntered:{
                ok_button.show_image = "img/door/2.png"
            }
            onExited:{
                ok_button.show_image = "img/door/1.png"
            }
        }
    }

    Row{
        x:212
        y:200
        width: 600
        height: 300
        spacing:25
        Column{
            spacing:20
            Text {
                text: qsTr("Operator")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Phone Number")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Product Type")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Amount")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Product Name")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
        }
        Row{
            width: 400
            spacing:10
            Column{
                spacing:20
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
            Column{
                spacing:20
                Text {
                    id: show_provider_txt
                    text: show_provider
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: phone_no_txt
                    text: phone_no
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    id: product_type_txt
                    text: product_type
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: amount_txt
                    text: insert_flg(amount)
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    id: service_txt
                    width: 500
                    height: 80
                    text: service
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                    wrapMode: Text.WordWrap
                }
            }
        }
    }

    Text {
        id: prepare_text
        x: 250
        y: 504
        width: 525
        height: 104
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 28
        text: qsTr("Please prepare your pre-paid card and ensure the balance is sufficient.")
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: confirm_text
        x: 0
        y: 709
        width: 1024
        height: 32
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 24
        text: qsTr("By clicking confirm button you agree to the Terms and Conditions.")
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    LoadingView{
        id:loading
    }

    Component.onCompleted: {
        root.start_global_payment_emoney_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_global_payment_emoney_result.disconnect(handle_text)
    }   

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    } 

    function handle_text(result){
        console.log("status_payment_signal : " + result )
        if(result == "UNFINISHED"){
            trial_count += 1
            payment_page.close()
            nonfinished_notif.open()
            show_timer_payment.stop()
            show_timer_nf.restart()
            ok_button.enabled = false
            back_button.enabled = false
            trial_count_text.text = trial_count
            time_nf.counter = show_timer_value_nf
            if(trial_count >= 4){
                //slot_handler.start_reset_partial_transaction()
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }else if(result == "WRONG-CARD"){
            wrong_card_notif.open()
            payment_page.close()
            ok_button.enabled = false
            back_button.enabled = false
        }else if(result == "ERROR" || result == "FAILED"){
            my_stack_view.push(other_payment_failed_page)
            return
        }

        var obj = JSON.parse(result)
        if(obj.length <= 0){
            my_stack_view.push(other_payment_failed_page)
        }else{
            my_stack_view.push(sepulsa_payment_success_view, {payment_result:result, amount:amount, service:service, phone_no:phone_no, product_id: product_id, product_type:prod_type_temp})
        }
    }

    function parseDataProduct(data_product){
        console.log("parse_sepulsa_data_product : " + JSON.stringify(data_product))
        for(var x in data_product){
            if(data_product[x].product_id == product_id){
                denom = data_product[x].nominal
                //service = data_product[x].label.replace(show_provider + " ",'')
                service = data_product[x].label
                if(data_product[x].is_promo == true){
                    service = service + " (" + data_product[x].promo_name.replace('.000', 'K') + ")"
                    amount = data_product[x].price
                }else{
                    amount = denom
                }
                prod_type_temp = data_product[x].type
                if(prod_type_temp == "mobile"){
                    if(data_product[x].is_packet_data == true){
                        product_type = "Paket Data"
                    }
                    if(data_product[x].is_packet_data == false){
                        product_type = "TopUp Pulsa"
                    }
                }else if(prod_type_temp == "electricity"){
                    product_type = "PLN Pra-Bayar"
                }else if(prod_type_temp == "bpjs_kesehatan"){
                    product_type = "BPJS Kesehatan"
                }else if(prod_type_temp == "game"){
                    product_type = "Voucher Game"
                }else{
                    product_type = "Layanan Lainnya"
                }
            }
        }
    }

    PaymentWindow{
        id:payment_page
        //visible:true

        Text {
            id: text_payment_window
            x:334
            y:257
            width: 561
            height: 140
            text: qsTr("Please place your card to the reader")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_payment
                property int counter
                Component.onCompleted:{
                    time_payment.counter = show_timer_value
                }
            }
            Row{
                x:548
                y:399
                spacing:5
                Text {
                    id:show_time_payment_text
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pointSize:25
                    wrapMode: Text.WordWrap
                }
                Text {
                    id:show_seconds_left_text
                    color: "#ab312e"
                    text: qsTr("seconds left")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }

            Timer{
                id:show_timer_payment
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_payment_text.text = time_payment.counter
                    time_payment.counter -= 1
                    if(time_payment.counter == 2){
                        slot_handler.start_global_payment_emoney(amount)
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_payment.counter < 0){
                        show_timer_payment.stop()
                        show_time_payment_text.visible = false
                        show_seconds_left_text.visible = false
                        ok_button.enabled = true
                        back_button.enabled = true
                        abc.counter = timer_value
                    }
                }
            }
        }
    }

    PaymentWindow{
        id:nonfinished_notif
        //visible:true

        Text {
            id: text_nonfisnihed_1
            x:130
            y:218
            width: 765
            height: 50
            text: qsTr("Transaction Unfinished")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_nonfisnihed_2
            x:340
            y:285
            width: 555
            height: 88
            text: qsTr("Please put back your previous prepaid card.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_nf
                property int counter
                Component.onCompleted:{
                    time_nf.counter = show_timer_value_nf
                }
            }
            Row{
                x:563
                y:391
                spacing:5
                Text {
                    id:show_time_nf
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pointSize:30
                    wrapMode: Text.WordWrap
                }
                Text {
                    id: second_left_nf
                    color: "#ab312e"
                    text: qsTr("seconds left")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 30
                }
            }
            Row{
                x:862
                y:520
                width: 30
                height: 30
                spacing:5
                Text {
                    id:trial_count_text
                    width: 30
                    height: 30
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    textFormat: Text.PlainText
                    font.pointSize:18
                    wrapMode: Text.WordWrap
                }
            }

            Timer{
                id:show_timer_nf
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{                    
                    show_time_nf.text = time_nf.counter
                    time_nf.counter -= 1
                    if(time_nf.counter == 2){
                        slot_handler.start_global_payment_emoney(amount)
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_nf.counter < 0){
                        show_timer_nf.stop()
                        show_time_nf.visible = false
                        second_left_nf.visible = false
                    }
                }
            }
        }
    }

    HideWindow{
        id:wrong_card_notif
        //visible:true

        Text {
            id: text_wc_1
            x: 93
            y:464
            width: 850
            height: 50
            text: qsTr("Transaction Failed")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:28
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: text_wc_2
            x: 93
            y:520
            width: 850
            height: 50
            text: qsTr("Please use Mandiri e-Money prepaid card.")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:25
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_wrong_card_notif
            x: 410
            y: 235
            width: 215
            height: 215
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/no_credit.png"
        }

        OverTimeButton{
            id:wrong_card_notif_back
            x:378
            y:589
            show_text:qsTr("Cancel")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                    //slot_handler.start_reset_partial_transaction()
                }
                onEntered:{
                    wrong_card_notif_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    wrong_card_notif_back.show_source = "img/button/7.png"
                }
            }
        }
    }
}
