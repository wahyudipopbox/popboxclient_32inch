import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_rec
    width:200
    height:300
    color:"transparent"

    property var prod_name: ""
    property var prod_price: ""
    property var prod_disc: ""
    property var prod_init_price: ""
    property var prod_sku: ""
    property url prod_image: ""
    property url prod_image_qr: ""
    property bool text_click: true
    property bool prod_image_vis : true
    property bool prod_image_qr_vis : false
    property bool prod_price_vis : true
    property bool prod_sku_vis : false

    Rectangle{
        id: base_background
        anchors.fill: parent
        radius: 10
        color: "white"
        border.width: 3
        border.color: "#00000000"
    }

    Image{
        id: prod_image_view
        visible: prod_image_vis
        x: 0
        y: 0
        source: prod_image
        fillMode: Image.PreserveAspectFit
        width:200
        height:300
    }

    Image{
        id: prod_image_qr_view
        visible: prod_image_qr_vis
        x: 0
        y: -42
        source: prod_image_qr
        fillMode: Image.PreserveAspectFit
        width:200
        height:300
    }

    Rectangle{
        id: rec_prod_price
        x:0
        y: 242
        width: parent_rec.width
        height: 60
        color: "#00000000"

        Text{
            id: text_prod_name
            visible: text_click
            x: 0
            y: 0
            width:parent.width
            height: 57
            text: prod_name
            font.italic: true
            textFormat: Text.PlainText
            color: "black"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 12
            font.family: "Microsoft YaHei"
        }

        Rectangle{
            id: rec_background
            x: 0
            y: -40
            color: "black"
            opacity: 0.7
            width: parent.width
            height: 40

            Text {
                id: text_prod_disc
                visible: prod_price_vis
                x: 8
                y: 0
                width: 102
                height: 14
                color: "orange"
                text: prod_disc
                font.family: "Microsoft YaHei"
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                textFormat: Text.PlainText
                font.pixelSize: 12
                font.bold: true
                verticalAlignment: Text.AlignTop
                font.italic: true
            }

            Text{
                id: text_prod_price
                visible: prod_price_vis
                x: 68
                y: 5
                width: 124
                height: 27
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                color: "white"
                font.family: "Microsoft YaHei"
                font.bold: true
                text: prod_price
                font.pixelSize: 25
            }

            Text{
                id: text_prod_sku
                visible: prod_sku_vis
                x: 0
                y: 5
                width: 200
                height: 27
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: "white"
                font.family: "Microsoft YaHei"
                font.bold: true
                text: prod_sku
                font.pixelSize: 25
            }

            Text {
                id: text_prod_init_price
                visible: prod_price_vis
                x: 8
                y: 20
                width: 92
                height: 14
                color: "yellow"
                text: prod_init_price
                font.italic: true
                font.bold: false
                font.strikeout: true
                wrapMode: Text.WordWrap
                textFormat: Text.PlainText
                verticalAlignment: Text.AlignBottom
                font.pixelSize: 12
                font.family: "Microsoft YaHei"
                horizontalAlignment: Text.AlignLeft
            }
        }
    }

    Text {
        id: text_click_item
        x: 0
        y: 8
        width: parent.width
        height: 27
        color: "darkred"
        text: qsTr("Touch to Get This")
        horizontalAlignment: Text.AlignHCenter
        font.family: "Microsoft YaHei"
        font.italic: true
        font.pixelSize: 14
        verticalAlignment: Text.AlignVCenter
        visible: text_click
    }

    /*MouseArea{
        id: emoney_click
        //enabled: (item_view.prod_sku_vis==true) ? true : false
        enabled: true
        x: 0
        y: parent.height - 57
        width: parent.width
        height: 57
        onClicked: {
            var usageOf = "popshop"
            var data_product = item_view.array_prod_
            my_stack_view.push(grocery_input_quantity, {data_prod_: data_product, usageOf: usageOf})
        }

        Text{
            id: text_notif_with_emoney
            visible: emoney_click.enabled
            anchors.fill: parent
            text: qsTr("Pay with ")
            font.family: "Microsoft YaHei"
            font.italic: true
            font.pixelSize: 14
            verticalAlignment: Text.AlignLeft
        }

        Image{
            id:img_logo_emoney
            anchors.fill: parent
            visible: emoney_click.enabled
            source: "img/otherservice/logo-emoney300.png"
            fillMode: Image.PreserveAspectFit
        }
    }*/
}
