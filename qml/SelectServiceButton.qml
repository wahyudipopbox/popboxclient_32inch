import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:281
    height:277
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_source:"img/button/1.png"


    Image{
        x:0
        y:0
        width:281
        height:277
        source:show_source
    }

    Image{
        x:0
        y:0
        width:281
        height:277
        source:show_image
    }
}
