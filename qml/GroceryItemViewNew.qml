import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_rec
    width:300
    height:400
    color:"transparent"

    property var prod_name: "Lazy Bag - Biru"
    property var prod_price: "129000"
    property var prod_disc: "71000"
    property var prod_init_price: "200000"
    property var prod_sku: "PBSDIMO0442"
    property url prod_image: ""
    property url prod_image_qr: ""
    property bool text_click: true
    property bool prod_image_vis : true
    property bool prod_image_qr_vis : false
    property bool prod_price_vis : true
    property bool prod_sku_vis : false

    Rectangle{
        id: base_background
        anchors.fill: parent
        radius: 10
        color: "white"
        border.color: "white"
    }

    Image{
        id: prod_image_view
        x: 12
        y: 10
        visible: prod_image_vis
        source: prod_image
        fillMode: Image.PreserveAspectFit
        width: parent.width - 20
        height: parent.height - 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    Image{
        id: prod_image_qr_view
        visible: prod_image_vis
        x: 178
        y: 250
        source: prod_image_qr
        fillMode: Image.PreserveAspectFit
        width:112
        height:112
    }

    Rectangle{
        id: rec_background
        x: 10
        y: 342
        width: 170
        color: "black"
        anchors.horizontalCenterOffset: -57
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 0.7
        height: 40

        Text {
            id: text_prod_disc
            //visible: prod_price_vis
            visible: false
            x: 8
            y: 0
            width: 102
            height: 14
            color: "orange"
            text: prod_disc
            font.family: "Microsoft YaHei"
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.WordWrap
            textFormat: Text.PlainText
            font.pixelSize: 12
            font.bold: true
            verticalAlignment: Text.AlignTop
            font.italic: true
        }


        Text {
            id: text_prod_init_price
            //visible: prod_price_vis
            visible: false
            x: 8
            y: 20
            width: 92
            height: 14
            color: "yellow"
            text: prod_init_price
            font.italic: true
            font.bold: false
            font.strikeout: true
            wrapMode: Text.WordWrap
            textFormat: Text.PlainText
            verticalAlignment: Text.AlignBottom
            font.pixelSize: 12
            font.family: "Microsoft YaHei"
            horizontalAlignment: Text.AlignLeft
        }
    }

    Text{
        id: text_prod_price
        visible: prod_price_vis
        anchors.fill: rec_background
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "white"
        font.family: "Microsoft YaHei"
        font.bold: false
        text: "Rp. " + prod_price + ",-"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize: 21
    }

    Text{
        id: text_prod_sku
        //visible: prod_sku_vis
        x: 178
        y: 362
        width: 112
        height: 20
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        color: "#000000"
        font.family: "Microsoft YaHei"
        font.bold: true
        text: prod_sku
        font.pixelSize: 13
    }

    Rectangle {
        id: rec_base_name
        x: 14
        y: 20
        width: parent_rec.width
        height: 45
        color: "#000000"
        anchors.horizontalCenterOffset: 0
        opacity: 0.4
        anchors.horizontalCenter: parent.horizontalCenter

    }
    Text{
        id: text_prod_name
        y: 0
        visible: text_click
        anchors.fill: rec_base_name
        text: prod_name
        font.bold: false
        font.italic: true
        textFormat: Text.PlainText
        color: "white"
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 15
        font.family: "Microsoft YaHei"
    }

    /*MouseArea{
        id: emoney_click
        //enabled: (item_view.prod_sku_vis==true) ? true : false
        enabled: true
        x: 0
        y: parent.height - 57
        width: parent.width
        height: 57
        onClicked: {
            var usageOf = "popshop"
            var data_product = item_view.array_prod_
            my_stack_view.push(grocery_input_quantity, {data_prod_: data_product, usageOf: usageOf})
        }

        Text{
            id: text_notif_with_emoney
            visible: emoney_click.enabled
            anchors.fill: parent
            text: qsTr("Pay with ")
            font.family: "Microsoft YaHei"
            font.italic: true
            font.pixelSize: 14
            verticalAlignment: Text.AlignLeft
        }

        Image{
            id:img_logo_emoney
            anchors.fill: parent
            visible: emoney_click.enabled
            source: "img/otherservice/logo-emoney300.png"
            fillMode: Image.PreserveAspectFit
        }
    }*/
}
