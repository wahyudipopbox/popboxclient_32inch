import json
import logging
import random
import time
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import Configurator
import box.repository.BoxDao as BoxDao
import company.repository.CompanyDao as CompanyDao
import company.service.CompanyService as CompanyService
import express.service.ExpressService as ExpressService
import user.service.UserService as UserService
import alert.service.AlertService as AlertService
from database import ClientDatabase
from device import LockMachine
from device import UPSMachine
from network import HttpClient

class BoxSignalHandler(QObject):
    mouth_status_signal = pyqtSignal(str)
    init_client_signal = pyqtSignal(str)
    load_mouth_list_signal = pyqtSignal(str)
    manager_mouth_count_signal = pyqtSignal(int)
    mouth_list_signal = pyqtSignal(str)
    manager_set_mouth_signal = pyqtSignal(str)
    manager_open_mouth_by_id_signal = pyqtSignal(str)
    manager_open_all_mouth_signal = pyqtSignal(str)
    choose_mouth_signal = pyqtSignal(str)
    mouth_number_signal = pyqtSignal(str)
    free_mouth_num_signal = pyqtSignal(str)
    free_mouth_by_size_result = pyqtSignal(str)
    get_version_signal = pyqtSignal(str)
    manager_get_box_info_signal = pyqtSignal(str)
    open_main_box_signal = pyqtSignal(str)

logger = logging.getLogger()
version = '1.30412.006.160729'
box_signal_handler = BoxSignalHandler()
box_config = ''

def start_get_version():
    ClientTools.get_global_pool().apply_async(get_version)

def get_version():
    global version
    box_signal_handler.get_version_signal.emit(version)

def start_manager_get_box_info():
    ClientTools.get_global_pool().apply_async(manager_get_box_info)

def manager_get_box_info():
    box_info = get_box()
    box_info['operator_name'] = CompanyDao.get_company_by_id({'id': box_info['operator_id']})[0]['name']
    box_info['order_no'] = Configurator.get_value('ClientInfo', 'OrderNo')
    box_info['token'] = Configurator.get_value('ClientInfo', 'Token')
    if Configurator.get_value('Scanner', 'port'):
        box_info['scanner_port'] = Configurator.get_value('Scanner', 'port')
    else:
        box_info['scanner_port'] = 'None'
    if Configurator.get_value('LockMachine', 'port'):
        box_info['lock_machine_port'] = Configurator.get_value('LockMachine', 'port')
    else:
        box_info['lock_machine_port'] = 'None'
    if Configurator.get_value('Ups', 'port'):
        box_info['ups_port'] = Configurator.get_value('Ups', 'port')
    else:
        box_info['ups_port'] = 'None'
    box_signal_handler.manager_get_box_info_signal.emit(json.dumps(box_info))

def start_init_client():
    ClientTools.get_global_pool().apply_async(init_client)

def init_client():
    global box_config
    message, status_code = HttpClient.get_message('box/init')
    box_config = message
    #print("box_config : ", json.dumps(box_config))
    logger.info(('box_config:', box_config))
    if status_code != 200:
        return message['statusMessage']
    try:
        ClientDatabase.init_database()
        box_param = {'id': message['id'],
                     'name': message['name'],
                     'orderNo': message['orderNo'],
                     'validateType': message['validateType'],
                     'operator_id': message['operator']['id'],
                     'syncFlag': message['syncFlag'],
                     'currencyUnit': message['currencyUnit'],
                     'overdueType': message['overdueType'],
                     'freeHours': ClientTools.get_value('freeHours', message),
                     'freeDays': ClientTools.get_value('freeDays', message),
                     'receiptNo': ClientTools.get_value('receiptNo', message, 0)}
        CompanyService.init_company(message['operator'])
        BoxDao.init_box(box_param)
        cabinets = message['cabinets']
        try:
            for cabinet in cabinets:
                BoxDao.init_cabinet(cabinet)
                mouths = cabinet['mouths']
                for m in mouths:
                    __mouth = {'id': m['id'],
                               'deleteFlag': m['deleteFlag'],
                               'number': m['number'],
                               'usePrice': m['usePrice'],
                               'overduePrice': m['overduePrice'],
                               'status': m['status'],
                               'box_id': message['id'],
                               'cabinet_id': cabinet['id'],
                               'numberInCabinet': m['numberInCabinet'],
                               'syncFlag': m['syncFlag'],
                               'openOrder': None}
                    if 'express' in m.keys():
                        __mouth['express_id'] = m['express']['id']
                        ExpressService.init_express(m['express'], __mouth, box_param)
                    else:
                        __mouth['express_id'] = None
                    __mouth['mouthType_id'] = m['mouthType']['id']
                    mouth_type_param = m['mouthType']
                    mouth_type = {'id': mouth_type_param['id'],
                                  'name': mouth_type_param['name'],
                                  'defaultUsePrice': mouth_type_param['defaultUsePrice'],
                                  'defaultOverduePrice': mouth_type_param['defaultOverduePrice'],
                                  'deleteFlag': mouth_type_param['deleteFlag']}
                    BoxDao.init_mouth_type(mouth_type)
                    BoxDao.init_mouth(__mouth)
        except Exception as e:
            logger.debug(("Parsing Cabinet & Mouth ERROR : ", e))
    except Exception as e:
        logger.debug(("Parsing Inititation DB ERROR : ", e))
    box_signal_handler.init_client_signal.emit('Success')

def start_update_box(message_result):
    ClientTools.get_global_pool().apply_async(update_box, (message_result,))

def update_box(message_result):
    logger.info(('init_box_info:', message_result))
    try:
        box_param = {'id': message_result['box']['id'],
                     'name': message_result['box']['name'],
                     'orderNo': message_result['box']['orderNo'],
                     'validateType': message_result['box']['validateType'],
                     'currencyUnit': message_result['box']['currencyUnit'],
                     'overdueType': message_result['box']['overdueType'],
                     'freeHours': message_result['box']['freeHours'],
                     'freeDays': message_result['box']['freeDays']}
        BoxDao.update_box(box_param)
        box_post_message(message_result['id'], 'UPDATE BOX SUCCESS', 'SUCCESS')
    except Exception as e:
        logger.debug(('update_box ERROR :', e))
        box_post_message(message_result['id'], 'UPDATE BOX ERROR', 'ERROR')

def start_free_mouth_by_size(mouth_size):
    free_mouth_by_size(mouth_size)

def free_mouth_by_size(mouth_size):
    mouth_type_param = {'name': mouth_size}
    mouth_type_list = BoxDao.get_mouth_type(mouth_type_param)
    if len(mouth_type_list) != 1:
        box_signal_handler.free_mouth_by_size_result.emit('Failure')
    mouth_type = mouth_type_list[0]
    mouth_param = {'mouthType_id': mouth_type['id'],
                   'deleteFlag': 0,
                   'status': 'ENABLE'}
    mouth_list = BoxDao.get_free_mouth_by_type(mouth_param)
    if len(mouth_list) == 0:
        box_signal_handler.free_mouth_by_size_result.emit('Failure')
    box_signal_handler.free_mouth_by_size_result.emit('Success')

def get_free_mouth_by_size(mouth_size):
    mouth_type_param = {'name': mouth_size}
    mouth_type_list = BoxDao.get_mouth_type(mouth_type_param)
    if len(mouth_type_list) != 1:
        return False
    mouth_type = mouth_type_list[0]
    mouth_param = {'mouthType_id': mouth_type['id'],
                   'deleteFlag': 0,
                   'status': 'ENABLE'}
    mouth_list = BoxDao.get_free_mouth_by_type(mouth_param)
    if len(mouth_list) == 0:
        return False
    mouth_no = random.randint(0, len(mouth_list) - 1)
    return mouth_list[mouth_no]

def get_box():
    order_no = Configurator.get_value('ClientInfo', 'OrderNo')
    param = {'orderNo': order_no, 'deleteFlag': 0}
    box_list = BoxDao.get_box_by_order_no(param)
    if len(box_list) != 1:
        return False
    return box_list[0]

def get_mouth(express__):
    return BoxDao.get_mouth(express__)

def free_mouth(mouth__):
    BoxDao.free_mouth(mouth__)

def use_mouth(mouth_param__):
    BoxDao.use_mouth(mouth_param__)

last_open_mouth_id = None
last_mouth_number = ''

def start_open_mouth():
    ClientTools.get_global_pool().apply_async(open_mouth)

def open_mouth(mouth_id=None):
    global last_open_mouth_id
    global last_mouth_number
    if mouth_id is None:
        logger.info(('open mouth again!', last_open_mouth_id))
        mouth_id = last_open_mouth_id
    else:
        logger.info(('open mouth id :', mouth_id))
        last_open_mouth_id = mouth_id
    mouth_list = BoxDao.get_mouth_by_id({'id': mouth_id})
    if len(mouth_list) != 1:
        #logger.debug('mouth list error!')
        return
    _mouth = mouth_list[0]
    cabinet_list = BoxDao.get_cabinet_by_id({'id': _mouth['cabinet_id']})
    if len(cabinet_list) != 1:
        #logger.warning('cabinet list error!')
        return
    cabinet = cabinet_list[0]
    last_mouth_number = _mouth['number']
    LockMachine.start_open_door(int(cabinet['number']), int(_mouth['numberInCabinet']))

def start_get_mouth_status():
    ClientTools.get_global_pool().apply_async(get_mouth_status)

def get_mouth_status():
    result = ''
    for name in ['XL', 'L', 'M', 'S', 'MINI']:
        param = {'status': 'ENABLE', 'name': name}
        free_mouth_count = BoxDao.get_free_mouth_count_by_mouth_type_name(param)[0]
        if free_mouth_count['count'] > 0:
            result += 'T'
        else:
            result += 'F'

    box_signal_handler.mouth_status_signal.emit(result)

def start_load_manager_mouth_count():
    ClientTools.get_global_pool().apply_async(load_manager_mouth_count)

def load_manager_mouth_count():
    count_list = BoxDao.get_all_mouth_count({'deleteFlag': 0})
    logger.debug(('load_manager_mouth_count : ', len(count_list)))
    box_signal_handler.manager_mouth_count_signal.emit(count_list[0]['count'])

def start_load_mouth_list(page):
    ClientTools.get_global_pool().apply_async(manager_load_mouth_list, (page,))
    box_signal_handler.load_mouth_list_signal.emit('Success')

def manager_load_mouth_list(page):
    __user = UserService.get_user()
    #Force Showing Door
    __user['role'] = 'OPERATOR_USER'
    get_mouth_param = {'deleteFlag': 0,
                       'startLine': (int(page) - 1) * 25}
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        mouth_list_ = BoxDao.get_mouth_list(get_mouth_param)
        box_signal_handler.mouth_list_signal.emit(json.dumps(mouth_list_))

def start_manager_set_mouth(box_id, box_status):
    ClientTools.get_global_pool().apply_async(manager_set_mouth, (box_id, box_status))

def manager_set_mouth(mouth_id, box_status):
    set_mouth_param = {'id': mouth_id,
                       'status': box_status,
                       'syncFlag': 0}
    BoxDao.manage_set_mouth(set_mouth_param)
    box_signal_handler.manager_set_mouth_signal.emit('Success')
    message, status_code = HttpClient.post_message('box/mouth/sync', set_mouth_param)
    if status_code == 200:
        BoxDao.mark_sync_success(set_mouth_param)

def start_manager_open_mouth(mouth_id):
    ClientTools.get_global_pool().apply_async(manager_open_mouth, (mouth_id,), callback=return_signal)

def start_manager_open_all_mouth():
    ClientTools.get_global_pool().apply_async(manager_open_all_mouth)

def manager_open_all_mouth():
    mouth_list = BoxDao.get_all_mouth({'deleteFlag': 0})
    for _mouth in mouth_list:
        manager_open_mouth(_mouth['id'])
        time.sleep(2)

    box_signal_handler.manager_open_all_mouth_signal.emit('Success')

def manager_open_mouth(mouth_id):
    __user = UserService.get_user()
    if __user['role'] != 'OPERATOR_USER' and __user['role'] != 'OPERATOR_ADMIN':
        box_signal_handler.manager_open_mouth_by_id_signal.emit('Error')
        return False
    open_mouth(mouth_id)
    return True

def return_signal(result):
    logger.debug(result)
    if not result:
        return
    box_signal_handler.manager_open_mouth_by_id_signal.emit('Success')

def start_manager_set_free_mouth(box_id):
    ClientTools.get_global_pool().apply_async(manager_set_free_mouth, (box_id,))

def manager_set_free_mouth(box_id):
    pass

mouth = None

def start_choose_mouth_size(mouth_size, method_type):
    ClientTools.get_global_pool().apply_async(choose_mouth_size, (mouth_size, method_type))

def choose_mouth_size(mouth_size, method_type):
    global mouth
    logger.info(('choose_mouth_size:', mouth_size, ' type:', method_type))
    mouth = get_free_mouth_by_size(mouth_size)
    if not mouth:
        mouth = None
        #logger.warning('not ' + mouth_size + ' mouth!')
        box_signal_handler.choose_mouth_signal.emit('NotMouth')
        return
    user_result = UserService.get_user()
    if method_type == 'staff_store_express' and user_result['wallet']['balance'] < mouth['usePrice']:
        #logger.warning('not enough money!')
        box_signal_handler.choose_mouth_signal.emit('NotBalance')
        return
    open_mouth(mouth['id'])
    logger.info('open mouth id:' + str(mouth['id']) + ' number:' + str(mouth['number']))
    box_signal_handler.choose_mouth_signal.emit('Success')

def get_express_mouth_number():
    box_signal_handler.mouth_number_signal.emit(str(last_mouth_number))

def update_free_time(param):
    box_list = BoxDao.get_box_by_order_no({'orderNo': param['orderNo'], 'deleteFlag': 0})
    if len(box_list) == 0:
        #logger.warning('box not found!')
        return False
    param['overdueType'] = ClientTools.get_value('overdueType', param)
    param['freeDays'] = ClientTools.get_value('freeDays', param)
    param['freeHours'] = ClientTools.get_value('freeHours', param)
    BoxDao.update_free_time(param)
    return True

def start_get_free_mouth_mun():
    ClientTools.get_global_pool().apply_async(get_free_mouth_num)

def get_free_mouth_num():
    __result = {}
    __mouth_type_list = BoxDao.get_all_mouth_type({'deleteFlag': 0})
    for __mouth_type in __mouth_type_list:
        param = {'status': 'ENABLE',
                 'mouthType_id': __mouth_type['id'],
                 'deleteFlag': 0}
        __free_mouth_count = BoxDao.get_count_by_status_and_mouth_type_id(param)[0]
        __result[__mouth_type['name']] = __free_mouth_count['mouth_count']

    box_signal_handler.free_mouth_num_signal.emit(json.dumps(__result))

def get_scan_not_sync_mouth_list():
    return BoxDao.get_not_sync_mouth_list({'syncFlag': 0})

def mark_sync_success(mouth):
    return BoxDao.mark_sync_success(mouth)

def mark_box_sync_success():
    return BoxDao.mark_box_sync_success({'deleteFlag': 0})

last_pull_open_mouth_id = None
last_pull_mouth_number = ''

def pull_open_mouth(message_result):
    global last_pull_mouth_number
    global last_pull_open_mouth_id
    if message_result['mouth']['id'] is None:
        box_post_message(message_result['id'], 'MOUTH NOT AVAILABLE!', 'ERROR')
        return
    logger.info(('Server_pull open mouth id :', message_result['mouth']['id']))
    last_pull_open_mouth_id = message_result['mouth']['id']
    mouth_list = BoxDao.get_mouth_by_id({'id': message_result['mouth']['id']})
    if len(mouth_list) != 1:
        box_post_message(message_result['id'], 'MOUTH LIST ERROR!', 'ERROR')
        return
    _mouth = mouth_list[0]
    cabinet_list = BoxDao.get_cabinet_by_id({'id': _mouth['cabinet_id']})
    if len(cabinet_list) != 1:
        box_post_message(message_result['id'], 'CABINET LIST ERROR!', 'ERROR')
        return
    cabinet = cabinet_list[0]
    last_pull_mouth_number = _mouth['number']
    LockMachine.start_open_door(int(cabinet['number']), int(_mouth['numberInCabinet']))
    box_post_message(message_result['id'], 'SUCCESSFULLY OPEN!', 'SUCCESS')

def box_post_message(task_id, result, reset_status):
    reset_express_result = {'id': task_id, 'result': result, 'statusType': reset_status}
    HttpClient.post_message('task/finish', reset_express_result)

def start_get_ups_status():
    ClientTools.get_global_pool().apply_async(get_ups_status)

def get_ups_status():
    UPSMachine.start_get_ups_status()

def start_update_mouth_status(message_result):
    ClientTools.get_global_pool().apply_async(update_mouth_status, (message_result,))

def update_mouth_status(message_result):
    try:
        logger.info(('update_mouth_status:', message_result))
        mouth_result = BoxDao.get_mouth_by_id({'id': message_result['mouth']['id']})
        logger.debug(('mouth_result is :', mouth_result))
        if len(mouth_result) == 0:
            box_post_message(message_result['id'], 'MOUTH NOT AVAILABLE', 'ERROR')
        else:
            mouth_param = {'id': message_result['mouth']['id'],
                           'status': message_result['mouth']['status']}
            BoxDao.update_mouth_status(mouth_param)
            box_post_message(message_result['id'], 'MOUTH SUCCESSFULLY UPDATED!', 'SUCCESS')
    except Exception as e:
        logger.debug(('update_mouth_status ERROR!!', e))
        box_post_message(message_result['id'], 'UPDATE MOUTH FAILED', 'ERROR')

def start_open_main_box():
    ClientTools.get_global_pool().apply_async(open_main_box)

def open_main_box():
    try:
        main_box_cabinet = Configurator.get_value('MainBox', 'cabinet')
        main_box_number_in_cabinet = Configurator.get_value('MainBox', 'numberInCabinet')
        result = LockMachine.open_door(int(main_box_cabinet), int(main_box_number_in_cabinet))
        if result:
            logger.info('main box open Success')
            AlertService.main_box_open_alert()
            box_signal_handler.open_main_box_signal.emit('Success')
        else:
            logger.info('main box open Failed')
            box_signal_handler.open_main_box_signal.emit('Failed')
    except Exception as e:
        logger.debug('open_main_box ERROR :', e)