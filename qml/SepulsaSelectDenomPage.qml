import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

Background{
    id:sepulsa_select_denom
    width: 1024
    height: 768
    property int timer_value: 60
    property var press: "0"
    property var show_provider: ""
    property var phone_no: ""
    property var operator_temp: ""
    //property var data_product: '[{"is_available": true,"is_packet_data": false,"product_id": "11","type": "mobile","label": "XL Rp. 50,000","operator": "xl","nominal": "50000","price": 49500},{"is_available": true,"is_packet_data": true,"product_id": "78","type": "mobile","label": "Combo XTRA L","operator": "xl","nominal": "59000","price": 52000}]'
    property var data_product: ""
    property real count_int_stext: 0
    property int qty_product: 0
    property var button_bg: ""

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            loadingGif.open()
            group_button.visible = false
            button_listModel.clear()
            operator_logo(show_provider)
            slot_handler.start_get_sepulsa_product(phone_no)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_sepulsa_product_result.connect(handle_result_product)
    }

    Component.onDestruction: {
        root.start_get_sepulsa_product_result.disconnect(handle_result_product)
    }

    LoadingView{id: loadingGif}

    Rectangle{
        id: foregorund_greet_text
        color: "white"
        opacity: 0.7
        x: 0
        y: 157
        width: parent.width
        height: 100
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter == 30 && data_product == ""){
                    network_error.open()
                }
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id: main_page
        Text {
            id: greeting_text
            x: 37
            y: 115
            width: 200
            height: 40
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            text: 'Hi, ' + phone_no
            verticalAlignment: Text.AlignVCenter
            textFormat: Text.PlainText
            font.pointSize:24
        }

        Row{
            id: row_text_select
            x: 150
            y: 170
            width: 700
            height: 60
            layoutDirection: Qt.LeftToRight
            spacing: 10

            Text {
                id: text_select_denom
                width: 500
                height: 60
                font.family:"Microsoft YaHei"
                color:"#C02D26"
                text: qsTr("Please Choose Denom :")
                font.bold: true
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.PlainText
                font.pointSize:30
            }

            Text {
                id: text_provider_choosen
                visible: false
                width: 200
                height: 60
                font.family:"Microsoft YaHei"
                color:"#C02D26"
                text: show_provider
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.PlainText
                font.pointSize:28
            }
        }

        BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("Back")

            MouseArea {
                id: ms_back
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
        }

        Item{
            id: group_button
            x: 120
            y: 266
            width: 900
            height: 500
            anchors.horizontalCenterOffset: 515
            anchors.horizontalCenter: parent.horizontalCenter
            focus: true

            ScrollBarPop{
                id: vertical_sbar
                flickable: gridview
                x: 955
                y: 266
                height: group_button.height
                color: "white"
            }

            GridView{
                id: gridview
                height: 470
                anchors.bottomMargin: 29
                anchors.topMargin: 0
                flickDeceleration: 750
                maximumFlickVelocity: 1500
                anchors.centerIn: parent
                layoutDirection: Qt.LeftToRight
                flow: GridView.FlowLeftToRight
                boundsBehavior: Flickable.StopAtBounds
                cacheBuffer: 500
                keyNavigationWraps: true
                snapMode: GridView.SnapToRow
                anchors.fill: parent
                anchors.margins: 20
                clip: true
                focus: true
                delegate: delegate_button
                model: button_listModel
                cellWidth: 170
                cellHeight: 220
            }

            ListModel {
                id: button_listModel
            }

            Component{
                id: delegate_button

                SepulsaButton{
                    id:id_button
                    show_text: prod_nominal_
                    button_type: butt_type_
                    button_desc: butt_desc_
                    show_bg_color: "white"
                    show_text_color: "red"
                    show_image: button_bg
                    show_init_price: init_price_
                    isPromo: is_promo_

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(press != "0"){
                                return
                            }
                            press = "1"
                            my_stack_view.push(sepulsa_express_info_view,{show_provider:show_provider, phone_no: phone_no, product_id: prod_id_, data_product: data_product})
                        }
                    }
                }
            }
        }

        GroupBox {
            id: group_logo
            x: 652
            y: 135
            width: 240
            height: 140
            flat: true
            checked: false
            checkable: false

            Image {
                id: logo_indosat
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/indosat.png"
            }

            Image {
                id: logo_telkomsel
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/telkomsel.png"
            }

            Image {
                id: logo_smartfren
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/smartfren.png"
            }

            Image {
                id: logo_xl
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/xl.png"
            }

            Image {
                id: logo_bolt
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/bolt.png"
            }

            Image {
                id: logo_three
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/three.png"
            }

            Image {
                id: logo_pln
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/pln.png"
            }

            Image {
                id: logo_axis
                visible: false
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/sepulsa/logo/axis.png"
            }
        }
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function operator_logo(show_provider){
        if(show_provider == "Telkomsel"){
            logo_telkomsel.visible = true
            button_bg = logo_telkomsel.source
        }else if(show_provider == "Indosat"){
            logo_indosat.visible = true
            button_bg = logo_indosat.source
        }else if(show_provider == "XL-Axiata"){
            logo_xl.visible = true
            button_bg = logo_xl.source
        }else if(show_provider == "Bolt"){
            logo_bolt.visible = true
            button_bg = logo_bolt.source
        }else if(show_provider == "Smartfren"){
            logo_smartfren.visible = true
            button_bg = logo_smartfren.source
        }else if(show_provider == "3-Three"){
            logo_three.visible = true
            button_bg = logo_three.source
        }else if(show_provider == "Axis"){
            logo_axis.visible = true
            button_bg = logo_axis.source
        }else{
            text_provider_choosen.visible = true
        }
    }

    function handle_result_product(result){
        console.log("start_get_sepulsa_product_result : " + result)
        if(result == "ERROR"){
            network_error.open()
            main_page.enabled = false
        }else{
            var xObj = JSON.parse(result)
            operator_temp = xObj.operator
            qty_product = xObj.avail_prod
            data_product = xObj.data
            if(data_product != ""){
                loadingGif.close()
                group_button.visible = true
            }
            //Choose Method Parsing Data
            //parseDataProduct(data_product) -> Old Without Promo
            parseDataProduct2(data_product)
        }
    }

    function parseDataProduct(data_product){
        //console.log("parse_sepulsa_data_product : " + JSON.stringify(data_product))
        for(var x in data_product){
            var prod_id_temp = data_product[x].product_id
            var butt_desc_temp = data_product[x].label.replace(operator_temp.toString(),'')
            if(data_product[x].is_packet_data == true){
                var butt_type_temp = "DATA"
            }else{
                butt_type_temp = "PULSA"
            }
            var prod_nominal_temp = data_product[x].nominal
            var count_int_stext =  parseInt(prod_nominal_temp)/1000
            if(prod_nominal_temp.indexOf("000") > -1){
                var show_text_button_temp = prod_nominal_temp.replace('000','') + "K"
            }else{
                show_text_button_temp = count_int_stext.toLocaleString(Qt.locale("id_ID"), "f", 1) + "K"
            }
            button_listModel.append({"show_text_":show_text_button_temp,
                                        "prod_id_":prod_id_temp,
                                        "butt_type_":butt_type_temp,
                                        "butt_desc_":butt_desc_temp})
        }
    }

    function short_denom(initial){
        var new_init = parseInt(initial)/1000
        if(initial.indexOf("000") > -1){
            return initial.replace('000','') + "K"
        }else{
            return new_init.toLocaleString(Qt.locale("id_ID"), "f", 1) + "K"
        }
    }

    function parseDataProduct2(data_product){
        //console.log("parse_sepulsa_data_product2 : " + JSON.stringify(data_product))
        for(var x in data_product){
            var prod_id_temp = data_product[x].product_id
            var butt_desc_temp = data_product[x].label.replace(operator_temp.toString(),'')
            if(data_product[x].is_packet_data == true){
                var butt_type_temp = "DATA"
            }else{
                butt_type_temp = "PULSA"
            }
            if(data_product[x].is_promo == true){
                var is_promo_temp = "true"
                var init_price_temp = insert_flg(data_product[x].nominal)
                var promo_name_temp = data_product[x].promo_name
                var prod_nominal_temp = insert_flg(data_product[x].price)
                butt_desc_temp = butt_desc_temp + " - " + promo_name_temp
            }else{
                is_promo_temp = "false"
                promo_name_temp = "undefined"
                init_price_temp = "undefined"
                prod_nominal_temp = insert_flg(data_product[x].nominal)
            }
            button_listModel.append({"prod_nominal_":prod_nominal_temp,
                                        "prod_id_":prod_id_temp,
                                        "butt_type_":butt_type_temp,
                                        "butt_desc_":butt_desc_temp,
                                        "is_promo_":is_promo_temp,
                                        "promo_name_":promo_name_temp,
                                        "init_price_":init_price_temp})
        }
    }

    Text{
        id: tnc_text
        x: 86
        y: 740
        height: 20
        width: 500
        text: qsTr("Terms and Conditions Applied for Promo Products.")
        verticalAlignment: Text.AlignBottom
        font.pixelSize: 15
        wrapMode: Text.WordWrap
        font.family:"Microsoft YaHei"
        color:"#FFFFFF"
        textFormat: Text.PlainText
    }

    HideWindow{
        id:network_error
        //visible: true

        Image {
            id: img_network_error
            x: 437
            y: 412
            width: 150
            height: 180
            source: "img/otherImages/network-offline.png"
        }

        Text {
            id: text_notif
            x: 112
            y:232
            width: 800
            height: 150
            text: qsTr("Currently we are not connected to the system, Please retry later")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:network_error_back_button
            x:373
            y:607
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    network_error_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    network_error_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }
}


