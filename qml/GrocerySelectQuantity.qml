import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:customer_input_qty
    width: 1024
    height: 768
    property int timer_value: 60
    property var usageOf:"undefined"
    property int product_qty:1
    property var data_prod_:"-"
    property var prod_name:"-"
    property var prod_price:"0"
    property var prod_init_price:"0"
    property var prod_img_url:""
    property var prod_qr_url:""
    property var prod_sku:"-"
    //property var prod_disc:"-"
    property var prod_price_clean:"0"
    property var prod_stock: "1"

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log('data_product : ' + JSON.stringify(data_prod_) + ", for usage : " + usageOf)
            parse_data_product(data_prod_)
            abc.counter = timer_value
            my_timer.restart()
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    function parse_data_product(prod_){
        if(prod_==""){
            return
        }
        prod_name = prod_.name
        prod_sku = prod_.sku
        prod_price = prod_.total
        prod_init_price = prod_.price
        prod_img_url = patch_url(prod_.image)
        prod_qr_url = patch_url(prod_.qrcode)
        //prod_disc = insert_flg(prod_.discount)
        prod_price_clean = prod_.total
        prod_stock = prod_.stock
    }

    function patch_url(b){
        var newstrB=""
        if(b != null){
            newstrB = b.replace("https://shop.popbox.asia/assets/images","img/grocery_prod")
        }else{
            return b
        }
        return newstrB
    }

    function insert_flg(a){
        if(a != undefined){
            var newstrA=""
            newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
            return newstrA
        }else{
            return a
        }

    }

    function def_total_price(price, qty){
        if(price == "" || qty < 0){
            return
        }else{
            var total_price = parseInt(price) * qty
            if(total_price>1000000){
                main_page.enabled = false
                txt_notif.text = qsTr("Oops, Total purchase cannot be greater than 1.000.000 IDR at this time.")
                error_tips.open()
            }else if(qty>parseInt(prod_stock)){
                main_page.enabled = false
                var good_qty = qty - 1
                txt_notif.text = qsTr("Oops, At this time the maximum quantity cannot be greater than ") + good_qty.toString() + qsTr(" units.")
                error_tips.open()
            }else{
                return total_price.toString()
            }
        }
    }

    Rectangle{
        width: 50
        height: 50
        color: "#00000000"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Text{
            id:countShow_test
            x: 10
            y: 10
            anchors.centerIn:parent
            color:"#fff000"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:20
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow_test.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id:opacity_bground
        x: 0
        y: 0
        width: 1024
        height: 768
        color: "#472f2f"
        opacity: 0.4
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            id: ms_back
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
            onEntered:{
                select_service_back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                select_service_back_button.show_source = "img/button/7.png"
            }
        }
    }

    Rectangle{
        id: main_page
        x: 112
        y: 175
        width: 800
        height: 531
        color: "#00000000"
        visible: true
        enabled: true

        Text {
            id: notif_select_qty
            x: 8
            y: -35
            width: 800
            height: 60
            color: "#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("Please Select Quantity")
            font.bold: false
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 25
            textFormat: Text.AutoText
        }



        Rectangle {
            id: box_prod_detail
            width: 566
            height: 457
            color: "#e6dddd"
            x: 252
            y: 74
            radius: 25
            border.color: "#e6dddd"

            Text {
                id: prod_name_text
                x: 91
                y: 29
                width: 470
                height: 60
                color: "darkred"
                font.family:"Microsoft YaHei"
                text: prod_name + " - " + prod_sku
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignLeft
                font.pointSize:20
            }

            Text {
                id: prod_price_text
                x: 347
                y: 165
                width: 180
                height: 25
                color: "darkred"
                text: "Rp. " + insert_flg(prod_price) + ",-"
                font.bold: true
                wrapMode: Text.WordWrap
                font.pointSize: 15
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.family: "Microsoft YaHei"
            }

            Text {
                id: prod_init_price_label
                x: 117
                y: 122
                width: 178
                height: 25
                color: "darkred"
                text: qsTr("Initial Price/unit :")
                wrapMode: Text.WordWrap
                font.pointSize: 15
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                font.family: "Microsoft YaHei"
            }

            Text {
                id: prod_init_price_text
                x: 347
                y: 122
                width: 180
                height: 25
                color: "darkred"
                text: "Rp. " + insert_flg(prod_init_price) + ",-"
                font.italic: true
                font.strikeout: true
                wrapMode: Text.WordWrap
                font.pointSize: 15
                verticalAlignment: Text.AlignVCenter
                font.bold: false
                horizontalAlignment: Text.AlignRight
                font.family: "Microsoft YaHei"
            }

            Text {
                id: prod_price_label
                x: 117
                y: 165
                width: 178
                height: 25
                color: "darkred"
                text: qsTr("Price/unit :")
                wrapMode: Text.WordWrap
                font.pointSize: 15
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                font.family: "Microsoft YaHei"
            }

            Text{
                id: quantity_text
                x:412
                y:209
                width: 50
                height: 50
                color: "darkred"
                font.family: "Microsoft YaHei"
                font.bold: false
                text: product_qty
                font.pointSize: 30
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            Image{
                id: add_qty
                x:468
                y:209
                width: 50
                height: 50
                source:"img/item/add_red.png"
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        product_qty +=1
                        prod_total_price_text.text = "Rp. " + insert_flg(def_total_price(prod_price_clean, product_qty)) + ",-"
                    }
                }
            }

            Image{
                id: min_qty
                x:356
                y:209
                width: 50
                height: 50
                source:"img/item/min_red.png"
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        if(product_qty==1){
                            return
                        }
                        product_qty -=1
                        prod_total_price_text.text = "Rp. " + insert_flg(def_total_price(prod_price_clean, product_qty)) + ",-"
                    }
                }
            }

            Text {
                id: prod_total_price_text
                x: 127
                y: 299
                width: 400
                height: 60
                color: "darkred"
                text:  "Rp. " + insert_flg(def_total_price(prod_price_clean, product_qty)) + ",-"
                wrapMode: Text.WordWrap
                font.pointSize: 20
                verticalAlignment: Text.AlignTop
                font.bold: true
                horizontalAlignment: Text.AlignRight
                font.family: "Microsoft YaHei"
            }

            Text {
                id: total_purchase_label
                x: 117
                y: 299
                width: 179
                height: 42
                color: "darkred"
                text: qsTr("Total Purchase :")
                wrapMode: Text.WordWrap
                font.pointSize: 15
                verticalAlignment: Text.AlignTop
                horizontalAlignment: Text.AlignLeft
                font.strikeout: false
                font.family: "Microsoft YaHei"
            }

            Text {
                id: qty_prod_label
                x: 117
                y: 221
                width: 150
                height: 25
                color: "#8b0000"
                text: qsTr("Quantity :")
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignLeft
                font.family: "Microsoft YaHei"
                font.pointSize: 15
                verticalAlignment: Text.AlignVCenter
            }

            GroceryButton{
                id:continue_button
                x:144
                y:375
                show_text:qsTr("Buy")

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(usageOf=="popshop"){
                            var total_amount = def_total_price(prod_price_clean, product_qty)
                            var data_product = prod_sku + "|" + prod_name + "|" + product_qty + "|" + total_amount
                            my_stack_view.push(customer_input_details, {total_amount:total_amount, data_product: data_product, usageOf:usageOf})
                        }else{
                            return
                        }
                    }
                    onEntered:{
                        continue_button.show_source = "img/05/button.png"
                    }
                    onExited:{
                        continue_button.show_source = "img/05/red_1.png"
                    }
                }
            }

            Rectangle {
                id: row_red_1
                x: 99
                y: 100
                width: 455
                height: 2
                color: "darkred"
            }

            Rectangle{
                id: row_red_2
                x:99
                y:280
                height: 2
                width: 455
                color: "darkred"
            }

        }

        Rectangle{
            id: box_prod_img
            x: -14
            y: 41
            width: 350
            height: 450
            color: "white"
            radius: 25
            border.color: "white"

            Rectangle{
                id: rec_img
                x:25
                y:75
                width: parent.width - 50
                height: parent.height - 50
                color: "transparent"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                Image{
                    id: prod_img
                    anchors.fill: parent
                    source: prod_img_url
                    fillMode: Image.PreserveAspectFit

                }
            }

            Image{
                id: url_img
                x:192
                y:292
                width: 150
                height: 150
                source: prod_qr_url
                fillMode: Image.PreserveAspectFit
            }
        }

    }


    HideWindow{
        id:error_tips
        //visible: true

        Image {
            id: img_error_tips
            x: 115
            y: 298
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            id: txt_notif
            x: 343
            y:323
            width: 554
            height: 150
            //text: qsTr("Oops, Total purchase cannot be greater than 1.000.000 IDR at this time.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:error_tips_back
            x:383
            y:561
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    abc.counter = timer_value
                    my_timer.restart()
                    product_qty -= 1
                    prod_total_price_text.text = "Rp. " + insert_flg(def_total_price(prod_price_clean, product_qty)) + ",-"
                    main_page.enabled = true
                    error_tips.close()
                }
                onEntered:{
                    error_tips_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    error_tips_back.show_source = "img/button/7.png"
                }
            }
        }
    }

}
