<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id_ID">
<context>
    <name>Advertisement</name>
    <message>
        <location filename="Advertisement.qml" line="143"/>
        <source>Click to enter the system</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Advertisement.qml" line="155"/>
        <source>Click to enter the system _en</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <location filename="BackButton.qml" line="12"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>BATAL</translation>
    </message>
</context>
<context>
    <name>BackButtonMenu</name>
    <message>
        <location filename="BackButtonMenu.qml" line="12"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>BATAL</translation>
    </message>
</context>
<context>
    <name>OkButton</name>
    <message>
        <location filename="OkButton.qml" line="12"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>BackgroundLoginPage</name>
    <message>
        <location filename="BackgroundLoginPage.qml" line="260"/>
        <source>Please wait</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="277"/>
        <source>Network error, Please retry later</source>
        <translation>Oops, Loker sedang mencoba kembali menghubungkan ke sistem, Silakan menunggu beberapa saat.</translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="291"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="321"/>
        <source>CARDID:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="330"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BackgroundOverdueTimePage</name>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="89"/>
        <location filename="BackgroundOverdueTimePage.qml" line="639"/>
        <location filename="BackgroundOverdueTimePage.qml" line="687"/>
        <location filename="BackgroundOverdueTimePage.qml" line="741"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="111"/>
        <source>take back it</source>
        <translation>Ambil kembali</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="147"/>
        <source>take back all</source>
        <translation>Ambil semua</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="210"/>
        <source>Phone</source>
        <translation>Nomor HP</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="223"/>
        <source>store-time</source>
        <translation>Waktu Simpan</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="236"/>
        <source>box</source>
        <translation>Loker</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="249"/>
        <source>overdue-time</source>
        <translation>Waktu Terlambat</translation>
    </message>
    <message>
        <source>other</source>
        <translation>Lainnya</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="609"/>
        <source>waiting</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="626"/>
        <source>set_null</source>
        <translation>Tidak ada paket
melewati batas waktu</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="674"/>
        <source>Successful</source>
        <translation>Berhasil</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="728"/>
        <source>Failed</source>
        <translation>    Pengambilan gagal
Silakan hubungi layanan pelanggan PopBox.</translation>
    </message>
    <message>
        <source>Please wait</source>
        <translation type="vanished">請稍等</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">操作成功</translation>
    </message>
    <message>
        <source>Can not set the box</source>
        <translation type="vanished">操作失敗</translation>
    </message>
</context>
<context>
    <name>BackspaceButton</name>
    <message>
        <location filename="BackspaceButton.qml" line="17"/>
        <source>backspace</source>
        <translation>BATAL</translation>
    </message>
</context>
<context>
    <name>BoxManagePage</name>
    <message>
        <location filename="BoxManagePage.qml" line="93"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="110"/>
        <source>Init</source>
        <translation>Inisiasi DB</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="129"/>
        <source>main locker</source>
        <translation>Pintu Utama</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="152"/>
        <source>Please wait</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="169"/>
        <source>Successful</source>
        <translation>Berhasil</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="182"/>
        <location filename="BoxManagePage.qml" line="242"/>
        <location filename="BoxManagePage.qml" line="283"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="208"/>
        <source>Version:</source>
        <translation>Nomor versi:</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="216"/>
        <source>Text</source>
        <translation>  </translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="229"/>
        <source>open mouth success</source>
        <translation>Buka pintu loker berhasil</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="270"/>
        <source>open mouth failure</source>
        <translation>Buka pintu loker gagal</translation>
    </message>
</context>
<context>
    <name>CourierInputPhonePage</name>   
    <message>
        <location filename="CourierInputPhonePage.qml" line="61"/>
        <source>Enter Recipient Phone Number</source>
        <translation>Masukkan Nomor Telepon Seluler</translation>
    </message>
    <message>
        <location filename="CourierInputPhonePage.qml" line="98"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="CourierInputPhonePage.qml" line="183"/>
        <source>The memory is not input or not correct</source>
        <translation>Oops, Terjadi kesalahan. Pastikan nomor telepon yang dimasukkan adalah benar.</translation>
    </message>
    <message>
        <location filename="CourierInputPhonePage.qml" line="272"/>
        <source>This parcel is detected under Lazada Account</source>
        <translation>Paket ini dikenali sebagai Paket Lazada</translation>
    </message>
    <message>
        <location filename="CourierInputPhonePage.qml" line="196"/>
        <source>back</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>CourierInputPhoneSurePage</name>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="113"/>
        <source>Please check the customer mobile phone number</source>
        <translation>Mohon periksa kembali nomor seluler pelanggan</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="126"/>
        <source>(customer verification code and QR CODE will be sent)</source>
        <translation>(Kode verifikasi dan PIN pelanggan akan dikirimkan)</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="152"/>
        <location filename="CourierInputPhoneSurePage.qml" line="251"/>
        <source>cancel</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="174"/>
        <location filename="CourierInputPhoneSurePage.qml" line="273"/>
        <source>ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="221"/>
        <source>Notification will be sent immediately to the administrator number.</source>
        <translation>Notifikasi dan kode PIN paket akan dikirimkan ke nomor Admin.</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="236"/>
        <source>Please Press OK to continue the process.</source>
        <translation>Tekan OK untuk melanjutkan.</translation>
    </message>    
</context>
<context>
    <name>CourierLoginButton</name>
    <message>
        <location filename="CourierLoginButton.qml" line="27"/>
        <source>login</source>
        <translation>MASUK</translation>
    </message>
</context>
<context>
    <name>CourierMemoryPage</name>
    <message>
        <location filename="CourierMemoryPage.qml" line="98"/>
        <source>Please scan or enter a tracking number</source>
        <translation>Pindai/Masukkan Nomor AWB</translation>
    </message>
    <message>
        <location filename="CourierMemoryPage.qml" line="243"/>
        <source>We detected the order number is not entered yet. Please retry and enter the correct order number.</source>
        <translation>Kami dapati nomor AWB belum dimasukkan. Silakan ulangi dan masukkan nomor AWB yang benar.</translation>
    </message>
    <message>
        <location filename="CourierMemoryPage.qml" line="221"/>
        <location filename="CourierMemoryPage.qml" line="277"/>
        <source>back</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="CourierMemoryPage.qml" line="305"/>
        <source>Please ensure the order number and locker location is correct. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Pastikan nomor AWB dan lokasi loker adalah benar. Silakan hubungi CS PopBox di 021-22538719 untuk bantuan.</translation>
    </message>
</context>
<context>
    <name>CourierOverdueTimePage</name>
    <message>
        <location filename="CourierOverdueTimePage.qml" line="56"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="CourierOverdueTimePage.qml" line="79"/>
        <source>Retrieve</source>
        <translation>Ambil Kembali</translation>
    </message>
    <message>
        <location filename="CourierOverdueTimePage.qml" line="100"/>
        <source>Retrieve All</source>
        <translation>Ambil Semua</translation>
    </message>
</context>
<context>
    <name>CourierPswErrorPage</name>
    <message>
        <location filename="CourierPswErrorPage.qml" line="53"/>
        <source>Oops, Failed Authentication!</source>
        <translation>Maaf, Data login tidak dikenali!</translation>
    </message>
    <message>
        <location filename="CourierPswErrorPage.qml" line="86"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="CourierPswErrorPage.qml" line="77"/>
        <source>Please re-enter your account name and password, thank you!</source>
        <translation>Masukkan kembali data login yang benar!</translation>
    </message>
</context>
<context>
    <name>CourierSelectBoxSizePage</name>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="130"/>
        <source>please select service option</source>
        <translation>Pilih Ukuran Loker</translation>
    </message>    
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="319"/>
        <source>return</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="383"/>
        <source>not_mouth</source>
        <translation>Loker sudah penuh</translation>
    </message>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="396"/>
        <location filename="CourierSelectBoxSizePage.qml" line="439"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="426"/>
        <source>not_balance</source>
        <translation>Saldo tidak cukup</translation>
    </message>
</context>
<context>
    <name>CourierServicePage</name>        
    <message>
        <location filename="CourierServicePage.qml" line="67"/>
        <source>Store</source>
        <translation>Simpan</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="96"/>
        <source>Overdue</source>
        <translation>Terlambat</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="121"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="139"/>
        <source>Take</source>
        <translation>Ambil</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="168"/>
        <source>History</source>
        <translation>Catatan</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="197"/>
        <source>Reject</source>
        <translation>Kembalikan</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="226"/>
        <source>Other services</source>
        <translation>Lain-lain</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="255"/>
        <source>return</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="270"/>
        <source>Please select service options</source>
        <translation>Pilih Opsi Layanan</translation>
    </message>   
    <message>
        <location filename="CourierServicePage.qml" line="359"/>
        <source>TAKE :</source>
        <translation>AMBIL :</translation>
    </message> 
    <message>
        <location filename="CourierServicePage.qml" line="371"/>
        <source>Select this button to collect parcel/laundry from this locker.</source>
        <translation>Pilih menu ini untuk mengambil paket/laundri dari loker ini.</translation>
    </message> 
    <message>
        <location filename="CourierServicePage.qml" line="395"/>
        <source>STORE :</source>
        <translation>SIMPAN :</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="407"/>
        <source>Select this button to store parcels into this locker.</source>
        <translation>Pilih menu ini untuk menyimpan paket yang akan dikirimkan.</translation>
    </message>   
</context>
<context>
    <name>CustomerInputCustomerStoreNumber</name>
    <message>
        <location filename="CustomerInputCustomerStoreNumber.qml" line="84"/>
        <source>Please scan or enter a tracking number</source>
        <translation>Pindai/Masukkan Nomor Pesanan</translation>
    </message>
</context>
<context>
    <name>CustomerScanQrCodePage</name>
    <message>
        <location filename="CustomerScanQrCodePage.qml" line="58"/>
        <source>Please open your QR code, 
 the code sent by SMS 
 to your mobile. Please QR 
 code placed in front of the scan window 
 10cm Office</source>
        <translation>Letakkan kode 
QR yang sudah 
dikirim ke hp 
anda via SMS di 
depan Pemindai 
di bawah ini.</translation>
    </message>
</context>
<context>
    <name>CustomerTakeExpressPage</name>    
    <message>
        <location filename="CustomerTakeExpressPage.qml" line="37"/>
        <source>Scan or Enter Barcode</source>
        <translation>Masukkan Kode Pin dan Tekan OK</translation>
    </message>
    <message>
        <source>The memory is not input</source>
        <translation>Oops, Kode pin belum dimasukkan dengan benar.</translation>
    </message>
    <message>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>DoorEvey</name>
    <message>
        <location filename="DoorEvey.qml" line="12"/>
        <source>return to menu</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>DoorOpenPage</name>
    <message>
        <location filename="DoorOpenPage.qml" line="38"/>
        <source>Help</source>
        <translation>Bantuan</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="95"/>
        <source>Your locker is located at Box No </source>
        <translation>Paket Anda berada di loker no. </translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="120"/>
        <source>The system will refresh in</source>
        <translation>Sistem akan dimuat ulang dalam</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="177"/>
        <source>sec</source>
        <translation> detik</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="189"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Pastikan pintu loker tertutup kembali</translation>
    </message>
    <message>
        <source>re-open</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <source>Store packages again</source>
        <translation>Simpan Paket Lainnya</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="64"/>
        <source>Complete and exit</source>
        <translation>Selesai</translation>
    </message>
    <message>
        <source>Please put the express into NO.</source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="102"/>
        <source>X</source>
        <translation>x</translation>
    </message>
    <message>
        <source>,thank you</source>
        <translation type="vanished">號格口已經打開，請存件，謝謝！</translation>
    </message>
</context>
<context>
    <name>DropOffThankspage1</name>
    <message>
        <location filename="DropOffThankspage1.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Terima kasih telah menggunakan PopBox.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="67"/>
        <source>A SMS notification has been sent to</source>
        <translation>SMS notifikasi telah dikirim ke</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="75"/>
        <source>the recipient of this parcel.</source>
        <translation>penerima paket.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="109"/>
        <source>Send Another Parcel?</source>
        <translation>Kirim paket lain?</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>Sudah selesai</translation>
    </message>
</context>
<context>
    <name>DropOffThankspage2</name>
    <message>
        <location filename="DropOffThankspage2.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Terima kasih telah menggunakan PopBox.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="67"/>
        <source>A SMS notification has been sent to</source>
        <translation>SMS notifikasi telah dikirim ke</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="75"/>
        <source>the sender of this parcel.</source>
        <translation>pengirim paket.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="109"/>
        <source>Send Another Parcel?</source>
        <translation>Kirim paket lain?</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>Sudah selesai</translation>
    </message>
</context>
<context>
    <name>FAQWebPage</name>
    <message>
        <location filename="FAQWebPage.qml" line="54"/>
        <source>Back</source>
        <translation>Batal</translation>
    </message>
</context>
<context>
    <name>InsertCoinPage</name>
    <message>
        <location filename="InsertCoinPage.qml" line="38"/>
        <source>Welcome to use the system</source>
        <translation>Selamat datang</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="55"/>
        <source>You pay the overdue amount</source>
        <translation>Anda telah membayar</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="65"/>
        <source>return</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="88"/>
        <source>Please put a coin in the slot</source>
        <translation>Masukkan koin</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="148"/>
        <source>yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>KeyboardFunctionButton</name>
    <message>
        <location filename="KeyboardFunctionButton.qml" line="19"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>KeyboardFunctionButtondelete</name>
    <message>
        <location filename="KeyboardFunctionButtondelete.qml" line="18"/>
        <source>DEL</source>
        <translation>HAPUS</translation>
    </message>
</context>
<context>
    <name>LoadingView</name>
    <message>
        <location filename="LoadingView.qml" line="36"/>
        <source>Preparing...</source>
        <translation>Mempersiapkan...</translation>
    </message>
</context>
<context>
    <name>LockerHelpPage</name>
    <message>
        <location filename="LockerHelpPage.qml" line="78"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Jika ada pertanyaan, Tekan tombol Batal dan Silakan hubungi Kami.</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="92"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation>Paket Anda terlalu besar, Silakan tekan tombol Ganti-Loker</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="106"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation>Jika pintu loker tidak terbuka, Silakan tekan tombol Buka-Kembali</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="120"/>
        <source>back to menu</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="146"/>
        <source>re-open</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="175"/>
        <source>change-box</source>
        <translation>Ganti-Loker</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="203"/>
        <source>cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="231"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Silakan tekan tombol di bawah ini :</translation>
    </message>
</context>
<context>
    <name>ManageInitPage</name>
    <message>
        <location filename="ManageInitPage.qml" line="95"/>
        <source>Back</source>
        <translation>KEMBALI</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="113"/>
        <location filename="ManageInitPage.qml" line="132"/>
        <source>Init</source>
        <translation>Inisiasi DB</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="157"/>
        <source>Please wait</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="174"/>
        <source>Successful</source>
        <translation>Berhasil</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="187"/>
        <location filename="ManageInitPage.qml" line="247"/>
        <location filename="ManageInitPage.qml" line="288"/>
        <source>back</source>
        <translation>KEMBALI</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="213"/>
        <source>Version:</source>
        <translation>Nomor versi:</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="221"/>
        <source>Text</source>
        <translation>  </translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="234"/>
        <source>open mouth success</source>
        <translation>Buka pintu loker berhasil</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="275"/>
        <source>open mouth failure</source>
        <translation>Buka pintu loker gagal</translation>
    </message>
</context>
<context>
    <name>ManagerCabinetPage</name>
    <message>
        <location filename="ManagerCabinetPage.qml" line="270"/>
        <source>Not Use</source>
        <translation>Rusak</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="283"/>
        <source>Available</source>
        <translation>Kosong</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="296"/>
        <source>Used</source>
        <translation>Penuh</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="309"/>
        <source>XS</source>
        <translation>XS:</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="322"/>
        <source>S</source>
        <translation>S :</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="335"/>
        <source>M</source>
        <translation>M :</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="348"/>
        <source>L</source>
        <translation>L :</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="361"/>
        <source>XL</source>
        <translation>XL:</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="377"/>
        <source>From the ark set</source>
        <translation>Pengaturan</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="399"/>
        <source>exit</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="423"/>
        <source>A key to unlock</source>
        <translation>Buka semua</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="531"/>
        <source>Is a key to unlock</source>
        <translation>Buka semua pintu?</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="544"/>
        <source>Yes</source>
        <translation>Ya</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="583"/>
        <source>No</source>
        <translation>Tidak</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="617"/>
        <source>Please wait</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="634"/>
        <source>Success</source>
        <translation>Sukses</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="647"/>
        <location filename="ManagerCabinetPage.qml" line="694"/>
        <location filename="ManagerCabinetPage.qml" line="742"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="681"/>
        <source>Coming Soon</source>
        <translation>Segera Datang</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="729"/>
        <source>Can not set the box</source>
        <translation>Gagal</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="784"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <source> LastUsed Time: </source>
        <translation type="vanished">最近使用時間：</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="829"/>
        <source>The door is type</source>
        <translation>Tipe:</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="864"/>
        <source>The door is number</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="899"/>
        <source>set enabled</source>
        <translation>Pengaturan：</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="914"/>
        <source>unenable to set the box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1115"/>
        <source> ENABLE</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1137"/>
        <source> LOCKED</source>
        <translation>Dikunci</translation>
    </message>
    <message>
        <source> USED</source>
        <translation>Terpakai</translation>
    </message>
    <message>
        <source>LOCKED</source>
        <translation>Dikunci</translation>
    </message>
    <message>
        <source>ENABLE</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <source>USED</source>
        <translation>Terpakai</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="970"/>
        <source>unlocking</source>
        <translation>Dibuka</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1008"/>
        <source>Empty the box</source>
        <translation>Kosong</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1039"/>
        <source>sure</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1076"/>
        <source> return</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>ManagerServicePage</name>
    <message>
        <location filename="ManagerServicePage.qml" line="86"/>
        <source>sys-info</source>
        <translation>Sis-Info</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="116"/>
        <source>box</source>
        <translation>Loker</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="145"/>
        <source>unusual</source>
        <translation>Tidak Biasa</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="174"/>
        <source>overdue</source>
        <translation>Terlambat</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="198"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="219"/>
        <source>address:</source>
        <translation>Alamat :</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="234"/>
        <source>number:</source>
        <translation>Nomor :</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="248"/>
        <source>name:</source>
        <translation>Nama :</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="381"/>
        <source>take_reject</source>
        <translation>Pengembalian</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="408"/>
        <source>sure to quit</source>
        <translation>Anda ingin Keluar?</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="421"/>
        <source>sure</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="444"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <source>pakpobox</source>
        <translation>PopBox</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="301"/>
        <source>exit</source>
        <translation>Keluar</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="332"/>
        <source>return</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="357"/>
        <source>take_send</source>
        <translation>Ambil</translation>
    </message>
</context>
<context>
    <name>ManagerTakeOverdueExpressPage</name>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="96"/>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="574"/>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="622"/>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="674"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="118"/>
        <source>take back it</source>
        <translation>Ambil kembali</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="154"/>
        <source>take back all</source>
        <translation>Ambil semua</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="217"/>
        <source>Phone</source>
        <translation>Nomor HP</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="230"/>
        <source>store-time</source>
        <translation>Waktu Simpan</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="243"/>
        <source>box</source>
        <translation>Loker</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="256"/>
        <source>overdue-time</source>
        <translation>Waktu terlambat</translation>
    </message>
    <message>
        <source>other</source>
        <translation type="vanished">備註</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="544"/>
        <source>waiting</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="561"/>
        <source>set_null</source>
        <translation>Tidak ada paket 
melewati batas waktu</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="609"/>
        <source>Successful</source>
        <translation>Berhasil</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="661"/>
        <source>Failed</source>
        <translation>    Pengambilan gagal
Silakan hubungi layanan pelanggan PopBox.</translation>
    </message>
</context>
<context>
    <name>NumKeyboard</name>
    <message>
        <location filename="NumKeyboard.qml" line="72"/>
        <source>return</source>
        <translation>BATAL</translation>
    </message>
    <message>
        <location filename="NumKeyboard.qml" line="80"/>
        <source>DEL</source>
        <oldsource>delete</oldsource>
        <translation>HAPUS</translation>
    </message>
</context>
<context>
    <name>OnDevelopPage</name>
    <message>
        <location filename="OnDevelopPage.qml" line="56"/>
        <source>Coming Soon</source>
        <translation>Segera Datang</translation>
    </message>
    <message>
        <location filename="OnDevelopPage.qml" line="67"/>
        <source>return</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>OpenDoorPage</name>
    <message>
        <location filename="OpenDoorPage.qml" line="49"/>
        <source>Open again</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="77"/>
        <source>Confirm receipt</source>
        <translation>Selesai</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="105"/>
        <source>Your locker is located at Box No </source>
        <translation>Paket Anda di dalam box no.</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="130"/>
        <source>The system will refresh in</source>
        <translation>Sistem akan dimuat ulang dalam</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="182"/>
        <source>sec</source>
        <translation>detik</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="192"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Pastikan pintu loker tertutup setelah selesai</translation>
    </message>
    <message>
        <source>Please</source>
        <translation type="vanished">請於</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="112"/>
        <source>X</source>
        <translation>x</translation>
    </message>
    <message>
        <source>No.</source>
        <translation type="vanished">號</translation>
    </message>
    <message>
        <source>box take your package</source>
        <translation type="vanished">箱領取您的物件</translation>
    </message>
    <message>
        <source>(such as a problem, please call 39043668)</source>
        <oldsource>(such as a problem, please call 88888888)</oldsource>
        <translation>Membutuhkan bantuan, Silakan hubungi 021 290 22537</translation>
    </message>
    <message>
        <source>The system will be</source>
        <translation type="vanished">系統將於</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="vanished">秒</translation>
    </message>
    <message>
        <source>Confirmation automatically</source>
        <translation type="vanished">後自動確認</translation>
    </message>
    <message>
        <source>After stripping, please close the door, thank you for your use of the service</source>
        <translation>Setelah penggunaan, Harap tutup kembali pintu loker. Terima Kasih.</translation>
    </message>
</context>
<context>
    <name>OtherInputMemoryPage</name>
    <message>
        <location filename="OtherInputMemoryPage.qml" line="66"/>
        <source>Scan or Enter AWB code</source>
        <translation>Pindai/Masukkan Nomor Pesanan</translation>
    </message>
    <message>
        <location filename="OtherInputMemoryPage.qml" line="217"/>
        <source>We detected the order number is not entered yet. Please retry and enter the correct order number.</source>
        <translation>Kami dapati nomor pesanan belum dimasukkan. Silakan ulangi dan masukkan nomor pesanan yang benar.</translation>
    </message>
    <message>
        <location filename="OtherInputMemoryPage.qml" line="206"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>OtherInsufficientBalancePage</name>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="56"/>
        <source>Insufficient balance</source>
        <translation>Saldo Tidak Cukup</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="64"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="89"/>
        <source>Balance</source>
        <translation>Sisa saldo</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="96"/>
        <source>Total Amount</source>
        <translation>Jumlah bayar</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="107"/>
        <location filename="OtherInsufficientBalancePage.qml" line="113"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="139"/>
        <source>Please top up your prepaid card.</source>
        <translation>Nilai transaksi maksimal menggunakan e-Money adalah Rp 1.000.000.</translation>
    </message>
</context>
<context>
    <name>OtherPaymentConfirmationPage</name>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="71"/>
        <source>Payment confirmation</source>
        <translation>Konfirmasi Pembayaran</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="79"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="100"/>
        <source>Confirm</source>
        <translation>Konfirmasi</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="132"/>
        <source>Balance</source>
        <translation>Sisa saldo</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="139"/>
        <source>Total Amount</source>
        <translation>Jumlah bayar</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="150"/>
        <location filename="OtherPaymentConfirmationPage.qml" line="156"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="182"/>
        <source>Please do not remove your pre-paid card.</source>
        <translation>Mohon tidak memindahkan kartu e-Money Anda.</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="188"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>             Dengan mengklik tombol konfirmasi,
Anda setuju terhadap syarat dan ketentuan yang berlaku.</translation>
    </message>
</context>
<context>
    <name>OtherPaymentFailedPage</name>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="58"/>
        <source>Unfortunately your payment has failed.</source>
        <translation>Mohon maaf pembayaran Anda gagal.</translation>
    </message>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="67"/>
        <source>Please ensure that your balance is sufficient and the total amount is not greater than 1.000.000 IDR.</source>
        <translation>Pastikan saldo e-Money Anda mencukupi dan jumlah bayar tidak lebih besar dari Rp. 1.000.000</translation>
    </message>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="83"/>
        <source>Cancel</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="104"/>
        <source>Retry</source>
        <translation>Coba Lagi</translation>
    </message>
</context>
<context>
    <name>OtherPaymentSuccessfulPage</name>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="64"/>
        <source>Order Details</source>
        <translation>Pembayaran Berhasil</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="71"/>
        <source>The sms notification will be sent to you to pickup your parcel</source>
        <translation>Anda akan dikirimkan SMS untuk mengambil barang.</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="79"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="105"/>
        <source>Amount</source>
        <translation>Jumlah bayar</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="112"/>
        <source>Last Balance</source>
        <translation>Sisa Saldo</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="118"/>
        <source>Card</source>
        <translation>Nomor Kartu</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="124"/>
        <source>Data</source>
        <translation>Tanggal</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="130"/>
        <source>Locker</source>
        <translation>Loker</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="136"/>
        <source>Terminal ID</source>
        <translation>ID terminal</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="147"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="153"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="159"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="165"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="171"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="177"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="229"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Silakan mengambil kembali kartu e-Money Anda.</translation>
    </message>
</context>
<context>
    <name>OtherSearchFailPage</name>
    <message>
        <location filename="OtherSearchFailPage.qml" line="53"/>
        <source>Sorry we&apos;re unable to verify the order number</source>
        <translation>Maaf kami tidak dapat memverifikasi nomor pesanan Anda.</translation>
    </message>
    <message>
        <location filename="OtherSearchFailPage.qml" line="59"/>
        <source>Please contact Customer Service at</source>
        <translation>Untuk bantuan, mohon menghubungi</translation>
    </message>
    <message>
        <location filename="OtherSearchFailPage.qml" line="65"/>
        <source>021 290 22537 for the assistance</source>
        <translation>Layanan pelanggan kami di nomor 021 290 22537.</translation>
    </message>
    <message>
        <location filename="OtherSearchFailPage.qml" line="73"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>OtherSearchPaidPage</name>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="81"/>
        <source>This order number is </source>
        <translation>Nomor order ini sudah dibayar pada</translation>
    </message>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="88"/>
        <source>This  is </source>
        <translation>Untuk bantuan, mohon menghubungi layanan</translation>
    </message>
    <message>
        <source>already paid on .....</source>
        <translation type="vanished">Untuk bantuan,mohon menghubungi layanan</translation>
    </message>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="97"/>
        <source>.....</source>
        <translation>pelanggan kami di nomor 021 290 22537.</translation>
    </message>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="57"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>OtherServiceExpressInfo</name>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="94"/>
        <source>Order Details</source>
        <translation>Rincian Pesanan</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="102"/>
        <source>Back</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="123"/>
        <source>Confirm</source>
        <translation>Konfirmasi</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="156"/>
        <source>Order Id</source>
        <translation>No Pesanan</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="162"/>
        <source>Online Shop</source>
        <translation>Toko Online</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="168"/>
        <source>Customer Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="174"/>
        <source>Customer Phone</source>
        <translation>No. Telepon</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="180"/>
        <source>Total Amount</source>
        <translation>Jumlah bayar</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="372"/>
        <source>Please place your card to the reader</source>
        <translation>Silakan tempelkan kartu Anda
pada reader e-Money</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="404"/>
        <location filename="OtherServiceExpressInfo.qml" line="490"/>
        <source>seconds left</source>
        <translation>detik lagi</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="267"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Siapkan kartu e-money Anda dan pastikan saldo di kartu mencukupi.</translation>
    </message>    
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="284"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>Dengan mengklik tombol `Konfirmasi`, Anda setuju terhadap syarat dan ketentuan yang berlaku.</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="442"/>
        <source>Transaction Unfinished</source>
        <translation>Transaksi Anda belum selesai</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="458"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Mohon tempelkan kembali kartu e-Money Anda.</translation>
    </message>    
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="543"/>
        <source>Transaction Failed</source>
        <translation>Transaksi Gagal</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="558"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Pastikan Anda menggunakan kartu Mandiri e-Money.</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="581"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
</context>
<context>
    <name>OtherServicePage</name>
    <message>
        <location filename="OtherServicePage.qml" line="50"/>
        <source>other service</source>
        <translation>Layanan Lain</translation>
    </message>
    <message>
        <location filename="OtherServicePage.qml" line="58"/>
        <source>Cash On Pick Up</source>
        <translation>Pembayaran</translation>
    </message>
    <message>
        <location filename="OtherServicePage.qml" line="85"/>
        <source>Laundry</source>
        <translation>Laundry</translation>
    </message>
    <message>
        <location filename="OtherServicePage.qml" line="116"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>OverTimePage</name>
    <message>
        <source>Your package has been more than a free pick-up time</source>
        <translation type="vanished">Paket anda sudah melebihi jangka waktu gratis yang diberikan</translation>
    </message>
    <message>
        <source>If you want to take it, please pay</source>
        <translation type="vanished">Jika anda ingin tetap mengambil, mohon membayar</translation>
    </message>
    <message>
        <source>If you have any questions, please call 888888</source>
        <oldsource>If you have any questions, please call 39043668</oldsource>
        <translation type="vanished">Jika ada pertanyaan, Silakan hubungi 39043668</translation>
    </message>
    <message>
        <source>yuan</source>
        <translation type="vanished">IDR</translation>
    </message>
    <message>
        <location filename="OverTimePage.qml" line="75"/>
        <source>Your parcel has expired and returned to the sender.</source>
        <translation>     Paket Anda sudah melebihi batas waktu
    dan akan dikembalikan kepada pengirim.</translation>
    </message>
    <message>
        <location filename="OverTimePage.qml" line="81"/>
        <source>Please contact (021) 2902 2537 for further inquiry.</source>
        <translation>Tolong hubungi Kami di 021-22538719 untuk bantuan.</translation>
    </message>
    <message>
        <location filename="OverTimePage.qml" line="90"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>PassWordErrorPage</name>
    <message>
        <location filename="PassWordErrorPage.qml" line="59"/>
        <source>The code is not correct, please re-input</source>
        <translation>Kode salah, Silakan masukkan kembali</translation>
    </message>
    <message>
        <location filename="PassWordErrorPage.qml" line="68"/>
        <source>MAIN MENU</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="PassWordErrorPage.qml" line="92"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>PayMentPage</name>
    <message>
        <location filename="PayMentPage.qml" line="65"/>
        <source>Please choose the method of payment</source>
        <translation>Pilih metode pembayaran</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="71"/>
        <source>If you have any questions, please contact ********</source>
        <translation>Jika ada pertanyaan, Silakan hubungi 39043668</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="80"/>
        <source>octopus</source>
        <translation>OCTOPUS</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="105"/>
        <source>coin</source>
        <translation>KOIN</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="130"/>
        <source>app</source>
        <translation>APP</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="156"/>
        <source>back to menu</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>PickUpThankspage</name>
    <message>
        <location filename="PickUpThankspage.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Terima kasih telah menggunakan PopBox.</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="67"/>
        <source>A SMS notification has been sent to </source>
        <oldsource>A SMS notification has been sent to</oldsource>
        <translation>SMS notifikasi telah dikirimkan kepada</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="75"/>
        <source>the sender of this parcel.</source>
        <translation>pengirim paket.</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="109"/>
        <source>Open Another Locker?</source>
        <translation>Buka Loker Lain?</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>Sudah selesai</translation>
    </message>
</context>
<context>
    <name>RejectDoorOpenPage</name>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="52"/>
        <source>change-box</source>
        <translation>Ganti-Loker</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="79"/>
        <source>Open again</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="109"/>
        <source>Confirm receipt</source>
        <translation>Selesai</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="143"/>
        <source>Your locker is located at Box No </source>
        <translation>Letakkan paket Anda di loker no. </translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="151"/>
        <source>X</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="168"/>
        <source>The system will refresh in</source>
        <translation>Sistem akan dimuat ulang dalam</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="224"/>
        <source>sec</source>
        <translation>detik</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="233"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Pastikan pintu loker tertutup kembali</translation>
    </message>
</context>
<context>
    <name>RejectExpressHelp</name>
    <message>
        <location filename="RejectExpressHelp.qml" line="78"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Jika ada pertanyaan, Silakan tekan tombol batal dan hubungi Kami.</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="92"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation>Paket Anda terlalu besar, Silakan tekan tombol Ganti-Loker</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="106"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation>Jika pintu loker tidak terbuka, Silakan tekan tombol Buka-Kembali</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="120"/>
        <source>back to menu</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="146"/>
        <source>re-open</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="175"/>
        <source>change-box</source>
        <translation>Ganti-Loker</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="203"/>
        <source>cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="231"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Silakan tekan tombol di bawah ini :</translation>
    </message>
</context>
<context>
    <name>RejectExpressInfo</name>
    <message>
        <location filename="RejectExpressInfo.qml" line="252"/>
        <source>cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="273"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="118"/>
        <source>Return Details</source>
        <translation>Rincian Pengembalian</translation>
    </message>    
    <message>
        <location filename="RejectExpressInfo.qml" line="237"/>
        <source>Package ID:</source>
        <translation>ID Paket:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="221"/>
        <source>Onlineshop:</source>
        <translation>Toko Online:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="299"/>
        <source>Address:</source>
        <translation>Alamat:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="205"/>
        <source>Sender No:</source>
        <translation>No. Pengirim:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="189"/>
        <source>Locker Location:</source>
        <translation>Lokasi Loker:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="314"/>
        <source>not_mouth</source>
        <translation>Loker tidak tersedia</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="327"/>
        <location filename="RejectExpressInfo.qml" line="367"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="354"/>
        <source>no_payOfAmount</source>
        <translation>Metode pembayaran tidak ditemukan</translation>
    </message>
</context>
<context>
    <name>RejectExpressThankPage</name>
    <message>
        <location filename="RejectExpressThankPage.qml" line="49"/>
        <source>Thank You for using PopBox.</source>
        <translation>Terima kasih telah menggunakan PopBox.</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="57"/>
        <source>The Merchant has been notified of</source>
        <translation>Penjual telah diberitahukan mengenai</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="65"/>
        <source>the return of this parcel.</source>
        <translation>pengembalian paket ini.</translation>
    </message>
    <message>
        <source>MAIN MENU</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="73"/>
        <source>Send Another Parcel?</source>
        <translation>Retur Paket Lain?</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="94"/>
        <source>I&apos;m Done!</source>
        <translation>Sudah selesai!</translation>
    </message>
</context>
<context>
    <name>RejectExpressWeightPage</name>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="77"/>
        <location filename="RejectExpressWeightPage.qml" line="99"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="122"/>
        <source>Cancel and Exit</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="149"/>
        <source>Please put the package into ths platform</source>
        <translation>Masukkan paket ke dalam sini</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="158"/>
        <source>The system will calculate the postage for you</source>
        <translation>Sistem akan menghitung 
ongkos kirim anda</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="169"/>
        <source>Weight</source>
        <translation>Berat</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="179"/>
        <source>Cost</source>
        <translation>Harga</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="199"/>
        <source>Continue</source>
        <translation>Lanjutkan</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="233"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="242"/>
        <source>Yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>RejectInputMemoryPage</name>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="134"/>
        <source>Scan or Enter Barcode</source>
        <translation>Pindai/Masukkan Nomor ID Paket</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="244"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="260"/>
        <source>We detected the package id is not entered yet. Please retry and enter the correct package id.</source>
        <translation>Kami dapati nomor id paket belum dimasukkan. Silakan ulangi dan masukkan nomor id paket yang benar.</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="309"/>
        <source>Dear Customer</source>
        <translation>Pelanggan YTH</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="324"/>
        <source>Sorry We are unable to verify the package id. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Mohon maaf nomor id paket tidak dapat diverifikasi. Silakan hubungi CS PopBox di 021-22538719 untuk bantuan.</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="382"/>
        <source>retry</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="411"/>
        <source>main menu</source>
        <translation>Menu Utama</translation>
    </message>
</context>
<context>
    <name>RejectInputPhonePage</name>
    <message>
        <location filename="RejectInputPhonePage.qml" line="49"/>
        <source>Enter Recipient Phone Number</source>
        <translation>Masukkan Nomor Telepon Seluler</translation>
    </message>
    <message>
        <location filename="RejectInputPhonePage.qml" line="86"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="RejectInputPhonePage.qml" line="171"/>
        <source>The memory is not input or not correct</source>
        <translation>Oops, Terjadi kesalahan. Pastikan nomor telepon yang dimasukkan adalah benar.</translation>
    </message>
    <message>
        <location filename="RejectInputPhonePage.qml" line="184"/>
        <source>back</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>RejectInputPhoneSurePage</name>
    <message>
        <location filename="RejectInputPhoneSurePage.qml" line="83"/>
        <source>Please Verify Phone Number</source>
        <translation>Mohon Periksa Nomor Telepon</translation>
    </message>
    <message>
        <location filename="RejectInputPhoneSurePage.qml" line="108"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="RejectInputPhoneSurePage.qml" line="129"/>
        <source>continue</source>
        <translation>Lanjutkan</translation>
    </message>
</context>
<context>
    <name>RejectInsertCoinPage</name>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="71"/>
        <source>Welcome to use the system</source>
        <translation>Welcome to use the system</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="88"/>
        <source>You pay the overdue amount</source>
        <translation>You pay the overdue amount</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="98"/>
        <source>return</source>
        <translation>KEMBALI</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="117"/>
        <source>Please put a coin in the slot</source>
        <translation>Please put a coin in the slot</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="177"/>
        <source>yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>RejectNumboardFunctionButtondelete</name>
    <message>
        <location filename="RejectNumboardFunctionButtondelete.qml" line="18"/>
        <source>DEL</source>
        <translation>HAPUS</translation>
    </message>
</context>
<context>
    <name>RejectNumboardFunctionButtonreturn</name>
    <message>
        <location filename="RejectNumboardFunctionButtonreturn.qml" line="18"/>
        <source>DEL</source>
        <translation>BATAL</translation>
    </message>
</context>
<context>
    <name>RejectPayMentPage</name>
    <message>
        <location filename="RejectPayMentPage.qml" line="68"/>
        <source>Please choose the method of payment</source>
        <translation>Pilih metode pembayaran</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="74"/>
        <source>If you have any questions, please contact ********</source>
        <translation>Jika ada pertanyaan, Silakan hubungi 021 290 22 537</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="83"/>
        <source>octopus</source>
        <translation>OCTOPUS</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="107"/>
        <source>coin</source>
        <translation>COIN</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="131"/>
        <source>app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="157"/>
        <source>back to menu</source>
        <translation>Menu Utama</translation>
    </message>
</context>
<context>
    <name>RejectSelectBoxSizePage</name>
    <message>
        <location filename="RejectSelectBoxSizePage.qml" line="130"/>
        <source>please select service option</source>
        <translation>Pilih Ukuran Loker</translation>
    </message>
    <message>
        <location filename="RejectSelectBoxSizePage.qml" line="394"/>
        <source>return</source>
        <translation>Batal</translation>
    </message>
    <message>
        <source>Mini</source>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="RejectSelectBoxSizePage.qml" line="167"/>
        <location filename="RejectSelectBoxSizePage.qml" line="212"/>
        <location filename="RejectSelectBoxSizePage.qml" line="260"/>
        <location filename="RejectSelectBoxSizePage.qml" line="307"/>
        <location filename="RejectSelectBoxSizePage.qml" line="354"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <source>Big</source>
        <translation>Besar</translation>
    </message>
    <message>
        <source>Mid</source>
        <translation>Medium</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Kecil</translation>
    </message>
</context>
<context>
    <name>RejectSelectMerchantNamePage</name>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="47"/>
        <source>Available merchant :</source>
        <translation>Toko online tersedia :</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="59"/>
        <source>*Click merchants logo for returning parcel</source>
        <translation>*Pilih gambar Toko Online untuk mengembalikan barang</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="189"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="210"/>
        <source>Steps to return parcel :</source>
        <translation>Langkah mengembalikan paket :</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="239"/>
        <source>Fill out Merchant Return Form.</source>
        <translation>Isi Form Pengembalian Barang di website Merchant.</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="268"/>
        <source>Scan or Enter the Package ID.</source>
        <translation>Pindai/Masukkan Nomor ID Paket.</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="297"/>
        <source>Select size and drop parcel to locker.</source>
        <translation>Pilih ukuran loker dan masukkan paket.</translation>
    </message>
</context>
<context>
    <name>SelectLanguagePage</name>
    <message>
        <location filename="SelectLanguagePage.qml" line="27"/>
        <source>Please select main language</source>
        <translation>Pilih Opsi Bahasa</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="33"/>
        <source>Please select second language</source>
        <translation>Please Select your Language</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="41"/>
        <source>main language</source>
        <translation>Bahasa</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="76"/>
        <source>second language</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="114"/>
        <source>Available Lockers: </source>
        <translation>Loker Tersedia:</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="124"/>
        <source>XS :</source>
        <translation>XS:</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="143"/>
        <source>S :</source>
        <translation>S :</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="132"/>
        <location filename="SelectLanguagePage.qml" line="151"/>
        <location filename="SelectLanguagePage.qml" line="170"/>
        <location filename="SelectLanguagePage.qml" line="189"/>
        <location filename="SelectLanguagePage.qml" line="208"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="162"/>
        <source>M :</source>
        <translation>M :</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="181"/>
        <source>L :</source>
        <translation>L :</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="200"/>
        <source>XL:</source>
        <translation>XL:</translation>
    </message>
</context>
<context>
    <name>SelectProductPage</name>
    <message>
        <location filename="SelectProductPage.qml" line="81"/>
        <source>Please choose the product</source>
        <translation>Silakan pilih produk</translation>
    </message>
    <message>
        <location filename="SelectProductPage.qml" line="105"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>SelectServicePageB</name>
    <message>
        <location filename="SelectServicePageB.qml" line="65"/>
        <source>please select service option</source>
        <translation>Pilih Opsi Layanan</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="150"/>
        <source>return</source>
        <translation>KEMBALI</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="328"/>
        <source>Available Lockers: </source>
        <translation>Loker Tersedia: </translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="337"/>
        <source>XS :</source>
        <translation>XS:</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="356"/>
        <source>S :</source>
        <translation>S :</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="345"/>
        <location filename="SelectServicePageB.qml" line="364"/>
        <location filename="SelectServicePageB.qml" line="383"/>
        <location filename="SelectServicePageB.qml" line="402"/>
        <location filename="SelectServicePageB.qml" line="421"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="375"/>
        <source>M:</source>
        <translation>M :</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="394"/>
        <source>L:</source>
        <translation>L :</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="413"/>
        <source>XL:</source>
        <translation>XL:</translation>
    </message>
</context>
<context>
    <name>SelectServicePageE</name>
    <message>
        <location filename="SelectServicePageE.qml" line="65"/>
        <source>please select service option</source>
        <translation>Select Service Option</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="150"/>
        <source>return</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="329"/>
        <source>Available Lockers: </source>
        <translation>Available Lockers: </translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="338"/>
        <source>XS :</source>
        <translation>XS:</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="358"/>
        <source>S :</source>
        <translation>S :</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="346"/>
        <location filename="SelectServicePageE.qml" line="366"/>
        <location filename="SelectServicePageE.qml" line="385"/>
        <location filename="SelectServicePageE.qml" line="404"/>
        <location filename="SelectServicePageE.qml" line="423"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="377"/>
        <source>M:</source>
        <translation>M :</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="396"/>
        <source>L:</source>
        <translation>L :</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="415"/>
        <source>XL:</source>
        <translation>XL:</translation>
    </message>
</context>
<context>
    <name>SendDoorOpenPage</name>      
    <message>
        <location filename="SendDoorOpenPage.qml" line="99"/>
        <source>Open again</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="64"/>
        <source>change-box</source>
        <translation>Ganti-Loker</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="129"/>
        <source>Confirm receipt</source>
        <translation>Selesai</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="159"/>
        <source>Your locker is located at Box No </source>
        <translation>Letakkan paket Anda di loker no. </translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="166"/>
        <source>X</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="182"/>
        <source>The system will refresh in</source>
        <translation>Sistem akan mengulang dalam</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="233"/>
        <source>sec</source>
        <translation> detik</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="243"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Mohon tutup kembali pintu loker setelah selesai</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="285"/>
        <source>Please ensure you have closed the empty previous door before continuing</source>
        <translation>Mohon tutup kembali pintu loker sebelumnya dalam keadaan kosong</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="299"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>        
</context>
<context>
    <name>SendExpressHelp</name>
    <message>
        <location filename="SendExpressHelp.qml" line="78"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Jika ada pertanyaan lain, Silakan tekan
 &apos;batal&apos; dan hubungi Kami.</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="92"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation>Tekan ini untuk mengganti ukuran loker.</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="106"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation>Tekan ini jika pintu tidak terbuka.</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="120"/>
        <source>back to menu</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="146"/>
        <source>re-open</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="175"/>
        <source>change-box</source>
        <translation>Ganti-Loker</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="203"/>
        <source>cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="231"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Silakan memilih fungsi di bawah ini:</translation>
    </message>
</context>
<context>
    <name>SendExpressInfo</name>
    <message>
        <location filename="SendExpressInfo.qml" line="174"/>
        <source>Back</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="195"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="120"/>
        <source>Name:</source>
        <translation>Nama :</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="110"/>
        <source>From:</source>
        <translation>Loker :</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="134"/>
        <source>Address:</source>
        <translation>Tujuan :</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="97"/>
        <source>PopSend Details</source>
        <translation>Detail PopSend</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="237"/>
        <source>not_mouth</source>
        <translation>Loker tidak tersedia</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="250"/>
        <location filename="SendExpressInfo.qml" line="292"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="279"/>
        <source>no_payOfAmount</source>
        <translation>Pembayaran tidak ditemukan</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="376"/>
        <source>We are sorry</source>
        <translation>Mohon Maaf</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="394"/>
        <source>This order cannot be processed, Please ensure you have choose the right locker.</source>
        <translation>Nomor order ini tidak dapat diproses, Pastikan Anda telah memilih loker yang tepat.</translation>
    </message>
</context>
<context>
    <name>SendExpressPage</name>
    <message>
        <source>2.5</source>
        <translation type="vanished">2.5</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="vanished">10</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="77"/>
        <location filename="SendExpressPage.qml" line="99"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="122"/>
        <source>Cancel and Exit</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="149"/>
        <source>Please put the package into ths platform</source>
        <translation>Masukkan paket ke dalam sini</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="158"/>
        <source>The system will calculate the postage for you</source>
        <translation>Sistem akan menghitung 
ongkos kirim anda</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="169"/>
        <source>Weight</source>
        <translation>Berat</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="179"/>
        <source>Cost</source>
        <translation>Harga</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="199"/>
        <source>Continue</source>
        <translation>Lanjutkan</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="231"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="240"/>
        <source>Yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>SendExpressWeightPage</name>
    <message>
        <location filename="SendExpressWeightPage.qml" line="77"/>
        <location filename="SendExpressWeightPage.qml" line="99"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="122"/>
        <source>Cancel and Exit</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="149"/>
        <source>Please put the package into ths platform</source>
        <translation>Masukkan paket ke dalam sini</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="158"/>
        <source>The system will calculate the postage for you</source>
        <translation>Sistem akan menghitung 
ongkos kirim anda</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="169"/>
        <source>Weight</source>
        <translation>Berat</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="179"/>
        <source>Cost</source>
        <translation>Harga</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="199"/>
        <source>Continue</source>
        <translation>Lanjutkan</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="233"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="242"/>
        <source>Yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>SendInputMemoryPage</name>
    <message>
        <location filename="SendInputMemoryPage.qml" line="87"/>
        <source>Scan Barcode or Enter Parcel Code</source>
        <oldsource>Please scan or enter a tracking number</oldsource>
        <translation>Pindai/Masukkan Nomor Paket</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="241"/>
        <source>We detected the parcel number is not entered yet. Please retry and enter the correct parcel number.</source>
        <translation>Kami dapati nomor paket belum dimasukkan. Silakan ulangi dan masukkan nomor paket yang benar.</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="215"/>
        <location filename="SendInputMemoryPage.qml" line="270"/>
        <location filename="SendInputMemoryPage.qml" line="325"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="305"/>
        <source>We detected the parcel number is not correct. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Kami dapati nomor paket belum benar. Silakan hubungi CS PopBox di 021-22538719 untuk bantuan.</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="355"/>
        <source>Dear Customer</source>
        <translation>Pelanggan YTH</translation>
    </message>   
    <message>
        <location filename="SendInputMemoryPage.qml" line="355"/>
        <source>Sorry We are unable to verify the parcel number. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Mohon maaf nomor paket tidak dapat diverifikasi. Silakan hubungi CS PopBox di 021-22538719 untuk bantuan.</translation>
    </message>  
</context>
<context>
    <name>SendInserCoinPage</name>
    <message>
        <location filename="SendInserCoinPage.qml" line="71"/>
        <source>Welcome to use the system</source>
        <translation>Silakan menggunakan sistem</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="88"/>
        <source>You pay the overdue amount</source>
        <translation>Anda membayar sejumlah</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="99"/>
        <source>return</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="119"/>
        <source>Please put a coin in the slot</source>
        <translation>Silakan masukkan bayaran ke dalam</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="180"/>
        <source>yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>SendPayMentPage</name>
    <message>
        <location filename="SendPayMentPage.qml" line="68"/>
        <source>Please choose the method of payment</source>
        <translation>Pilih metode pembayaran</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="74"/>
        <source>If you have any questions, please contact ********</source>
        <translation>Jika ada masalah mohon hubungi 021 290 22537</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="83"/>
        <source>octopus</source>
        <translation>OCTOPUS</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="108"/>
        <source>coin</source>
        <translation>COIN</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="133"/>
        <source>app</source>
        <translation>APP HP</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="160"/>
        <source>back to menu</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>SendSelectBoxSizePage</name>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="120"/>
        <source>please select service option</source>
        <translation>Silakan pilih ukuran loker</translation>
    </message>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="354"/>
        <source>not_mouth</source>
        <translation>Loker sudah penuh</translation>
    </message>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="367"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="348"/>
        <source>back to menu</source>
        <translation>Batal</translation>
    </message>
</context>
<context>
    <name>TakeRejectExpress</name>
    <message>
        <location filename="TakeRejectExpress.qml" line="105"/>
        <location filename="TakeRejectExpress.qml" line="524"/>
        <location filename="TakeRejectExpress.qml" line="572"/>
        <location filename="TakeRejectExpress.qml" line="624"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="127"/>
        <source>take back it</source>
        <translation>Ambil</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="157"/>
        <source>take back all</source>
        <translation>Ambil semua</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="203"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="217"/>
        <source>store-time</source>
        <translation>Waktu Simpan</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="231"/>
        <source>COMPANY</source>
        <translation>Perusahaan</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="245"/>
        <source>box</source>
        <translation>Loker</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="494"/>
        <source>waiting</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="511"/>
        <source>set_null</source>
        <translation>Tidak ada paket</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="559"/>
        <source>Successful</source>
        <translation>Berhasil</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="611"/>
        <source>Failed</source>
        <translation>    Pengambilan gagal
Silakan hubungi layanan pelanggan PopBox.</translation>
    </message>
</context>
<context>
    <name>TakeSendExpress</name>
    <message>
        <location filename="TakeSendExpress.qml" line="103"/>
        <location filename="TakeSendExpress.qml" line="530"/>
        <location filename="TakeSendExpress.qml" line="579"/>
        <location filename="TakeSendExpress.qml" line="631"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="125"/>
        <source>take back it</source>
        <translation>Ambil kembali</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="155"/>
        <source>take back all</source>
        <translation>Ambil semua</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="vanished">手機號碼</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="215"/>
        <source>store-time</source>
        <translation>Waktu Simpan</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="229"/>
        <source>box</source>
        <translation>Loker</translation>
    </message>
    <message>
        <source>send-time</source>
        <oldsource>overdue-time</oldsource>
        <translation type="vanished">寄件時間</translation>
    </message>
    <message>
        <source>other</source>
        <translation type="vanished">Lainnya</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="201"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="500"/>
        <source>waiting</source>
        <translation>Mohon tunggu sebentar</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="517"/>
        <source>set_null</source>
        <translation>Tidak ada paket</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="566"/>
        <source>Successful</source>
        <translation>Berhasil</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="618"/>
        <source>Failed</source>
        <translation>    Pengambilan gagal
Silakan hubungi layanan pelanggan PopBox.</translation>
    </message>
</context>
<context>
    <name>UserQuitPage</name>
    <message>
        <location filename="UserQuitPage.qml" line="55"/>
        <source>please choice</source>
        <translation>Silakan pilih</translation>
    </message>
    <message>
        <location filename="UserQuitPage.qml" line="169"/>
        <source>back to menu</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="UserQuitPage.qml" line="191"/>
        <source>ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>UserSelectExpressPage</name>
    <message>
        <location filename="UserSelectExpressPage.qml" line="67"/>
        <source>Please choose the courier company</source>
        <translation>Silakan pilih kurir</translation>
    </message>
    <message>
        <location filename="UserSelectExpressPage.qml" line="91"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="UserSelectExpressPage.qml" line="115"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>UserSelectPage</name>
    <message>
        <location filename="UserSelectPage.qml" line="81"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Punya pertanyaan lain, Silakan tekan
 &apos;batal&apos; dan hubungi kami.</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="95"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation>Tekan tombol ini untuk
 mengganti ukuran loker.</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="109"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation>Tekan tombol ini jika pintu
 loker tidak terbuka.</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="123"/>
        <source>back to menu</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">確定</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="154"/>
        <source>re-open</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="183"/>
        <source>change-box</source>
        <translation>Ganti-Loker</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="211"/>
        <source>cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="239"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Silakan pilih fungsi</translation>
    </message>
</context>
<context>
    <name>WebPage</name>
    <message>
        <location filename="WebPage.qml" line="108"/>
        <source>Back</source>
        <translation>BATAL</translation>
    </message>
</context>
<context>
    <name>OtherServiceEmoney</name>
    <message>
        <location filename="OtherServiceEmoney.qml" line="50"/>
        <source>How To Do Payment</source>
        <translation>Cara membayar dengan e-Money</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="89"/>
        <source>Scan or enter your order number.</source>
        <translation>Masukkan atau pindai nomor transaksi Anda.</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="103"/>
        <source>Put your pre-paid card into reader and confirm payment.</source>
        <translation>Letakkan kartu pada reader e-Money dan konfirmasi pembayaran.</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="116"/>
        <source>Do not forget to take back your pre-paid card.</source>
        <translation>Silakan ambil kembali kartu e-Money Anda jika selesai.</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="130"/>
        <source>Go To Payment</source>
        <translation>Lanjutkan</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="171"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>QRshopinfo</name>
    <message>
        <location filename="QRshopinfo.qml" line="51"/>
        <source>How To Shop with QR</source>
        <translation>Cara belanja dengan QR</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="90"/>
        <source>Download UANGKU App for IOS or ANDROID.</source>
        <translation>Pindai kode QR di atas dan install aplikasi UANGKU.</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="104"/>
        <source>Register and top up your balance.</source>
        <translation>Daftarkan diri dan segera isi saldo Anda.</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="117"/>
        <source>Scan and shop the product you like.</source>
        <translation>Pindai dan belanja produk-produk yang Anda sukai.</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="131"/>
        <source>Touch Everywhere to Start</source>
        <translation>Tekan Dimanasaja untuk Mulai Belanja</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="172"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>MMreturninfo</name>
    <message>
        <location filename="MMreturninfo.qml" line="51"/>
        <source>How To Return Parcel</source>
        <translation>Cara Mengembalikan Paket</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="90"/>
        <source>Fill out Return Form on Mataharimall.com</source>
        <translation>Isi form pengembalian barang di Mataharimall.com</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="106"/>
        <source>Go to locker, Select Return Parcel and Scan the AWB Number.</source>
        <translation>Di loker, Pilih Mengembalikan Barang dan pindai nomor order.</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="121"/>
        <source>Drop off the returned parcel to opened door.</source>
        <translation>Masukkan paket Anda pada pintu yang terbuka.</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="137"/>
        <source>Return My Parcel</source>
        <translation>Kembalikan Paket</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="177"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>EmoneyCheckBalance</name>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="69"/>
        <source>Balance Information</source>
        <translation>Informasi Saldo</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="77"/>
        <source>Cancel</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="99"/>
        <source>Next</source>
        <translation>Selanjutnya</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="203"/>
        <source>Please place your pre-paid card into the reader now before continuing.</source>
        <translation>Silakan tempelkan kartu e-Money Anda pada reader sebelum melanjutkan.</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="223"/>
        <source>Your prepaid balance is 0</source>
        <translation>Saldo kartu e-Money Anda kosong</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="238"/>
        <source>Please top up your prepaid card.</source>
        <translation>Silakan isi ulang kartu e-Money Anda.</translation>
    </message>    
    <message>
        <location filename="EmoneyCheckBalance.qml" line="290"/>
        <source>Balance Check Failed</source>
        <translation>Transaksi Gagal</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="305"/>
        <source>Please use e-Money Mandiri which placed properly into the reader.</source>
        <translation>Silakan hanya gunakan e-Money Mandiri untuk transaksi di reader.</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="329"/>
        <location filename="EmoneyCheckBalance.qml" line="261"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>    
</context>
<context>
    <name>BalanceWindow</name>
    <message>
        <location filename="BalanceWindow.qml" line="33"/>
        <source>Your pre-paid balance is </source>
        <translation>Saldo kartu e-Money Anda adalah</translation>
    </message>    
</context>
<context>
    <name>EmoneyBalanceInfo</name>
    <message>
        <location filename="EmoneyBalanceInfo.qml" line="65"/>
        <source>Balance Information</source>
        <translation>Informasi Saldo</translation>
    </message>
    <message>
        <location filename="EmoneyBalanceInfo.qml" line="73"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="EmoneyBalanceInfo.qml" line="111"/>
        <source>Your pre-paid balance is </source>
        <translation>Saldo kartu pra-bayar Anda :</translation>
    </message>     
</context>
<context>
    <name>EmoneyServicePage</name>
    <message>
        <location filename="EmoneyServicePage.qml" line="53"/>
        <source>Please select option</source>
        <translation>Silakan Pilih Layanan</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="64"/>
        <source>Pay Order</source>
        <translation>Bayar Pesanan</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="95"/>
        <source>Check Balance</source>
        <translation>Cek Saldo</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="127"/>
        <source>Phone Credit</source>
        <translation>Isi Pulsa</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="190"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="163"/>
        <source>Powered by :</source>
        <translation>Didukung oleh :</translation>
    </message>           
</context>
<context>
    <name>LaundryInputMemoryPage</name>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="88"/>
        <source>Scan Barcode or Enter Parcel Code</source>
        <oldsource>Please scan or enter a tracking number</oldsource>
        <translation>Pindai/Masukkan Nomor Laundri</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="235"/>
        <source>We detected the laundry number is not entered yet. Please retry and enter the correct laundry number.</source>
        <translation>Kami dapati nomor laundri belum dimasukkan. Silakan ulangi dan masukkan nomor laundri yang benar.</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="241"/>
        <location filename="LaundryInputMemoryPage.qml" line="296"/>
        <location filename="LaundryInputMemoryPage.qml" line="351"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="299"/>
        <source>We detected the laundry number is not correct. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Kami dapati nomor laundri belum benar. Silakan hubungi CS PopBox di 021-22538719 untuk bantuan.</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="349"/>
        <source>Dear Customer</source>
        <translation>Pelanggan YTH</translation>
    </message>  
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="363"/>
        <source>Sorry We are unable to verify the laundry number. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Mohon maaf nomor laundri tidak dapat diverifikasi. Silakan hubungi CS PopBox di 021-22538719 untuk bantuan.</translation>
    </message>   
</context>
<context>
    <name>LaundryInfo</name>
    <message>
        <location filename="LaundryInfo.qml" line="207"/>
        <source>Supported by :</source>
        <translation>Didukung :</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="163"/>
        <source>Order your laundry from merchant web/apps, Ensure the laundry confirmation</source>
        <translation>Dapatkan layanan laundri dari aplikasi mobile/web dan dapatkan nomor pesanan laundri. </translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="179"/>
        <source>Go to Locker, Scan/Enter the laundry number and choose suitable door size (M or L).</source>
        <translation>Di loker, Masukkan nomor laundri dan pilih ukuran loker yang sesuai (ukuran M atau L).</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="194"/>
        <source>Drop off your laundry in opened door, Then Courier will pick it up soon.</source>
        <translation>Letakkan laundri pada pintu yang terbuka, Kurir laundri akan segera memproses.</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="76"/>
        <source>Drop My Laundry</source>
        <translation>Letakkan Laundri</translation>
    </message>
    <message>    
        <location filename="LaundryInfo.qml" line="52"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>LaundryExpressInfo</name>
    <message>
        <location filename="LaundryExpressInfo.qml" line="274"/>
        <source>Back</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="295"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="142"/>
        <source>Provider :</source>
        <translation>Layanan :</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="195"/>
        <source>Customer :</source>
        <translation>Pelanggan :</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="222"/>
        <source>Address :</source>
        <translation>Alamat :</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="115"/>
        <source>Laundry Details</source>
        <translation>Rincian Laundry</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="337"/>
        <source>not_mouth</source>
        <translation>Loker tidak tersedia</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="350"/>
        <location filename="LaundryExpressInfo.qml" line="392"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="379"/>
        <source>no_payOfAmount</source>
        <translation>Pembayaran Tidak Diketemukan</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="169"/>
        <source>Order No.:</source>
        <translation>No Pesanan :</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="467"/>
        <source>We are sorry</source>
        <translation>Mohon Maaf</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="486"/>
        <source>This order cannot be processed, Please ensure you have choose the right locker.</source>
        <translation>Nomor order ini tidak dapat diproses, Pastikan Anda telah memilih loker yang tepat.</translation>
    </message>
</context>
<context>
    <name>LaundryThanksPage</name>
    <message>
        <location filename="LaundryThanksPage.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Terima kasih telah menggunakan PopBox.</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="67"/>
        <source>A SMS notification has been sent to</source>
        <translation>Notifikasi SMS telah dikirimkan ke</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="75"/>
        <source>the sender of this parcel.</source>
        <translation>Penyedia Jasa Laundri yang Anda Pilih.</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Menu Utama</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="109"/>
        <source>Send Another Parcel?</source>
        <translation>Letakkan laundri lain?</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>Selesai</translation>
    </message>
</context>
<context>
    <name>LaundryDoorOpenPage</name>    
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="100"/>
        <source>Open again</source>
        <translation>Buka-Kembali</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="65"/>
        <source>change-box</source>
        <translation>Ganti-Loker</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="130"/>
        <source>Confirm receipt</source>
        <translation>Selesai</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="252"/>
        <source>Your locker is located at Box No </source>
        <translation>Letakkan laundry Anda di loker no. </translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="118"/>
        <source>X</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="164"/>
        <source>The system will refresh in</source>
        <translation>Sistem akan mengulang dalam</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="219"/>
        <source>sec</source>
        <translation> detik</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="230"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Mohon tutup kembali pintu loker setelah selesai</translation>
    </message>    
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="308"/>
        <source>Please ensure you have closed the empty previous door before continuing</source>
        <translation>Pastikan pintu loker sebelumnya telah tertutup dalam keadaan kosong.</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="322"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>LaundrySelectBoxSizePage</name>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="120"/>
        <source>please select service option</source>
        <translation>Silakan pilih ukuran loker</translation>
    </message>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="354"/>
        <source>not_mouth</source>
        <translation>Loker sudah penuh</translation>
    </message>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="367"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="348"/>
        <source>back to menu</source>
        <translation>Batal</translation>
    </message>
</context>
<context>
    <name>SepulsaInputPhonePage</name>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="82"/>
        <source>Please enter your phone number</source>
        <translation>Masukkan Nomor Telepon Seluler</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="118"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="218"/>
        <source>Please enter the correct number</source>
        <translation>Masukkan nomor yang benar</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="231"/>
        <source>back</source>
        <translation>Kembali</translation>
    </message>
</context>
<context>
    <name>SepulsaInputPhoneSurePage</name>
    <message>
        <location filename="SepulsaInputPhoneSurePage.qml" line="101"/>
        <source>Kindly verify your phone number</source>
        <translation>Silakan pastikan nomor telepon Anda</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhoneSurePage.qml" line="140"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhoneSurePage.qml" line="162"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>SepulsaPaymentSuccessfulPage</name>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="79"/>
        <source>Order Details</source>
        <translation>Pembayaran Berhasil</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="87"/>
        <source>The sms notification will be sent to you for this credit purchase</source>
        <translation>Pemberitahuan SMS akan dikirimkan untuk pembelian pulsa ini</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="95"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="123"/>
        <source>Card No.</source>
        <translation>Nomor Kartu</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="129"/>
        <source>Last Balance</source>
        <translation>Saldo Terakhir</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="135"/>
        <source>Product Item</source>
        <translation>Item Produk</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="141"/>
        <source>Amount</source>
        <translation>Jumlah Bayar</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="147"/>
        <source>Date</source>
        <translation>Tanggal</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="153"/>
        <source>Locker Name</source>
        <translation>Nama loker</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="159"/>
        <source>Terminal ID</source>
        <translation>ID terminal</translation>
    </message>  
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="266"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Jangan lupa untuk mengambil kartu e-Money Anda</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="433"/>
        <source>Thank you for purchasing this product with PopBox Locker. For assistance, Please contact us at 021-29022537/38.</source>
        <translation>Terima kasih untuk pembelian produk ini di Loker PopBox. Untuk bantuan, Silakan hubungi kami di 021-29022537.</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="515"/>
        <source>For further assistance, Please contact us at 021-22538719.</source>
        <translation>Untuk bantuan lebih lanjut, Silakan hubungi kami di 021-22538719.</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="480"/>
        <source>Please keep the transaction number </source>
        <translation>Silahkan simpan nomor transaksi Anda </translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="480"/>
        <source> for tracking purpose.</source>
        <translation> untuk tujuan pemeriksaan.</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="497"/>
        <source>Please wait a moment for the transaction final status...</source>
        <translation>Silakan tunggu sejenak untuk status transaksi Anda...</translation>
    </message>
</context>
<context>
    <name>SepulsaSelectDenomPage</name>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="99"/>
        <source>Please Choose Denom :</source>
        <translation>Silakan Pilih Produk :</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="125"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="478"/>
        <source>Currently we are not connected to the system, Please retry later</source>
        <translation>Saat ini kami tidak terhubung ke sistem, Silakan coba lagi nanti</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="491"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="438"/>
        <source>Terms and Conditions Applied for Promo Products.</source>
        <translation>*Syarat dan Ketentuan berlaku untuk Produk Promo.</translation>
    </message>
</context>
<context>
    <name>SepulsaExpressInfo</name>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="94"/>
        <source>Order Details</source>
        <translation>Rincian Pesanan</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="102"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="124"/>
        <source>Confirm</source>
        <translation>Konfirmasi</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="158"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="164"/>
        <source>Phone Number</source>
        <translation>Nomor Telepon</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="170"/>
        <source>Product Type</source>
        <translation>Jenis Produk</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="176"/>
        <source>Product Name</source>
        <translation>Nama Produk</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="182"/>
        <source>Amount</source>
        <translation>Jumlah Bayar</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="271"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Silakan siapkan kartu prabayar Anda dan pastikan saldo Anda cukup.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="286"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>Dengan mengklik tombol Konfirmasi Anda telah setuju dengan Syarat dan Ketentuan.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="370"/>
        <source>Please place your card to the reader</source>
        <translation>Silakan tempelkan kartu Anda di reader</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="440"/>
        <source>Transaction Unfinished</source>
        <translation>Transaksi belum selesai</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="456"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Silakan tempelkan kembali kartu prabayar sebelumnya.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="488"/>
        <source>seconds left</source>
        <translation>detik lagi</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="541"/>
        <source>Transaction Failed</source>
        <translation>Transaksi Gagal</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="556"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Pastikan Anda menggunakan kartu Mandiri e-Money.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="579"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
</context>
<context>
    <name>PopsendDenomTopup</name>
    <message>
        <location filename="PopsendDenomTopup.qml" line="98"/>
        <source>Please Choose Denom :</source>
        <translation>Silakan Pilih Nilai Top Up :</translation>
    </message>
    <message>
        <location filename="PopsendDenomTopup.qml" line="60"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="PopsendDenomTopup.qml" line="208"/>
        <source>Powered by :</source>
        <translation>Didukung Oleh :</translation>
    </message>
    <message>
        <location filename="PopsendDenomTopup.qml" line="303"/>
        <source>Currently we are not connected to the system, Please retry later</source>
        <translation>Oops, Saat ini Mesin sedang tidak terhubung ke sistem, Silahkan mencoba lagi beberapa saat.</translation>
    </message> 
</context>
<context>
    <name>PopsendExpressInfo</name>
    <message>
        <location filename="PopsendExpressInfo.qml" line="147"/>
        <source>Order Details</source>
        <translation>Rincian Pesanan</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="296"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="317"/>
        <source>Confirm</source>
        <translation>Konfirmasi</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="160"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="166"/>
        <source>Phone Number</source>
        <translation>Nomor Telepon</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="172"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="178"/>
        <source>TopUp Denom</source>
        <translation>Nilai Top Up</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="184"/>
        <source>Amount</source>
        <translation>Jumlah Bayar</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="271"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Silakan siapkan kartu prabayar Anda dan pastikan saldo Anda cukup.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="286"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>Dengan mengklik tombol Konfirmasi Anda telah setuju dengan Syarat dan Ketentuan.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="355"/>
        <source>Please place your card to the reader</source>
        <translation>Silakan tempelkan kartu Anda di reader</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="430"/>
        <source>Transaction Unfinished</source>
        <translation>Transaksi belum selesai</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="446"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Silakan tempelkan kembali kartu prabayar sebelumnya.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="479"/>
        <location filename="PopsendExpressInfo.qml" line="388"/>
        <source>seconds left</source>
        <translation>detik lagi</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="537"/>
        <source>Transaction Failed</source>
        <translation>Transaksi Gagal</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="552"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Pastikan Anda menggunakan kartu Mandiri e-Money.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="575"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
</context>
<context>
    <name>PopsendTopupSuccessPage</name>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="182"/>
        <source>Order Details</source>
        <translation>Pembayaran Berhasil</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="190"/>
        <source>The sms notification will be sent to you for this credit purchase</source>
        <translation>Pemberitahuan SMS akan dikirimkan untuk pembelian pulsa ini</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="198"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="226"/>
        <source>Card No.</source>
        <translation>Nomor Kartu</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="232"/>
        <source>Last Balance</source>
        <translation>Saldo Terakhir</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="238"/>
        <source>Product Item</source>
        <translation>Item Produk</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="244"/>
        <source>Amount</source>
        <translation>Jumlah Bayar</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="250"/>
        <source>Date</source>
        <translation>Tanggal</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="262"/>
        <source>Locker Name</source>
        <translation>Nama loker</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="256"/>
        <source>Terminal ID</source>
        <translation>ID terminal</translation>
    </message>  
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="369"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Jangan lupa untuk mengambil kartu e-Money Anda</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="400"/>
        <source>Your current PopSend balance is </source>
        <translation>Saldo PopSend Anda saat ini </translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="416"/>
        <source>Thank you for purchasing Your PopSend balance with PopBox Locker.</source>
        <translation>Terima kasih telah membeli saldo PopSend di Loker PopBox.</translation>
    </message>
</context>
<context>
    <name>LockerCardView</name>
    <message>
        <location filename="LockerCardView.qml" line="60"/>
        <source>Select This Locker</source>
        <translation>Pilih Loker Ini</translation>
    </message>
</context>
<context>
    <name>CustomerSelectLocker</name>
    <message>
        <location filename="CustomerSelectLocker.qml" line="380"/>
        <source>Jakarta Barat</source>
        <translation>Jakarta Barat</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="403"/>
        <source>Jakarta Pusat</source>
        <translation>Jakarta Pusat</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="426"/>
        <source>Jakarta Selatan</source>
        <translation>Jakarta Selatan</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="449"/>
        <source>Jakarta Timur</source>
        <translation>Jakarta Timur</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="472"/>
        <source>Jakarta Utara</source>
        <translation>Jakarta Utara</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="535"/>
        <source>Please press Locker City on The Left Side Panel</source>
        <translation>Silahkan Tekan Kota Lokasi Loker di Panel Sebelah Kiri</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="579"/>
        <source>Currently we are not connected to the system, Please retry later</source>
        <translation>Oops, Saat ini Mesin sedang tidak terhubung ke sistem, Silahkan mencoba lagi beberapa saat</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="593"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>GrocerySelectQuantity</name>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="176"/>
        <source>Please Select Quantity</source>
        <translation>Tentukan Jumlah Produk</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="260"/>
        <source>Price/unit :</source>
        <translation>Harga/Unit :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="357"/>
        <source>Quantity :</source>
        <translation>Jumlah :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="227"/>
        <source>Initial Price/unit :</source>
        <translation>Harga Awal/unit :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="326"/>
        <source>Saving/unit :</source>
        <translation>Potongan/unit :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="341"/>
        <source>Total Purchase :</source>
        <translation>Total Belanja :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="369"/>
        <source>Buy</source>
        <translation>Beli</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="73"/>
        <source>Oops, Total purchase cannot be greater than 1.000.000 IDR at this time.</source>
        <translation>Oops, Jumlah belanja tidak dapat lebih dari Rp. 1.000.000,- untuk saat ini.</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="77"/>
        <source>Oops, At this time the maximum quantity cannot be greater than </source>
        <translation>Oops, Untuk saat ini maksimal jumlah pesanan hanya </translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="77"/>
        <source> units.</source>
        <translation> unit.</translation>
    </message>
</context>
<context>
    <name>CustomerInputDetailsPage</name>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="126"/>
        <source>Next</source>
        <translation>Berikutnya</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="405"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="414"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="423"/>
        <source>Phone</source>
        <translation>No. HP</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="579"/>
        <source>Oops, Something is wrong. Please ensure all the data is filled out correctly.</source>
        <translation>Oops, Terjadi kesalahan. Pastikan data yang dimasukkan adalah benar.</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="178"/>
        <source>Oops, Something is wrong. Please ensure the email address is correct.</source>
        <translation>Oops, Terjadi kesalahan. Pastikan alamat email Anda adalah benar.</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="190"/>
        <source>Oops, Something is wrong. Please ensure the phone number is correct.</source>
        <translation>Oops, Terjadi kesalahan. Pastikan nomor telepon Anda adalah benar.</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="664"/>
        <source>Delivery Confirmation</source>
        <translation>Konfirmasi Pengiriman</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="689"/>
        <source>We will deliver the purchased products to this locker :</source>
        <translation>Kami akan mengirim produk tersebut ke lokasi loker ini :</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="703"/>
        <source>NO, CHOOSE OTHER</source>
        <translation>TIDAK, YANG LAIN</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="725"/>
        <source>YES, I AGREE</source>
        <translation>YA, SETUJU</translation>
    </message>
</context>
<context>
    <name>GroceryExpressInfo</name>
    <message>
        <location filename="GroceryExpressInfo.qml" line="164"/>
        <source>Order Details</source>
        <translation>Rincian Pesanan</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="399"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="420"/>
        <source>Confirm</source>
        <translation>Konfirmasi</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="178"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="185"/>
        <source>Phone Number</source>
        <translation>Nomor Telepon</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="190"/>
        <source>Email</source>
        <translation>Alamat Email</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="199"/>
        <source>Product Name</source>
        <translation>Detail Produk</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="301"/>
        <source>Total Amount</source>
        <translation>Jumlah Bayar</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="287"/>
        <source>Quantity</source>
        <translation>Jumlah Produk</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="294"/>
        <source>Delivery Address</source>
        <translation>Alamat Kirim</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="374"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Silakan siapkan kartu prabayar Anda dan pastikan saldo Anda cukup.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="389"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>Dengan mengklik tombol Konfirmasi Anda telah setuju dengan Syarat dan Ketentuan.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="458"/>
        <source>Please place your card to the reader</source>
        <translation>Silakan tempelkan kartu Anda di reader</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="533"/>
        <source>Transaction Unfinished</source>
        <translation>Transaksi belum selesai</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="549"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Silakan tempelkan kembali kartu prabayar sebelumnya.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="491"/>
        <location filename="GroceryExpressInfo.qml" line="582"/>
        <source>seconds left</source>
        <translation>detik lagi</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="640"/>
        <source>Transaction Failed</source>
        <translation>Transaksi Gagal</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="655"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Pastikan Anda menggunakan kartu Mandiri e-Money.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="678"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
</context>
<context>
    <name>GroceryItemView</name>
    <message>
        <location filename="GroceryItemView.qml" line="169"/>
        <source>Touch to Get This</source>
        <translation>Dapatkan Produk Ini</translation>
    </message>
</context>
<context>
    <name>GroceryPaymentSuccessPage</name>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="212"/>
        <source>Order Details</source>
        <translation>Pembayaran Berhasil</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="220"/>
        <source>The sms notification will be sent to you for this credit purchase</source>
        <translation>Pemberitahuan SMS akan dikirimkan untuk pembelian produk ini.</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="228"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="259"/>
        <source>Card No.</source>
        <translation>Nomor Kartu</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="266"/>
        <source>Last Balance</source>
        <translation>Saldo Terakhir</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="273"/>
        <source>Product Item</source>
        <translation>Item Produk</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="280"/>
        <source>Amount</source>
        <translation>Jumlah Bayar</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="287"/>
        <source>Date</source>
        <translation>Tanggal</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="301"/>
        <source>Locker Name</source>
        <translation>Nama loker</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="294"/>
        <source>Terminal ID</source>
        <translation>ID terminal</translation>
    </message>  
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="426"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Jangan lupa untuk mengambil kartu e-Money Anda.</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="457"/>
        <source>Your order : </source>
        <translation>Pesanan Anda : </translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="457"/>
        <source>, will be processed within 1 x 24 hours. Kindly check your email.</source>
        <translation>, akan segera diproses dalam 1 x 24 jam. Silahkan cek email Anda.</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="416"/>
        <source>Thank you for shopping with PopBox Locker Shop. For assistance please call 021-29022537/38</source>
        <translation>Terima kasih telah berbelanja di Loker PopBox. Untuk bantuan, Silakan hubungi 021-22538719.</translation>
    </message>
</context>
<context>
    <name>GroceryProductListNew</name>
    <message>
        <location filename="GroceryProductListNew.qml" line="209"/>
        <source>Please Select Best Product Below</source>
        <translation>Silakan Pilih Produk Terbaik Berikut</translation>
    </message>
    <message>
        <location filename="GroceryProductListNew.qml" line="309"/>
        <source>Powered by :</source>
        <translation>Didukung Oleh:</translation>
    </message>
</context>
<context>
    <name>ExpressLoginPage</name>
    <message>
        <location filename="ExpressLoginPage.qml" line="289"/>
        <source>Please wait</source>
        <translation>Silakan Tunggu Sebentar</translation>
    </message>
    <message>
        <location filename="ExpressLoginPage.qml" line="318"/>
        <source>Network error, Please retry later</source>
        <translation>Oops, Loker sedang mencoba kembali menghubungkan ke sistem, Silakan menunggu beberapa saat.</translation>
    </message>
    <message>
        <location filename="ExpressLoginPage.qml" line="379"/>
        <source>Oops, Please ensure You are using the correct button menu.</source>
        <translation>Oops, Pastikan Anda memilih menu Login yang benar.</translation>
    </message>
</context>
<context>
    <name>SepulsaCheckInputPage</name>
    <message>
        <location filename="SepulsaCheckInputPage.qml" line="79"/>
        <source>Enter Transaction Record Number</source>
        <translation>Silakan Masukkan Nomor Transaksi</translation>
    </message>
    <message>
        <location filename="SepulsaCheckInputPage.qml" line="205"/>
        <source>Oops...</source>
        <translation>Pelanggan YTH</translation>
    </message>
    <message>
        <location filename="SepulsaCheckInputPage.qml" line="219"/>
        <source>The transaction number cannot be verified. Please retry and enter the correct transaction number.</source>
        <translation>Nomor transaksi salah atau tidak dapat diverifikasi. Mohon masukkan nomor transaksi yang benar.</translation>
    </message>
</context>
<context>
    <name>SepulsaCheckExpressInfo</name>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="112"/>
        <source>Transaction Details</source>
        <translation>Rincian Transaksi</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="162"/>
        <source>Transaction No.</source>
        <translation>No Transaksi</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="168"/>
        <source>Phone Number</source>
        <translation>Nomor Telepon</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="174"/>
        <source>Product Name</source>
        <translation>Nama Produk</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="180"/>
        <source>Amount</source>
        <translation>Nominal</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="186"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="279"/>
        <source>Check Another</source>
        <translation>Cek Lainnya</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="279"/>
        <source>For further assistance, Please feel free to contact us at 021-29022537/38.</source>
        <translation>Bantuan lebih lanjut, Silakan hubungi kami di 021-22538719.</translation>
    </message>
</context>
</TS>
