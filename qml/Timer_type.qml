import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:30
    height:11
    color:"transparent"
    property var show_x
    property var show_y
    property var time

    QtObject{
        id:abc
        property int counter
        Component.onCompleted:{
            abc.counter = time
        }
    }

    Text{
        id:countShow_test
        x:show_x
        y:show_y
        anchors.centerIn:parent
        color:"#32adb1"
        font.family:"Microsoft YaHei"
        font.pixelSize:40
    }


    Timer{
        id:my_timer
        interval:1000
        repeat:true
        running:true
        triggeredOnStart:true
        onTriggered:{
            countShow_test.text = abc.counter
            abc.counter -= 1
            if(abc .counter < 0){
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }
}
