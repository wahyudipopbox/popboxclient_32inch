import QtQuick 2.4
import QtQuick.Controls 1.2

BackgroundReject{
    id:user_reject_memory_input
    width: 1024
    height: 768
    property var show_text:""
    property int timer_value: 60
    property var press: "0"
    property var show_text_count:0
    property string reject_data:'{"id":"f37fa91cf7a211e4b7926c40088b8482","customerStoreNumber":"P201500010001","expressType":"CUSTOMER_STORE","storeUser":{"id":"ace76ad2f7a211e49b736c40088b8482"},"recipientName":"高阳","takeUserPhoneNumber":"18566691650","startAddress":{"id":"48eb7c50f7a311e49ec16c40088b8482","region":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"高新产业园"},"endAddress":{"id":"2494f228f7a311e4bb216c40088b8482","region":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"任达科技园"},"rangePrice":{"id":"a4ded9a6f7a011e48bfb6c40088b8482","businessType":{"id":"ca6ef91cf7a011e490516c40088b8482","name":"今日送"},"startRegion":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"endRegion":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"firstHeavy":5000,"firstPrice":1000,"continuedHeavy":1000,"continuedPrice":200},"version":0,"status":"IMPORTED"}'
    property string chargeType:"BY_WEIGHT"
    property string designationSize: "S"
    property int payOfAmount:0
    property var prefill: "0"
    property var allow_prefix: ["RMA"]

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            //console.log("prefill : ", show_text)
            if(allow_prefix.indexOf(show_text.substring(0,3)) > -1){
                show_text = show_text.substring(0,3)
            } else {
                show_text = ""
            }
            show_text_count = 0
            slot_handler.start_courier_scan_barcode()
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            slot_handler.stop_courier_scan_barcode()
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
    }

    Rectangle{
        id:main_page
        Component.onCompleted: {
            root.barcode_result.connect(handle_text)
            root.customer_reject_express_result.connect(handle_store_express_result)
        }

        Component.onDestruction: {
            root.barcode_result.disconnect(handle_text)
            root.customer_reject_express_result.disconnect(handle_store_express_result)
        }

        function handle_store_express_result(text){
            console.log("handle_store_express_result : ", text)
            if(text != "False"){
                slot_handler.set_express_number(show_text)
                //my_stack_view.push(reject_express_info, {reject_data:text,memory:show_text})
                show_return_data(text)
            }
            else{
                main_page.enabled = false
                no_record.open()
            }
        }

        function handle_text(text){
            console.log("handle_text : ", text)
            show_text = text
            touch_keyboard.count = text.length
        }

        function show_return_data(text){
            //console.log("show_return_data : ", text)
            if(text==""){
                return
            }
            reject_data = text
            console.log("result_text : ", reject_data)
            var a = JSON.parse(text)
            chargeType = a.chargeType
            /*if(chargeType == "BY_WEIGHT"){
                my_stack_view.push(reject_express_weigh_page)
            }
            if(chargeType == "NOT_CHARGE"){
                if(a.designationSize == null){
                    my_stack_view.push(reject_select_box_size_page)
                }
                else if(a.designationSize == ""){
                    my_stack_view.push(reject_select_box_size_page)
                }
                else{
                    designationSize = a.designationSize
                    slot_handler.start_choose_mouth_size(designationSize,"customer_store_express")
                }
            }
            if(chargeType == "FIXED_VALUE"){
                payOfAmount = a.payOfAmount
                designationSize = a.designationSize
                if(payOfAmount == 0){
                    no_payOfAmount.open()
                }
                if(payOfAmount != 0){
                    slot_handler.start_free_mouth_by_size(designationSize)
                }
            }*/
            if(chargeType == "NOT_CHARGE"){
                console.log("chargeType_text : ", chargeType)
                my_stack_view.push(reject_inputphone_page)
            }
        }

        FullWidthReminderText{
            id:text
            y:125
            remind_text:qsTr("Scan or Enter Barcode")
            remind_text_size:"35"
        }

        Rectangle{
            x:220
            y:190
             width:620
             height:72
             color:"transparent"

         Image{
             width:620
             height:72
             source:"img/courier11/input1.png"
         }

         Text{
             id:textin
             y:10
             x:20
             font.family:"Microsoft YaHei"
             text:show_text
             color:"#FFFFFF"
             font.pixelSize:40
             }
         }

        Image{
            x: 412
            y: 280
            width:199
            height:64
            source:"img/button/barcode.png"
        }

        FullKeyboard{
            id:touch_keyboard
            x:29
            y:360

            property var count:show_text_count
            property var validate_code:""

            Component.onCompleted: {
                touch_keyboard.letter_button_clicked.connect(show_validate_code)
                touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
            }

            function on_function_button_clicked(str){
                if(str == "ok"){                    
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    if(user_reject_memory_input.show_text == ""){
                        main_page.enabled = false
                        error_tips.open()
                    }
                    else{
                        count = 0
                        slot_handler.start_video_capture("enduser_reject" + "_" + show_text)
                        slot_handler.start_customer_reject_for_electronic_commerce(show_text)
                    }
                }

                if(str=="delete"){
                    if(count>=20){
                        count=19
                    }
                }
            }

            function show_validate_code(str){
                if (str == "" && count > 0){
                    if(count>=20){
                        count=20
                    }
                    count--
                    user_reject_memory_input.show_text=user_reject_memory_input.show_text.substring(0,count);
                }
                if (str != "" && count < 20){
                    count++
                }
                if (count>=20){
                    str=""
                }
                else{
                    user_reject_memory_input.show_text += str
                }
                abc.counter = timer_value
                my_timer.restart()
            }
        }
    }

    HideWindow{
        id:error_tips
        //visible: true

        Text {
            x: 112
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Oops...")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: text_error
            x: 120
            y:300
            width: 809
            height: 250
            text: qsTr("We detected the package id is not entered yet. Please retry and enter the correct package id.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:ok_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    if(allow_prefix.indexOf(user_reject_memory_input.show_text.substring(0,3)) > -1){
                        user_reject_memory_input.show_text = user_reject_memory_input.show_text.substring(0,3)
                    } else {
                        user_reject_memory_input.show_text = ""
                    }
                    show_text_count = 0
                    main_page.enabled = true
                    error_tips.close()
                }
                onEntered:{
                    ok_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    ok_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:no_record
        //visible: true

        Text {
            id: dear_text
            x: 112
            y:256
            width: 800
            height: 60
            text: qsTr("Dear Customer")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        Text {
            id: details_text
            x:128
            y:356
            width: 768
            height: 150
            text: qsTr("Sorry We are unable to verify the package id. Please contact our CS line at 021-29022537/38 for assistance.")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
        }

        OverTimeButton{
            id:no_record_back
            x:232
            y:598
            show_text:qsTr("OK")
            show_x:15
            show_source:"img/button/9.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    if(allow_prefix.indexOf(user_reject_memory_input.show_text.substring(0,3)) > -1){
                        user_reject_memory_input.show_text = user_reject_memory_input.show_text.substring(0,3)
                    } else {
                        user_reject_memory_input.show_text = ""
                    }
                    show_text_count = 0
                    main_page.enabled = true
                    no_record.close()
                }
                onEntered:{
                    no_record_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    no_record_back.show_source = "img/button/9.png"
                }
            }
        }

        OverTimeButton{
            id:no_record_main
            x:522
            y:598
            show_text:qsTr("main menu")
            show_x:15
            show_source:"img/button/9.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    user_reject_memory_input.show_text=""
                    show_text_count = 0
                    main_page.enabled = true
                    no_record.close()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                }
                onEntered:{
                    no_record_main.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    no_record_main.show_source = "img/button/9.png"
                }
            }
        }
    }
}
