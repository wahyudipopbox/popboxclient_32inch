import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_rec
    width:180
    height:215
    color:"transparent"
    property var but_name: "50K"
    property var but_price: "50000"
    property var but_disc: "15%"
    property var but_denom: "57.500"
    property var base_color: "white"
    property var base_text: "red"

    Rectangle{
        id: base_background
        anchors.fill: parent
        radius: 10
        color: base_color
        border.color: base_color
    }    

    Text {
        id: but_name_text
        x: 0
        y: 83
        width: parent.width
        height: 50
        color: "darkred"
        text: but_name
        font.pointSize: 30
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
        visible: false
    }

    Text {
        id: triple_zero_text
        x: 105
        y: 61
        width: 75
        height: 50
        color: base_text
        text: ".000"
        font.italic: true
        style: Text.Normal
        font.bold: false
        font.pointSize: 22
        horizontalAlignment: Text.AlignLeft
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
    }

    Text {
        id: but_price_text
        x: 8
        y: 73
        width: parent.width/2
        height: 60
        color: base_text
        text: but_price.replace("000","")
        font.italic: true
        font.bold: false
        font.pointSize: 40
        horizontalAlignment: Text.AlignRight
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle{
        id: box_disc_text
        x: 0
        y: 18
        width: parent.width/2
        height: 30
        color: (but_denom==but_price) ? "transparent" : "green"

        Text {
            id: but_disc_text
            anchors.fill: parent
            color: (but_denom==but_price) ? "transparent" : "white"
            text: "Free " + but_disc
            font.bold: false
            font.italic: true
            font.pointSize: 12
            horizontalAlignment: Text.AlignHCenter
            font.family: "Microsoft YaHei"
            verticalAlignment: Text.AlignVCenter
        }
    }

    Rectangle{
        id: box_get_denom
        x: 0
        y: 168
        width: parent.width
        height: 30
        color: "red"

        Text {
            id: but_denom_text
            anchors.fill: parent
            color: "white"
            text: "GET " + but_denom
            font.bold: true
            font.pointSize: 12
            horizontalAlignment: Text.AlignHCenter
            font.family: "Microsoft YaHei"
            verticalAlignment: Text.AlignVCenter
        }
    }
}
