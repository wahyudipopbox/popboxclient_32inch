import QtQuick 2.4
import QtQuick.Controls 1.2


Background{
    property int timer_value: 60

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }



    FullWidthReminderText {
        x: 0
        y: 200
        remind_text:qsTr("Thank You for using PopBox.")
        remind_text_size:"35"
    }


    FullWidthReminderText {
        x: 0
        y: 250
        remind_text:qsTr("A SMS notification has been sent to")
        remind_text_size:"35"
    }


    FullWidthReminderText {
        x: 0
        y: 300
        remind_text:qsTr("the recipient of this parcel.")
        remind_text_size:"35"
    }



    OverTimeButton3{
        id:main_error_button
        x:389
        y:576
        show_text:qsTr("MAIN MENU")
        show_x:25
        show_image:"img/07/rewrite.png"


        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            onEntered:{
                main_error_button.show_source = "img/bottondown/error2.png"
            }
            onExited:{
                main_error_button.show_source = "img/05/button2.png"
            }
        }
    }


    OverTimeButton2{
        id:pass_error_button
        x:232
        y:400
        show_text:qsTr("Send Another Parcel?")



        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 4) return true }))
            }
            onEntered:{
                pass_error_button.show_source = "img/bottondown/error1.png"
            }
            onExited:{
                pass_error_button.show_source = "img/05/button1.png"
            }
        }
    }


    OverTimeButton2{
        id:pass_button
        x:513
        y:400
        show_text:qsTr("I'm Done!")



        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 3) return true }))
            }
            onEntered:{
                pass_error_button.show_source = "img/bottondown/error1.png"
            }
            onExited:{
                pass_error_button.show_source = "img/05/button1.png"
            }
        }
    }

}
