import json
import os
import sys
from PyQt5.QtCore import QUrl, QObject, pyqtSlot, QTranslator, Qt
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQuick import QQuickView
import pygame
import wmi
import Configurator
from advertisement.service import AdvertisermentService
from box.service import BoxService
import device
from download.service import DownloadService
from database import ClientDatabase
from device import Camera
from express.service import ExpressService
from express.popbox import PopboxService
from network import HttpClient
from sync import PushMessage, PullMessage
from user.service import UserService
from alert.service import AlertService
from device import QP3000S
import logging
import logging.handlers
import subprocess

class SlotHandler(QObject):
    __qualname__ = 'SlotHandler'
    
    def select_language(self, string):
        translator.load(path + string)

    select_language = pyqtSlot(str)(select_language)
    
    def start_get_version(self):
        BoxService.start_get_version()

    start_get_version = pyqtSlot()(start_get_version)
    
    def clean_user_token(self):
        HttpClient.clean_user_token()

    clean_user_token = pyqtSlot()(clean_user_token)
    
    def start_update_system(self):
        os.system('update.bat')

    start_update_system = pyqtSlot()(start_update_system)
    
    def customer_take_express(self, string):
        ExpressService.start_customer_take_express(string)

    customer_take_express = pyqtSlot(str)(customer_take_express)
    
    def customer_scan_barcode_take_express(self):
        ExpressService.start_barcode_take_express()

    customer_scan_barcode_take_express = pyqtSlot()(customer_scan_barcode_take_express)
    
    def customer_cancel_scan_barcode(self):
        ExpressService.stop_barcode_take_express()

    customer_cancel_scan_barcode = pyqtSlot()(customer_cancel_scan_barcode)
    
    def get_express_mouth_number(self):
        BoxService.get_express_mouth_number()

    get_express_mouth_number = pyqtSlot()(get_express_mouth_number)
    
    def start_get_ups_status(self):
        BoxService.start_get_ups_status()

    start_get_ups_status = pyqtSlot()(start_get_ups_status)
    
    def customer_get_overdue_cost(self):
        ExpressService.get_overdue_cost()

    customer_get_overdue_cost = pyqtSlot()(customer_get_overdue_cost)
    
    def background_login(self, username, password, identity):
        UserService.background_login_start(username, password, identity)

    background_login = pyqtSlot(str, str, str)(background_login)
    
    def start_courier_scan_barcode(self):
        ExpressService.start_get_express_number_by_barcode()

    start_courier_scan_barcode = pyqtSlot()(start_courier_scan_barcode)
    
    def stop_courier_scan_barcode(self):
        ExpressService.stop_get_express_number_by_barcode()

    stop_courier_scan_barcode = pyqtSlot()(stop_courier_scan_barcode)
    
    def set_express_number(self, express_number):
        ExpressService.set_express_number(express_number)

    set_express_number = pyqtSlot(str)(set_express_number)
    
    def set_phone_number(self, phone_number):
        ExpressService.set_phone_number(phone_number)

    set_phone_number = pyqtSlot(str)(set_phone_number)
    
    def start_store_express(self):
        ExpressService.start_store_express()

    start_store_express = pyqtSlot()(start_store_express)
    
    def start_pay_cash_for_overdue_express(self):
        ExpressService.start_pay_cash_for_overdue_express()

    start_pay_cash_for_overdue_express = pyqtSlot()(start_pay_cash_for_overdue_express)
    
    def get_user_info(self):
        UserService.get_user_info()

    get_user_info = pyqtSlot()(get_user_info)
    
    def manager_get_box_info(self):
        BoxService.start_manager_get_box_info()

    manager_get_box_info = pyqtSlot()(manager_get_box_info)
    
    def start_get_customer_store_express_info(self, customer_store_number):
        ExpressService.start_get_customer_store_express_info(customer_store_number)

    start_get_customer_store_express_info = pyqtSlot(str)(start_get_customer_store_express_info)
    
    def start_get_customer_reject_express_info(self, customer_reject_number):
        ExpressService.start_get_customer_reject_express_info(customer_reject_number)

    start_get_customer_reject_express_info = pyqtSlot(str)(start_get_customer_reject_express_info)
    
    def start_customer_scan_qr_code(self):
        PopboxService.start_customer_scan_qr_code()

    start_customer_scan_qr_code = pyqtSlot()(start_customer_scan_qr_code)
    
    def stop_customer_scan_qr_code(self):
        PopboxService.stop_customer_scan_qr_code()

    stop_customer_scan_qr_code = pyqtSlot()(stop_customer_scan_qr_code)
    
    def start_get_mouth_status(self):
        BoxService.start_get_mouth_status()

    start_get_mouth_status = pyqtSlot()(start_get_mouth_status)
    
    def start_get_free_mouth_mun(self):
        BoxService.start_get_free_mouth_mun()

    start_get_free_mouth_mun = pyqtSlot()(start_get_free_mouth_mun)
    
    def start_courier_load_overdue_express_list(self, page):
        ExpressService.start_courier_load_overdue_express_list(page)

    start_courier_load_overdue_express_list = pyqtSlot(int)(start_courier_load_overdue_express_list)
    
    def start_manager_load_overdue_express_list(self, page):
        ExpressService.start_manager_load_overdue_express_list(page)

    start_manager_load_overdue_express_list = pyqtSlot(int)(start_manager_load_overdue_express_list)
    
    def start_load_courier_overdue_express_count(self):
        ExpressService.start_load_courier_overdue_express_count()

    start_load_courier_overdue_express_count = pyqtSlot()(start_load_courier_overdue_express_count)
    
    def start_load_manager_overdue_express_count(self):
        ExpressService.start_load_manager_overdue_express_count()

    start_load_manager_overdue_express_count = pyqtSlot()(start_load_manager_overdue_express_count)
    
    def start_open_mouth_again(self):
        BoxService.start_open_mouth()

    start_open_mouth_again = pyqtSlot()(start_open_mouth_again)
    
    def start_init_client(self):
        BoxService.start_init_client()

    start_init_client = pyqtSlot()(start_init_client)
    
    def start_courier_take_overdue_express(self, id_list):
        ExpressService.start_staff_take_overdue_express_list(id_list)

    start_courier_take_overdue_express = pyqtSlot(str)(start_courier_take_overdue_express)
    
    def start_courier_take_overdue_all(self):
        ExpressService.start_staff_take_all_overdue_express()

    start_courier_take_overdue_all = pyqtSlot()(start_courier_take_overdue_all)
    
    def start_calculate_customer_express_cost(self):
        ExpressService.start_calculate_customer_express_cost()

    start_calculate_customer_express_cost = pyqtSlot()(start_calculate_customer_express_cost)
    
    def start_calculate_customer_reject_express_cost(self):
        ExpressService.start_calculate_customer_reject_express_cost()

    start_calculate_customer_reject_express_cost = pyqtSlot()(start_calculate_customer_reject_express_cost)
    
    def stop_calculate_customer_express_cost(self):
        ExpressService.stop_calculate_customer_express_cost()

    stop_calculate_customer_express_cost = pyqtSlot()(stop_calculate_customer_express_cost)
    
    def stop_calculate_customer_reject_express_cost(self):
        ExpressService.stop_calculate_customer_reject_express_cost()

    stop_calculate_customer_reject_express_cost = pyqtSlot()(stop_calculate_customer_reject_express_cost)
    
    def start_pay_cash_for_customer_express(self):
        ExpressService.start_pay_cash_for_customer_express()

    start_pay_cash_for_customer_express = pyqtSlot()(start_pay_cash_for_customer_express)
    
    def start_pay_cash_for_customer_reject_express(self):
        ExpressService.start_pay_cash_for_customer_reject_express()

    start_pay_cash_for_customer_reject_express = pyqtSlot()(start_pay_cash_for_customer_reject_express)
    
    def start_pull_pre_pay_cash_for_customer_express(self, express_cost):
        ExpressService.start_pull_pre_pay_cash_for_customer_express(express_cost)

    start_pull_pre_pay_cash_for_customer_express = pyqtSlot(int)(start_pull_pre_pay_cash_for_customer_express)
    
    def start_pull_pre_pay_cash_for_customer_reject_express(self, express_cost):
        ExpressService.start_pull_pre_pay_cash_for_customer_reject_express(express_cost)

    start_pull_pre_pay_cash_for_customer_reject_express = pyqtSlot(int)(start_pull_pre_pay_cash_for_customer_reject_express)
    
    def start_free_mouth_by_size(self, mouth_size):
        BoxService.start_free_mouth_by_size(mouth_size)

    start_free_mouth_by_size = pyqtSlot(str)(start_free_mouth_by_size)
    
    def stop_pay_cash_for_customer_express(self):
        ExpressService.stop_pay_cash_for_customer_express()

    stop_pay_cash_for_customer_express = pyqtSlot()(stop_pay_cash_for_customer_express)
    
    def stop_pay_cash_for_customer_reject_express(self):
        ExpressService.stop_pay_cash_for_customer_reject_express()

    stop_pay_cash_for_customer_reject_express = pyqtSlot()(stop_pay_cash_for_customer_reject_express)
    
    def start_get_customer_express_cost(self):
        ExpressService.start_get_customer_express_cost()

    start_get_customer_express_cost = pyqtSlot()(start_get_customer_express_cost)
    
    def start_get_customer_reject_express_cost(self):
        ExpressService.start_get_customer_reject_express_cost()

    start_get_customer_reject_express_cost = pyqtSlot()(start_get_customer_reject_express_cost)
    
    def start_store_customer_express(self):
        ExpressService.start_store_customer_express()

    start_store_customer_express = pyqtSlot()(start_store_customer_express)
    
    def start_store_customer_reject_express(self):
        ExpressService.start_store_customer_reject_express()

    start_store_customer_reject_express = pyqtSlot()(start_store_customer_reject_express)
    
    def start_customer_load_send_express_list(self, page):
        ExpressService.start_customer_load_send_express_list(page)

    start_customer_load_send_express_list = pyqtSlot(int)(start_customer_load_send_express_list)
    
    def start_customer_load_reject_express_list(self, page):
        ExpressService.start_customer_load_reject_express_list(page)

    start_customer_load_reject_express_list = pyqtSlot(int)(start_customer_load_reject_express_list)
    
    def start_load_customer_send_express_count(self):
        ExpressService.start_load_customer_send_express_count()

    start_load_customer_send_express_count = pyqtSlot()(start_load_customer_send_express_count)
    
    def start_load_customer_reject_express_count(self):
        ExpressService.start_load_customer_reject_express_count()

    start_load_customer_reject_express_count = pyqtSlot()(start_load_customer_reject_express_count)
    
    def start_courier_take_send_express(self, send_express_id_list):
        ExpressService.start_staff_take_send_express_list(send_express_id_list)

    start_courier_take_send_express = pyqtSlot(str)(start_courier_take_send_express)
    
    def start_courier_take_send_express_all(self):
        ExpressService.start_staff_take_all_send_express()

    start_courier_take_send_express_all = pyqtSlot()(start_courier_take_send_express_all)
    
    def start_courier_take_reject_express(self, send_express_id_list):
        ExpressService.start_staff_take_reject_express_list(send_express_id_list)

    start_courier_take_reject_express = pyqtSlot(str)(start_courier_take_reject_express)
    
    def start_courier_take_reject_express_all(self):
        ExpressService.start_staff_take_all_reject_express()

    start_courier_take_reject_express_all = pyqtSlot()(start_courier_take_reject_express_all)
    
    def start_load_mouth_list(self, page):
        BoxService.start_load_mouth_list(page)

    start_load_mouth_list = pyqtSlot(int)(start_load_mouth_list)
    
    def start_load_manager_mouth_count(self):
        BoxService.start_load_manager_mouth_count()

    start_load_manager_mouth_count = pyqtSlot()(start_load_manager_mouth_count)
    
    def start_manager_set_mouth(self, box_id, box_status):
        BoxService.start_manager_set_mouth(box_id, box_status)

    start_manager_set_mouth = pyqtSlot(str, str)(start_manager_set_mouth)
    
    def start_manager_open_mouth(self, mouth_id):
        BoxService.start_manager_open_mouth(mouth_id)

    start_manager_open_mouth = pyqtSlot(str)(start_manager_open_mouth)
    
    def start_manager_open_all_mouth(self):
        BoxService.start_manager_open_all_mouth()

    start_manager_open_all_mouth = pyqtSlot()(start_manager_open_all_mouth)
    
    def start_manager_set_free_mouth(self, box_id):
        BoxService.start_manager_set_free_mouth(box_id)

    start_manager_set_free_mouth = pyqtSlot(str)(start_manager_set_free_mouth)
    
    def courier_get_user(self):
        UserService.courier_get_user()

    courier_get_user = pyqtSlot()(courier_get_user)
    
    def start_choose_mouth_size(self, mouth_size, method_type):
        BoxService.start_choose_mouth_size(mouth_size, method_type)

    start_choose_mouth_size = pyqtSlot(str, str)(start_choose_mouth_size)
    
    def start_get_imported_express(self, text):
        ExpressService.start_get_imported_express(text)

    start_get_imported_express = pyqtSlot(str)(start_get_imported_express)
    
    def start_get_electronic_commerce_reject_express(self):
        ExpressService.start_get_electronic_commerce_reject_express()

    start_get_electronic_commerce_reject_express = pyqtSlot()(start_get_electronic_commerce_reject_express)
    
    def start_customer_reject_for_electronic_commerce(self, barcode):
        ExpressService.start_customer_reject_for_electronic_commerce(barcode)

    start_customer_reject_for_electronic_commerce = pyqtSlot(str)(start_customer_reject_for_electronic_commerce)
    
    def start_customer_reject_select_merchant(self, merchant_name):
        ExpressService.start_customer_reject_select_merchant(merchant_name)

    start_customer_reject_select_merchant = pyqtSlot(str)(start_customer_reject_select_merchant)
    
    def start_store_customer_reject_for_electronic_commerce(self):
        ExpressService.start_store_customer_reject_for_electronic_commerce()

    start_store_customer_reject_for_electronic_commerce = pyqtSlot()(start_store_customer_reject_for_electronic_commerce)
    
    def start_video_capture(self, filename):
        Camera.start_video_capture(filename)

    start_video_capture = pyqtSlot(str)(start_video_capture)
    
    def start_explorer(self):
        logging.warning('SYSTEM CLOSED BY OPERATOR')
        AlertService.system_closed_alert()
        os.system('start explorer.exe')

    start_explorer = pyqtSlot()(start_explorer)
    
    def start_get_product_file(self):
        ExpressService.start_get_product_file()

    start_get_product_file = pyqtSlot()(start_get_product_file)
    
    def start_get_card_info(self, text, identity):
        UserService.start_get_card_info(text, identity)

    start_get_card_info = pyqtSlot(str, str)(start_get_card_info)
    
    def start_open_main_box(self):
        BoxService.start_open_main_box()

    start_open_main_box = pyqtSlot()(start_open_main_box)
    
    def start_download_source(self, a):
        DownloadService.start_download_source(a)

    start_download_source = pyqtSlot(str)(start_download_source)
    
    def start_get_ad_file(self):
        AdvertisermentService.start_get_ad_file()

    start_get_ad_file = pyqtSlot()(start_get_ad_file)
    
    def start_get_express_info_by_barcode(self):
        ExpressService.start_get_express_info_by_barcode()

    start_get_express_info_by_barcode = pyqtSlot()(start_get_express_info_by_barcode)
    
    def stop_get_express_info_by_barcode(self):
        ExpressService.stop_get_express_info_by_barcode()

    stop_get_express_info_by_barcode = pyqtSlot()(stop_get_express_info_by_barcode)
    
    def start_get_express_info(self, express_text):
        PopboxService.start_get_express_info(express_text)

    start_get_express_info = pyqtSlot(str)(start_get_express_info)
    
    def start_get_popbox_express_info(self):
        PopboxService.start_get_popbox_express_info()

    start_get_popbox_express_info = pyqtSlot()(start_get_popbox_express_info)
    
    def start_get_customer_info_by_card(self):
        PopboxService.start_get_customer_info_by_card()

    start_get_customer_info_by_card = pyqtSlot()(start_get_customer_info_by_card)
    
    def start_payment_by_card(self):
        PopboxService.start_payment_by_card()

    start_payment_by_card = pyqtSlot()(start_payment_by_card)
    
    def start_get_cod_status(self):
        PopboxService.start_get_cod_status()

    start_get_cod_status = pyqtSlot()(start_get_cod_status)
    
    def start_reset_partial_transaction(self):
        PopboxService.start_reset_partial_transaction()

    start_reset_partial_transaction = pyqtSlot()(start_reset_partial_transaction)

    def start_post_capture_file(self):
        PopboxService.start_post_capture_file()

    start_post_capture_file = pyqtSlot()(start_post_capture_file)

    def start_get_capture_file_location(self):
        PopboxService.start_get_capture_file_location()

    start_get_capture_file_location = pyqtSlot()(start_get_capture_file_location)

    def start_get_locker_name(self):
        PopboxService.start_get_locker_name()

    start_get_locker_name = pyqtSlot()(start_get_locker_name)

    def start_global_payment_emoney(self, amount):
        PopboxService.start_global_payment_emoney(amount)

    start_global_payment_emoney = pyqtSlot(str)(start_global_payment_emoney)

    def start_get_sepulsa_product(self, phone):
        PopboxService.start_get_sepulsa_product(phone)

    start_get_sepulsa_product = pyqtSlot(str)(start_get_sepulsa_product)

    def start_sepulsa_transaction(self, phone_no, prod_id, prod_type, pay_type):
        PopboxService.start_sepulsa_transaction(phone_no, prod_id, prod_type, pay_type)

    start_sepulsa_transaction = pyqtSlot(str, str, str, str)(start_sepulsa_transaction)

    def start_push_data_settlement(self, type):
        PopboxService.start_push_data_settlement(type)

    start_push_data_settlement = pyqtSlot(str)(start_push_data_settlement)

    def start_get_gui_version(self):
        PopboxService.start_get_gui_version()

    start_get_gui_version = pyqtSlot()(start_get_gui_version)

    def get_popshop_product(self, category, maxcount):
        PopboxService.get_popshop_product(category, maxcount)

    get_popshop_product = pyqtSlot(str, str)(get_popshop_product)

    def get_query_member_popsend(self, phone):
        PopboxService.get_query_member_popsend(phone)

    get_query_member_popsend = pyqtSlot(str)(get_query_member_popsend)

    def start_popsend_topup(self, amount):
        PopboxService.start_popsend_topup(amount)

    start_popsend_topup = pyqtSlot(str)(start_popsend_topup)

    def get_locker_data(self, province, city):
        PopboxService.get_locker_data(province, city)

    get_locker_data = pyqtSlot(str, str)(get_locker_data)

    def start_popshop_transaction(self, product, customer, purchase, address):
        PopboxService.start_popshop_transaction(product, customer, purchase, address)

    start_popshop_transaction = pyqtSlot(str, str, str, str)(start_popshop_transaction)

    def get_popsend_button(self):
        PopboxService.get_popsend_button()

    get_popsend_button = pyqtSlot()(get_popsend_button)

    def set_tvc_player(self, command):
        set_tvc_player(command)

    set_tvc_player = pyqtSlot(str)(set_tvc_player)

    def start_get_ads_images(self, zpath):
        PopboxService.start_get_ads_images(zpath)

    start_get_ads_images = pyqtSlot(str)(start_get_ads_images)

    def sepulsa_trx_check(self, trx_id):
        PopboxService.sepulsa_trx_check(trx_id)

    sepulsa_trx_check = pyqtSlot(str)(sepulsa_trx_check)

    def start_deposit_express(self):
        PopboxService.start_deposit_express()

    start_deposit_express = pyqtSlot()(start_deposit_express)

    def box_start_migrate(self):
        PopboxService.box_start_migrate()

    box_start_migrate = pyqtSlot()(box_start_migrate)

def signal_handler():
    ExpressService.express_signal_handler.product_file_signal.connect(view.rootObject().product_file_result)
    ExpressService.express_signal_handler.customer_take_express_signal.connect(view.rootObject().customer_take_express_result)
    BoxService.box_signal_handler.mouth_number_signal.connect(view.rootObject().mouth_number_result)
    BoxService.box_signal_handler.manager_get_box_info_signal.connect(view.rootObject().manager_get_box_info_result)
    BoxService.box_signal_handler.get_version_signal.connect(view.rootObject().get_version_result)
    UserService.user_signal_handler.user_login_signal.connect(view.rootObject().user_login_result)
    ExpressService.express_signal_handler.barcode_signal.connect(view.rootObject().barcode_result)
    ExpressService.express_signal_handler.store_express_signal.connect(view.rootObject().store_express_result)
    ExpressService.express_signal_handler.phone_number_signal.connect(view.rootObject().phone_number_result)
    ExpressService.express_signal_handler.paid_amount_signal.connect(view.rootObject().paid_amount_result)
    UserService.user_signal_handler.user_info_signal.connect(view.rootObject().user_info_result)
    ExpressService.express_signal_handler.customer_store_express_signal.connect(view.rootObject().customer_store_express_result)
    BoxService.box_signal_handler.mouth_status_signal.connect(view.rootObject().mouth_status_result)
    BoxService.box_signal_handler.free_mouth_num_signal.connect(view.rootObject().free_mouth_result)
    ExpressService.express_signal_handler.overdue_express_list_signal.connect(view.rootObject().overdue_express_list_result)
    ExpressService.express_signal_handler.overdue_express_count_signal.connect(view.rootObject().overdue_express_count_result)
    BoxService.box_signal_handler.init_client_signal.connect(view.rootObject().init_client_result)
    ExpressService.express_signal_handler.staff_take_overdue_express_signal.connect(view.rootObject().courier_take_overdue_express_result)
    ExpressService.express_signal_handler.load_express_list_signal.connect(view.rootObject().load_express_list_result)
    ExpressService.express_signal_handler.customer_store_express_cost_signal.connect(view.rootObject().customer_store_express_cost_result)
    ExpressService.express_signal_handler.customer_express_cost_insert_coin_signal.connect(view.rootObject().customer_express_cost_insert_coin_result)
    ExpressService.express_signal_handler.store_customer_express_result_signal.connect(view.rootObject().store_customer_express_result_result)
    ExpressService.express_signal_handler.send_express_list_signal.connect(view.rootObject().send_express_list_result)
    ExpressService.express_signal_handler.send_express_count_signal.connect(view.rootObject().send_express_count_result)
    ExpressService.express_signal_handler.staff_take_send_express_signal.connect(view.rootObject().take_send_express_result)
    BoxService.box_signal_handler.manager_mouth_count_signal.connect(view.rootObject().manager_mouth_count_result)
    BoxService.box_signal_handler.load_mouth_list_signal.connect(view.rootObject().load_mouth_list_result)
    BoxService.box_signal_handler.mouth_list_signal.connect(view.rootObject().mouth_list_result)
    BoxService.box_signal_handler.manager_set_mouth_signal.connect(view.rootObject().manager_set_mouth_result)
    BoxService.box_signal_handler.manager_open_mouth_by_id_signal.connect(view.rootObject().manager_open_mouth_by_id_result)
    UserService.user_signal_handler.courier_get_user_signal.connect(view.rootObject().courier_get_user_result)
    BoxService.box_signal_handler.manager_open_all_mouth_signal.connect(view.rootObject().manager_open_all_mouth_result)
    BoxService.box_signal_handler.choose_mouth_signal.connect(view.rootObject().choose_mouth_result)
    BoxService.box_signal_handler.free_mouth_by_size_result.connect(view.rootObject().free_mouth_by_size_result)
    ExpressService.express_signal_handler.imported_express_result_signal.connect(view.rootObject().imported_express_result)
    ExpressService.express_signal_handler.customer_reject_express_signal.connect(view.rootObject().customer_reject_express_result)
    ExpressService.express_signal_handler.reject_express_signal.connect(view.rootObject().reject_express_result)
    ExpressService.express_signal_handler.reject_express_list_signal.connect(view.rootObject().reject_express_list_result)
    BoxService.box_signal_handler.open_main_box_signal.connect(view.rootObject().open_main_box_result)
    DownloadService.download_signal_handler.ad_download_result_signal.connect(view.rootObject().ad_download_result)
    DownloadService.download_signal_handler.delete_result_signal.connect(view.rootObject().delete_result)
    AdvertisermentService.ad_signal_handler.ad_source_signal.connect(view.rootObject().ad_source_result)
    AdvertisermentService.ad_signal_handler.ad_source_number_signal.connect(view.rootObject().ad_source_number_result)
    PopboxService.pop_signal_handler.start_get_express_info_result_signal.connect(view.rootObject().start_get_express_info_result)
    PopboxService.pop_signal_handler.start_get_popbox_express_info_signal.connect(view.rootObject().start_get_popbox_express_info_result)
    PopboxService.pop_signal_handler.start_get_customer_info_by_card_signal.connect(view.rootObject().start_get_customer_info_by_card_result)
    PopboxService.pop_signal_handler.start_payment_by_card_signal.connect(view.rootObject().start_payment_by_card_result)
    PopboxService.pop_signal_handler.start_get_cod_status_signal.connect(view.rootObject().start_get_cod_status_result)
    PopboxService.pop_signal_handler.start_reset_partial_transaction_signal.connect(view.rootObject().start_reset_partial_transaction_result)
    PopboxService.pop_signal_handler.start_post_capture_file_signal.connect(view.rootObject().start_post_capture_file_result)
    PopboxService.pop_signal_handler.start_get_locker_name_signal.connect(view.rootObject().start_get_locker_name_result)
    PopboxService.pop_signal_handler.start_global_payment_emoney_signal.connect(view.rootObject().start_global_payment_emoney_result)
    PopboxService.pop_signal_handler.start_get_sepulsa_product_signal.connect(view.rootObject().start_get_sepulsa_product_result)
    PopboxService.pop_signal_handler.start_sepulsa_transaction_signal.connect(view.rootObject().start_sepulsa_transaction_result)
    PopboxService.pop_signal_handler.start_get_gui_version_signal.connect(view.rootObject().start_get_gui_version_result)
    PopboxService.pop_signal_handler.start_customer_scan_qr_code_signal.connect(view.rootObject().start_customer_scan_qr_code_result)
    PopboxService.pop_signal_handler.stop_customer_scan_qr_code_signal.connect(view.rootObject().stop_customer_scan_qr_code_result)
    PopboxService.pop_signal_handler.get_popshop_product_signal.connect(view.rootObject().get_popshop_product_result)
    PopboxService.pop_signal_handler.get_query_member_popsend_signal.connect(view.rootObject().get_query_member_popsend_result)
    PopboxService.pop_signal_handler.start_popsend_topup_signal.connect(view.rootObject().start_popsend_topup_result)
    PopboxService.pop_signal_handler.get_locker_data_signal.connect(view.rootObject().get_locker_data_result)
    PopboxService.pop_signal_handler.start_popshop_transaction_signal.connect(view.rootObject().start_popshop_transaction_result)
    PopboxService.pop_signal_handler.get_popsend_button_signal.connect(view.rootObject().get_popsend_button_result)
    PopboxService.pop_signal_handler.start_get_ads_images_signal.connect(view.rootObject().get_ads_images_result)
    PopboxService.pop_signal_handler.sepulsa_trx_check_signal.connect(view.rootObject().sepulsa_trx_check_result)
    PopboxService.pop_signal_handler.box_start_migrate_signal.connect(view.rootObject().box_start_migrate_result)

def processExists(processname):
    tlcall = 'TASKLIST', '/FI', 'imagename eq %s' % processname
    tlproc = subprocess.Popen(tlcall, shell=True, stdout=subprocess.PIPE)
    tlout = tlproc.communicate()[0].decode('utf-8').strip().split("\r\n")
    if len(tlout) > 1 and processname in tlout[-1]:
        print('process "%s" is running!' % processname)
        return True
    else:
        print('process "%s" is NOT running!' % processname)
        return False

def set_tvc_player(command):
    if command == "":
        return
    elif command == "STOP":
        os.system(sys.path[0] + '/player/stop.bat')
    elif command == "START":
        if not processExists("PopBoxTVCPlayer.scr"):
            os.system(sys.path[0] + '/player/start.bat')
        else:
            return

def configuration_log():
    try:
        if not os.path.exists(sys.path[0] + '/log/'):
            os.makedirs(sys.path[0] + '/log/')
        handler = logging.handlers.TimedRotatingFileHandler(filename=sys.path[0] + '/log/base.log',
                                                            when='MIDNIGHT',
                                                            interval=1,
                                                            backupCount=60)
        # print("Logging handler : ", handler)
        logging.basicConfig(handlers=[handler],
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s',
                            datefmt='%d/%m %H:%M:%S')
        # logger = logging.getLogger()
        # logger.setLevel(logging.DEBUG)
    except Exception as e:
        print("Logging Configuration ERROR : ", e)

def get_disk_info():
    encrypt_str = ''
    disk_info = []
    try:
        c = wmi.WMI()
        for physical_disk in c.Win32_DiskDrive():
            encrypt_str = encrypt_str + physical_disk.SerialNumber.strip()
    except Exception as e:
        encrypt_str = 'None'
        logging.getLogger().warning(('Error Getting Disk Info : ', e))

    disk_info.append(encrypt_str)
    HttpClient.disk_serial_number = disk_info[0]

def check_database(data_name):
    if not os.path.exists(sys.path[0] + '/database/' + data_name + '.db'):
        ClientDatabase.init_database()
    logging.getLogger().info(("DB : ", data_name))

def init_Emoneyreader():
    init_Emoney = device.QP3000S.init_serial(x=1)
    logging.warning(('init_Emoney flag is ', init_Emoney))
    if init_Emoney:
        init_result = device.QP3000S.initSAM()
        sync_time = device.QP3000S.syncTime()
        logging.getLogger().info(('init_SAM & sync_Time result is ', init_result, sync_time))
    else:
        logging.getLogger().warning(('init_SAM & sync_Time result is ERROR'))

if Configurator.get_value('panel', 'gui') == "development":
    development_mode = True
else:
    development_mode = False

if Configurator.get_value('panel', 'emoney') == "enabled":
    reader_activated = True
else:
    reader_activated = False

if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
    db_name = 'pakpobox'
else:
    db_name = 'popboxclient'

def kill_explorer():
    if not development_mode:
        os.system('taskkill /f /im explorer.exe')
    else:
        logging.getLogger().info(('Development Mode is ON'))

def init_nfc_reader():
    if reader_activated and not development_mode:
        init_Emoneyreader()
    else:
        logging.getLogger().info(('eMoney Reader is not ACTIVATED!'))

if __name__ == '__main__':
    configuration_log()
    kill_explorer()
    check_database(db_name)
    path = sys.path[0] + '/qml/'
    if os.name == 'nt':
        path = 'qml/'
    slot_handler = SlotHandler()
    app = QGuiApplication(sys.argv)
    view = QQuickView()
    context = view.rootContext()
    translator = QTranslator()
    context.setContextProperty('slot_handler', slot_handler)
    translator.load(path + 'first.qm')
    app.installTranslator(translator)
    view.engine().quit.connect(app.quit)
    view.setSource(QUrl(path + 'Main.qml'))
    signal_handler()
    if not development_mode:
        app.setOverrideCursor(Qt.BlankCursor)
    view.setFlags(Qt.WindowFullscreenButtonHint)
    view.setFlags(Qt.FramelessWindowHint)
    view.resize(1080, 1920)
    get_disk_info()
    view.show()
    init_nfc_reader()
    PushMessage.start_sync_message()
    PullMessage.start_pull_message()
    pygame.init()
    Camera.init_camera()
    app.exec_()
