import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:floating_window
    visible: false

    Rectangle{
        x: 122
        y: 209
        width: 780
        height: 350
        color: "#ffffff"
        opacity: 0.9
        Image{
            id: image_checklist
            x: 0
            y: 108
            width:200
            height:200
            fillMode: Image.PreserveAspectFit
            source: "img/otherImages/checklist_red.png"
        }
    }
    function open(){
        floating_window.visible = true
    }
    function close(){
        floating_window.visible = false
    }
}
