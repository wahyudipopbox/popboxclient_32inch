import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle {
    id:root
    //Default 32 inch Monitor Resolution
    width: 1080
    height: 1920
    color: "black" //"#C02D26"

    signal customer_take_express_result(string str)
    signal mouth_number_result(string str)
    signal overdue_cost_result(string str)
    signal user_login_result(string str)
    signal get_version_result(string str)
    signal barcode_result(string str)
    signal store_express_result(string str)
    signal phone_number_result(string str)
    signal paid_amount_result(string str)
    signal user_info_result(string str)
    signal customer_store_express_result(string str)
    signal mouth_status_result(string str)
    signal overdue_express_list_result(string str)
    signal overdue_express_count_result(int i)
    signal load_express_list_result(string str)
    signal init_client_result(string str)
    signal courier_take_overdue_express_result(string str)
    signal customer_store_express_cost_result(string str)
    signal customer_express_cost_insert_coin_result(string str)
    signal store_customer_express_result_result(string str)
    signal send_express_list_result(string str)
    signal send_express_count_result(int i)
    signal take_send_express_result(string str)
    signal manager_mouth_count_result(int i)
    signal load_mouth_list_result(string str)
    signal mouth_list_result(string str)
    signal manager_set_mouth_result(string str)
    signal manager_open_mouth_by_id_result(string str)
    signal courier_get_user_result(string str)
    signal manager_open_all_mouth_result(string str)
    signal choose_mouth_result(string str)
    signal free_mouth_result(string str)
    signal free_mouth_by_size_result(string str)
    signal imported_express_result(string str)
    signal customer_reject_express_result(string str)
    signal reject_express_count_result(int i)
    signal take_reject_express_result(string str)
    signal reject_express_list_result(string str)
    signal reject_express_result(string str)
    signal manager_get_box_info_result(string str)
    signal ad_download_result(string str)
    signal ad_source_result(string str)
    signal ad_source_number_result(string str)
    signal delete_result(string str)
    signal product_file_result(string str)
    signal open_main_box_result(string str)
    signal start_get_express_info_result(string str)
    signal start_get_pakpobox_express_info_result(string str)
    signal start_get_customer_info_by_card_result(string str)
    signal start_payment_by_card_result(string str)
    signal start_get_cod_status_result(string str)
    signal start_get_popbox_express_info_result(string str)
    signal start_reset_partial_transaction_result(string str)
    signal start_post_capture_file_result(string str)
    signal start_get_capture_file_location_result(string str)
    signal start_get_locker_name_result(string str)
    signal start_global_payment_emoney_result(string str)
    signal start_get_sepulsa_product_result(string str)
    signal start_sepulsa_transaction_result(string str)
    signal start_get_gui_version_result(string str)
    signal start_customer_scan_qr_code_result(string str)
    signal stop_customer_scan_qr_code_result(string str)
    signal get_popshop_product_result(string str)
    signal get_query_member_popsend_result(string str)
    signal start_popsend_topup_result(string str)
    signal get_locker_data_result(string str)
    signal start_popshop_transaction_result(string str)
    signal get_popsend_button_result(string str)
    signal get_ads_images_result(string str)
    signal sepulsa_trx_check_result(string str)
    signal box_start_migrate_result(string str)

    //==================================================================================================//

    Image{
        id: top_base_bground
        x: 0
        y: 0
        width: root.width
        height: root.height - my_stack_view.height
        source:"img/popbox_base.png"
        fillMode: Image.PreserveAspectFit
    }

    Advertisement{
        id: ads_slider
        x: 0
        y: 0
        width: root.width
        height: root.height - my_stack_view.height
    }

    property int call_rule: 3

    MouseArea{
        id: hidden_button_mouse
        x: 500
        //y: 0
        y: 1152
        width: 80
        height: 80
        onDoubleClicked: {
            call_rule += 1
            if(call_rule % 3 == 0){
                slot_handler.set_tvc_player("START")
            }else{
                slot_handler.set_tvc_player("STOP")
            }
        }
    }

    //==================================================================================================//

    Image{
        id: bottom_base_bground
        width: root.width
        height: my_stack_view.height
        anchors.bottom: root.bottom
        anchors.bottomMargin: 0
        source: "img/background/1.jpg"
        fillMode: Image.Stretch
    }

    //=================================================================================================//


    StackView {
        id: my_stack_view
        width: 1024
        height: 768
        anchors.bottom: root.bottom
        anchors.bottomMargin: 0
        //x: 0
        //y: 0
        anchors.horizontalCenter: root.horizontalCenter
        initialItem: select_language_view

        delegate: StackViewDelegate {
            function transitionFinished(properties)
            {
                properties.exitItem.opacity = 1
            }

            pushTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                }
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                }
            }
        }
    }

    Component {
        id: sepulsa_check_input_view
        SepulsaCheckInputPage{
        }
    }

    Component {
        id: sepulsa_check_info_view
        SepulsaCheckExpressInfo{
        }
    }

    Component {
        id: grocery_product_list_new
        GroceryProductListNew{
        }
    }

    Component {
        id: grocery_input_quantity
        GrocerySelectQuantity{
        }
    }

    Component {
        id: grocery_payment_success
        GroceryPaymentSuccessPage{
        }
    }

    Component {
        id: grocery_express_info
        GroceryExpressInfo{
        }
    }

    Component {
        id: customer_input_details
        CustomerInputDetailsPage{
        }
    }

    Component {
        id: customer_select_locker
        CustomerSelectLocker{
        }
    }

    Component {
        id: popsend_topup_success
        PopsendTopupSuccessPage{
        }
    }

    Component {
        id: popsend_express_info
        PopsendExpressInfo{
        }
    }

    Component {
        id: popsend_denom_topup
        PopsendDenomTopup{
        }
    }

    Component {
        id:new_select_service_b_view
        NewSelectServicePageB{
        }
    }

    Component {
        id:new_select_service_e_view
        NewSelectServicePageE{
        }
    }

    Component {
        id:signature_capture_view
        SignatureCapture{
        }
    }

    Component {
        id:faq_page_ina
        FAQPageINA{
        }
    }

    Component {
        id:faq_page_en
        FAQPageEN{
        }
    }

    Component {
        id:select_language_view
        SelectLanguagePage{
        }
    }

    Component {
        id:select_service_e_view
        SelectServicePageE{
        }
    }

    Component {
        id:select_service_b_view
        SelectServicePageB{
        }
    }

    Component{
        id:customer_scan_qr_code_view
        CustomerScanQrCodePage{
        }
    }

    Component{
        id:manager_cabinet_view
        ManagerCabinetPage{
        }
    }

    Component{
        id:customer_take_express_view
        CustomerTakeExpressPage{
        }
    }

    Component{
        id:door_open_view
        DoorOpenPage{
        }
    }

    Component{
        id:background_login_view
        BackgroundLoginPage{
        }
    }

    Component{
        id:courier_psw_error_view
        CourierPswErrorPage{
        }
    }

    Component{
        id:courier_service_view
        CourierServicePage{
        }
    }

    Component{
        id:courier_memory_view
        CourierMemoryPage{
        }
    }

    Component{
        id:courier_select_box_view
        CourierSelectBoxSizePage{
        }
    }

    Component{
        id:courier_input_phone_view
        CourierInputPhonePage{
        }
    }

    Component{
        id:customer_take_express_opendoor_view
        OpenDoorPage{
        }
    }

    Component{
        id:customer_take_express_overtime_view
        OverTimePage{
        }
    }

    Component{
        id:customer_take_express_payment_view
        PayMentPage{
        }
    }

    Component{
        id:customer_take_express_error_view
        PassWordErrorPage{
        }
    }

    Component{
        id:courier_input_phone_sure_view
        CourierInputPhoneSurePage{
        }
    }

    Component{
        id:courier_select_box_size_view
        CourierSelectBoxSizePage{
        }
    }

    Component{
        id:customer_send_express_view
        SendExpressPage{
        }
    }

    Component{
        id:background_overdue_time_view
        BackgroundOverdueTimePage{
        }
    }

    Component{
        id:manager_service_view
        ManagerServicePage{
        }
    }

    Component{
        id:user_quit_view
        UserQuitPage{
        }
    }

    Component{
        id:user_select_view
        UserSelectPage{
        }
    }

    Component{
        id:user_select_express_page
        UserSelectExpressPage{
        }
    }

    Component{
        id:send_input_memory_page
        SendInputMemoryPage{
        }
    }

    Component{
        id:send_pay_page
        SendPayMentPage{
        }
    }

    Component{
        id:send_inser_coin_pagee
        SendInserCoinPage{
        }
    }

    Component{
        id:send_express_info
        SendExpressInfo{
        }
    }

    Component{
        id:send_select_box
        SendSelectBoxSizePage{
        }
    }

    Component{
        id:on_develop_view
        OnDevelopPage{
        }
    }

    Component{
        id:input_coin_view
        InsertCoinPage{
        }
    }

    Component{
        id:box_manage_page
        BoxManagePage{
        }
    }

    Component{
        id:user_agreement_page
        UserAgreementPage{
        }
    }

    Component{
        id:manage_take_overdue_page
        ManagerTakeOverdueExpressPage{
        }
    }

    Component{
        id:take_send_express_page
        TakeSendExpress{
        }
    }

    Component{
        id:pick_up_thanks_page
        PickUpThankspage{
        }
    }

    Component{
        id:r_drop_off_thanks_page
        DropOffThankspage1{
        }
    }

    Component{
        id:s_drop_off_thanks_page
        DropOffThankspage2{
        }
    }
    
    Component{
        id:send_door_open_page
        SendDoorOpenPage{
	    }
	}

    Component{
        id:reject_input_memory_page
        RejectInputMemoryPage{
        }
    }

    Component{
        id:reject_Door_open_page
        RejectDoorOpenPage{
        }
    }

    Component{
        id:reject_express_help
        RejectExpressHelp{
        }
    }

    Component{
        id:reject_express_info
        RejectExpressInfo{
        }
    }

    Component{
        id:reject_express_weigh_page
        RejectExpressWeightPage{
        }
    }

    Component{
        id:reject_insert_coin_page
        RejectInsertCoinPage{
        }
    }

    Component{
        id:reject_pay_ment_page
        RejectPayMentPage{
        }
    }

    Component{
        id:reject_select_box_size_page
        RejectSelectBoxSizePage{
        }
    }

    Component{
        id:take_reject_express
        TakeRejectExpress{
        }
    }

    Component{
        id:reject_inputphone_page
        RejectInputPhonePage{
        }
    }

    Component{
        id:reject_input_phone_sure_view
        RejectInputPhoneSurePage{
        }
    }

    Component{
        id:reject_express_thank_page
        RejectExpressThankPage{
        }
    }

    Component{
        id:web_view_page
        WebPage{
        }
    }

    Component{
        id:ad_page
        Advertisement{
        }
    }
    
    Component{
        id:reject_select_merchant_name_page
        RejectSelectMerchantNamePage{
        }
    }

    Component{
        id:emoney_service_page
        EmoneyServicePage{
        }
    }

    Component{
        id:other_input_memory_page
        OtherInputMemoryPage{
        }
    }

    Component{
        id:other_service_express_info_page
        OtherServiceExpressInfo{
        }
    }

    Component{
        id:other_payment_confirmation_page
        OtherPaymentConfirmationPage{
        }
    }

    Component{
        id:other_insufficient_balance_page
        OtherInsufficientBalancePage{

        }
    }

    Component{
        id:other_search_fail_page
        OtherSearchFailPage{
        }
    }

    Component{
        id:other_payment_failed_page
        OtherPaymentFailedPage{
        }
    }

    Component{
        id:other_payment_wrongcard_page
        OtherPaymentWrongCardPage{
        }
    }

    Component{
        id:other_payment_unfinished_page
        OtherPaymentUnfinishedPage{
        }
    }

    Component{
        id:other_payment_successful_page
        OtherPaymentSuccessfulPage{
        }
    }

    Component{
        id:other_search_paid_page
        OtherSearchPaidPage{
        }
    }

    Component{
        id:other_service_emoney
        OtherServiceEmoney{
        }
    }

    Component{
        id:qr_shop_info
        QRshopinfo{
        }
    }

    Component{
        id:mm_return_info
        MMreturninfo{
        }
    }

    Component{
        id:express_login_view
        ExpressLoginPage{
        }
    }

    Component{
        id:emoney_check_balance
        EmoneyCheckBalance{
        }
    }

    Component{
        id:emoney_balance_info
        EmoneyBalanceInfo{
        }
    }

    Component{
        id:laundry_input_memory_page
        LaundryInputMemoryPage{
        }
    }

    Component{
        id:laundry_info_page
        LaundryInfo{
        }
    }

    Component{
        id:laundry_express_info
        LaundryExpressInfo{
        }
    }

    Component{
        id:laundry_thanks_page
        LaundryThanksPage{
        }
    }

    Component{
        id:laundry_door_open_page
        LaundryDoorOpenPage{
        }
    }

    Component{
        id:laundry_select_box
        LaundrySelectBoxSizePage{
        }
    }

    Component{
        id:camera_capture_view
        CameraCapture{
        }
    }

    Component{
        id:sepulsa_input_phone_view
        SepulsaInputPhonePage{
        }
    }

    Component{
        id:sepulsa_input_phone_sure_view
        SepulsaInputPhoneSurePage{
        }
    }

    Component{
        id:sepulsa_express_info_view
        SepulsaExpressInfo{
        }
    }

    Component{
        id:sepulsa_select_denom_view
        SepulsaSelectDenomPage{
        }
    }

    Component{
        id:sepulsa_payment_success_view
        SepulsaPaymentSuccessfulPage{
        }
    }

    Component{
       id:customer_input_email_view
       CustomerInputEmailPage{
       }
    }

    Component{
       id:grocery_product_list_view
       GroceryProductList{
       }
    }

    Component{
        id:grocery_catalogue_view
        GroceryProductListCatalogue{
        }
    }

}




