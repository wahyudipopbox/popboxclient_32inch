import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: customer_input_info
    width: 1024
    height: 768
    property int timer_value: 60
    property var show_name:"-"
    property var show_email:"-"
    property var show_phone:"-"
    property var press_step: "0"
    property var show_count:0
    property var usageOf:"undefined"
    property var data_product:"-"
    property var total_amount:"0"
    property var locker_name:"undefined"
    property variant check_number: ['0814','0815', '0816', '0855', '0856', '0857', '0858',
        '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
        '0817', '0818', '0819', '0859', '0877', '0878', '0879',
        '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988',
        '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889',
        '0895', '0896', '0897', '0898', '0899']
    property variant check_domain: [".com", ".net", ".co.id", ".asia", ".ac.id", ".id",
        ".my" ,".sg", ".ph", ".info", ".tv", ".sch.id", ".go.id"]

    Stack.onStatusChanged:{
        console.log("data_product_received : " + data_product + ", with amount : " + total_amount)
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_locker_name()
            if(customer_input_info.show_name != "" || customer_input_info.show_email != "" || customer_input_info.show_phone != ""){
                customer_input_info.show_name=""
                customer_input_info.show_email=""
                customer_input_info.show_phone=""
            }
            nextok_button.text = qsTr("Next")
            main_page.enabled = true
            textin.focus = true
            name_check.visible = false
            email_check.visible = false
            phone_check.visible = false
            touch_keyboard.alphaActive = true
            error_tips.close()
            confirm_locker.close()
            press_step = "0"
            show_count = 0
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_locker_name_result.connect(show_locker_name)
    }

    Component.onDestruction: {
        root.start_get_locker_name_result.disconnect(show_locker_name)
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name = text
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
    }

    Rectangle{
        id:main_page

        EmailKeyboard{
            id:touch_keyboard
            x:29
            y:360
            property var count:show_count
            alphaActive: true

            Component.onCompleted: {
                touch_keyboard.letter_button_clicked.connect(show_validate_code)
                touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
            }

            MouseArea{
                id: del_button_mouse_area
                x:857
                y:0
                height: 80
                width: 80
                onClicked: {
                    touch_keyboard.define_del_text()
                }
            }

            Text{
                id: nextok_button
                x:273
                y:335
                width:415
                height:50
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.bold: true
                color:"#ffffff"
                font.family:"Microsoft YaHei"
                font.pixelSize:24

            }


            function define_del_text(){
                console.log("press_step : " + press_step + ", with count : " + count)
                count = 0
                if(press_step=="0"){
                    customer_input_info.show_name = ""
                }else if(press_step=="1"){
                    customer_input_info.show_email = ""
                }else if(press_step=="2"){
                    customer_input_info.show_phone = ""
                }

            }

            /*function validate_email(x){
                var y = x.split("@")
                var z = y[1]
                for (var i = 0; i < check_domain.length; i++){
                    if(z.indexOf(i)){
                        return true
                    }else{
                        return false
                    }
                }
            }*/

            function validate_email(str) {
                var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(net|org|biz|com|edu|gov|info|net|asia|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
                return pattern.test(str)
             }

            function on_function_button_clicked(str){
                if(str == "ok"){
                    console.log("press_step : " + press_step)
                    if(press_step == "0" &&
                            customer_input_info.show_name.length > 3){
                        press_step = "1"
                        count = 0
                        name_check.visible = true
                        textin.focus = false
                        emailin.focus = true
                        txt_notif.text = qsTr("Oops, Something is wrong. Please ensure the email address is correct.")
                    }else if(press_step == "1" &&
                             customer_input_info.show_email.length > 10 &&
                             customer_input_info.show_email.indexOf("@") > -1 &&
                             validate_email(customer_input_info.show_email)==true){
                        press_step = "2"
                        count = 0
                        email_check.visible = true
                        emailin.focus = false
                        phonein.focus = true
                        nextok_button.text = "OK"
                        touch_keyboard.alphaActive = false
                        txt_notif.text = qsTr("Oops, Something is wrong. Please ensure the phone number is correct.")
                    }else if(press_step == "2" &&
                             check_number.indexOf(customer_input_info.show_phone.substring(0,4)) > -1 &&
                             customer_input_info.show_phone.length > 9){
                        phone_check.visible = true
                        count = 0
                        if(usageOf=="popshop"){
                            main_page.enabled = false
                            confirm_locker.open()
                        }else{
                            my_stack_view.push(on_develop_view)
                        }
                    }else{
                        main_page.enabled = false
                        error_tips.open()
                    }
                }
                if(str=="delete"){
                    console.log("press_step : " + press_step)
                    if(count>=30){
                        count=29
                    }
                    /*if(press_step=="0" && count>=72){
                        count=71
                        customer_input_info.show_name -= str
                    }else if(press_step=="1" && count>=72){
                        count=71
                        customer_input_info.show_email -= str
                    }else if(press_step=="2" && count>=72){
                        count=71
                        customer_input_info.show_phone -= str
                    }*/
                }
            }

            function show_validate_code(str){
                console.log("str : " + str)
                if (str == "" && count > 0){
                    /*if(count >= 72){
                        count = 71
                    }*/
                    if(press_step=="0" && count>=30){
                        count=29
                    }else if(press_step=="1" && count>=30){
                        count=29
                    }else if(press_step=="2" && count>=30){
                        count=29
                    }
                    count--
                    if(press_step=="0"){
                        customer_input_info.show_name=customer_input_info.show_name.substring(0,count);
                    }else if(press_step=="1"){
                        customer_input_info.show_email=customer_input_info.show_email.substring(0,count);
                    }else if(press_step=="2"){
                        customer_input_info.show_phone=customer_input_info.show_phone.substring(0,count);
                    }
                }
                if (str != "" && count < 30){
                    count++
                }
                if (count >= 30){
                    str = ""
                }
                else{
                    if(press_step=="0"){
                        customer_input_info.show_name += str
                    }else if(press_step=="1"){
                        customer_input_info.show_email += str
                    }else if(press_step=="2"){
                        customer_input_info.show_phone += str
                    }
                }
                abc.counter = timer_value
                my_timer.restart()
            }
        }
    }


    /*GroupBox{
        id: group_data_input
        x: 145
        y: 137
        width: 800
        height: 210
        flat: true
        checkable: false
        checked: false

        Row{
            id: row_name
            Text{
                id:name_label
                y:10
                x:20
                width: 150
                font.family:"Microsoft YaHei"
                text:qsTr("Name")
                color:"#FFFFFF"
                font.pixelSize:35
            }

            Rectangle{
                id: name_field
                x:180
                y:0
                width:545
                height:58
                color:"transparent"

                Image{
                    width:545
                    height:58
                    source:"img/courier11/input1.png"
                }

                TextEdit{
                    id:textin
                    y:5
                    x:20
                    font.family:"Microsoft YaHei"
                    text:show_name
                    color:"#FFFFFF"
                    font.pixelSize:35
                }
            }
        }

        Row{
            id: row_email
            x: 0
            y: 70
            Text{
                id:email_label
                y:10
                x:20
                width: 150
                font.family:"Microsoft YaHei"
                text:qsTr("Email")
                color:"#FFFFFF"
                font.pixelSize:35
            }

            Rectangle{
                id: email_field
                x:158
                y:1
                width:545
                height:58
                color:"transparent"

                Image{
                    width:545
                    height:58
                    source:"img/courier11/input1.png"
                }

                TextEdit{
                    id:emailin
                    y:5
                    x:20
                    font.family:"Microsoft YaHei"
                    text:show_email
                    color:"#FFFFFF"
                    font.pixelSize:35
                }
            }
        }

        Row{
            id: row_phone
            x: 0
            y: 140
            Text{
                id:phone_label
                y:10
                x:20
                width: 150
                font.family:"Microsoft YaHei"
                text:qsTr("Phone")
                color:"#FFFFFF"
                font.pixelSize:35
            }

            Rectangle{
                id: phone_field
                x:158
                y:1
                width:400
                height:58
                color:"transparent"

                Image{
                    width:400
                    height:58
                    source:"img/courier11/input1.png"
                }

                TextEdit{
                    id:phonein
                    y:5
                    x:20
                    font.family:"Microsoft YaHei"
                    text:show_phone
                    color:"#FFFFFF"
                    font.pixelSize:35
                }
            }
        }
    }*/

    Column{
        spacing: 23
        y:158
        x:105

        Text{
            id:name_label
            width: 150
            font.family:"Microsoft YaHei"
            text:qsTr("Name")
            color:"#FFFFFF"
            font.pixelSize:32
        }

        Text{
            id:email_label
            width: 150
            font.family:"Microsoft YaHei"
            text:qsTr("Email")
            color:"#FFFFFF"
            font.pixelSize:32
        }

        Text{
            id:phone_label
            width: 150
            font.family:"Microsoft YaHei"
            text:qsTr("Phone")
            color:"#FFFFFF"
            font.pixelSize:32
        }
    }

    Rectangle{
        id: name_field
        x:295
        y:143
        width:625
        height:58
        color:"transparent"

        Image{
            anchors.fill: parent
            source:"img/courier11/input1.png"
        }

        TextInput{
            id:textin
            y:5
            width: 600
            x:20
            clip: true
            cursorVisible: (textin.focus) ? true : false
            font.family:"Microsoft YaHei"
            text:show_name
            color:"#FFFFFF"
            font.pixelSize:35
        }
    }

    Rectangle{
        id: email_field
        x:295
        y:212
        width:625
        height:58
        color:"transparent"

        Image{
            anchors.fill: parent
            source:"img/courier11/input1.png"
        }

        TextInput{
            id:emailin
            y:5
            width: 600
            height: 40
            x:20
            clip: true
            cursorVisible: (emailin.focus) ? true : false
            font.family:"Microsoft YaHei"
            text:show_email
            //text:"fitrah.wahyudi.imam@gmail.com"
            color:"#FFFFFF"
            font.pixelSize:35
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                touch_keyboard.on_function_button_clicked("ok")
            }
        }
    }

    Rectangle{
        id: phone_field
        x:295
        y:281
        width:400
        height:58
        color:"transparent"

        Image{
            width:400
            height:58
            source:"img/courier11/input1.png"
        }

        TextInput{
            id:phonein
            y:5
            clip: true
            cursorVisible: (phonein.focus) ? true : false
            x:20
            font.family:"Microsoft YaHei"
            text:show_phone
            color:"#FFFFFF"
            font.pixelSize:35
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                touch_keyboard.on_function_button_clicked("ok")
            }
        }
    }

    GroupBox{
        id: validation_check
        x: 701
        y: 136
        width: 300
        height: 210
        flat: true
        Image{
            id: name_check
                visible: false
                x:237
                y:8
                source: "img/otherImages/checked_white.png"
                width: 40
                height: 40
                fillMode: Image.Stretch
        }

        Image {
            id: email_check
            visible: false
            x: 237
            y: 79
            width: 40
            height: 40
            fillMode: Image.Stretch
            source: "img/otherImages/checked_white.png"
        }

        Image {
            id: phone_check
            visible: false
            x: 7
            y: 147
            width: 40
            height: 40
            fillMode: Image.Stretch
            source: "img/otherImages/checked_white.png"
        }
    }

    HideWindow{
        id:error_tips
        //visible: true

        Image {
            id: img_error_tips
            x: 115
            y: 298
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            id: txt_notif
            x: 343
            y:323
            width: 554
            height: 150
            text: qsTr("Oops, Something is wrong. Please ensure all the data is filled out correctly.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:error_tips_back
            x:383
            y:561
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press_step=="0"){
                        customer_input_info.show_name = ""
                    }else if(press_step=="1"){
                        customer_input_info.show_email = ""
                    }else if(press_step=="2"){
                        customer_input_info.show_phone = ""
                    }
                    show_count = 0
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    error_tips.close()
                }
                onEntered:{
                    error_tips_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    error_tips_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:confirm_locker
        //visible: true

        Text {
            id: txt_confirm_title
            x: 123
            y:215
            width: 800
            height: 50
            text: qsTr("Delivery Confirmation")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_confirm_locker
            x: 123
            y: 309
            width: 200
            height: 200
            source: "img/door/door.png"
        }

        Text {
            id: txt_confirm_locker
            x: 329
            y:309
            width: 594
            height: 200
            text: qsTr("We will deliver the purchased products to this locker :") + "\n" + locker_name
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:28
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:confirm_locker_no
            x:215
            y:561
            show_text:qsTr("NO, CHOOSE OTHER")
            text_size: 24

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var datas = data_product + "|" + customer_input_info.show_name + "|" + customer_input_info.show_email + "|" + customer_input_info.show_phone
                    my_stack_view.push(customer_select_locker, {datas:datas, usageOf:usageOf, total_amount: total_amount})
                }
                onEntered:{
                    confirm_locker_no.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    confirm_locker_no.show_source = "img/button/7.png"
                }
            }
        }

        OverTimeButton{
            id:confirm_locker_yes
            x:537
            y:561
            show_text:qsTr("YES, I AGREE")
            text_size: 24

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var datas = data_product + "|" + customer_input_info.show_name + "|"
                            + customer_input_info.show_email + "|" + customer_input_info.show_phone + "|" + locker_name
                    my_stack_view.push(grocery_express_info, {data_all:datas, amount:total_amount})
                }
                onEntered:{
                    confirm_locker_yes.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    confirm_locker_yes.show_source = "img/button/7.png"
                }
            }
        }
    }

}
