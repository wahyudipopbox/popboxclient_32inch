<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>Advertisement</name>
    <message>
        <location filename="Advertisement.qml" line="143"/>
        <source>Click to enter the system</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Advertisement.qml" line="155"/>
        <source>Click to enter the system _en</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <location filename="BackButton.qml" line="12"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>BACK</translation>
    </message>
</context>
<context>
    <name>BackButtonMenu</name>
    <message>
        <location filename="BackButtonMenu.qml" line="12"/>
        <source>BACK</source>
        <oldsource>Back</oldsource>
        <translation>BACK</translation>
    </message>
</context>
<context>
    <name>OkButton</name>
    <message>
        <location filename="OkButton.qml" line="12"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>BackgroundLoginPage</name>
    <message>
        <location filename="BackgroundLoginPage.qml" line="260"/>
        <source>Please wait</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="277"/>
        <source>Network error, Please retry later</source>
        <translation>Oops, Locker is trying to re-connect to the system, Please wait a moment.</translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="291"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="321"/>
        <source>CARDID:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BackgroundLoginPage.qml" line="330"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BackgroundOverdueTimePage</name>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="89"/>
        <location filename="BackgroundOverdueTimePage.qml" line="639"/>
        <location filename="BackgroundOverdueTimePage.qml" line="687"/>
        <location filename="BackgroundOverdueTimePage.qml" line="741"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="111"/>
        <source>take back it</source>
        <translation>Retrieve</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="147"/>
        <source>take back all</source>
        <translation>Retrieve-All</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="210"/>
        <source>Phone</source>
        <translation>Mobile No.</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="223"/>
        <source>store-time</source>
        <translation>Store-Time</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="236"/>
        <source>box</source>
        <translation>Locker</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="249"/>
        <source>overdue-time</source>
        <translation>Overdue-time</translation>
    </message>
    <message>
        <source>other</source>
        <translation type="vanished">Other</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="609"/>
        <source>waiting</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="626"/>
        <source>set_null</source>
        <translation>No Overdue Parcel</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="674"/>
        <source>Successful</source>
        <translation>Successful</translation>
    </message>
    <message>
        <location filename="BackgroundOverdueTimePage.qml" line="728"/>
        <source>Failed</source>
        <translation>Failed, Please contact us for assistance</translation>
    </message>
    <message>
        <source>Please wait</source>
        <translation type="vanished">Please wait a moment</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">Success</translation>
    </message>
    <message>
        <source>Can not set the box</source>
        <translation type="vanished">Failed</translation>
    </message>
</context>
<context>
    <name>BackspaceButton</name>
    <message>
        <location filename="BackspaceButton.qml" line="17"/>
        <source>backspace</source>
        <translation>BACK</translation>
    </message>
</context>
<context>
    <name>BoxManagePage</name>
    <message>
        <location filename="BoxManagePage.qml" line="93"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="110"/>
        <source>Init</source>
        <translation>Init DB</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="129"/>
        <source>main locker</source>
        <translation>Main Door</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="152"/>
        <source>Please wait</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="169"/>
        <source>Successful</source>
        <translation>Successful</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="182"/>
        <location filename="BoxManagePage.qml" line="242"/>
        <location filename="BoxManagePage.qml" line="283"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="208"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="216"/>
        <source>Text</source>
        <translation>  </translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="229"/>
        <source>open mouth success</source>
        <translation>Open Door is Success</translation>
    </message>
    <message>
        <location filename="BoxManagePage.qml" line="270"/>
        <source>open mouth failure</source>
        <translation>Open Door is Failed</translation>
    </message>
</context>
<context>
    <name>CourierInputPhonePage</name>    
    <message>
        <location filename="CourierInputPhonePage.qml" line="61"/>
        <source>Enter Recipient Phone Number</source>
        <translation>Enter Recipient Phone Number</translation>
    </message>
    <message>
        <location filename="CourierInputPhonePage.qml" line="98"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="CourierInputPhonePage.qml" line="183"/>
        <source>The memory is not input or not correct</source>
        <translation>Oops, Something is wrong. Please ensure the mobile phone number is correct</translation>
    </message>
      <message>
        <location filename="CourierInputPhonePage.qml" line="272"/>
        <source>This parcel is detected under Lazada Account</source>
        <translation>This parcel is detected as Lazada parcel</translation>
    </message>
    <message>
        <location filename="CourierInputPhonePage.qml" line="196"/>
        <source>back</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>CourierInputPhoneSurePage</name>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="113"/>
        <source>Please check the customer mobile phone number</source>
        <translation>Please re-check the enterred customer phone number</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="126"/>
        <source>(customer verification code and QR CODE will be sent)</source>
        <translation>(Customer Verification and the QR CODE will be sent)</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="152"/>
        <location filename="CourierInputPhoneSurePage.qml" line="251"/>
        <source>cancel</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="174"/>
        <location filename="CourierInputPhoneSurePage.qml" line="273"/>
        <source>ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="221"/>
        <source>Notification will be sent immediately to the administrator number.</source>
        <translation>Notification will be sent immediately to the administrator number.</translation>
    </message>
    <message>
        <location filename="CourierInputPhoneSurePage.qml" line="236"/>
        <source>Please Press OK to continue the process.</source>
        <translation>Please Press OK to continue the process.</translation>
    </message>    
</context>
<context>
    <name>CourierLoginButton</name>
    <message>
        <location filename="CourierLoginButton.qml" line="27"/>
        <source>login</source>
        <translation>LOGIN</translation>
    </message>
</context>
<context>
    <name>CourierMemoryPage</name>
    <message>
        <location filename="CourierMemoryPage.qml" line="98"/>
        <source>Please scan or enter a tracking number</source>
        <translation>Scan/Enter AWB Number</translation>
    </message>
    <message>
        <location filename="CourierMemoryPage.qml" line="243"/>
        <source>We detected the order number is not entered yet. Please retry and enter the correct order number.</source>
        <translation>We detected the AWB number is not entered yet. Please retry and enter the correct AWB number.</translation>
    </message>
    <message>
        <location filename="CourierMemoryPage.qml" line="221"/>
        <location filename="CourierMemoryPage.qml" line="277"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="CourierMemoryPage.qml" line="305"/>
        <source>Please ensure the order number and locker location is correct. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Please ensure the AWB number and locker location is correct. Please contact our CS line at 021-22538719 for assistance.</translation>
    </message>
</context>
<context>
    <name>CourierOverdueTimePage</name>
    <message>
        <location filename="CourierOverdueTimePage.qml" line="56"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="CourierOverdueTimePage.qml" line="79"/>
        <source>Retrieve</source>
        <translation>Retrieve</translation>
    </message>
    <message>
        <location filename="CourierOverdueTimePage.qml" line="100"/>
        <source>Retrieve All</source>
        <translation>Retrieve-All</translation>
    </message>
</context>
<context>
    <name>CourierPswErrorPage</name>
    <message>
        <location filename="CourierPswErrorPage.qml" line="53"/>
        <source>Oops, Failed Authentication!</source>
        <translation>Oops, Failed Authentication!</translation>
    </message>
    <message>
        <location filename="CourierPswErrorPage.qml" line="86"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="CourierPswErrorPage.qml" line="77"/>
        <source>Please re-enter your account name and password, thank you!</source>
        <translation>Please re-enter the correct username and password!</translation>
    </message>
</context>
<context>
    <name>CourierSelectBoxSizePage</name>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="130"/>
        <source>please select service option</source>
        <translation>Please choose locker size</translation>
    </message>    
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="319"/>
        <source>return</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="383"/>
        <source>not_mouth</source>
        <translation>Locker is full</translation>
    </message>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="396"/>
        <location filename="CourierSelectBoxSizePage.qml" line="439"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="CourierSelectBoxSizePage.qml" line="426"/>
        <source>not_balance</source>
        <translation>Shortage of balance</translation>
    </message>
</context>
<context>
    <name>CourierServicePage</name>
    <message>
        <location filename="CourierServicePage.qml" line="67"/>
        <source>Store</source>
        <translation>Store</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="96"/>
        <source>Overdue</source>
        <translation>Overdue</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="121"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="139"/>
        <source>Take</source>
        <translation>Take</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="168"/>
        <source>History</source>
        <translation>History</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="197"/>
        <source>Reject</source>
        <translation>Returns</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="226"/>
        <source>Other services</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="255"/>
        <source>return</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="270"/>
        <source>Please select service options</source>
        <translation>Select Menu Below</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="359"/>
        <source>TAKE :</source>
        <translation>TAKE :</translation>
    </message> 
    <message>
        <location filename="CourierServicePage.qml" line="371"/>
        <source>Select this button to collect parcel/laundry from this locker.</source>
        <translation>Select this button to collect parcel/laundry from this locker.</translation>
    </message> 
    <message>
        <location filename="CourierServicePage.qml" line="395"/>
        <source>STORE :</source>
        <translation>STORE :</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="407"/>
        <source>Select this button to store parcels into this locker.</source>
        <translation>Select this button to store parcels into this locker.</translation>
    </message>     
</context>
<context>
    <name>CustomerInputCustomerStoreNumber</name>
    <message>
        <location filename="CustomerInputCustomerStoreNumber.qml" line="84"/>
        <source>Please scan or enter a tracking number</source>
        <translation>Scan/Enter Order No.</translation>
    </message>
</context>
<context>
    <name>CustomerScanQrCodePage</name>
    <message>
        <location filename="CustomerScanQrCodePage.qml" line="58"/>
        <source>Please open your QR code, 
 the code sent by SMS 
 to your mobile. Please QR 
 code placed in front of the scan window 
 10cm Office</source>
        <translation>Please place the 
QR Code that has 
been sent to your 
mobile phone via SMS 
in front of 
our scanner below.</translation>
    </message>
</context>
<context>
    <name>CustomerTakeExpressPage</name>
    <message>
        <source>Please enter the SMS verification code (6 alphanumeric combination)</source>
        <translation>Please enter 6 digit alphanumeric pin</translation>
    </message>
    <message>
        <location filename="CustomerTakeExpressPage.qml" line="37"/>
        <source>Scan or Enter Barcode</source>
        <translation>Enter Pin Code and Press OK</translation>
    </message>
    <message>
        <source>The memory is not input</source>
        <translation>Oops, Pin code is not enterred correctly.</translation>
    </message>
    <message>
        <source>back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>DoorEvey</name>
    <message>
        <location filename="DoorEvey.qml" line="12"/>
        <source>return to menu</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>DoorOpenPage</name>
    <message>
        <location filename="DoorOpenPage.qml" line="38"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="95"/>
        <source>Your locker is located at Box No </source>
        <translation>Your parcel is located at Box No. </translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="120"/>
        <source>The system will refresh in</source>
        <translation>The system will refresh in</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="177"/>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="189"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Please ensure compartment door closure upon completion</translation>
    </message>
    <message>
        <source>re-open</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <source>Store packages again</source>
        <translation>Re-Store</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="64"/>
        <source>Complete and exit</source>
        <translation>Complete</translation>
    </message>
    <message>
        <source>Please put the express into NO.</source>
        <translation>Please input the express into No.</translation>
    </message>
    <message>
        <location filename="DoorOpenPage.qml" line="102"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>,thank you</source>
        <translation type="vanished">,thank you!</translation>
    </message>
</context>
<context>
    <name>DropOffThankspage1</name>
    <message>
        <location filename="DropOffThankspage1.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Thank You for using PopBox.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="67"/>
        <source>A SMS notification has been sent to</source>
        <translation>A SMS notification has been sent to</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="75"/>
        <source>the recipient of this parcel.</source>
        <translation>the recipient of this parcel.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Main Menu</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="109"/>
        <source>Send Another Parcel?</source>
        <translation>Send Another?</translation>
    </message>
    <message>
        <location filename="DropOffThankspage1.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>I am Done</translation>
    </message>
</context>
<context>
    <name>DropOffThankspage2</name>
    <message>
        <location filename="DropOffThankspage2.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Thank You for using PopBox.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="67"/>
        <source>A SMS notification has been sent to</source>
        <translation>A SMS notification has been sent to</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="75"/>
        <source>the sender of this parcel.</source>
        <translation>the sender of this parcel.</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Main Menu</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="109"/>
        <source>Send Another Parcel?</source>
        <translation>Send Another?</translation>
    </message>
    <message>
        <location filename="DropOffThankspage2.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>I am Done</translation>
    </message>
</context>
<context>
    <name>FAQWebPage</name>
    <message>
        <location filename="FAQWebPage.qml" line="54"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>InsertCoinPage</name>
    <message>
        <location filename="InsertCoinPage.qml" line="38"/>
        <source>Welcome to use the system</source>
        <translation>Welcome</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="55"/>
        <source>You pay the overdue amount</source>
        <translation>You pay the amount</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="65"/>
        <source>return</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="88"/>
        <source>Please put a coin in the slot</source>
        <translation>Please put a coin in the slot</translation>
    </message>
    <message>
        <location filename="InsertCoinPage.qml" line="148"/>
        <source>yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>KeyboardFunctionButton</name>
    <message>
        <location filename="KeyboardFunctionButton.qml" line="19"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>KeyboardFunctionButtondelete</name>
    <message>
        <location filename="KeyboardFunctionButtondelete.qml" line="18"/>
        <source>DEL</source>
        <translation>DEL</translation>
    </message>
</context>
<context>
    <name>LoadingView</name>
    <message>
        <location filename="LoadingView.qml" line="36"/>
        <source>Preparing...</source>
        <translation>Preparing...</translation>
    </message>
</context>
<context>
    <name>LockerHelpPage</name>
    <message>
        <location filename="LockerHelpPage.qml" line="78"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Have other question,please press the
 &apos;cancel&apos; button and call us.</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="92"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="106"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="120"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="146"/>
        <source>re-open</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="175"/>
        <source>change-box</source>
        <translation>Change-Door</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="203"/>
        <source>cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="LockerHelpPage.qml" line="231"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Please Select</translation>
    </message>
</context>
<context>
    <name>ManageInitPage</name>
    <message>
        <location filename="ManageInitPage.qml" line="95"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="113"/>
        <location filename="ManageInitPage.qml" line="132"/>
        <source>Init</source>
        <translation>Init DB</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="157"/>
        <source>Please wait</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="174"/>
        <source>Successful</source>
        <translation>Successful</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="187"/>
        <location filename="ManageInitPage.qml" line="247"/>
        <location filename="ManageInitPage.qml" line="288"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="213"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="221"/>
        <source>Text</source>
        <translation>  </translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="234"/>
        <source>open mouth success</source>
        <translation>Open door is success</translation>
    </message>
    <message>
        <location filename="ManageInitPage.qml" line="275"/>
        <source>open mouth failure</source>
        <translation>Open door is failed</translation>
    </message>
</context>
<context>
    <name>ManagerCabinetPage</name>
    <message>
        <location filename="ManagerCabinetPage.qml" line="270"/>
        <source>Not Use</source>
        <translation>Not Used</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="283"/>
        <source>Available</source>
        <translation>Available</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="296"/>
        <source>Used</source>
        <translation>Used</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="309"/>
        <source>XS</source>
        <translation>XS</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="322"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="335"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="348"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="361"/>
        <source>XL</source>
        <translation>XL</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="377"/>
        <source>From the ark set</source>
        <translation>Setting</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="399"/>
        <source>exit</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="423"/>
        <source>A key to unlock</source>
        <translation>Open All</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="531"/>
        <source>Is a key to unlock</source>
        <translation>Open All Doors?</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="544"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="583"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="617"/>
        <source>Please wait</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="634"/>
        <source>Success</source>
        <translation>Success</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="647"/>
        <location filename="ManagerCabinetPage.qml" line="694"/>
        <location filename="ManagerCabinetPage.qml" line="742"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="681"/>
        <source>Coming Soon</source>
        <translation>Coming Soon</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="729"/>
        <source>Can not set the box</source>
        <translation>Failed</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="784"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <source> LastUsed Time: </source>
        <translation type="vanished">Last-Time:</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="829"/>
        <source>The door is type</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="864"/>
        <source>The door is number</source>
        <translation>ID :</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="899"/>
        <source>set enabled</source>
        <translation>Set :</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="914"/>
        <source>unenable to set the box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1115"/>
        <source> ENABLE</source>
        <translation>Enable</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1137"/>
        <source> LOCKED</source>
        <translation>Locked</translation>
    </message>
    <message>
        <source> USED</source>
        <translation>Used</translation>
    </message>
    <message>
        <source>LOCKED</source>
        <translation>Locked</translation>
    </message>
    <message>
        <source>ENABLE</source>
        <translation>Enable</translation>
    </message>
    <message>
        <source>USED</source>
        <translation>Used</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="970"/>
        <source>unlocking</source>
        <translation>Unlock</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1008"/>
        <source>Empty the box</source>
        <translation>Empty</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1039"/>
        <source>sure</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ManagerCabinetPage.qml" line="1076"/>
        <source> return</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>ManagerServicePage</name>
    <message>
        <location filename="ManagerServicePage.qml" line="86"/>
        <source>sys-info</source>
        <translation>Sys-Info</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="116"/>
        <source>box</source>
        <translation>Doors</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="145"/>
        <source>unusual</source>
        <translation>Unusual</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="174"/>
        <source>overdue</source>
        <translation>Overdue</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="198"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="219"/>
        <source>address:</source>
        <translation>Address :</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="234"/>
        <source>number:</source>
        <translation>Number :</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="248"/>
        <source>name:</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="381"/>
        <source>take_reject</source>
        <translation>Returns</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="408"/>
        <source>sure to quit</source>
        <translation>Confirm to Exit?</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="421"/>
        <source>sure</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="444"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <source>pakpobox</source>
        <translation>PopBox</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="301"/>
        <source>exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="332"/>
        <source>return</source>
        <translation>Main Menu</translation>
    </message>
    <message>
        <location filename="ManagerServicePage.qml" line="357"/>
        <source>take_send</source>
        <translation>Take</translation>
    </message>
</context>
<context>
    <name>ManagerTakeOverdueExpressPage</name>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="96"/>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="574"/>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="622"/>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="674"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="118"/>
        <source>take back it</source>
        <translation>Retrieve</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="154"/>
        <source>take back all</source>
        <translation>Retrieve-All</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="217"/>
        <source>Phone</source>
        <translation>Mobile No.</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="230"/>
        <source>store-time</source>
        <translation>Store-Time</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="243"/>
        <source>box</source>
        <translation>Locker</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="256"/>
        <source>overdue-time</source>
        <translation>Overdue-time</translation>
    </message>
    <message>
        <source>other</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="544"/>
        <source>waiting</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="561"/>
        <source>set_null</source>
        <translation>No Overdue Parcel</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="609"/>
        <source>Successful</source>
        <translation>Successful</translation>
    </message>
    <message>
        <location filename="ManagerTakeOverdueExpressPage.qml" line="661"/>
        <source>Failed</source>
        <translation>Failed, Please contact us for assistance</translation>
    </message>
</context>
<context>
    <name>NumKeyboard</name>
    <message>
        <location filename="NumKeyboard.qml" line="72"/>
        <source>return</source>
        <translation>BACK</translation>
    </message>
    <message>
        <location filename="NumKeyboard.qml" line="80"/>
        <source>DEL</source>
        <translation>DELETE</translation>
    </message>
</context>
<context>
    <name>NumboardFunctionButtondelete</name>
    <message>
        <source>DEL</source>
        <translation>DEL</translation>
    </message>
</context>
<context>
    <name>OnDevelopPage</name>
    <message>
        <location filename="OnDevelopPage.qml" line="56"/>
        <source>Coming Soon</source>
        <translation>Coming Soon</translation>
    </message>
    <message>
        <location filename="OnDevelopPage.qml" line="67"/>
        <source>return</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>OpenDoorPage</name>
    <message>
        <location filename="OpenDoorPage.qml" line="49"/>
        <source>Open again</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="77"/>
        <source>Confirm receipt</source>
        <translation>Complete</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="105"/>
        <source>Your locker is located at Box No </source>
        <translation>Your parcel is located at locker no. </translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="130"/>
        <source>The system will refresh in</source>
        <translation>The system will refresh in</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="182"/>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="192"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Please ensure compartment door closure upon completion</translation>
    </message>
    <message>
        <source>Please</source>
        <translation type="vanished">Please take your express in NO.</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="112"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>No.</source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <source>box take your package</source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <source>(such as a problem, please call 39043668)</source>
        <oldsource>(such as a problem, please call 88888888)</oldsource>
        <translation>(Such as a problem, Please call 021 290 22537)</translation>
    </message>
    <message>
        <source>The system will be</source>
        <translation>The door will close in </translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="vanished">seconds</translation>
    </message>
    <message>
        <source>Confirmation automatically</source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <source>After using, Please close the door, Thank you for your use of the service</source>
        <translation type="vanished">Please close the door,thanks???</translation>
    </message>
</context>
<context>
    <name>OtherInputMemoryPage</name>
    <message>
        <location filename="OtherInputMemoryPage.qml" line="66"/>
        <source>Scan or Enter AWB code</source>
        <translation>Scan/Enter Order Number</translation>
    </message>
    <message>
        <location filename="OtherInputMemoryPage.qml" line="217"/>
        <source>We detected the order number is not entered yet. Please retry and enter the correct order number.</source>
        <translation>We detected the order number is not entered yet. Please retry and enter the correct order number.</translation>
    </message>
    <message>
        <location filename="OtherInputMemoryPage.qml" line="206"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>OtherInsufficientBalancePage</name>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="56"/>
        <source>Insufficient balance</source>
        <translation>Insufficient Balance</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="64"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="89"/>
        <source>Balance</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="96"/>
        <source>Total Amount</source>
        <translation>Total Amount</translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="107"/>
        <location filename="OtherInsufficientBalancePage.qml" line="113"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="OtherInsufficientBalancePage.qml" line="139"/>
        <source>Please top up your prepaid card.</source>
        <translation>Maximum transaction amount using e-Money is 1.000.000 IDR.</translation>
    </message>
</context>
<context>
    <name>OtherPaymentConfirmationPage</name>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="71"/>
        <source>Payment confirmation</source>
        <translation>Payment confirmation</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="79"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="100"/>
        <source>Confirm</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="132"/>
        <source>Balance</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="139"/>
        <source>Total Amount</source>
        <translation>Total Amount</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="150"/>
        <location filename="OtherPaymentConfirmationPage.qml" line="156"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="182"/>
        <source>Please do not remove your pre-paid card.</source>
        <translation>Please do not remove your e-Money pre-paid card.</translation>
    </message>
    <message>
        <location filename="OtherPaymentConfirmationPage.qml" line="188"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>By clicking confirm button you agree to the Terms and Conditions.</translation>
    </message>
</context>
<context>
    <name>OtherPaymentFailedPage</name>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="58"/>
        <source>Unfortunately your payment has failed.</source>
        <translation>Unfortunately your payment has failed.</translation>
    </message>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="67"/>
        <source>Please ensure that your balance is sufficient and the total amount is not greater than 1.000.000 IDR.</source>
        <translation>Please ensure that your balance is sufficient and the total amount is not greater than 1.000.000 IDR.</translation>
    </message>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="83"/>
        <source>Cancel</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="OtherPaymentFailedPage.qml" line="104"/>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
</context>
<context>
    <name>OtherPaymentSuccessfulPage</name>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="64"/>
        <source>Order Details</source>
        <translation>Payment Successful</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="71"/>
        <source>The sms notification will be sent to you to pickup your parcel</source>
        <translation>The sms notification will be sent to you to pickup your parcel.</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="79"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="105"/>
        <source>Amount</source>
        <translation>Amount</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="112"/>
        <source>Last Balance</source>
        <translation>Last Balance</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="118"/>
        <source>Card</source>
        <translation>Card No.</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="124"/>
        <source>Data</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="130"/>
        <source>Locker</source>
        <translation>Locker</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="136"/>
        <source>Terminal ID</source>
        <translation>Terminal ID</translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="147"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="153"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="159"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="165"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="171"/>
        <location filename="OtherPaymentSuccessfulPage.qml" line="177"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="OtherPaymentSuccessfulPage.qml" line="229"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Please get your e-Money prepaid card back upon completion.</translation>
    </message>
</context>
<context>
    <name>OtherSearchFailPage</name>
    <message>
        <location filename="OtherSearchFailPage.qml" line="53"/>
        <source>Sorry we&apos;re unable to verify the order number</source>
        <translation>Sorry we are unable to verify the order number.</translation>
    </message>
    <message>
        <location filename="OtherSearchFailPage.qml" line="59"/>
        <source>Please contact Customer Service at</source>
        <translation>Please contact Customer Service at</translation>
    </message>
    <message>
        <location filename="OtherSearchFailPage.qml" line="65"/>
        <source>021 290 22537 for the assistance</source>
        <translation>021 290 22537 for assistance.</translation>
    </message>
    <message>
        <location filename="OtherSearchFailPage.qml" line="73"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>OtherSearchPaidPage</name>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="81"/>
        <source>This order number is </source>
        <translation>This order number is already paid on</translation>
    </message>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="88"/>
        <source>This  is </source>
        <translation>Please contact Customer Service at</translation>
    </message>
    <message>
        <source>already paid on .....</source>
        <translation type="vanished">Please contact Customer Service at</translation>
    </message>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="97"/>
        <source>.....</source>
        <translation>021 290 22537 for assistance.</translation>
    </message>
    <message>
        <location filename="OtherSearchPaidPage.qml" line="57"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>OtherServiceExpressInfo</name>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="94"/>
        <source>Order Details</source>
        <translation>Order Details</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="102"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="123"/>
        <source>Confirm</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="156"/>
        <source>Order Id</source>
        <translation>Order Id</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="162"/>
        <source>Online Shop</source>
        <translation>Online Shop</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="168"/>
        <source>Customer Email</source>
        <translation>Customer Email</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="174"/>
        <source>Customer Phone</source>
        <translation>Customer Phone</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="180"/>
        <source>Total Amount</source>
        <translation>Total Amount</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="372"/>
        <source>Please place your card to the reader</source>
        <translation>Please place your pre-paid card
to the e-Money reader</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="404"/>
        <location filename="OtherServiceExpressInfo.qml" line="490"/>
        <source>seconds left</source>
        <translation>seconds left</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="267"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Please prepare your pre-paid card and ensure the balance is sufficient.</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="284"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>By clicking `Confirm` button you agree to the Terms and Conditions.</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="442"/>
        <source>Transaction Unfinished</source>
        <translation>Your transaction is unfinished</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="458"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Please put back your previous prepaid card.</translation>
    </message>    
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="543"/>
        <source>Transaction Failed</source>
        <translation>Transaction Failed</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="558"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Please only use Mandiri e-Money prepaid card.</translation>
    </message>
    <message>
        <location filename="OtherServiceExpressInfo.qml" line="581"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>OtherServicePage</name>
    <message>
        <location filename="OtherServicePage.qml" line="50"/>
        <source>other service</source>
        <translation>Other Service</translation>
    </message>
    <message>
        <location filename="OtherServicePage.qml" line="58"/>
        <source>Cash On Pick Up</source>
        <translation>Payment</translation>
    </message>
    <message>
        <location filename="OtherServicePage.qml" line="85"/>
        <source>Laundry</source>
        <translation>Laundry</translation>
    </message>
    <message>
        <location filename="OtherServicePage.qml" line="116"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>OverTimePage</name>
    <message>
        <source>Your package has been more than a free pick-up time</source>
        <translation type="vanished">Your package has been more than a free pick-up time</translation>
    </message>
    <message>
        <source>If you want to take it, please pay</source>
        <translation type="vanished">If you want to take it, please pay</translation>
    </message>
    <message>
        <source>If you have any questions, please call 888888</source>
        <oldsource>If you have any questions, please call 39043668</oldsource>
        <translation type="vanished">If you have any questions, please call 39043668</translation>
    </message>
    <message>
        <source>yuan</source>
        <translation type="vanished">IDR</translation>
    </message>
    <message>
        <location filename="OverTimePage.qml" line="75"/>
        <source>Your parcel has expired and will be returned to the sender.</source>
        <translation>Your parcel has expired and will be returned to the sender.</translation>
    </message>
    <message>
        <location filename="OverTimePage.qml" line="81"/>
        <source>Please contact (021) 2902 2537 for further inquiry.</source>
        <translation>Please contact us at 021-22538719 for further inquiry.</translation>
    </message>
    <message>
        <location filename="OverTimePage.qml" line="90"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>PassWordErrorPage</name>
    <message>
        <location filename="PassWordErrorPage.qml" line="59"/>
        <source>The code is not correct, please re-input</source>
        <translation>The code is incorrect, Please re-enter</translation>
    </message>
    <message>
        <location filename="PassWordErrorPage.qml" line="68"/>
        <source>MAIN MENU</source>
        <translation>Main Menu</translation>
    </message>
    <message>
        <location filename="PassWordErrorPage.qml" line="92"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>PayMentPage</name>
    <message>
        <location filename="PayMentPage.qml" line="65"/>
        <source>Please choose the method of payment</source>
        <translation>Please choose the method of payment</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="71"/>
        <source>If you have any questions, please contact ********</source>
        <translation>If you have any questions, please contact 39043668</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="80"/>
        <source>octopus</source>
        <translation>OCTOPUS</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="105"/>
        <source>coin</source>
        <translation>COIN</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="130"/>
        <source>app</source>
        <translation>APP</translation>
    </message>
    <message>
        <location filename="PayMentPage.qml" line="156"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>PickUpThankspage</name>
    <message>
        <location filename="PickUpThankspage.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Thank You for using PopBox.</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="67"/>
        <source>A SMS notification has been sent to </source>
        <oldsource>A SMS notification has been sent to</oldsource>
        <translation>A SMS notification has been sent to</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="75"/>
        <source>the sender of this parcel.</source>
        <translation>the sender of this parcel.</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Main Menu</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="109"/>
        <source>Open Another Locker?</source>
        <translation>Open another locker?</translation>
    </message>
    <message>
        <location filename="PickUpThankspage.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>I am done</translation>
    </message>
</context>
<context>
    <name>RejectDoorOpenPage</name>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="52"/>
        <source>change-box</source>
        <translation>Change-Door</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="79"/>
        <source>Open again</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="109"/>
        <source>Confirm receipt</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="143"/>
        <source>Your locker is located at Box No </source>
        <translation>Please put your parcel into box no. </translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="151"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="168"/>
        <source>The system will refresh in</source>
        <translation>The system will refresh in</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="224"/>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="RejectDoorOpenPage.qml" line="233"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Please ensure compartment door closure upon completion</translation>
    </message>
</context>
<context>
    <name>RejectExpressHelp</name>
    <message>
        <location filename="RejectExpressHelp.qml" line="78"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Have other question,please press the
 &apos;cancel&apos; button and call us.</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="92"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="106"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="120"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="146"/>
        <source>re-open</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="175"/>
        <source>change-box</source>
        <translation>Change-Door</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="203"/>
        <source>cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="RejectExpressHelp.qml" line="231"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Please Select</translation>
    </message>
</context>
<context>
    <name>RejectExpressInfo</name>
    <message>
        <location filename="RejectExpressInfo.qml" line="252"/>
        <source>cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="273"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>    
    <message>
        <location filename="RejectExpressInfo.qml" line="118"/>
        <source>Return Details</source>
        <translation>Return Details</translation>
    </message>    
    <message>
        <source>The recipient informations </source>
        <translation>Recipient Information :</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="237"/>
        <source>Package ID:</source>
        <translation>Package ID:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="221"/>
        <source>Onlineshop:</source>
        <translation>Online Shop:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="299"/>
        <source>Address:</source>
        <translation>Address :</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="205"/>
        <source>Sender No:</source>
        <translation>Sender No:</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="189"/>
        <source>Locker Location:</source>
        <translation>Locker Location :</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="314"/>
        <source>not_mouth</source>
        <translation>Locker not available</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="327"/>
        <location filename="RejectExpressInfo.qml" line="367"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="RejectExpressInfo.qml" line="354"/>
        <source>no_payOfAmount</source>
        <translation>Payment not found</translation>
    </message>
</context>
<context>
    <name>RejectExpressThankPage</name>
    <message>
        <location filename="RejectExpressThankPage.qml" line="49"/>
        <source>Thank You for using PopBox.</source>
        <translation>Thank You for using PopBox.</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="57"/>
        <source>The Merchant has been notified of</source>
        <translation>The Merchant has been notified of</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="65"/>
        <source>the return of this parcel.</source>
        <translation>the return of this parcel.</translation>
    </message>
    <message>
        <source>MAIN MENU</source>
        <translation type="vanished">Main Menu</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="73"/>
        <source>Send Another Parcel?</source>
        <translation>Return Another?</translation>
    </message>
    <message>
        <location filename="RejectExpressThankPage.qml" line="94"/>
        <source>I&apos;m Done!</source>
        <translation>I am done!</translation>
    </message>
</context>
<context>
    <name>RejectExpressWeightPage</name>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="77"/>
        <location filename="RejectExpressWeightPage.qml" line="99"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="122"/>
        <source>Cancel and Exit</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="149"/>
        <source>Please put the package into ths platform</source>
        <translation>Please put the package into this Machine</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="158"/>
        <source>The system will calculate the postage for you</source>
        <translation>The system will calculate 
the postage for you</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="169"/>
        <source>Weight</source>
        <translation>Weight</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="179"/>
        <source>Cost</source>
        <translation>Cost</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="199"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="233"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="RejectExpressWeightPage.qml" line="242"/>
        <source>Yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>RejectInputMemoryPage</name>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="134"/>
        <source>Scan or Enter Barcode</source>
        <translation>Scan/Enter Package ID Number</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="244"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="260"/>
        <source>We detected the package id is not entered yet. Please retry and enter the correct package id.</source>
        <translation>We detected the package id is not entered yet. Please retry and enter the correct package id.</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="309"/>
        <source>Dear Customer</source>
        <translation>Dear Customer</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="324"/>
        <source>Sorry We are unable to verify the package id. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Sorry We are unable to verify the package id. Please contact our CS line at 021-22538719 for assistance.</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="382"/>
        <source>retry</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="RejectInputMemoryPage.qml" line="411"/>
        <source>main menu</source>
        <translation>Main Menu</translation>
    </message>
</context>
<context>
    <name>RejectInputPhonePage</name>
    <message>
        <location filename="RejectInputPhonePage.qml" line="49"/>
        <source>Enter Recipient Phone Number</source>
        <translation>Enter Sender Phone Number</translation>
    </message>
    <message>
        <location filename="RejectInputPhonePage.qml" line="86"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="RejectInputPhonePage.qml" line="171"/>
        <source>The memory is not input or not correct</source>
        <translation>Oops, Something is wrong. Please ensure the mobile phone number is correct</translation>
    </message>
    <message>
        <location filename="RejectInputPhonePage.qml" line="184"/>
        <source>back</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>RejectInputPhoneSurePage</name>
    <message>
        <location filename="RejectInputPhoneSurePage.qml" line="83"/>
        <source>Please Verify Phone Number</source>
        <translation>Please Verify Phone Number</translation>
    </message>
    <message>
        <location filename="RejectInputPhoneSurePage.qml" line="108"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="RejectInputPhoneSurePage.qml" line="129"/>
        <source>continue</source>
        <translation>Continue</translation>
    </message>
</context>
<context>
    <name>RejectInsertCoinPage</name>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="71"/>
        <source>Welcome to use the system</source>
        <translation>Welcome to use the system</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="88"/>
        <source>You pay the overdue amount</source>
        <translation>You pay the amount</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="98"/>
        <source>return</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="117"/>
        <source>Please put a coin in the slot</source>
        <translation>Please put a coin in the slot</translation>
    </message>
    <message>
        <location filename="RejectInsertCoinPage.qml" line="177"/>
        <source>yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>RejectNumboardFunctionButtondelete</name>
    <message>
        <location filename="RejectNumboardFunctionButtondelete.qml" line="18"/>
        <source>DEL</source>
        <translation>DELETE</translation>
    </message>
</context>
<context>
    <name>RejectNumboardFunctionButtonreturn</name>
    <message>
        <location filename="RejectNumboardFunctionButtonreturn.qml" line="18"/>
        <source>DEL</source>
        <translation>BACK</translation>
    </message>
</context>
<context>
    <name>RejectPayMentPage</name>
    <message>
        <location filename="RejectPayMentPage.qml" line="68"/>
        <source>Please choose the method of payment</source>
        <translation>Please choose the method of payment</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="74"/>
        <source>If you have any questions, please contact ********</source>
        <translation>If you have any questions, please contact ********</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="83"/>
        <source>octopus</source>
        <translation>OCTOPUS</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="107"/>
        <source>coin</source>
        <translation>COIN</translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="131"/>
        <source>app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="RejectPayMentPage.qml" line="157"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>RejectSelectBoxSizePage</name>
    <message>
        <location filename="RejectSelectBoxSizePage.qml" line="130"/>
        <source>please select service option</source>
        <translation>Please choose a locker size</translation>
    </message>
    <message>
        <location filename="RejectSelectBoxSizePage.qml" line="394"/>
        <source>return</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Mini</source>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="RejectSelectBoxSizePage.qml" line="167"/>
        <location filename="RejectSelectBoxSizePage.qml" line="212"/>
        <location filename="RejectSelectBoxSizePage.qml" line="260"/>
        <location filename="RejectSelectBoxSizePage.qml" line="307"/>
        <location filename="RejectSelectBoxSizePage.qml" line="354"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <source>Big</source>
        <translation>Big</translation>
    </message>
    <message>
        <source>Mid</source>
        <translation>Mid</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Small</translation>
    </message>
    <message>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>RejectSelectMerchantNamePage</name>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="47"/>
        <source>Available merchant :</source>
        <translation>Available merchant :</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="59"/>
        <source>*Click merchants logo for returning parcel</source>
        <translation>*Click merchants logo for returning parcel</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="189"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="210"/>
        <source>Steps to return parcel :</source>
        <translation>Steps to return parcel :</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="239"/>
        <source>Fill out Merchant Return Form.</source>
        <translation>Fill out Return Form on the Merchants website.</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="268"/>
        <source>Scan or Enter the Package ID.</source>
        <translation>Scan or Enter the Package ID.</translation>
    </message>
    <message>
        <location filename="RejectSelectMerchantNamePage.qml" line="297"/>
        <source>Select size and drop parcel to locker.</source>
        <translation>Select size and drop parcel to locker.</translation>
    </message>
</context>
<context>
    <name>SelectLanguagePage</name>
    <message>
        <location filename="SelectLanguagePage.qml" line="27"/>
        <source>Please select main language</source>
        <translation>Pilih Opsi Bahasa</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="33"/>
        <source>Please select second language</source>
        <translation>Please Select your Language</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="41"/>
        <source>main language</source>
        <translation>Bahasa</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="76"/>
        <source>second language</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="114"/>
        <source>Available Lockers: </source>
        <translation>Loker Tersedia:</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="124"/>
        <source>XS :</source>
        <translation>XS:</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="143"/>
        <source>S :</source>
        <translation>S :</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="132"/>
        <location filename="SelectLanguagePage.qml" line="151"/>
        <location filename="SelectLanguagePage.qml" line="170"/>
        <location filename="SelectLanguagePage.qml" line="189"/>
        <location filename="SelectLanguagePage.qml" line="208"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="162"/>
        <source>M :</source>
        <translation>M :</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="181"/>
        <source>L :</source>
        <translation>L :</translation>
    </message>
    <message>
        <location filename="SelectLanguagePage.qml" line="200"/>
        <source>XL:</source>
        <translation>XL:</translation>
    </message>
</context>
<context>
    <name>SelectProductPage</name>
    <message>
        <location filename="SelectProductPage.qml" line="81"/>
        <source>Please choose the product</source>
        <translation>Please choose the product</translation>
    </message>
    <message>
        <location filename="SelectProductPage.qml" line="105"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>SelectServicePageB</name>
    <message>
        <location filename="SelectServicePageB.qml" line="65"/>
        <source>please select service option</source>
        <translation>Pilih Opsi Layanan</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="150"/>
        <source>return</source>
        <translation>KEMBALI</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="328"/>
        <source>Available Lockers: </source>
        <translation>Loker Tersedia: </translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="337"/>
        <source>XS :</source>
        <translation>XS:</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="356"/>
        <source>S :</source>
        <translation>S :</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="345"/>
        <location filename="SelectServicePageB.qml" line="364"/>
        <location filename="SelectServicePageB.qml" line="383"/>
        <location filename="SelectServicePageB.qml" line="402"/>
        <location filename="SelectServicePageB.qml" line="421"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="375"/>
        <source>M:</source>
        <translation>M :</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="394"/>
        <source>L:</source>
        <translation>L :</translation>
    </message>
    <message>
        <location filename="SelectServicePageB.qml" line="413"/>
        <source>XL:</source>
        <translation>XL:</translation>
    </message>
</context>
<context>
    <name>SelectServicePageE</name>
    <message>
        <location filename="SelectServicePageE.qml" line="65"/>
        <source>please select service option</source>
        <translation>Select Service Option</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="150"/>
        <source>return</source>
        <translation>BACK</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="329"/>
        <source>Available Lockers: </source>
        <translation>Available Lockers: </translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="338"/>
        <source>XS :</source>
        <translation>XS:</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="358"/>
        <source>S :</source>
        <translation>S :</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="346"/>
        <location filename="SelectServicePageE.qml" line="366"/>
        <location filename="SelectServicePageE.qml" line="385"/>
        <location filename="SelectServicePageE.qml" line="404"/>
        <location filename="SelectServicePageE.qml" line="423"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="377"/>
        <source>M:</source>
        <translation>M :</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="396"/>
        <source>L:</source>
        <translation>L :</translation>
    </message>
    <message>
        <location filename="SelectServicePageE.qml" line="415"/>
        <source>XL:</source>
        <translation>XL:</translation>
    </message>
</context>
<context>
    <name>SendDoorOpenPage</name>    
    <message>
        <location filename="SendDoorOpenPage.qml" line="99"/>
        <source>Open again</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="129"/>
        <source>Confirm receipt</source>
        <translation>Complete</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="159"/>
        <source>Your locker is located at Box No </source>
        <translation>Your parcel is located at locker no. </translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="166"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="64"/>
        <source>change-box</source>
        <translation>Change-Door</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="182"/>
        <source>The system will refresh in</source>
        <translation>The system will refresh in</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="233"/>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="243"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Please ensure compartment door closure upon completion</translation>
    </message>
    <message>
        <location filename="SendDoorOpenPage.qml" line="285"/>
        <source>Please ensure you have closed the empty previous door before continuing</source>
        <translation>Please ensure you have closed the empty previous door before continuing</translation>
    </message>   
    <message>
        <location filename="SendDoorOpenPage.qml" line="299"/>
        <source>OK</source>
        <translation>OK</translation>
    </message> 
</context>
<context>
    <name>SendExpressHelp</name>
    <message>
        <location filename="SendExpressHelp.qml" line="78"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Have other question,please press the
 &apos;cancel&apos; button and call us.</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="92"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation>Press this button to change locker size.</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="106"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation>Press this button if the door does not open.</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="120"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="146"/>
        <source>re-open</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="175"/>
        <source>change-box</source>
        <translation>Change-Door</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="203"/>
        <source>cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="SendExpressHelp.qml" line="231"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Please Select</translation>
    </message>
</context>
<context>
    <name>SendExpressInfo</name>
    <message>
        <location filename="SendExpressInfo.qml" line="174"/>
        <source>Back</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="195"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="120"/>
        <source>Name:</source>
        <translation>Name :</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="110"/>
        <source>From:</source>
        <translation>Locker :</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="134"/>
        <source>Address:</source>
        <translation>Address :</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="97"/>
        <source>PopSend Details</source>
        <translation>PopSend Details</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="237"/>
        <source>not_mouth</source>
        <translation>Locker not available</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="250"/>
        <location filename="SendExpressInfo.qml" line="292"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SendExpressInfo.qml" line="279"/>
        <source>no_payOfAmount</source>
        <translation>Payment not found</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="376"/>
        <source>We are sorry</source>
        <translation>We are sorry</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="394"/>
        <source>This order cannot be processed, Please ensure you have choose the right locker.</source>
        <translation>This order cannot be processed, Please ensure you have choose the right locker.</translation>
    </message>
</context>
<context>
    <name>SendExpressPage</name>
    <message>
        <source>2.5</source>
        <translation type="vanished">2.5</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="vanished">10</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="77"/>
        <location filename="SendExpressPage.qml" line="99"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="122"/>
        <source>Cancel and Exit</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="149"/>
        <source>Please put the package into ths platform</source>
        <translation>Please put the package into this Machine</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="158"/>
        <source>The system will calculate the postage for you</source>
        <translation>The system will calculate 
the postage for you</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="169"/>
        <source>Weight</source>
        <translation>Weight</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="179"/>
        <source>Cost</source>
        <translation>Cost</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="199"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="231"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="SendExpressPage.qml" line="240"/>
        <source>Yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>SendExpressWeightPage</name>
    <message>
        <location filename="SendExpressWeightPage.qml" line="77"/>
        <location filename="SendExpressWeightPage.qml" line="99"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="122"/>
        <source>Cancel and Exit</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="149"/>
        <source>Please put the package into ths platform</source>
        <translation>Please put the package into this Machine</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="158"/>
        <source>The system will calculate the postage for you</source>
        <translation>The system will calculate 
the postage for you</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="169"/>
        <source>Weight</source>
        <translation>Weight</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="179"/>
        <source>Cost</source>
        <translation>Cost</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="199"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="233"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="SendExpressWeightPage.qml" line="242"/>
        <source>Yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>SendInputMemoryPage</name>
    <message>
        <location filename="SendInputMemoryPage.qml" line="87"/>
        <source>Scan Barcode or Enter Parcel Code</source>
        <oldsource>Please scan or enter a tracking number</oldsource>
        <translation>Scan/Enter Parcel Number</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="241"/>
        <source>We detected the parcel number is not entered yet. Please retry and enter the correct parcel number.</source>
        <translation>We detected the parcel number is not entered yet. Please retry and enter the correct parcel number.</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="215"/>
        <location filename="SendInputMemoryPage.qml" line="270"/>
        <location filename="SendInputMemoryPage.qml" line="325"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="305"/>
        <source>We detected the parcel number is not correct. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>We detected the parcel number is not correct. Please contact our CS line at 021-22538719 for assistance.</translation>
    </message>
    <message>
        <location filename="SendInputMemoryPage.qml" line="355"/>
        <source>Dear Customer</source>
        <translation>Dear Customer</translation>
    </message>   
    <message>
        <location filename="SendInputMemoryPage.qml" line="355"/>
        <source>Sorry We are unable to verify the parcel number. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Sorry We are unable to verify the parcel number. Please contact our CS line at 021-22538719 for assistance.</translation>
    </message>  
</context>
<context>
    <name>SendInserCoinPage</name>
    <message>
        <location filename="SendInserCoinPage.qml" line="71"/>
        <source>Welcome to use the system</source>
        <translation>Welcome to use the system</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="88"/>
        <source>You pay the overdue amount</source>
        <translation>You pay the amount</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="99"/>
        <source>return</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="119"/>
        <source>Please put a coin in the slot</source>
        <translation>Please insert payment into coin slot</translation>
    </message>
    <message>
        <location filename="SendInserCoinPage.qml" line="180"/>
        <source>yuan</source>
        <translation>IDR</translation>
    </message>
</context>
<context>
    <name>SendPayMentPage</name>
    <message>
        <location filename="SendPayMentPage.qml" line="68"/>
        <source>Please choose the method of payment</source>
        <translation>Please choose a payment method</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="74"/>
        <source>If you have any questions, please contact ********</source>
        <translation>For any problems, please contact 021 290 22537</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="83"/>
        <source>octopus</source>
        <translation>OCTOPUS</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="108"/>
        <source>coin</source>
        <translation>COIN</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="133"/>
        <source>app</source>
        <translation>Mobile APP</translation>
    </message>
    <message>
        <location filename="SendPayMentPage.qml" line="160"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>SendSelectBoxSizePage</name>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="120"/>
        <source>please select service option</source>
        <translation>Please choose a locker size</translation>
    </message>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="354"/>
        <source>not_mouth</source>
        <translation>Locker is full</translation>
    </message>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="367"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SendSelectBoxSizePage.qml" line="348"/>
        <source>back to menu</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>TakeRejectExpress</name>
    <message>
        <location filename="TakeRejectExpress.qml" line="105"/>
        <location filename="TakeRejectExpress.qml" line="524"/>
        <location filename="TakeRejectExpress.qml" line="572"/>
        <location filename="TakeRejectExpress.qml" line="624"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="127"/>
        <source>take back it</source>
        <translation>Retrieve</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="157"/>
        <source>take back all</source>
        <translation>Retrieve-All</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="203"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="217"/>
        <source>store-time</source>
        <translation>Store-Time</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="231"/>
        <source>COMPANY</source>
        <translation>Company</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="245"/>
        <source>box</source>
        <translation>Locker</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="494"/>
        <source>waiting</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="511"/>
        <source>set_null</source>
        <translation>No Parcel</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="559"/>
        <source>Successful</source>
        <translation>Successful</translation>
    </message>
    <message>
        <location filename="TakeRejectExpress.qml" line="611"/>
        <source>Failed</source>
        <translation>Failed, Please contact us for assistance</translation>
    </message>
</context>
<context>
    <name>TakeSendExpress</name>
    <message>
        <location filename="TakeSendExpress.qml" line="103"/>
        <location filename="TakeSendExpress.qml" line="530"/>
        <location filename="TakeSendExpress.qml" line="579"/>
        <location filename="TakeSendExpress.qml" line="631"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="125"/>
        <source>take back it</source>
        <translation>Retrieve</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="155"/>
        <source>take back all</source>
        <translation>Retrieve-All</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="vanished">Mobile No.</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="215"/>
        <source>store-time</source>
        <translation>Store-Time</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="229"/>
        <source>box</source>
        <translation>Locker</translation>
    </message>
    <message>
        <source>send-time</source>
        <oldsource>overdue-time</oldsource>
        <translation type="vanished">Store-time</translation>
    </message>
    <message>
        <source>other</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="201"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="500"/>
        <source>waiting</source>
        <translation>Please wait a moment...</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="517"/>
        <source>set_null</source>
        <translation>No Parcel</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="566"/>
        <source>Successful</source>
        <translation>Successful</translation>
    </message>
    <message>
        <location filename="TakeSendExpress.qml" line="618"/>
        <source>Failed</source>
        <translation>Failed, Please contact us for assistance</translation>
    </message>
</context>
<context>
    <name>UserQuitPage</name>
    <message>
        <location filename="UserQuitPage.qml" line="55"/>
        <source>please choice</source>
        <translation>Please Select</translation>
    </message>
    <message>
        <location filename="UserQuitPage.qml" line="169"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="UserQuitPage.qml" line="191"/>
        <source>ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>UserSelectExpressPage</name>
    <message>
        <location filename="UserSelectExpressPage.qml" line="67"/>
        <source>Please choose the courier company</source>
        <translation>Please choose the courier company</translation>
    </message>
    <message>
        <location filename="UserSelectExpressPage.qml" line="91"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="UserSelectExpressPage.qml" line="115"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>UserSelectPage</name>
    <message>
        <location filename="UserSelectPage.qml" line="81"/>
        <source>Have other question,please press the &apos;cancel&apos; button and call us.</source>
        <translation>Have other question,please press the
 &apos;cancel&apos; button and call us.</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="95"/>
        <source>Package size is too large, please press the &apos;change-box&apos; button.</source>
        <translation>Press this button to 
 change locker size.</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="109"/>
        <source>If the door is not open, please press the &apos;re-open&apos; button.</source>
        <translation>Press this button if the door
 does not open.</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="123"/>
        <source>back to menu</source>
        <translation>Back</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="154"/>
        <source>re-open</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="183"/>
        <source>change-box</source>
        <translation>Change-Door</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="211"/>
        <source>cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="UserSelectPage.qml" line="239"/>
        <source>You select the button, if you have the problem:</source>
        <translation>Please Select</translation>
    </message>
</context>
<context>
    <name>WebPage</name>
    <message>
        <location filename="WebPage.qml" line="108"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>OtherServiceEmoney</name>
    <message>
        <location filename="OtherServiceEmoney.qml" line="50"/>
        <source>How To Do Payment</source>
        <translation>How To Pay with e-Money</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="89"/>
        <source>Scan or enter your order number.</source>
        <translation>Scan or enter your order number.</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="103"/>
        <source>Put your pre-paid card into reader and confirm payment.</source>
        <translation>Put your pre-paid card into e-Money reader and confirm payment.</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="116"/>
        <source>Do not forget to take back your pre-paid card.</source>
        <translation>Please get your e-Money card back upon completion.</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="130"/>
        <source>Go To Payment</source>
        <translation>Go to Payment</translation>
    </message>
    <message>
        <location filename="OtherServiceEmoney.qml" line="171"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>QRshopinfo</name>
    <message>
        <location filename="QRshopinfo.qml" line="51"/>
        <source>How To Shop with QR</source>
        <translation>How To Shop with QR</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="90"/>
        <source>Download UANGKU App for IOS or ANDROID.</source>
        <translation>Scan the QR code above and install UANGKU App.</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="104"/>
        <source>Register and top up your balance.</source>
        <translation>Register and top up your balance.</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="117"/>
        <source>Scan and shop the product you like.</source>
        <translation>Scan and shop the product you like.</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="131"/>
        <source>Touch Everywhere to Start</source>
        <translation>Touch Everywhere to Start Shopping</translation>
    </message>
    <message>
        <location filename="QRshopinfo.qml" line="172"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>MMreturninfo</name>
    <message>
        <location filename="MMreturninfo.qml" line="51"/>
        <source>How To Return Parcel</source>
        <translation>How To Return Parcel</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="90"/>
        <source>Fill out Return Form on Mataharimall.com</source>
        <translation>Fill out Return Form on Mataharimall.com</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="106"/>
        <source>Go to locker, Select Return Parcel and Scan the AWB Number.</source>
        <translation>Go to locker, Select Return Parcel and Scan the order number.</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="121"/>
        <source>Drop off the returned parcel to opened door.</source>
        <translation>Drop off the returned parcel to opened door.</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="137"/>
        <source>Return My Parcel</source>
        <translation>Return My Parcel</translation>
    </message>
    <message>
        <location filename="MMreturninfo.qml" line="177"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>EmoneyCheckBalance</name>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="69"/>
        <source>Balance Information</source>
        <translation>Balance Information</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="77"/>
        <source>Cancel</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="99"/>
        <source>Next</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="203"/>
        <source>Please place your pre-paid card into the reader now before continuing.</source>
        <translation>Please place your pre-paid card into the reader before continuing.</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="223"/>
        <source>Your prepaid balance is 0</source>
        <translation>Your e-Money prepaid balance is 0</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="238"/>
        <source>Please top up your prepaid card.</source>
        <translation>Please top up your e-Money card.</translation>
    </message>    
    <message>
        <location filename="EmoneyCheckBalance.qml" line="290"/>
        <source>Balance Check Failed</source>
        <translation>Balance Check Failed</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="305"/>
        <source>Please use e-Money Mandiri which placed properly into the reader.</source>
        <translation>Please use e-Money Mandiri which placed properly into the reader.</translation>
    </message>
    <message>
        <location filename="EmoneyCheckBalance.qml" line="329"/>
        <location filename="EmoneyCheckBalance.qml" line="261"/>
        <source>back</source>
        <translation>Back</translation>
    </message>    
</context>
<context>
    <name>BalanceWindow</name>
    <message>
        <location filename="BalanceWindow.qml" line="33"/>
        <source>Your pre-paid balance is </source>
        <translation>Your e-Money card balance is </translation>
    </message>    
</context>
<context>
    <name>EmoneyBalanceInfo</name>
    <message>
        <location filename="EmoneyBalanceInfo.qml" line="65"/>
        <source>Balance Information</source>
        <translation>Balance Information</translation>
    </message>
    <message>
        <location filename="EmoneyBalanceInfo.qml" line="73"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="EmoneyBalanceInfo.qml" line="111"/>
        <source>Your pre-paid balance is </source>
        <translation>Your pre-paid card balance :</translation>
    </message>     
</context>
<context>
    <name>EmoneyServicePage</name>
    <message>
        <location filename="EmoneyServicePage.qml" line="53"/>
        <source>Please select option</source>
        <translation>Select Service Option</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="64"/>
        <source>Pay Order</source>
        <translation>Pay Order</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="95"/>
        <source>Check Balance</source>
        <translation>Check Balance</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="127"/>
        <source>Phone Credit</source>
        <translation>Phone Credit</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="190"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="EmoneyServicePage.qml" line="163"/>
        <source>Powered by :</source>
        <translation>Powered by :</translation>
    </message>          
</context>
<context>
    <name>LaundryInputMemoryPage</name>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="88"/>
        <source>Scan Barcode or Enter Parcel Code</source>
        <oldsource>Please scan or enter a tracking number</oldsource>
        <translation>Scan/Enter Laundry Number</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="235"/>
        <source>We detected the laundry number is not entered yet. Please retry and enter the correct laundry number.</source>
        <translation>We detected the laundry number is not entered yet. Please retry and enter the correct laundry number.</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="241"/>
        <location filename="LaundryInputMemoryPage.qml" line="296"/>
        <location filename="LaundryInputMemoryPage.qml" line="351"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="299"/>
        <source>We detected the laundry number is not correct. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>We detected the laundry number is not correct. Please contact our CS line at 021-22538719 for assistance.</translation>
    </message>
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="349"/>
        <source>Dear Customer</source>
        <translation>Dear Customer</translation>
    </message>  
    <message>
        <location filename="LaundryInputMemoryPage.qml" line="363"/>
        <source>Sorry We are unable to verify the laundry number. Please contact our CS line at 021-29022537/38 for assistance.</source>
        <translation>Sorry We are unable to verify the laundry number. Please contact our CS line at 021-22538719 for assistance.</translation>
    </message>   
</context>
<context>
    <name>LaundryInfo</name>
    <message>
        <location filename="LaundryInfo.qml" line="207"/>
        <source>Supported by :</source>
        <translation>Supported by :</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="163"/>
        <source>Order your laundry from merchant web/apps, Ensure the laundry confirmation</source>
        <translation>Order your laundry from merchant web/apps, Ensure the laundry confirmation</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="179"/>
        <source>Go to Locker, Scan/Enter the laundry number and choose suitable door size (M or L).</source>
        <translation>Go to Locker, Scan/Enter the laundry number and choose suitable door size (M or L).</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="194"/>
        <source>Drop off your laundry in opened door, Then Courier will pick it up soon.</source>
        <translation>Drop off your laundry in opened door, Then Courier will pick it up soon.</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="76"/>
        <source>Drop My Laundry</source>
        <translation>Drop My Laundry</translation>
    </message>
    <message>
        <location filename="LaundryInfo.qml" line="52"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
</context>
<context>
    <name>LaundryExpressInfo</name>
    <message>
        <location filename="LaundryExpressInfo.qml" line="274"/>
        <source>Back</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="295"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="142"/>
        <source>Provider :</source>
        <translation>Provider :</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="195"/>
        <source>Customer :</source>
        <translation>Customer :</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="222"/>
        <source>Address :</source>
        <translation>Address :</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="115"/>
        <source>Laundry Details</source>
        <translation>Laundry Details</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="337"/>
        <source>not_mouth</source>
        <translation>Locker not available</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="350"/>
        <location filename="LaundryExpressInfo.qml" line="392"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="379"/>
        <source>no_payOfAmount</source>
        <translation>Payment not found</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="169"/>
        <source>Order No.:</source>
        <translation>Order No.:</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="467"/>
        <source>We are sorry</source>
        <translation>We are sorry</translation>
    </message>
    <message>
        <location filename="LaundryExpressInfo.qml" line="486"/>
        <source>This order cannot be processed, Please ensure you have choose the right locker.</source>
        <translation>This order cannot be processed, Please ensure you have choose the right locker.</translation>
    </message>
</context>
<context>
    <name>LaundryThanksPage</name>
    <message>
        <location filename="LaundryThanksPage.qml" line="59"/>
        <source>Thank You for using PopBox.</source>
        <translation>Thank You for using PopBox.</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="67"/>
        <source>A SMS notification has been sent to</source>
        <translation>A notification has been sent to</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="75"/>
        <source>the sender of this parcel.</source>
        <translation>the Laundry Service Provider.</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="85"/>
        <source>MAIN MENU</source>
        <translation>Main Menu</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="109"/>
        <source>Send Another Parcel?</source>
        <translation>Put another laundry?</translation>
    </message>
    <message>
        <location filename="LaundryThanksPage.qml" line="132"/>
        <source>I&apos;m Done!</source>
        <translation>I am done</translation>
    </message>
</context>
<context>
    <name>LaundryDoorOpenPage</name>    
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="100"/>
        <source>Open again</source>
        <translation>Re-Open</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="130"/>
        <source>Confirm receipt</source>
        <translation>Complete</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="252"/>
        <source>Your locker is located at Box No </source>
        <translation>Please put your laundry into box no. </translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="118"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="65"/>
        <source>change-box</source>
        <translation>Change-Door</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="164"/>
        <source>The system will refresh in</source>
        <translation>The system will refresh in</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="219"/>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="230"/>
        <source>Please ensure compartment door closure after deposit</source>
        <translation>Please ensure compartment door closure upon completion</translation>
    </message>
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="308"/>
        <source>Please ensure you have closed the empty previous door before continuing</source>
        <translation>Please ensure you have closed the empty previous door before continuing.</translation>
    </message>    
    <message>
        <location filename="LaundryDoorOpenPage.qml" line="322"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>LaundrySelectBoxSizePage</name>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="120"/>
        <source>please select service option</source>
        <translation>Please choose a locker size</translation>
    </message>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="354"/>
        <source>not_mouth</source>
        <translation>Locker is full</translation>
    </message>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="367"/>
        <source>back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="LaundrySelectBoxSizePage.qml" line="348"/>
        <source>back to menu</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>SepulsaInputPhonePage</name>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="82"/>
        <source>Please enter your phone number</source>
        <translation>Please enter your phone number</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="118"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="218"/>
        <source>Please enter the correct number</source>
        <translation>Please enter the correct number</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhonePage.qml" line="231"/>
        <source>back</source>
        <translation>back</translation>
    </message>
</context>
<context>
    <name>SepulsaInputPhoneSurePage</name>
    <message>
        <location filename="SepulsaInputPhoneSurePage.qml" line="101"/>
        <source>Kindly verify your phone number</source>
        <translation>Kindly verify your phone number</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhoneSurePage.qml" line="140"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SepulsaInputPhoneSurePage.qml" line="162"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>SepulsaPaymentSuccessfulPage</name>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="79"/>
        <source>Order Details</source>
        <translation>Payment Successful</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="87"/>
        <source>The sms notification will be sent to you for this credit purchase</source>
        <translation>The sms notification will be sent to you for this credit purchase</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="95"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="123"/>
        <source>Card No.</source>
        <translation>Card no.</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="129"/>
        <source>Last Balance</source>
        <translation>Last Balance</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="135"/>
        <source>Product Item</source>
        <translation>Product Item</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="141"/>
        <source>Amount</source>
        <translation>Amount</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="147"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="153"/>
        <source>Locker Name</source>
        <translation>Locker Name</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="159"/>
        <source>Terminal ID</source>
        <translation>Terminal ID</translation>
    </message>  
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="266"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Please do not forget to take your e-Money card</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="433"/>
        <source>Thank you for purchasing this product with PopBox Locker. For assistance, Please contact us at 021-29022537/38.</source>
        <translation>Thank you for purchasing this product with PopBox Locker. For assistance, Please contact us at 021-29022537.</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="515"/>
        <source>For further assistance, Please contact us at 021-29022537/38.</source>
        <translation>For further assistance, Please contact us at 021-29022537.</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="480"/>
        <source>Please keep the transaction number </source>
        <translation>Please keep the transaction number </translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="480"/>
        <source> for tracking purpose.</source>
        <translation> for tracking purpose.</translation>
    </message>
    <message>
        <location filename="SepulsaPaymentSuccessfulPage.qml" line="497"/>
        <source>Please wait a moment for the transaction final status...</source>
        <translation>Please wait a moment for the transaction final status...</translation>
    </message>
</context>
<context>
    <name>SepulsaSelectDenomPage</name>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="99"/>
        <source>Please Choose Denom :</source>
        <translation>Please Choose Product :</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="125"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="478"/>
        <source>Currently we are not connected to the system, Please retry later</source>
        <translation>Currently We are not connected to the system, Please retry later</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="491"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="SepulsaSelectDenomPage.qml" line="438"/>
        <source>Terms and Conditions Applied for Promo Products.</source>
        <translation>*Terms and Conditions Applied for Promo Products.</translation>
    </message>
</context>
<context>
    <name>SepulsaExpressInfo</name>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="94"/>
        <source>Order Details</source>
        <translation>Order Details</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="102"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="124"/>
        <source>Confirm</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="158"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="164"/>
        <source>Phone Number</source>
        <translation>Phone Number</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="170"/>
        <source>Product Type</source>
        <translation>Product Type</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="176"/>
        <source>Product Name</source>
        <translation>Product Name</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="182"/>
        <source>Amount</source>
        <translation>Amount</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="271"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Please prepare your pre-paid card and ensure the balance is sufficient.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="286"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>By clicking confirm button You have agreed to the Terms and Conditions.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="370"/>
        <source>Please place your card to the reader</source>
        <translation>Please place your card to the reader</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="440"/>
        <source>Transaction Unfinished</source>
        <translation>Transaction Unfinished</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="456"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Please put back your previous prepaid card.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="488"/>
        <source>seconds left</source>
        <translation>Second left</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="541"/>
        <source>Transaction Failed</source>
        <translation>Transaction Failed</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="556"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Please only use Mandiri e-Money prepaid card.</translation>
    </message>
    <message>
        <location filename="SepulsaExpressInfo.qml" line="579"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>PopsendDenomTopup</name>
    <message>
        <location filename="PopsendDenomTopup.qml" line="98"/>
        <source>Please Choose Denom :</source>
        <translation>Please Choose Denom :</translation>
    </message>
    <message>
        <location filename="PopsendDenomTopup.qml" line="60"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="PopsendDenomTopup.qml" line="208"/>
        <source>Powered by :</source>
        <translation>Powered by :</translation>
    </message>
    <message>
        <location filename="PopsendDenomTopup.qml" line="303"/>
        <source>Currently we are not connected to the system, Please retry later</source>
        <translation>Oops, Currently we are not connected to the system, Please retry later.</translation>
    </message>   
</context>
<context>
    <name>PopsendExpressInfo</name>
    <message>
        <location filename="PopsendExpressInfo.qml" line="147"/>
        <source>Order Details</source>
        <translation>Order Details</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="296"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="317"/>
        <source>Confirm</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="160"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="166"/>
        <source>Phone Number</source>
        <translation>Phone Number</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="172"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="178"/>
        <source>TopUp Denom</source>
        <translation>Top Up Denom</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="184"/>
        <source>Amount</source>
        <translation>Amount</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="271"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Please prepare your pre-paid card and ensure the balance is sufficient.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="286"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>By clicking confirm button you agree to the Terms and Conditions.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="355"/>
        <source>Please place your card to the reader</source>
        <translation>Please place your card to the reader</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="430"/>
        <source>Transaction Unfinished</source>
        <translation>Transaction Unfinished</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="446"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Please put back your previous prepaid card.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="479"/>
        <location filename="PopsendExpressInfo.qml" line="388"/>
        <source>seconds left</source>
        <translation>seconds left</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="537"/>
        <source>Transaction Failed</source>
        <translation>Transaction Failed</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="552"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Please use Mandiri e-Money prepaid card.</translation>
    </message>
    <message>
        <location filename="PopsendExpressInfo.qml" line="575"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>PopsendTopupSuccessPage</name>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="182"/>
        <source>Order Details</source>
        <translation>Payment Successful</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="190"/>
        <source>The sms notification will be sent to you for this credit purchase</source>
        <translation>The sms notification will be sent to you for this credit purchase</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="198"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="226"/>
        <source>Card No.</source>
        <translation>Card No.</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="232"/>
        <source>Last Balance</source>
        <translation>Last Balance</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="238"/>
        <source>Product Item</source>
        <translation>Product Item</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="244"/>
        <source>Amount</source>
        <translation>Amount</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="250"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="262"/>
        <source>Locker Name</source>
        <translation>Locker Name</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="256"/>
        <source>Terminal ID</source>
        <translation>Terminal ID</translation>
    </message>  
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="369"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Please do not forget to take your e-Money card</translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="400"/>
        <source>Your current PopSend balance is </source>
        <translation>Your current PopSend balance is </translation>
    </message>
    <message>
        <location filename="PopsendTopupSuccessPage.qml" line="416"/>
        <source>Thank you for purchasing Your PopSend balance with PopBox Locker.</source>
        <translation>Thank you for purchasing Your PopSend balance with PopBox Locker.</translation>
    </message>
</context>
<context>
    <name>LockerCardView</name>
    <message>
        <location filename="LockerCardView.qml" line="60"/>
        <source>Select This Locker</source>
        <translation>Select This Locker</translation>
    </message>
</context>
<context>
    <name>CustomerSelectLocker</name>
    <message>
        <location filename="CustomerSelectLocker.qml" line="380"/>
        <source>Jakarta Barat</source>
        <translation>Jakarta Barat</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="403"/>
        <source>Jakarta Pusat</source>
        <translation>Central Jakarta</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="426"/>
        <source>Jakarta Selatan</source>
        <translation>South Jakarta</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="449"/>
        <source>Jakarta Timur</source>
        <translation>East Jakarta</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="472"/>
        <source>Jakarta Utara</source>
        <translation>North Jakarta</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="535"/>
        <source>Please press Locker City on The Left Side Panel</source>
        <translation>Please press Locker City on The Left Side Panel</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="579"/>
        <source>Currently we are not connected to the system, Please retry later</source>
        <translation>Oops, Currently we are not connected to the system, Please retry later</translation>
    </message>
    <message>
        <location filename="CustomerSelectLocker.qml" line="593"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>GrocerySelectQuantity</name>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="176"/>
        <source>Please Select Quantity</source>
        <translation>Please Select Quantity</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="260"/>
        <source>Price/unit :</source>
        <translation>Price/Unit :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="357"/>
        <source>Quantity :</source>
        <translation>Quantity :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="293"/>
        <source>Initial Price/unit :</source>
        <translation>Initial Price/unit :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="326"/>
        <source>Saving/unit :</source>
        <translation>Saving/unit :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="422"/>
        <source>Total Purchase :</source>
        <translation>Total Purchase :</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="369"/>
        <source>Buy</source>
        <translation>Buy</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="73"/>
        <source>Oops, Total purchase cannot be greater than 1.000.000 IDR at this time.</source>
        <translation>Oops, Total purchase cannot be greater than 1.000.000 IDR at this time.</translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="77"/>
        <source>Oops, At this time the maximum quantity cannot be greater than </source>
        <translation>Oops, At this time the maximum quantity only can be </translation>
    </message>
    <message>
        <location filename="GrocerySelectQuantity.qml" line="77"/>
        <source> units.</source>
        <translation> units.</translation>
    </message>
</context>
<context>
    <name>CustomerInputDetailsPage</name>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="126"/>
        <source>Next</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="405"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="414"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="423"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="579"/>
        <source>Oops, Something is wrong. Please ensure all the data is filled out correctly.</source>
        <translation>Oops, Something is wrong. Please ensure all the data is filled out correctly.</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="178"/>
        <source>Oops, Something is wrong. Please ensure the email address is correct.</source>
        <translation>Oops, Something is wrong. Please ensure the email address is correct.</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="190"/>
        <source>Oops, Something is wrong. Please ensure the phone number is correct.</source>
        <translation>Oops, Something is wrong. Please ensure the phone number is correct.</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="664"/>
        <source>Delivery Confirmation</source>
        <translation>Delivery Confirmation</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="689"/>
        <source>We will deliver the purchased products to this locker :</source>
        <translation>We will deliver the purchased products to this locker :</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="703"/>
        <source>NO, CHOOSE OTHER</source>
        <translation>NO, CHOOSE OTHER</translation>
    </message>
    <message>
        <location filename="CustomerInputDetailsPage.qml" line="725"/>
        <source>YES, I AGREE</source>
        <translation>YES, I AGREE</translation>
    </message>
</context>
<context>
    <name>GroceryExpressInfo</name>
    <message>
        <location filename="GroceryExpressInfo.qml" line="164"/>
        <source>Order Details</source>
        <translation>Order Details</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="399"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="420"/>
        <source>Confirm</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="178"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="185"/>
        <source>Phone Number</source>
        <translation>Phone Number</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="190"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="199"/>
        <source>Product Name</source>
        <translation>Product Name</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="301"/>
        <source>Total Amount</source>
        <translation>Total Amount</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="287"/>
        <source>Quantity</source>
        <translation>Quantity</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="294"/>
        <source>Delivery Address</source>
        <translation>Delivery Address</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="374"/>
        <source>Please prepare your pre-paid card and ensure the balance is sufficient.</source>
        <translation>Please prepare your pre-paid card and ensure the balance is sufficient.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="389"/>
        <source>By clicking confirm button you agree to the Terms and Conditions.</source>
        <translation>By clicking confirm button you agree to the Terms and Conditions.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="458"/>
        <source>Please place your card to the reader</source>
        <translation>Please place your card to the reader</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="533"/>
        <source>Transaction Unfinished</source>
        <translation>Transaction Unfinished</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="549"/>
        <source>Please put back your previous prepaid card.</source>
        <translation>Please put back your previous prepaid card.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="491"/>
        <location filename="GroceryExpressInfo.qml" line="582"/>
        <source>seconds left</source>
        <translation>seconds left</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="640"/>
        <source>Transaction Failed</source>
        <translation>Transaction Failed</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="655"/>
        <source>Please use Mandiri e-Money prepaid card.</source>
        <translation>Please use Mandiri e-Money prepaid card.</translation>
    </message>
    <message>
        <location filename="GroceryExpressInfo.qml" line="678"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>GroceryItemView</name>
    <message>
        <location filename="GroceryItemView.qml" line="169"/>
        <source>Touch to Get This</source>
        <translation>Touch to Get This</translation>
    </message>
</context>
<context>
    <name>GroceryPaymentSuccessPage</name>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="212"/>
        <source>Order Details</source>
        <translation>Payment Successful</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="220"/>
        <source>The sms notification will be sent to you for this credit purchase</source>
        <translation>The sms notification will be sent to you for this product purchase.</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="228"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="259"/>
        <source>Card No.</source>
        <translation>Card No.</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="266"/>
        <source>Last Balance</source>
        <translation>Last Balance</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="273"/>
        <source>Product Item</source>
        <translation>Product Item</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="280"/>
        <source>Amount</source>
        <translation>Amount</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="287"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="301"/>
        <source>Locker Name</source>
        <translation>Locker Name</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="294"/>
        <source>Terminal ID</source>
        <translation>Terminal ID</translation>
    </message>  
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="426"/>
        <source>Please do not forget to take your prepaid card</source>
        <translation>Please do not forget to take your e-Money card.</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="457"/>
        <source>Your order : </source>
        <translation>Your order : </translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="457"/>
        <source>, will be processed within 1 x 24 hours. Kindly check your email.</source>
        <translation>, will be processed within 1 x 24 hours. Kindly check your email.</translation>
    </message>
    <message>
        <location filename="GroceryPaymentSuccessPage.qml" line="416"/>
        <source>Thank you for shopping with PopBox Locker Shop. For assistance please call 021-29022537/38</source>
        <translation>Thank you for shopping with PopBox Locker Shop. For assistance please call 021-22538719.</translation>
    </message>
</context>
<context>
    <name>GroceryProductListNew</name>
    <message>
        <location filename="GroceryProductListNew.qml" line="209"/>
        <source>Please Select Best Product Below</source>
        <translation>Please Select Best Product Below</translation>
    </message>
    <message>
        <location filename="GroceryProductListNew.qml" line="309"/>
        <source>Powered by :</source>
        <translation>Powered by:</translation>
    </message>
</context>
<context>
    <name>ExpressLoginPage</name>
    <message>
        <location filename="ExpressLoginPage.qml" line="289"/>
        <source>Please wait</source>
        <translation>Please Wait A Moment</translation>
    </message>
    <message>
        <location filename="ExpressLoginPage.qml" line="318"/>
        <source>Network error, Please retry later</source>
        <translation>Oops, This machine is trying to reconnect to the system, Please retry later.</translation>
    </message>
    <message>
        <location filename="ExpressLoginPage.qml" line="379"/>
        <source>Oops, Please ensure You are using the correct button menu.</source>
        <translation>Oops, Please ensure you are using the corrent LOGIN menu.</translation>
    </message>
</context>
<context>
    <name>SepulsaCheckInputPage</name>
    <message>
        <location filename="SepulsaCheckInputPage.qml" line="79"/>
        <source>Enter Transaction Record Number</source>
        <translation>Enter Transaction Record Number</translation>
    </message>
    <message>
        <location filename="SepulsaCheckInputPage.qml" line="205"/>
        <source>Oops...</source>
        <translation>Dear Customer</translation>
    </message>
    <message>
        <location filename="SepulsaCheckInputPage.qml" line="219"/>
        <source>The transaction number cannot be verified. Please retry and enter the correct transaction number.</source>
        <translation>The transaction number cannot be verified. Please retry and enter the correct transaction number.</translation>
    </message>
</context>
<context>
    <name>SepulsaCheckExpressInfo</name>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="112"/>
        <source>Transaction Details</source>
        <translation>Transaction Details</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="162"/>
        <source>Transaction No.</source>
        <translation>Transaction No.</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="168"/>
        <source>Phone Number</source>
        <translation>Phone Number</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="174"/>
        <source>Product Name</source>
        <translation>Product Name</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="180"/>
        <source>Amount</source>
        <translation>Amount</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="186"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="279"/>
        <source>Check Another</source>
        <translation>Check Another</translation>
    </message>
    <message>
        <location filename="SepulsaCheckExpressInfo.qml" line="279"/>
        <source>For further assistance, Please feel free to contact us at 021-29022537/38.</source>
        <translation>For further assistance, Please feel free to contact us at 021-22538719.</translation>
    </message>
</context>
</TS>
