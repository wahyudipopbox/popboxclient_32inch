import hashlib
import json
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import box.service.BoxService
import Configurator
from network import HttpClient
__author__ = 'gaoyang'

class UserSignalHandler(QObject):
    user_login_signal = pyqtSignal(str)
    user_info_signal = pyqtSignal(str)
    courier_get_user_signal = pyqtSignal(str)

user_signal_handler = UserSignalHandler()
user_type = ''
user = ''

def background_login_start(username, password, identity):
    global user_type
    user_type = identity
    ClientTools.get_global_pool().apply_async(login, (username, password))

def get_user():
    global user
    return user

def select_wallet_for_box():
    user['wallet'] = {'balance': 0}
    __box_result = box.service.BoxService.get_box()
    if __box_result is False:
        return
    if 'currencyUnit' not in __box_result.keys():
        return
    __box_currency_unit = __box_result['currencyUnit']
    __user_wallets = user['wallets']
    for __wallet in __user_wallets:
        if __wallet['currencyUnit'] != __box_currency_unit:
            continue
        user['wallet'] = __wallet


def login(username, password):
    global user
    if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
        password_and_salt_ = password + '_SALT_PAKPOBOX'
    else:
        password_and_salt_ = password + '_POPBOX_NEWLOCKER_SALT'
    md_ = hashlib.sha256(password_and_salt_.encode(encoding='utf-8'))
    if not md_:
        user_signal_handler.user_login_signal.emit('Failure')
        return
    __user = {'loginName': username, 'password': str(md_.hexdigest())}
    __message, status_code = HttpClient.post_message('user/clientLogin', __user)
    if status_code == -1:
        user_signal_handler.user_login_signal.emit('NetworkError')
        return
    if status_code != 200:
        user_signal_handler.user_login_signal.emit('Failure')
        return
    if ClientTools.get_value('statusCode', __message) == 401:
        user_signal_handler.user_login_signal.emit('Failure')
        return
    user = __message
    #print("login_result : " + str(user))
    select_wallet_for_box()
    HttpClient.set_user_token(__message['token'])
    if user_type == 'LOGISTICS_COMPANY_USER':
        if user_type == __message['role'] or 'LOGISTICS_COMPANY_ADMIN' == __message['role']:
            user_signal_handler.user_login_signal.emit('Success')
        else:
            user_signal_handler.user_login_signal.emit('NoPermission')
    if user_type == 'OPERATOR_USER':
        if user_type == __message['role'] or 'OPERATOR_ADMIN' == __message['role']:
            user_signal_handler.user_login_signal.emit('Success')
        else:
            user_signal_handler.user_login_signal.emit('NoPermission')

def get_user_info():
    info = {'name': user['name'], 'phoneNumber': user['phoneNumber'], 'company_name': user['company']['name']}
    user_signal_handler.user_info_signal.emit(json.dumps(info))

def courier_get_user():
    user_signal_handler.courier_get_user_signal.emit(json.dumps(user))

def start_get_card_info(card_id, identity):
    ClientTools.get_global_pool().apply_async(login_by_card, (card_id, identity))

def login_by_card(card_id, identity):
    global user
    global user_type
    user_type = identity
    __user = {'value': card_id}
    __message, status_code = HttpClient.post_message('user/cardLogin', __user)
    if status_code == -1:
        user_signal_handler.user_login_signal.emit('NetworkError')
        return
    if status_code != 200:
        user_signal_handler.user_login_signal.emit('Failure')
        return
    user = __message
    select_wallet_for_box()
    HttpClient.set_user_token(__message['token'])
    if user_type == 'LOGISTICS_COMPANY_USER':
        if user_type == __message['role'] or 'LOGISTICS_COMPANY_ADMIN' == __message['role']:
            user_signal_handler.user_login_signal.emit('Success')
        else:
            user_signal_handler.user_login_signal.emit('NoPermission')
    if user_type == 'OPERATOR_USER':
        if user_type == __message['role'] or 'OPERATOR_ADMIN' == __message['role']:
            user_signal_handler.user_login_signal.emit('Success')
        else:
            user_signal_handler.user_login_signal.emit('NoPermission')