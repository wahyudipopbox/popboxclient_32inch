import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: parent_rec
    width:80
    height:80
    color:"silver"
    radius: 22
    property var show_text: ""

    /*Image{
        id:letter_button_image
        width:80
        height:80
        source:""
    }*/

    Text{
        text:show_text
        color:"black"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent
        font.bold: true
    }

    MouseArea {
        anchors.fill: parent_rec
        onClicked: {
            full_keyboard.letter_button_clicked(show_text)
            }
        onEntered:{
            parent_rec.color = "white"
            }
        onExited:{
            parent_rec.color = "silver"
        }
    }
}


