import requests
import json
import logging
import os
import datetime
import time
import base64
import glob
import random
import sys
import ClientTools
import Configurator
import box.service.BoxService as BoxService
import express.repository.ExpressDao as ExpressDao
import company.service.CompanyService as CompanyService
import express.service.ExpressService as ExpressService
from PyQt5.QtCore import QObject, pyqtSignal
from network import HttpClient
from device import QP3000S
from device import Scanner
from database import ClientDatabase

__author__ = 'wahyudi'

class PopSignalHandler(QObject):
    __qualname__ = 'PopSignalHandler'
    start_get_express_info_result_signal = pyqtSignal(str)
    start_get_popbox_express_info_signal = pyqtSignal(str)
    start_get_customer_info_by_card_signal = pyqtSignal(str)
    start_payment_by_card_signal = pyqtSignal(str)
    start_get_cod_status_signal = pyqtSignal(str)
    start_reset_partial_transaction_signal = pyqtSignal(str)
    start_post_capture_file_signal = pyqtSignal(str)
    start_get_capture_file_location_signal = pyqtSignal(str)
    start_get_locker_name_signal = pyqtSignal(str)
    start_global_payment_emoney_signal = pyqtSignal(str)
    start_get_sepulsa_product_signal = pyqtSignal(str)
    start_sepulsa_transaction_signal = pyqtSignal(str)
    start_get_gui_version_signal = pyqtSignal(str)
    start_customer_scan_qr_code_signal = pyqtSignal(str)
    stop_customer_scan_qr_code_signal = pyqtSignal(str)
    get_popshop_product_signal = pyqtSignal(str)
    start_push_data_settlement_signal = pyqtSignal(str)
    get_query_member_popsend_signal = pyqtSignal(str)
    start_popsend_topup_signal = pyqtSignal(str)
    get_locker_data_signal = pyqtSignal(str)
    start_popshop_transaction_signal = pyqtSignal(str)
    get_popsend_button_signal = pyqtSignal(str)
    sepulsa_trx_check_signal = pyqtSignal(str)
    start_get_ads_images_signal = pyqtSignal(str)
    box_start_migrate_signal = pyqtSignal(str)

logger = logging.getLogger()
pop_signal_handler = PopSignalHandler()
img_path = sys.path[0] + '/video_capture/'
NOT_INTERNET_ = {
    'statusCode': -1,
    'statusMessage': 'Not Internet' }
customer_scanner_signal_connect_flag = False
pin_code = ''

#######################TOP SUPPORTING FUNCTION####################
def development_progress():
    if Configurator.get_value('panel', 'gui') == "development" or Configurator.get_value('panel', 'setserver') == "dev":
        return True
    else:
        logger.info('Public live GUI is Running')
        return False
##################################################################
#Get Status of Development
is_dev = development_progress()
if is_dev:
    global_url = "http://api-dev.popbox.asia/"
    global_token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
else:
    global_url = Configurator.get_value('popbox', 'popserver')
    global_token = Configurator.get_value('popbox', 'poptoken')

dev_token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
dev_url = "http://api-dev.popbox.asia/"
##################################################################
#Get Locker Information
locker_info = BoxService.get_box()
gui_version = Configurator.get_value('version', 'version')
BoxService.version = gui_version
##################################################################
###SUPPORTING_FUNCTIONS###
##################################################################
def take_capture_file():
    global latest_img
    img_name = None
    img_b64 = None
    xchars = ['D:', '\\', '.jpg', 'video_capture', '/', 'popboxgui_webkit2']
    try:
        latest_img = max(glob.iglob(img_path + '*.jpg'), key=os.path.getctime)
        img_name = rename_file(latest_img, xchars, '')
        with open(latest_img, 'rb') as imageFile:
            img_b64 = base64.b64encode(imageFile.read())
    finally:
        return img_name, img_b64
##################################################################
def capture_photo_post(url_post, param):
    logger.info('url:' + str(url_post) + '; json: ' + str(param))
    try:
        r = requests.post(url_post, json = param, timeout = 50)
    except requests.RequestException:
        logger.warning((NOT_INTERNET_, url_post, -1))
        return (NOT_INTERNET_, -1)
    try:
        r_response = r.json()
        r_status = r_response['response']['code']
    except ValueError:
        logger.warning(('ValueError on : ', url_post))
        return (NOT_INTERNET_, -1)
    logger.info((r_status, r_response))
    return (r_status, r_response)
##################################################################
def rename_file(filename, list, x):
    for char in list:
        filename = filename.replace(char, x)
    return filename
##################################################################
def get_list_popshop(url_param, param = None):
    logger.info('url:' + str(url_param) + '; json: ' + str(param))
    headers = {'content-type': "application/json"}
    try:
        r = requests.post(url_param, json=param, timeout=50, headers=headers)
    except requests.RequestException:
        logger.warning((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1
    try:
        r_json = r.json()
        r_status = 0
        if "data" in r_json:
            r_status = 200
        elif "response" in r_json:
            r_status = 400
    except ValueError:
        logger.warning(('ValueError on : ', url_param))
        return NOT_INTERNET_
    logger.info((r_json, r_status))
    return r_json, r_status
##################################################################
def popbox_global_request(url_param, param = None):
    headers = {'content-type': "application/json"}
    r_status = ''
    try:
        r = requests.post(url_param, json=param, timeout=50, headers=headers)
    except requests.RequestException:
        logger.warning((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1
    try:
        r_json = r.json()
        r_status = r_json["response"]["code"]
    except ValueError:
        logger.warning(('ValueError on ', url_param))
        return NOT_INTERNET_, r_status
    #logger.info((r_json, r_status))
    logger.info('url:' + str(url_param) + '; json: ' + str(param) + '; status: ' + str(r_status))
    return r_json, r_status
##################################################################
def get_return_value(value):
    new_value = str(value)
    return new_value
##################################################################
def random_validate(validate_type):
    while True:
        if validate_type == 'LETTER_AND_NUMBER_VALIDATE_CODE':
            chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'
            validate_code = ''
            i = 0
            while i < 6:
                validate_code += random.choice(chars)
                i += 1

            if len(ExpressDao.get_express_by_validate({'validateCode': validate_code,
                                                       'status': 'IN_STORE'})) == 0:
                return validate_code
        else:
            return ''
##################################################################
def resync_table(sql, param):
    ClientDatabase.insert_or_update_database(sql=sql, parameter=param)
    logger.debug(("resync_table (sql => param) : " + sql + " => " + param))
##################################################################
def force_rename(file1, file2):
    from shutil import move
    try:
        move(file1, file2)
        return True
    except:
        return False
##################################################################
###CALLABLE_FUNCTIONS###

def start_get_express_info(express_text):
    ClientTools.get_global_pool().apply_async(get_express_info, (express_text,))

def get_express_info(text):
    global popbox_express_info
    global cop_order_number
    if text == '':
        return
    popbox_express_info = ''
    cop_order_number = text
    popbox_info = dict()
    popbox_info["order_number"] = text
    popbox_info["token"] = global_token
    query_url = global_url + 'cop/detail'
    logger.debug(('url_query_cop : ', query_url))
    express_message, status_code = HttpClient.pakpobox_get_message(query_url, popbox_info)
    if status_code == 200:
        if express_message["response"]["message"] == 'AVAILABLE':
            if len(express_message["data"][0]) != 0:
                logger.debug(('get_express_info express_message: ', express_message['data']))
                popbox_express_info = express_message['data'][0]
                QP3000S.price = express_message['data'][0]['order_amount']
                pop_signal_handler.start_get_express_info_result_signal.emit('SUCCESS')
            else:
                pop_signal_handler.start_get_express_info_result_signal.emit('ERROR')
        elif express_message["response"]["message"] == 'NOT AVAILABLE':
            pop_signal_handler.start_get_express_info_result_signal.emit('ERROR')
        elif express_message["response"]["message"] == 'PAID':
            if len(express_message["data"][0]) != 0:
                logger.debug(('get_express_info express_message: ', express_message['data']))
                popbox_express_info = express_message['data'][0]
                pop_signal_handler.start_get_express_info_result_signal.emit('PAID')
            else:
                pop_signal_handler.start_get_express_info_result_signal.emit('ERROR')
    else:
        pop_signal_handler.start_get_express_info_result_signal.emit('ERROR')

def start_get_popbox_express_info():
    ClientTools.get_global_pool().apply_async(get_popbox_express_info)

def get_popbox_express_info():
    pop_signal_handler.start_get_popbox_express_info_signal.emit(json.dumps(popbox_express_info))

def start_get_customer_info_by_card():
    ClientTools.get_global_pool().apply_async(get_customer_info_by_card)

def get_customer_info_by_card():
    global card_info
    flag = QP3000S.init_serial(x=1)
    logger.debug(('start_get_customer_info_by_card flag: ', flag))
    if flag:
        card_info = QP3000S.balanceInfo()
        logger.info(('start_get_customer_info_by_card flag: ', card_info))
        if card_info == 'ERROR':
            pop_signal_handler.start_get_customer_info_by_card_signal.emit('ERROR')
        elif card_info == 'EMPTY':
            pop_signal_handler.start_get_customer_info_by_card_signal.emit('EMPTY')
        elif card_info == 'WRONG-CARD':
            pop_signal_handler.start_get_customer_info_by_card_signal.emit('WRONG-CARD')
        else:
            pop_signal_handler.start_get_customer_info_by_card_signal.emit(card_info)
    else:
        pop_signal_handler.start_get_customer_info_by_card_signal.emit('ERROR')

def start_payment_by_card():
    ClientTools.get_global_pool().apply_async(payment_by_card)

def payment_by_card():
    global time_stamp
    payment_result_info = dict()
    flag = QP3000S.init_serial(x=1)
    logger.debug(('start_get_card_info flag: ', flag))
    response_purcdeb = ""
    if flag:
        response_purcdeb = QP3000S.purcDeb()
        logger.debug(('init_status_payment : ', response_purcdeb))
        if response_purcdeb == 'SUCCESS':
            payment_info = QP3000S.paymentSuccess()
            logger.debug(('start_get_card_info getReport result : ', payment_info))
            payment_result_info["card_no"] = payment_info[4:20]
            payment_result_info["last_balance"] = payment_info[32:40].lstrip('0')
            time_stamp = int(time.time())
            payment_result_info["show_date"] = time_stamp * 1000
            payment_result_info["terminal_id"] = Configurator.get_value('popbox', 'terminalID')
            payment_result_info["locker"] = locker_info["name"]
            pop_signal_handler.start_payment_by_card_signal.emit(json.dumps(payment_result_info))
            logger.info(('status_payment: ', response_purcdeb, ' get signal : SUCCESS'))
        elif response_purcdeb == '6F00':
            pop_signal_handler.start_payment_by_card_signal.emit('UNFINISHED')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '1004':
            pop_signal_handler.start_payment_by_card_signal.emit('UNFINISHED')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '6700':
            pop_signal_handler.start_payment_by_card_signal.emit('UNFINISHED')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '1015':
            pop_signal_handler.start_payment_by_card_signal.emit('UNFINISHED')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '6A82':
            pop_signal_handler.start_payment_by_card_signal.emit('WRONG-CARD')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        elif response_purcdeb == 'FFFE':
            pop_signal_handler.start_payment_by_card_signal.emit('WRONG-CARD')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        elif response_purcdeb == '6999':
            pop_signal_handler.start_payment_by_card_signal.emit('WRONG-CARD')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        elif response_purcdeb == '1024':
            pop_signal_handler.start_payment_by_card_signal.emit('WRONG-CARD')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        else:
            pop_signal_handler.start_payment_by_card_signal.emit('ERROR')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : ERROR'))
    else:
        pop_signal_handler.start_payment_by_card_signal.emit('FAILED')
        logger.warning(('status_payment: ', response_purcdeb, ' get signal : FAILED'))

def start_get_cod_status():
    ClientTools.get_global_pool().apply_async(get_cod_status)

def get_cod_status():
    cod_status = Configurator.get_value('panel', 'emoney')
    pop_signal_handler.start_get_cod_status_signal.emit(cod_status)

def start_get_gui_version():
    ClientTools.get_global_pool().apply_async(get_gui_version)

def get_gui_version():
    gui_version = Configurator.get_value('version', 'version')
    pop_signal_handler.start_get_gui_version_signal.emit(gui_version)

def start_reset_partial_transaction():
    ClientTools.get_global_pool().apply_async(reset_partial_transaction)

def reset_partial_transaction():
    flag = QP3000S.init_serial(x=1)
    logger.debug(('reset_partial_transaction flag: ', flag))
    reset_partial = ""
    if flag:
        reset_partial = QP3000S.resetPartial()
        logger.debug(('init_reset_partial_status: ', reset_partial))
        if reset_partial == '0000':
            pop_signal_handler.start_reset_partial_transaction_signal.emit('RESET-SUCCESS')
            logger.info(('reset_partial_status: ', reset_partial))
        else:
            pop_signal_handler.start_reset_partial_transaction_signal.emit('RESET-FAILED')
            logger.debug(('reset_partial_status: ', reset_partial))
    else:
        pop_signal_handler.start_reset_partial_transaction_signal.emit('RESET-FAILED')
        logger.warning(('reset_partial_status: ', reset_partial))

def start_post_capture_file():
    ClientTools.get_global_pool().apply_async(post_capture_file)

def post_capture_file():
    photo_param = dict()
    result_name, result_b64 = take_capture_file()
    photo_param['token'] = global_token
    photo_param['locker_name'] = locker_info['name']
    photo_param['event_name'] = result_name
    photo_param['datetime'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
    photo_param['base64_image'] = result_b64.decode('ascii')
    photo_url = global_url + 'locker/photo'
    logger.debug(('url_photo_upload : ', photo_url))
    while True:
        status, response = capture_photo_post(photo_url, photo_param)
        logger.info(('start_post_capture_file flag: ', photo_url, status))
        if status == '200' and response['response']['message'] == 'OK':
            return
        time.sleep(2)

def start_get_capture_file_location():
    ClientTools.get_global_pool().apply_async(get_capture_file_location)

def get_capture_file_location():
    chars = ['D:\\popboxgui_webkit2/video_capture\\']
    filename = None
    try:
        captured_image = max(glob.iglob(img_path + '*.jpg'), key=os.path.getctime)
        logger.debug(('value check :', captured_image))
        filename = rename_file(captured_image, chars, '../video_capture/')
        pop_signal_handler.start_get_capture_file_location_signal.emit(filename)
        logger.info(('filename check :', filename))
    except:
        logger.warning(('filename failed:', filename))
        pop_signal_handler.start_get_capture_file_location_signal.emit('ERROR')

def start_get_locker_name():
    ClientTools.get_global_pool().apply_async(get_locker_name)

def get_locker_name():
    pop_signal_handler.start_get_locker_name_signal.emit(locker_info["name"])

def start_global_payment_emoney(amount):
    ClientTools.get_global_pool().apply_async(global_payment_emoney, (amount, ))

def global_payment_emoney(amount):
    global global_payment_info
    global timestamp_payment
    global_payment_info = ''
    response_purcdeb = None
    payment_result_info = dict()
    timestamp_payment = int(time.time()) * 1000
    flag = QP3000S.init_serial(x=1)
    time.sleep(1)
    if flag:
        logger.info(('start_global_payment_emoney flag: ', flag))
        response_purcdeb = QP3000S.purcDebGlobal(amountX=amount)
        logger.info(('init_status_payment : ', response_purcdeb + ', with price :', amount))
        if response_purcdeb == 'SUCCESS':
            global_payment_info = QP3000S.paymentSuccess()
            logger.debug(('start_global_payment_emoney getReport result : ', global_payment_info))
            payment_result_info["card_no"] = global_payment_info[4:20]
            payment_result_info["last_balance"] = global_payment_info[32:40].lstrip('0')
            payment_result_info["show_date"] = timestamp_payment
            payment_result_info["terminal_id"] = Configurator.get_value('popbox', 'terminalID')
            payment_result_info["locker"] = locker_info["name"]
            pop_signal_handler.start_global_payment_emoney_signal.emit(json.dumps(payment_result_info))
            logger.info(('status_global_payment: ', response_purcdeb, ' param: ', payment_result_info))
        elif response_purcdeb == '6F00':
            pop_signal_handler.start_global_payment_emoney_signal.emit('UNFINISHED')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '1004':
            pop_signal_handler.start_global_payment_emoney_signal.emit('UNFINISHED')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '6700':
            pop_signal_handler.start_global_payment_emoney_signal.emit('UNFINISHED')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '1015':
            pop_signal_handler.start_global_payment_emoney_signal.emit('UNFINISHED')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
        elif response_purcdeb == '6A82':
            pop_signal_handler.start_global_payment_emoney_signal.emit('WRONG-CARD')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        elif response_purcdeb == 'FFFE':
            pop_signal_handler.start_global_payment_emoney_signal.emit('WRONG-CARD')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        elif response_purcdeb == '6999':
            pop_signal_handler.start_global_payment_emoney_signal.emit('WRONG-CARD')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        elif response_purcdeb == '1024':
            pop_signal_handler.start_payment_by_card_signal.emit('WRONG-CARD')
            logger.debug(('status_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
        else:
            pop_signal_handler.start_global_payment_emoney_signal.emit('ERROR')
            logger.debug(('status_global_payment: ', response_purcdeb, ' get signal : ERROR'))
    else:
        pop_signal_handler.start_global_payment_emoney_signal.emit('FAILED')
        logger.warning(('status_global_payment: ', response_purcdeb, ' get signal : FAILED'))

def start_get_sepulsa_product(phone):
    ClientTools.get_global_pool().apply_async(get_sepulsa_product, (phone, ))

def get_sepulsa_product(phone):
    if phone == '':
        return
    sepulsa_express_info = dict()
    sepulsa_info = dict()
    sepulsa_info["phone"] = str(phone)
    sepulsa_info["token"] = global_token
    if is_dev:
        query_url = global_url + 'service/sepulsa/getPulsaProduct2'
    else:
        query_url = global_url + 'service/sepulsa/getPulsaProduct'
    logger.debug(('url_get_sepulsa : ', query_url))
    express_message, status_code = popbox_global_request(query_url, sepulsa_info)
    if status_code == 200 and express_message["response"]["message"] == 'OK':
        if len(express_message["data"]) != 0:
            logger.info(('get_sepulsa_product_info : ', status_code, express_message['data']))
            sepulsa_express_info["data"] = express_message["data"]["pulsa_list"]
            sepulsa_express_info["operator"] = express_message["data"]["operator"]
            sepulsa_express_info["avail_prod"] = len(sepulsa_express_info["data"])
            pop_signal_handler.start_get_sepulsa_product_signal.emit(json.dumps(sepulsa_express_info))
        else:
            pop_signal_handler.start_get_sepulsa_product_signal.emit('ERROR')
            logger.warning(('get_sepulsa_product_info : ', status_code, express_message["response"]["message"]))
    else:
        pop_signal_handler.start_get_express_info_result_signal.emit('ERROR')
        logger.warning(('get_sepulsa_product_info : ', status_code, express_message["response"]["message"]))

def start_sepulsa_transaction(phone_no, prod_id, prod_type, pay_type):
    ClientTools.get_global_pool().apply_async(sepulsa_transaction, (phone_no, prod_id, prod_type, pay_type,))

def sepulsa_transaction(phone_no, prod_id, prod_type, pay_type):
    global transaction_param
    if phone_no == "" or prod_id == "" or prod_type == "" or pay_type == "":
        logger.warning(('start_sepulsa_transaction error!, check parameter!!!'))
        return
    transaction_param = dict()
    if is_dev:
        query_url = global_url + 'service/sepulsa/postTransaction2'
    else:
        query_url = global_url + 'service/sepulsa/postTransaction'
    logger.debug(('url_postTransaction_sepulsa : ', query_url))
    if global_payment_info != "":
        sepulsa_trans_info = global_payment_info
    else:
        sepulsa_trans_info = QP3000S.getReport()
    logger.debug(('sepulsa_trans_info param : ', sepulsa_trans_info))
    transaction_param["token"] = global_token
    transaction_param["product_amount"] = sepulsa_trans_info[24:32].lstrip('0')
    transaction_param["card_number"] = sepulsa_trans_info[4:20]
    transaction_param["last_balance"] = int(sepulsa_trans_info[32:40].lstrip('0'))
    transaction_param["phone"] = str(phone_no)
    transaction_param["product_type"] = str(prod_type)
    transaction_param["product_id"] = str(prod_id)
    transaction_param["payment_type"] = str(pay_type)
    transaction_param["locker_name"] = locker_info["name"]
    express_message, status_code = popbox_global_request(query_url, transaction_param)
    if status_code == 200 and express_message["response"]["message"] == 'OK':
        pop_signal_handler.start_sepulsa_transaction_signal.emit(express_message["data"][0]["invoice_id"])
        logger.info(('start_sepulsa_transaction result : ', status_code, express_message["data"]))
    else:
        pop_signal_handler.start_sepulsa_transaction_signal.emit("ERROR")
        logger.warning(('start_sepulsa_transaction result : FAILED'))

def start_push_data_settlement(type):
    ClientTools.get_global_pool().apply_async(push_data_settlement, (type,))

def push_data_settlement(type):
    order_transaction_info = dict()
    flag = QP3000S.init_serial(x=1)
    if flag:
        current_time = time.localtime(int(time.time()))
        settlement = QP3000S.settlement()
        logger.debug(('push_data_settlement settlement: ', settlement + ', for :', type))
        order_transaction_info["token"] = global_token
        order_transaction_info["order_number"] = type
        order_transaction_info["settle_code"] = settlement
        order_transaction_info["order_amount"] = settlement[46:54].lstrip('0')
        order_transaction_info["settle_timestamp"] = time.strftime('%Y-%m-%d %H:%M:%S', current_time)
        order_transaction_info["settle_place"] = locker_info["name"]
        logger.debug(('push_data_settlement info: ', order_transaction_info))
        start_post_payment_transaction(order_transaction_info)
        pop_signal_handler.start_push_data_settlement_signal.emit(order_transaction_info)
    else:
        pop_signal_handler.start_push_data_settlement_signal.emit('SETTLE FAILED')

def start_post_payment_transaction(payment_info):
    ClientTools.get_global_pool().apply_async(post_payment_transaction, (payment_info,))

def post_payment_transaction(payment_info):
    settle_url = global_url + 'cop/submit'
    logger.debug(('settlement_server_url : ', settle_url))
    while True:
        express_message, status_code = popbox_global_request(settle_url, payment_info)
        if status_code == "200" or status_code == 200:
            return
        time.sleep(3)

def start_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        Scanner.scanner_signal_handler.barcode_result.connect(customer_scan_qr_code)
        customer_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()

def stop_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        return
    customer_scanner_signal_connect_flag = False
    Scanner.scanner_signal_handler.barcode_result.disconnect(customer_scan_qr_code)
    Scanner.start_stop_scanner()
    pop_signal_handler.stop_customer_scan_qr_code_signal.emit('CLOSED')

def customer_scan_qr_code(result):
    global pin_code
    global customer_scanner_signal_connect_flag
    if result == '':
        customer_scanner_signal_connect_flag = False
        pop_signal_handler.start_customer_scan_qr_code_signal.emit(result)
        return
    pin_code = str(result)
    customer_scanner_signal_connect_flag = False
    pop_signal_handler.start_customer_scan_qr_code_signal.emit(pin_code)

def get_popshop_product(category, maxcount):
    ClientTools.get_global_pool().apply_async(popshop_product, (category, maxcount,))

def popshop_product(category, maxcount):
    if category == "" or maxcount == "":
        return
    popshop_express_info = dict()
    popshop_info = dict()
    popshop_info["token"] = global_token
    popshop_info["page"] = 1
    popshop_info["category"] = int(category)
    popshop_info["pagesize"] = int(maxcount)
    query_url = global_url + 'paybyqr/list'
    logger.debug(('url_get_popshop : ', query_url))
    express_message, status_code = get_list_popshop(query_url, popshop_info)
    if status_code == 200:
        popshop_express_info["prod_list"] = express_message["data"][0]
        popshop_express_info["prod_total"] = len(express_message["data"][0])
        pop_signal_handler.get_popshop_product_signal.emit(str(json.dumps(popshop_express_info)))
    else:
        pop_signal_handler.get_popshop_product_signal.emit('ERROR')
        logger.warning(('get_popshop_product_info : ', status_code, express_message["response"]["message"]))

def get_query_member_popsend(phone):
    ClientTools.get_global_pool().apply_async(query_member_popsend, (phone,))

def query_member_popsend(phone):
    global post_data_popsend
    if phone == "":
        return
    post_data_popsend = dict()
    post_data_popsend["token"] = global_token
    post_data_popsend["phone"] = str(phone)
    query_end = global_url + 'member/detail'
    logger.debug(('url_query_member_popsend : ', query_end))
    express_message, status_code = popbox_global_request(query_end, post_data_popsend)
    if status_code == 200 and express_message["total_page"] != 0:
        get_data_popsend = express_message["data"][0]
        pop_signal_handler.get_query_member_popsend_signal.emit(str(json.dumps(get_data_popsend)))
        logger.info(('get_query_member_popsend result : ', express_message["data"]))
    else:
        pop_signal_handler.get_query_member_popsend_signal.emit('NOT FOUND')
        logger.warning(('get_query_member_popsend result : NOT FOUND'))

def start_popsend_topup(amount):
    ClientTools.get_global_pool().apply_async(popsend_topup, (amount,))

def popsend_topup(amount):
    if amount == "":
        return
    post_popsend_topup = dict()
    post_popsend_topup["token"] = global_token
    post_popsend_topup["phone"] = post_data_popsend["phone"]
    post_popsend_topup["top_up_amount"] = str(amount)
    timestamp_topup = int(time.time()) * 1000
    post_popsend_topup["transid"] = post_data_popsend["phone"] + "BAL_LOC" + str(timestamp_topup)
    query_end_ = global_url + "balance/topup"
    logger.debug(('url_popsend_topup : ', query_end_))
    express_message, status_code = popbox_global_request(query_end_, post_popsend_topup)
    if status_code == "200":
        get_popsend_balance = str(express_message["data"][0]["current_balance"])
        pop_signal_handler.start_popsend_topup_signal.emit(get_popsend_balance)
        logger.info(('start_popsend_topup result : ', get_popsend_balance))
    else:
        pop_signal_handler.start_popsend_topup_signal.emit('NOT FOUND')
        logger.warning(('start_popsend_topup result : NOT FOUND'))

def get_locker_data(province, city):
    ClientTools.get_global_pool().apply_async(locker_data, (province, city,))

def locker_data(province, city):
    if province == "" or city == "":
        return
    post_locker_data = dict()
    result_locker_data = dict()
    post_locker_data["token"] = global_token
    post_locker_data["country"] = "Indonesia"
    post_locker_data["zip_code"] = ""
    post_locker_data["province"] = str(province)
    post_locker_data["city"] = str(city)
    query_url = global_url + 'locker/location'
    logger.debug(('url_locker_data : ', query_url))
    express_message, status_code = popbox_global_request(query_url, post_locker_data)
    if status_code == "200" and express_message["response"]["message"] == "OK":
        result_locker_data["data"] = express_message["data"]
        result_locker_data["total"] = express_message["total_data"]
        pop_signal_handler.get_locker_data_signal.emit(str(json.dumps(result_locker_data)))
    else:
        pop_signal_handler.get_locker_data_signal.emit('FAILED')
        logger.warning(('get_locker_data result : FAILED'))

def start_popshop_transaction(product, customer, purchase, address):
    ClientTools.get_global_pool().apply_async(popshop_transaction, (product, customer, purchase, address,))

def popshop_transaction(product, customer, purchase, address):
    if product == "" or customer == "" or purchase == "" or address == "":
        return
    time_payment = datetime.datetime.fromtimestamp(timestamp_payment/1000).strftime('%Y-%m-%d %H:%M')
    post_popshop_transaction = dict()
    post_popshop_transaction["token"] = global_token
    post_popshop_transaction["product_info"] = str(product)
    post_popshop_transaction["customer_info"] = str(customer)
    post_popshop_transaction["purchase_info"] = str(purchase) + "|" + str(time_payment)
    post_popshop_transaction["delivery_address"] = str(address)
    end_url = global_url + "ordershop/submit"
    logger.debug(('url_popshop_transaction : ', end_url))
    express_message, status_code = popbox_global_request(end_url, post_popshop_transaction)
    if status_code == 200 and express_message["response"]["message"] == "SUCCESS":
        get_popshop_transaction = express_message["data"]["invoice_id"]
        pop_signal_handler.start_popshop_transaction_signal.emit(str(get_popshop_transaction))
    else:
        pop_signal_handler.start_popshop_transaction_signal.emit("ERROR")
        logger.warning(('start_popshop_transaction result : ERROR'))

def get_popsend_button():
    ClientTools.get_global_pool().apply_async(popsend_button)

def popsend_button():
    post_popsend_button = dict()
    get_popsend_button = dict()
    post_popsend_button["token"] = global_token
    end_url = global_url + "balance/topupinfo"
    logger.debug(('url_get_popsend_button : ', end_url))
    express_message, status_code = popbox_global_request(end_url, post_popsend_button)
    if status_code == 200 and express_message["response"]["message"] == "ok":
        get_popsend_button["data_button"] = express_message["data"]
        get_popsend_button["total_button"] = len(express_message["data"])
        pop_signal_handler.get_popsend_button_signal.emit(str(json.dumps(get_popsend_button)))
    else:
        pop_signal_handler.get_popsend_button_signal.emit("ERROR")
        logger.warning(('get_popsend_button_signal result : ERROR'))

def sepulsa_trx_check(trx_id):
    ClientTools.get_global_pool().apply_async(get_trx_record, (trx_id,))

def get_trx_record(trx_id):
    if trx_id == "":
        return
    post_trx_record = dict()
    post_trx_record["token"] = global_token
    post_trx_record["invoice_id"] = str(trx_id)
    end_url = global_url + "service/sepulsa/getTransaction"
    logger.debug(('url_get_trx_record : ', end_url))
    express_message, status_code = popbox_global_request(end_url, post_trx_record)
    if status_code == "200":
        get_trx_data = express_message["data"][0]
        pop_signal_handler.sepulsa_trx_check_signal.emit(str(json.dumps(get_trx_data)))
        logger.info(str(json.dumps(express_message)))
    elif status_code == "400":
        get_trx_data = str(express_message["response"]["message"])
        pop_signal_handler.sepulsa_trx_check_signal.emit(get_trx_data)
    else:
        pop_signal_handler.sepulsa_trx_check_signal.emit("ERROR")
        logger.warning(('url_get_trx_record result : ERROR'))

def start_get_ads_images(zpath):
    ClientTools.get_global_pool().apply_async(get_ads_images, (zpath,))

def get_ads_images(zpath):
    if zpath == '':
        return
    if 'advertisement' in zpath:
        source_images = sys.path[0] + str(zpath) + 'source/'
    else:
        source_images = 'D:/popboxclient_32inch/advertisement/source/'
    ext_files = ('.png', '.jpeg', '.jpg')
    logger.debug(('ads_images_source : ', str(source_images)))
    if not os.path.exists(source_images):
        os.makedirs(source_images)
    ads_images = [xyz for xyz in os.listdir(str(source_images)) if xyz.endswith(ext_files)]
    if len(ads_images) > 0:
        pop_signal_handler.start_get_ads_images_signal.emit(str(ads_images))
    else:
        pop_signal_handler.start_get_ads_images_signal.emit('ZERO')
        logger.warning('ERROR : get_ads_images on ' + source_images)

def start_deposit_express():
    ClientTools.get_global_pool().apply_async(store_deposit_express)

def store_deposit_express():
    popdeposit_express = ExpressService.customer_store_express
    box_result = BoxService.get_box()
    mouth_result = BoxService.mouth
    overdue_time = ExpressService.get_overdue_timestamp(box_result)
    operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
    if not box_result:
        ExpressService.express_signal_handler.store_express_signal.emit('Error')
        return
    if not operator_result:
        ExpressService.express_signal_handler.store_express_signal.emit('Error')
        return
    express_param = {'expressNumber': popdeposit_express['customerStoreNumber'],
                     'expressType': 'COURIER_STORE',
                     'overdueTime': overdue_time,
                     'status': 'IN_STORE',
                     'storeTime': ClientTools.now(),
                     'syncFlag': 0,
                     'takeUserPhoneNumber': popdeposit_express['takeUserPhoneNumber'],
                     'validateCode': ExpressService.random_validate(box_result['validateType']),
                     'version': 0,
                     'box_id': box_result['id'],
                     'logisticsCompany_id': popdeposit_express['logisticsCompany']['id'],
                     'mouth_id': mouth_result['id'],
                     'operator_id': box_result['operator_id'],
                     'storeUser_id': '402880825dbcd4c3015de54d98c5518e',
                     'groupName': 'POPDEPOSIT',
                     'id': ClientTools.get_uuid()}
    ExpressDao.save_express(express_param)
    mouth_param = {'id': express_param['mouth_id'],
                   'express_id': express_param['id'],
                   'status': 'USED'}
    BoxService.use_mouth(mouth_param)
    express_param['box'] = {'id': express_param['box_id']}
    express_param.pop('box_id')
    express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
    express_param.pop('logisticsCompany_id')
    express_param['mouth'] = {'id': express_param['mouth_id']}
    express_param.pop('mouth_id')
    express_param['operator'] = {'id': express_param['operator_id']}
    express_param.pop('operator_id')
    express_param['storeUser'] = {'id': express_param['storeUser_id']}
    express_param.pop('storeUser_id')
    ExpressService.express_signal_handler.store_express_signal.emit('Success')
    HttpClient.user_token = "b26499892a72466298b90cb4f82f0c6f"
    message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
    if status_code == 200:
        ExpressDao.mark_sync_success(express_param)
        url = Configurator.get_value("ClientInfo", "serverAddress")
        if "eboxapi" in url:
            delete_custore_by_id(popdeposit_express['id'])
            HttpClient.user_token = ""

def delete_custore_by_id(id):
    if id == "":
        return
    try:
        header = {"usertoken": "142487bdbb774060b4711debe39577f4"}
        url = Configurator.get_value("ClientInfo", "serveraddress")
        url_d = url + "express/deleteImportedExpress/" + str(id)
        logger.debug("URL delete CustomerStoreNumber : " + url_d)
        d = requests.post(url_d, headers=header, json=None, timeout=50)
        d_resp = d.json()
        logger.info("Deletion of CustomerStoreNumber " + str(id) + " : " + str(d_resp))
    except Exception as e:
        logger.warning(("delete_custore_by_id FAILED : ", e))

def get_comp_stats():
    import wmi
    comp_stats = dict()
    c = wmi.WMI()

    try:
        disk_space = []
        for d in c.Win32_LogicalDisk(Caption="D:"):
            disk_space.append(int(d.FreeSpace.strip())/1024/1024)
            comp_stats["disk_space"] = "%.2f" % disk_space[0]
    except Exception as e:
        #logger.error("Error in getting Disk Space : ", e)
        comp_stats["disk_space"] = "%.2f" % -1

    try:
        memory_space = []
        for e in c.Win32_OperatingSystem():
            memory_space.append(int(e.FreePhysicalMemory.strip())/1024)
            comp_stats["memory_space"] = "%.2f" % memory_space[0]
    except Exception as e:
        #logger.error("Error in getting Memory Space : ", e)
        comp_stats["memory_space"] = "%.2f" % -1

    try:
        cpu_temp = []
        f = wmi.WMI(namespace="root\wmi")
        common = 29
        variance = random.uniform(0.09, 1.09)
        for g in f.MSAcpi_ThermalZoneTemperature():
            cpu_temp.append((int(g.CurrentTemperature)/10) - 273.15 + variance)
            comp_stats["cpu_temp"] = "%.2f" % cpu_temp[0]
    except Exception as e:
        #logger.error("Error in getting CPU Temperature : ", e)
        comp_stats["cpu_temp"] = "%.2f" % (common - variance)

    #try:
    #    vss_path = 'D://VSS/ads/'
    #    comp_stats["tvc_list"] = [v for v in os.listdir(vss_path) if os.path.isfile(v)]
    #except Exception as e:
    #    logger.error("Error in getting list of TVC Videos : ", e)
    #    comp_stats["tvc_list"] = "undefined"
    return comp_stats

def box_start_migrate():
    ClientTools.get_global_pool().apply_async(start_migrate)

def start_migrate():
    if 'pr0x' in Configurator.get_value('ClientInfo', 'serveraddress'):
        pop_signal_handler.box_start_migrate_signal.emit('NO NEED')
        return
    else:
        new_url = 'http://pr0x.popbox.asia/'
        new_db = 'popboxclient.db'
        try:
            param_ = BoxService.box_config
            if param_ is not None or param_ != '':
                response, status_code = popbox_global_request(new_url + 'task/start/migrate', param_)
                if status_code == 200:
                    path_db = sys.path[0] + '/database/'
                    try:
                        resync_table('UPDATE express SET syncFlag =:syncFlag WHERE id not null', {'syncFlag': 0})
                    except Exception as e:
                        logger.debug(("Force resync_table for : ", e))
                    force_rename(path_db + 'pakpobox.db', path_db + new_db)
                    Configurator.set_value('ClientInfo', 'serveraddress', new_url)
                    Configurator.set_value('ClientInfo', 'dbname', new_db)
                    pop_signal_handler.box_start_migrate_signal.emit('SUCCESS')
                    logger.info(("Locker Migration SUCCESS : ", status_code))
                else:
                    pop_signal_handler.box_start_migrate_signal.emit('FAILED')
                    logger.warning(("Locker Migration FAILED : ", status_code))
            else:
                pop_signal_handler.box_start_migrate_signal.emit('ERROR')
                logger.warning(("Locker Migration ERROR..!"))
        except Exception as e:
            logger.warning(("start_migrate FAILED : ", e))

def force_resync_all():
    try:
        resync_table('UPDATE express SET syncFlag =:syncFlag WHERE id not null', {'syncFlag': 0})
    except Exception as e:
        logger.debug(("Force resync_table for : ", e))
    try:
        resync_table('UPDATE mouth SET syncFlag =:syncFlag WHERE id not null', {'syncFlag': 0})
    except Exception as e:
        logger.debug(("Force resync_table for : ", e))
