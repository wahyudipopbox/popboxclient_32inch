﻿import QtQuick 2.4
import QtQuick.Controls 1.2
import QtWebKit 3.0

Background{
    property var geturl
    property var initurl
    property int timer_value: 300

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_product_file()
            abc.counter = timer_value
            my_timer.restart()
            loader.sourceComponent = component
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()

        }
        if(Stack.status==Stack.Active){
        }
    }

    Component.onCompleted:{
        root.product_file_result.connect(getboxInfo)
    }

    Component.onDestruction:{
        root.product_file_result.disconnect(getboxInfo)
    }

    function getboxInfo(text){
        initurl = initurl + "?" + "orderNo=" + text
    }

    Rectangle{
        width:39
        height:11
        y:10
        color:"transparent"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow_test.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component {
        id: component
        Rectangle {
            ScrollView {
                width: 1024
                height: 768
                WebView {
                    id: webview
                    url: initurl
                    anchors.fill: parent

                    onLinkHovered:{
                        abc.counter = timer_value
                        my_timer.restart()
                        geturl = hoveredUrl
                        initurl = geturl
                        loader.sourceComponent = component
                    }
                }
            }
        }
    }

    Loader { id: loader }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }

    Text{
    	id:countShow_test
    	x:10
    	y:738
     	color:"#FFFF00"
    	font.family:"Microsoft YaHei"
    	font.pixelSize:16
    }

}

