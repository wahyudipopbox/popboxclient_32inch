import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:email_keyboard
    width:966
    height:386
    color:"transparent"
    property bool alphaActive: true
    signal letter_button_clicked(string str)
    signal function_button_clicked(string str)


    NewLetterButton{
        id: text_1
        x:23
        y:0
        show_text:"1"
    }
    NewLetterButton{
        id: text_2
        x:107
        y:0
        show_text:"2"
    }
    NewLetterButton{
        id: text_3
        x:190
        y:0
        show_text:"3"
    }
    NewLetterButton{
        id: text_4
        x:273
        y:0
        show_text:"4"
    }
    NewLetterButton{
        id: text_5
        x:356
        y:0
        show_text:"5"
    }
    NewLetterButton{
        id: text_6
        x:440
        y:0
        show_text:"6"
    }
    NewLetterButton{
        id: text_7
        x:523
        y:0
        show_text:"7"
    }
    NewLetterButton{
        id: text_8
        x:606
        y:0
        show_text:"8"
    }
    NewLetterButton{
        id: text_9
        x:689
        y:0
        show_text:"9"
    }
    NewLetterButton{
        id: text_0
        x:773
        y:0
        show_text:"0"
    }
    NewLetterButton{
        id: text_Q
        x:61
        y:83
        show_text:"q"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_W
        x:145
        y:83
        show_text:"w"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_E
        x:229
        y:83
        show_text:"e"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_R
        x:313
        y:83
        show_text:"r"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_T
        x:397
        y:83
        show_text:"t"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_Y
        x:481
        y:83
        show_text:"y"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_U
        x:565
        y:83
        show_text:"u"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_I
        x:649
        y:83
        show_text:"i"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_O
        x:733
        y:83
        show_text:"o"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_P
        x:817
        y:83
        show_text:"p"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_A
        x:61
        y:166
        show_text:"a"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_S
        x:145
        y:166
        show_text:"s"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_D
        x:229
        y:166
        show_text:"d"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_F
        x:313
        y:166
        show_text:"f"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_G
        x:397
        y:166
        show_text:"g"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_H
        x:481
        y:166
        show_text:"h"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_J
        x:565
        y:166
        show_text:"j"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_K
        x:649
        y:166
        show_text:"k"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_L
        x:733
        y:166
        show_text:"l"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_Z
        x:107
        y:249
        show_text:"z"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_X
        x:190
        y:249
        show_text:"x"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_C
        x:273
        y:249
        show_text:"c"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_V
        x:356
        y:249
        show_text:"v"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_B
        x:440
        y:249
        show_text:"b"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_N
        x:523
        y:249
        show_text:"n"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_M
        x:606
        y:249
        show_text:"m"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_minus
        x:773
        y:249
        show_text:"-"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_at
        x:857
        y:249
        show_text:"@"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_uscore
        x:689
        y:249
        show_text:"_"
        status_enable: alphaActive
    }
    NewLetterButton{
        id: text_dot
        x:817
        y:166
        show_text:"."
        status_enable: alphaActive
    }
    BackspaceButton{
        x:23
        y:249
        width: 80
    }
    NewKeyboardOKFunction{
        id: text_ok
        x:273
        y:335
        slot_text:"ok"
    }
    NewKeyboardDeleteFunction{
        id: text_bspace
        x:857
        y:0
        height: 80
        width: 80
        slot_text:"delete"
    } 

}
