import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: grocery_express_info
    width: 1024
    height: 768
    property int timer_value: 60
    property int show_timer_value: 5
    property int show_timer_value_nf: 5
    property var press:"0"
    property int trial_count: 0
    property var cust_name: "-"
    property var cust_phone: "-"
    property var cust_email: "-"
    property var prod_name: "-"
    property var prod_sku: "-"
    property var prod_qty: "-"
    property var select_locker: "-"
    property var amount: "0"
    property var data_all: "-"
    property var new_amount:"0"

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log("data_all result : " + data_all)
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            trial_count = 0
            parse_data_all(data_all)
            time_payment.counter = show_timer_value
            time_nf.counter = show_timer_value_nf
            ok_button.enabled = true
            back_button.enabled = true
            nonfinished_notif.close()
            payment_page.close()
            wrong_card_notif.close()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            show_timer_payment.stop()
            show_timer_nf.stop()
        }
    }

    Component.onCompleted: {
        root.start_global_payment_emoney_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_global_payment_emoney_result.disconnect(handle_text)
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function handle_text(result){
        console.log("status_payment_signal : " + result )
        if(result == "UNFINISHED"){
            trial_count += 1
            payment_page.close()
            nonfinished_notif.open()
            show_timer_payment.stop()
            show_timer_nf.restart()
            ok_button.enabled = false
            back_button.enabled = false
            trial_count_text.text = trial_count
            time_nf.counter = show_timer_value_nf
            if(trial_count >= 4){
                //slot_handler.start_reset_partial_transaction()
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }else if(result == "WRONG-CARD"){
            wrong_card_notif.open()
            payment_page.close()
            ok_button.enabled = false
            back_button.enabled = false
        }else if(result == "ERROR" || result == "FAILED"){
            my_stack_view.push(other_payment_failed_page)
            return
        }

        var obj = JSON.parse(result)
        if(obj.length <= 0){
            my_stack_view.push(other_payment_failed_page)
        }else{
            my_stack_view.push(grocery_payment_success, {payment_result:result, amount:amount, summary_data:data_all})
        }
    }

    function parse_data_all(xxx){
        var x = xxx.split("|")
        prod_sku = x[0]
        prod_name = x[1]
        prod_qty = x[2]
        cust_name = x[4]
        cust_email = x[5]
        cust_phone = x[6]
        select_locker = x[7]
        new_amount = insert_flg(amount)
        check_proddetails(prod_sku,prod_name)
    }

    function check_proddetails(x,y){
        var z = x + " - " + y
        if(z.length > 30){
            second_row.y = 400
        }else{
            second_row.y = 360
        }
    }

    Rectangle{
        x: 10
        y: 10
        width: 20
        height: 20
        color: "transparent"

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                main_timer_text.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
        Text {
            id:main_timer_text
            visible:false
            width: 20
            height: 20
            font.family:"Microsoft YaHei"
            color:"#fff000"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.PlainText
            font.pointSize:10
            wrapMode: Text.WordWrap
        }
    }

    FullWidthReminderText{
        id:title_page
        x: 0
        y:140
        remind_text:qsTr("Order Details")
        remind_text_size:"35"
    }

    Item{
        id:first_row
        x:185
        y:195
        width: 750
        height: 150
        Column{
            spacing:10
            Text {
                id: name_label
                text: qsTr("Name")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: phone_label
                text: qsTr("Phone Number")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: email_label
                text: qsTr("Email")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: product_label
                text: qsTr("Product Name")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
        }
        Item{
            anchors.left: parent.left
            anchors.leftMargin: 215
            Column{
                spacing:10
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
            }
        }

        Item{
            y: 0
            anchors.left: parent.left
            anchors.leftMargin: 230
            Column{
                spacing:10
                Text {
                    id: name_text
                    text: cust_name
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: phone_text
                    text: cust_phone
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: email_text
                    text: cust_email.toLowerCase()
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: product_text
                    text: prod_sku + " - " + prod_name
                    wrapMode: Text.WordWrap
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                    width: 490
                    font.capitalization: Font.Capitalize
                }
            }
        }
    }

    Item{
        id:second_row
        x:185
        y:360
        width: 750
        height: 150
        Column{
            spacing:10
            Text {
                id: qty_label
                text: qsTr("Quantity")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: delivery_label
                text: qsTr("Delivery Address")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: amount_label
                text: qsTr("Total Amount")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
        }
        Item{
            anchors.left: parent.left
            anchors.leftMargin: 215
            Column{
                spacing:10
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
            }
        }

        Item{
            anchors.left: parent.left
            anchors.leftMargin: 230
            Column{
                spacing:10
                Text {
                    id: qty_text
                    width: 51
                    text: prod_qty + " " + qsTr("pcs")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: locker_text
                    text: "PopBox @ " + select_locker
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: amount_text
                    text: new_amount
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
            }
        }
    }

    Text {
        id: prepare_text
        x: 250
        y: 509
        width: 525
        height: 104
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 25
        text: qsTr("Please prepare your pre-paid card and ensure the balance is sufficient.")
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: confirm_text
        x: 0
        y: 709
        width: 1024
        height: 32
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 24
        text: qsTr("By clicking confirm button you agree to the Terms and Conditions.")
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    DoorButton{
        id:back_button
        y:613
        x:231
        show_text:qsTr("Back")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id:ok_button
        y:613
        x:524
        show_text:qsTr("Confirm")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                ok_button.enabled = false
                back_button.enabled = false
                payment_page.open()
                show_timer_payment.restart()
            }
            onEntered:{
                ok_button.show_image = "img/door/2.png"
            }
            onExited:{
                ok_button.show_image = "img/door/1.png"
            }
        }
    }

    LoadingView{
        id:loading
    }

    PaymentWindow{
        id:payment_page
        //visible:true

        Text {
            id: text_payment_window
            x:334
            y:257
            width: 561
            height: 140
            text: qsTr("Please place your card to the reader")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_payment
                property int counter
                Component.onCompleted:{
                    time_payment.counter = show_timer_value
                }
            }
            Row{
                x:548
                y:399
                spacing:5
                Text {
                    id:show_time_payment_text
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pointSize:25
                    wrapMode: Text.WordWrap
                }
                Text {
                    id:show_seconds_left_text
                    color: "#ab312e"
                    text: qsTr("seconds left")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }

            Timer{
                id:show_timer_payment
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_payment_text.text = time_payment.counter
                    time_payment.counter -= 1
                    if(time_payment.counter == 2){
                        slot_handler.start_global_payment_emoney(amount)
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_payment.counter < 0){
                        show_timer_payment.stop()
                        show_time_payment_text.visible = false
                        show_seconds_left_text.visible = false
                        ok_button.enabled = true
                        back_button.enabled = true
                        abc.counter = timer_value
                    }
                }
            }
        }
    }

    PaymentWindow{
        id:nonfinished_notif
        //visible:true

        Text {
            id: text_nonfisnihed_1
            x:130
            y:218
            width: 765
            height: 50
            text: qsTr("Transaction Unfinished")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_nonfisnihed_2
            x:340
            y:285
            width: 555
            height: 88
            text: qsTr("Please put back your previous prepaid card.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_nf
                property int counter
                Component.onCompleted:{
                    time_nf.counter = show_timer_value_nf
                }
            }
            Row{
                x:563
                y:391
                spacing:5
                Text {
                    id:show_time_nf
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pointSize:25
                    wrapMode: Text.WordWrap
                }
                Text {
                    id: second_left_nf
                    color: "#ab312e"
                    text: qsTr("seconds left")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }
            Row{
                x:862
                y:520
                width: 30
                height: 30
                spacing:5
                Text {
                    id:trial_count_text
                    width: 30
                    height: 30
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    textFormat: Text.PlainText
                    font.pointSize:18
                    wrapMode: Text.WordWrap
                }
            }

            Timer{
                id:show_timer_nf
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_nf.text = time_nf.counter
                    time_nf.counter -= 1
                    if(time_nf.counter == 2){
                        slot_handler.start_global_payment_emoney(amount)
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_nf.counter < 0){
                        show_timer_nf.stop()
                        show_time_nf.visible = false
                        second_left_nf.visible = false
                    }
                }
            }
        }
    }

    HideWindow{
        id:wrong_card_notif
        //visible:true

        Text {
            id: text_wc_1
            x: 93
            y:464
            width: 850
            height: 50
            text: qsTr("Transaction Failed")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:28
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: text_wc_2
            x: 93
            y:520
            width: 850
            height: 50
            text: qsTr("Please use Mandiri e-Money prepaid card.")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:25
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_wrong_card_notif
            x: 410
            y: 235
            width: 215
            height: 215
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/no_credit.png"
        }

        OverTimeButton{
            id:wrong_card_notif_back
            x:378
            y:589
            show_text:qsTr("Cancel")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                    //slot_handler.start_reset_partial_transaction()
                }
                onEntered:{
                    wrong_card_notif_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    wrong_card_notif_back.show_source = "img/button/7.png"
                }
            }
        }
    }

}
